/**
 * Automatically generated file. DO NOT MODIFY
 */
package app.profuturo.reclasificacion.com;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "app.profuturo.reclasificacion.com";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from build type: debug
  public static final String BASE_DEBUG = "https://proinversion.mx:2085";
  public static final String BASE_SALES = "http://172.16.58.38:9080/";
  public static final String BASE_URL = "https://www.proinversion.mx:2082/";
  public static final String HEADERS = "Authentication: Basic cndzcHJheGlzcDpQcjR4MXMjdTVS";
  public static final String PROFUTURO_BASE_URL = "https://www.proinversion.mx:2143/";
  public static final String QA = "https://www.proinversion.mx:2086/";
}
