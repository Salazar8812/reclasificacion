
package android.databinding;
import app.profuturo.reclasificacion.com.BR;
class DataBinderMapperImpl extends android.databinding.DataBinderMapper {
    public DataBinderMapperImpl() {
    }
    @Override
    public android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View view, int layoutId) {
        switch(layoutId) {
                case app.profuturo.reclasificacion.com.R.layout.fragment_client_search:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/fragment_client_search_0".equals(tag)) {
                            return new app.profuturo.reclasificacion.com.databinding.FragmentClientSearchBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for fragment_client_search is invalid. Received: " + tag);
                }
                case app.profuturo.reclasificacion.com.R.layout.activity_login:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/activity_login_0".equals(tag)) {
                            return new app.profuturo.reclasificacion.com.databinding.ActivityLoginBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for activity_login is invalid. Received: " + tag);
                }
                case app.profuturo.reclasificacion.com.R.layout.fragment_client_result:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/fragment_client_result_0".equals(tag)) {
                            return new app.profuturo.reclasificacion.com.databinding.FragmentClientResultBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for fragment_client_result is invalid. Received: " + tag);
                }
                case app.profuturo.reclasificacion.com.R.layout.fragment_sincronize_documents:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/fragment_sincronize_documents_0".equals(tag)) {
                            return new app.profuturo.reclasificacion.com.databinding.FragmentSincronizeDocumentsBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for fragment_sincronize_documents is invalid. Received: " + tag);
                }
                case app.profuturo.reclasificacion.com.R.layout.fragment_client_information:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/fragment_client_information_0".equals(tag)) {
                            return new app.profuturo.reclasificacion.com.databinding.FragmentClientInformationBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for fragment_client_information is invalid. Received: " + tag);
                }
                case app.profuturo.reclasificacion.com.R.layout.activity_procedure:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/activity_procedure_0".equals(tag)) {
                            return new app.profuturo.reclasificacion.com.databinding.ActivityProcedureBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for activity_procedure is invalid. Received: " + tag);
                }
                case app.profuturo.reclasificacion.com.R.layout.fragment_capture_reclasificacion:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/fragment_capture_reclasificacion_0".equals(tag)) {
                            return new app.profuturo.reclasificacion.com.databinding.FragmentCaptureReclasificacionBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for fragment_capture_reclasificacion is invalid. Received: " + tag);
                }
                case app.profuturo.reclasificacion.com.R.layout.activity_splash:
 {
                        final Object tag = view.getTag();
                        if(tag == null) throw new java.lang.RuntimeException("view must have a tag");
                    if ("layout/activity_splash_0".equals(tag)) {
                            return new app.profuturo.reclasificacion.com.databinding.ActivitySplashBinding(bindingComponent, view);
                    }
                        throw new java.lang.IllegalArgumentException("The tag for activity_splash is invalid. Received: " + tag);
                }
        }
        return null;
    }
    @Override
    public android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View[] views, int layoutId) {
        switch(layoutId) {
        }
        return null;
    }
    @Override
    public int getLayoutId(String tag) {
        if (tag == null) {
            return 0;
        }
        final int code = tag.hashCode();
        switch(code) {
            case 409517219: {
                if(tag.equals("layout/fragment_client_search_0")) {
                    return app.profuturo.reclasificacion.com.R.layout.fragment_client_search;
                }
                break;
            }
            case -237232145: {
                if(tag.equals("layout/activity_login_0")) {
                    return app.profuturo.reclasificacion.com.R.layout.activity_login;
                }
                break;
            }
            case -814918184: {
                if(tag.equals("layout/fragment_client_result_0")) {
                    return app.profuturo.reclasificacion.com.R.layout.fragment_client_result;
                }
                break;
            }
            case -2137139774: {
                if(tag.equals("layout/fragment_sincronize_documents_0")) {
                    return app.profuturo.reclasificacion.com.R.layout.fragment_sincronize_documents;
                }
                break;
            }
            case 1757974227: {
                if(tag.equals("layout/fragment_client_information_0")) {
                    return app.profuturo.reclasificacion.com.R.layout.fragment_client_information;
                }
                break;
            }
            case -107352487: {
                if(tag.equals("layout/activity_procedure_0")) {
                    return app.profuturo.reclasificacion.com.R.layout.activity_procedure;
                }
                break;
            }
            case 512771919: {
                if(tag.equals("layout/fragment_capture_reclasificacion_0")) {
                    return app.profuturo.reclasificacion.com.R.layout.fragment_capture_reclasificacion;
                }
                break;
            }
            case 1573928931: {
                if(tag.equals("layout/activity_splash_0")) {
                    return app.profuturo.reclasificacion.com.R.layout.activity_splash;
                }
                break;
            }
        }
        return 0;
    }
    @Override
    public String convertBrIdToString(int id) {
        if (id < 0 || id >= InnerBrLookup.sKeys.length) {
            return null;
        }
        return InnerBrLookup.sKeys[id];
    }
    private static class InnerBrLookup {
        static String[] sKeys = new String[]{
            "_all"
            ,"agentName"
            ,"agentSession"
            ,"client"
            ,"presenter"
            ,"versionNumber"};
    }
}