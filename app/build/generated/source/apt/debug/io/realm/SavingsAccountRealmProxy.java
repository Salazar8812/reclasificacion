package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.ProxyUtils;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class SavingsAccountRealmProxy extends app.profuturo.reclasificacion.com.model.SavingsAccount
    implements RealmObjectProxy, SavingsAccountRealmProxyInterface {

    static final class SavingsAccountColumnInfo extends ColumnInfo {
        long sharesIndex;
        long totalHousingInPesosIndex;
        long totalNotHousingInPesosIndex;
        long totalAllAccountsInPesosIndex;

        SavingsAccountColumnInfo(OsSchemaInfo schemaInfo) {
            super(4);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("SavingsAccount");
            this.sharesIndex = addColumnDetails("shares", objectSchemaInfo);
            this.totalHousingInPesosIndex = addColumnDetails("totalHousingInPesos", objectSchemaInfo);
            this.totalNotHousingInPesosIndex = addColumnDetails("totalNotHousingInPesos", objectSchemaInfo);
            this.totalAllAccountsInPesosIndex = addColumnDetails("totalAllAccountsInPesos", objectSchemaInfo);
        }

        SavingsAccountColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new SavingsAccountColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final SavingsAccountColumnInfo src = (SavingsAccountColumnInfo) rawSrc;
            final SavingsAccountColumnInfo dst = (SavingsAccountColumnInfo) rawDst;
            dst.sharesIndex = src.sharesIndex;
            dst.totalHousingInPesosIndex = src.totalHousingInPesosIndex;
            dst.totalNotHousingInPesosIndex = src.totalNotHousingInPesosIndex;
            dst.totalAllAccountsInPesosIndex = src.totalAllAccountsInPesosIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>(4);
        fieldNames.add("shares");
        fieldNames.add("totalHousingInPesos");
        fieldNames.add("totalNotHousingInPesos");
        fieldNames.add("totalAllAccountsInPesos");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    private SavingsAccountColumnInfo columnInfo;
    private ProxyState<app.profuturo.reclasificacion.com.model.SavingsAccount> proxyState;

    SavingsAccountRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (SavingsAccountColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<app.profuturo.reclasificacion.com.model.SavingsAccount>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    public app.profuturo.reclasificacion.com.model.Shares realmGet$shares() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNullLink(columnInfo.sharesIndex)) {
            return null;
        }
        return proxyState.getRealm$realm().get(app.profuturo.reclasificacion.com.model.Shares.class, proxyState.getRow$realm().getLink(columnInfo.sharesIndex), false, Collections.<String>emptyList());
    }

    @Override
    public void realmSet$shares(app.profuturo.reclasificacion.com.model.Shares value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("shares")) {
                return;
            }
            if (value != null && !RealmObject.isManaged(value)) {
                value = ((Realm) proxyState.getRealm$realm()).copyToRealm(value);
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                // Table#nullifyLink() does not support default value. Just using Row.
                row.nullifyLink(columnInfo.sharesIndex);
                return;
            }
            proxyState.checkValidObject(value);
            row.getTable().setLink(columnInfo.sharesIndex, row.getIndex(), ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex(), true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().nullifyLink(columnInfo.sharesIndex);
            return;
        }
        proxyState.checkValidObject(value);
        proxyState.getRow$realm().setLink(columnInfo.sharesIndex, ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex());
    }

    @Override
    @SuppressWarnings("cast")
    public Double realmGet$totalHousingInPesos() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.totalHousingInPesosIndex)) {
            return null;
        }
        return (double) proxyState.getRow$realm().getDouble(columnInfo.totalHousingInPesosIndex);
    }

    @Override
    public void realmSet$totalHousingInPesos(Double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.totalHousingInPesosIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setDouble(columnInfo.totalHousingInPesosIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.totalHousingInPesosIndex);
            return;
        }
        proxyState.getRow$realm().setDouble(columnInfo.totalHousingInPesosIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Double realmGet$totalNotHousingInPesos() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.totalNotHousingInPesosIndex)) {
            return null;
        }
        return (double) proxyState.getRow$realm().getDouble(columnInfo.totalNotHousingInPesosIndex);
    }

    @Override
    public void realmSet$totalNotHousingInPesos(Double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.totalNotHousingInPesosIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setDouble(columnInfo.totalNotHousingInPesosIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.totalNotHousingInPesosIndex);
            return;
        }
        proxyState.getRow$realm().setDouble(columnInfo.totalNotHousingInPesosIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Double realmGet$totalAllAccountsInPesos() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.totalAllAccountsInPesosIndex)) {
            return null;
        }
        return (double) proxyState.getRow$realm().getDouble(columnInfo.totalAllAccountsInPesosIndex);
    }

    @Override
    public void realmSet$totalAllAccountsInPesos(Double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.totalAllAccountsInPesosIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setDouble(columnInfo.totalAllAccountsInPesosIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.totalAllAccountsInPesosIndex);
            return;
        }
        proxyState.getRow$realm().setDouble(columnInfo.totalAllAccountsInPesosIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("SavingsAccount", 4, 0);
        builder.addPersistedLinkProperty("shares", RealmFieldType.OBJECT, "Shares");
        builder.addPersistedProperty("totalHousingInPesos", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("totalNotHousingInPesos", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("totalAllAccountsInPesos", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static SavingsAccountColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new SavingsAccountColumnInfo(schemaInfo);
    }

    public static String getTableName() {
        return "class_SavingsAccount";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static app.profuturo.reclasificacion.com.model.SavingsAccount createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = new ArrayList<String>(1);
        if (json.has("shares")) {
            excludeFields.add("shares");
        }
        app.profuturo.reclasificacion.com.model.SavingsAccount obj = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.SavingsAccount.class, true, excludeFields);

        final SavingsAccountRealmProxyInterface objProxy = (SavingsAccountRealmProxyInterface) obj;
        if (json.has("shares")) {
            if (json.isNull("shares")) {
                objProxy.realmSet$shares(null);
            } else {
                app.profuturo.reclasificacion.com.model.Shares sharesObj = SharesRealmProxy.createOrUpdateUsingJsonObject(realm, json.getJSONObject("shares"), update);
                objProxy.realmSet$shares(sharesObj);
            }
        }
        if (json.has("totalHousingInPesos")) {
            if (json.isNull("totalHousingInPesos")) {
                objProxy.realmSet$totalHousingInPesos(null);
            } else {
                objProxy.realmSet$totalHousingInPesos((double) json.getDouble("totalHousingInPesos"));
            }
        }
        if (json.has("totalNotHousingInPesos")) {
            if (json.isNull("totalNotHousingInPesos")) {
                objProxy.realmSet$totalNotHousingInPesos(null);
            } else {
                objProxy.realmSet$totalNotHousingInPesos((double) json.getDouble("totalNotHousingInPesos"));
            }
        }
        if (json.has("totalAllAccountsInPesos")) {
            if (json.isNull("totalAllAccountsInPesos")) {
                objProxy.realmSet$totalAllAccountsInPesos(null);
            } else {
                objProxy.realmSet$totalAllAccountsInPesos((double) json.getDouble("totalAllAccountsInPesos"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static app.profuturo.reclasificacion.com.model.SavingsAccount createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        final app.profuturo.reclasificacion.com.model.SavingsAccount obj = new app.profuturo.reclasificacion.com.model.SavingsAccount();
        final SavingsAccountRealmProxyInterface objProxy = (SavingsAccountRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("shares")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$shares(null);
                } else {
                    app.profuturo.reclasificacion.com.model.Shares sharesObj = SharesRealmProxy.createUsingJsonStream(realm, reader);
                    objProxy.realmSet$shares(sharesObj);
                }
            } else if (name.equals("totalHousingInPesos")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$totalHousingInPesos((double) reader.nextDouble());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$totalHousingInPesos(null);
                }
            } else if (name.equals("totalNotHousingInPesos")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$totalNotHousingInPesos((double) reader.nextDouble());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$totalNotHousingInPesos(null);
                }
            } else if (name.equals("totalAllAccountsInPesos")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$totalAllAccountsInPesos((double) reader.nextDouble());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$totalAllAccountsInPesos(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return realm.copyToRealm(obj);
    }

    public static app.profuturo.reclasificacion.com.model.SavingsAccount copyOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.SavingsAccount object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.SavingsAccount) cachedRealmObject;
        }

        return copy(realm, object, update, cache);
    }

    public static app.profuturo.reclasificacion.com.model.SavingsAccount copy(Realm realm, app.profuturo.reclasificacion.com.model.SavingsAccount newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.SavingsAccount) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        app.profuturo.reclasificacion.com.model.SavingsAccount realmObject = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.SavingsAccount.class, false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        SavingsAccountRealmProxyInterface realmObjectSource = (SavingsAccountRealmProxyInterface) newObject;
        SavingsAccountRealmProxyInterface realmObjectCopy = (SavingsAccountRealmProxyInterface) realmObject;


        app.profuturo.reclasificacion.com.model.Shares sharesObj = realmObjectSource.realmGet$shares();
        if (sharesObj == null) {
            realmObjectCopy.realmSet$shares(null);
        } else {
            app.profuturo.reclasificacion.com.model.Shares cacheshares = (app.profuturo.reclasificacion.com.model.Shares) cache.get(sharesObj);
            if (cacheshares != null) {
                realmObjectCopy.realmSet$shares(cacheshares);
            } else {
                realmObjectCopy.realmSet$shares(SharesRealmProxy.copyOrUpdate(realm, sharesObj, update, cache));
            }
        }
        realmObjectCopy.realmSet$totalHousingInPesos(realmObjectSource.realmGet$totalHousingInPesos());
        realmObjectCopy.realmSet$totalNotHousingInPesos(realmObjectSource.realmGet$totalNotHousingInPesos());
        realmObjectCopy.realmSet$totalAllAccountsInPesos(realmObjectSource.realmGet$totalAllAccountsInPesos());
        return realmObject;
    }

    public static long insert(Realm realm, app.profuturo.reclasificacion.com.model.SavingsAccount object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SavingsAccount.class);
        long tableNativePtr = table.getNativePtr();
        SavingsAccountColumnInfo columnInfo = (SavingsAccountColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SavingsAccount.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);

        app.profuturo.reclasificacion.com.model.Shares sharesObj = ((SavingsAccountRealmProxyInterface) object).realmGet$shares();
        if (sharesObj != null) {
            Long cacheshares = cache.get(sharesObj);
            if (cacheshares == null) {
                cacheshares = SharesRealmProxy.insert(realm, sharesObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.sharesIndex, rowIndex, cacheshares, false);
        }
        Double realmGet$totalHousingInPesos = ((SavingsAccountRealmProxyInterface) object).realmGet$totalHousingInPesos();
        if (realmGet$totalHousingInPesos != null) {
            Table.nativeSetDouble(tableNativePtr, columnInfo.totalHousingInPesosIndex, rowIndex, realmGet$totalHousingInPesos, false);
        }
        Double realmGet$totalNotHousingInPesos = ((SavingsAccountRealmProxyInterface) object).realmGet$totalNotHousingInPesos();
        if (realmGet$totalNotHousingInPesos != null) {
            Table.nativeSetDouble(tableNativePtr, columnInfo.totalNotHousingInPesosIndex, rowIndex, realmGet$totalNotHousingInPesos, false);
        }
        Double realmGet$totalAllAccountsInPesos = ((SavingsAccountRealmProxyInterface) object).realmGet$totalAllAccountsInPesos();
        if (realmGet$totalAllAccountsInPesos != null) {
            Table.nativeSetDouble(tableNativePtr, columnInfo.totalAllAccountsInPesosIndex, rowIndex, realmGet$totalAllAccountsInPesos, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SavingsAccount.class);
        long tableNativePtr = table.getNativePtr();
        SavingsAccountColumnInfo columnInfo = (SavingsAccountColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SavingsAccount.class);
        app.profuturo.reclasificacion.com.model.SavingsAccount object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.SavingsAccount) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);

            app.profuturo.reclasificacion.com.model.Shares sharesObj = ((SavingsAccountRealmProxyInterface) object).realmGet$shares();
            if (sharesObj != null) {
                Long cacheshares = cache.get(sharesObj);
                if (cacheshares == null) {
                    cacheshares = SharesRealmProxy.insert(realm, sharesObj, cache);
                }
                table.setLink(columnInfo.sharesIndex, rowIndex, cacheshares, false);
            }
            Double realmGet$totalHousingInPesos = ((SavingsAccountRealmProxyInterface) object).realmGet$totalHousingInPesos();
            if (realmGet$totalHousingInPesos != null) {
                Table.nativeSetDouble(tableNativePtr, columnInfo.totalHousingInPesosIndex, rowIndex, realmGet$totalHousingInPesos, false);
            }
            Double realmGet$totalNotHousingInPesos = ((SavingsAccountRealmProxyInterface) object).realmGet$totalNotHousingInPesos();
            if (realmGet$totalNotHousingInPesos != null) {
                Table.nativeSetDouble(tableNativePtr, columnInfo.totalNotHousingInPesosIndex, rowIndex, realmGet$totalNotHousingInPesos, false);
            }
            Double realmGet$totalAllAccountsInPesos = ((SavingsAccountRealmProxyInterface) object).realmGet$totalAllAccountsInPesos();
            if (realmGet$totalAllAccountsInPesos != null) {
                Table.nativeSetDouble(tableNativePtr, columnInfo.totalAllAccountsInPesosIndex, rowIndex, realmGet$totalAllAccountsInPesos, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.SavingsAccount object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SavingsAccount.class);
        long tableNativePtr = table.getNativePtr();
        SavingsAccountColumnInfo columnInfo = (SavingsAccountColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SavingsAccount.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);

        app.profuturo.reclasificacion.com.model.Shares sharesObj = ((SavingsAccountRealmProxyInterface) object).realmGet$shares();
        if (sharesObj != null) {
            Long cacheshares = cache.get(sharesObj);
            if (cacheshares == null) {
                cacheshares = SharesRealmProxy.insertOrUpdate(realm, sharesObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.sharesIndex, rowIndex, cacheshares, false);
        } else {
            Table.nativeNullifyLink(tableNativePtr, columnInfo.sharesIndex, rowIndex);
        }
        Double realmGet$totalHousingInPesos = ((SavingsAccountRealmProxyInterface) object).realmGet$totalHousingInPesos();
        if (realmGet$totalHousingInPesos != null) {
            Table.nativeSetDouble(tableNativePtr, columnInfo.totalHousingInPesosIndex, rowIndex, realmGet$totalHousingInPesos, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.totalHousingInPesosIndex, rowIndex, false);
        }
        Double realmGet$totalNotHousingInPesos = ((SavingsAccountRealmProxyInterface) object).realmGet$totalNotHousingInPesos();
        if (realmGet$totalNotHousingInPesos != null) {
            Table.nativeSetDouble(tableNativePtr, columnInfo.totalNotHousingInPesosIndex, rowIndex, realmGet$totalNotHousingInPesos, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.totalNotHousingInPesosIndex, rowIndex, false);
        }
        Double realmGet$totalAllAccountsInPesos = ((SavingsAccountRealmProxyInterface) object).realmGet$totalAllAccountsInPesos();
        if (realmGet$totalAllAccountsInPesos != null) {
            Table.nativeSetDouble(tableNativePtr, columnInfo.totalAllAccountsInPesosIndex, rowIndex, realmGet$totalAllAccountsInPesos, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.totalAllAccountsInPesosIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SavingsAccount.class);
        long tableNativePtr = table.getNativePtr();
        SavingsAccountColumnInfo columnInfo = (SavingsAccountColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SavingsAccount.class);
        app.profuturo.reclasificacion.com.model.SavingsAccount object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.SavingsAccount) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);

            app.profuturo.reclasificacion.com.model.Shares sharesObj = ((SavingsAccountRealmProxyInterface) object).realmGet$shares();
            if (sharesObj != null) {
                Long cacheshares = cache.get(sharesObj);
                if (cacheshares == null) {
                    cacheshares = SharesRealmProxy.insertOrUpdate(realm, sharesObj, cache);
                }
                Table.nativeSetLink(tableNativePtr, columnInfo.sharesIndex, rowIndex, cacheshares, false);
            } else {
                Table.nativeNullifyLink(tableNativePtr, columnInfo.sharesIndex, rowIndex);
            }
            Double realmGet$totalHousingInPesos = ((SavingsAccountRealmProxyInterface) object).realmGet$totalHousingInPesos();
            if (realmGet$totalHousingInPesos != null) {
                Table.nativeSetDouble(tableNativePtr, columnInfo.totalHousingInPesosIndex, rowIndex, realmGet$totalHousingInPesos, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.totalHousingInPesosIndex, rowIndex, false);
            }
            Double realmGet$totalNotHousingInPesos = ((SavingsAccountRealmProxyInterface) object).realmGet$totalNotHousingInPesos();
            if (realmGet$totalNotHousingInPesos != null) {
                Table.nativeSetDouble(tableNativePtr, columnInfo.totalNotHousingInPesosIndex, rowIndex, realmGet$totalNotHousingInPesos, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.totalNotHousingInPesosIndex, rowIndex, false);
            }
            Double realmGet$totalAllAccountsInPesos = ((SavingsAccountRealmProxyInterface) object).realmGet$totalAllAccountsInPesos();
            if (realmGet$totalAllAccountsInPesos != null) {
                Table.nativeSetDouble(tableNativePtr, columnInfo.totalAllAccountsInPesosIndex, rowIndex, realmGet$totalAllAccountsInPesos, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.totalAllAccountsInPesosIndex, rowIndex, false);
            }
        }
    }

    public static app.profuturo.reclasificacion.com.model.SavingsAccount createDetachedCopy(app.profuturo.reclasificacion.com.model.SavingsAccount realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        app.profuturo.reclasificacion.com.model.SavingsAccount unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new app.profuturo.reclasificacion.com.model.SavingsAccount();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (app.profuturo.reclasificacion.com.model.SavingsAccount) cachedObject.object;
            }
            unmanagedObject = (app.profuturo.reclasificacion.com.model.SavingsAccount) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        SavingsAccountRealmProxyInterface unmanagedCopy = (SavingsAccountRealmProxyInterface) unmanagedObject;
        SavingsAccountRealmProxyInterface realmSource = (SavingsAccountRealmProxyInterface) realmObject;

        // Deep copy of shares
        unmanagedCopy.realmSet$shares(SharesRealmProxy.createDetachedCopy(realmSource.realmGet$shares(), currentDepth + 1, maxDepth, cache));
        unmanagedCopy.realmSet$totalHousingInPesos(realmSource.realmGet$totalHousingInPesos());
        unmanagedCopy.realmSet$totalNotHousingInPesos(realmSource.realmGet$totalNotHousingInPesos());
        unmanagedCopy.realmSet$totalAllAccountsInPesos(realmSource.realmGet$totalAllAccountsInPesos());

        return unmanagedObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("SavingsAccount = proxy[");
        stringBuilder.append("{shares:");
        stringBuilder.append(realmGet$shares() != null ? "Shares" : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{totalHousingInPesos:");
        stringBuilder.append(realmGet$totalHousingInPesos() != null ? realmGet$totalHousingInPesos() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{totalNotHousingInPesos:");
        stringBuilder.append(realmGet$totalNotHousingInPesos() != null ? realmGet$totalNotHousingInPesos() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{totalAllAccountsInPesos:");
        stringBuilder.append(realmGet$totalAllAccountsInPesos() != null ? realmGet$totalAllAccountsInPesos() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SavingsAccountRealmProxy aSavingsAccount = (SavingsAccountRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aSavingsAccount.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aSavingsAccount.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aSavingsAccount.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
