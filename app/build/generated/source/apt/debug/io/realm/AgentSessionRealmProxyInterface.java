package io.realm;


public interface AgentSessionRealmProxyInterface {
    public String realmGet$agentNumber();
    public void realmSet$agentNumber(String value);
    public String realmGet$surName();
    public void realmSet$surName(String value);
    public String realmGet$lastName();
    public void realmSet$lastName(String value);
    public String realmGet$name();
    public void realmSet$name(String value);
    public String realmGet$consarCode();
    public void realmSet$consarCode(String value);
    public String realmGet$curp();
    public void realmSet$curp(String value);
    public String realmGet$creationDate();
    public void realmSet$creationDate(String value);
    public String realmGet$assignedBranchId();
    public void realmSet$assignedBranchId(String value);
    public String realmGet$assignedBranchName();
    public void realmSet$assignedBranchName(String value);
    public String realmGet$idLogin();
    public void realmSet$idLogin(String value);
}
