package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.ProxyUtils;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class AgentSessionRealmProxy extends app.profuturo.reclasificacion.com.model.AgentSession
    implements RealmObjectProxy, AgentSessionRealmProxyInterface {

    static final class AgentSessionColumnInfo extends ColumnInfo {
        long agentNumberIndex;
        long surNameIndex;
        long lastNameIndex;
        long nameIndex;
        long consarCodeIndex;
        long curpIndex;
        long creationDateIndex;
        long assignedBranchIdIndex;
        long assignedBranchNameIndex;
        long idLoginIndex;

        AgentSessionColumnInfo(OsSchemaInfo schemaInfo) {
            super(10);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("AgentSession");
            this.agentNumberIndex = addColumnDetails("agentNumber", objectSchemaInfo);
            this.surNameIndex = addColumnDetails("surName", objectSchemaInfo);
            this.lastNameIndex = addColumnDetails("lastName", objectSchemaInfo);
            this.nameIndex = addColumnDetails("name", objectSchemaInfo);
            this.consarCodeIndex = addColumnDetails("consarCode", objectSchemaInfo);
            this.curpIndex = addColumnDetails("curp", objectSchemaInfo);
            this.creationDateIndex = addColumnDetails("creationDate", objectSchemaInfo);
            this.assignedBranchIdIndex = addColumnDetails("assignedBranchId", objectSchemaInfo);
            this.assignedBranchNameIndex = addColumnDetails("assignedBranchName", objectSchemaInfo);
            this.idLoginIndex = addColumnDetails("idLogin", objectSchemaInfo);
        }

        AgentSessionColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new AgentSessionColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final AgentSessionColumnInfo src = (AgentSessionColumnInfo) rawSrc;
            final AgentSessionColumnInfo dst = (AgentSessionColumnInfo) rawDst;
            dst.agentNumberIndex = src.agentNumberIndex;
            dst.surNameIndex = src.surNameIndex;
            dst.lastNameIndex = src.lastNameIndex;
            dst.nameIndex = src.nameIndex;
            dst.consarCodeIndex = src.consarCodeIndex;
            dst.curpIndex = src.curpIndex;
            dst.creationDateIndex = src.creationDateIndex;
            dst.assignedBranchIdIndex = src.assignedBranchIdIndex;
            dst.assignedBranchNameIndex = src.assignedBranchNameIndex;
            dst.idLoginIndex = src.idLoginIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>(10);
        fieldNames.add("agentNumber");
        fieldNames.add("surName");
        fieldNames.add("lastName");
        fieldNames.add("name");
        fieldNames.add("consarCode");
        fieldNames.add("curp");
        fieldNames.add("creationDate");
        fieldNames.add("assignedBranchId");
        fieldNames.add("assignedBranchName");
        fieldNames.add("idLogin");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    private AgentSessionColumnInfo columnInfo;
    private ProxyState<app.profuturo.reclasificacion.com.model.AgentSession> proxyState;

    AgentSessionRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (AgentSessionColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<app.profuturo.reclasificacion.com.model.AgentSession>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$agentNumber() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.agentNumberIndex);
    }

    @Override
    public void realmSet$agentNumber(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'agentNumber' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$surName() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.surNameIndex);
    }

    @Override
    public void realmSet$surName(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.surNameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.surNameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.surNameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.surNameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$lastName() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.lastNameIndex);
    }

    @Override
    public void realmSet$lastName(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.lastNameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.lastNameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.lastNameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.lastNameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$name() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.nameIndex);
    }

    @Override
    public void realmSet$name(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.nameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.nameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.nameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.nameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$consarCode() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.consarCodeIndex);
    }

    @Override
    public void realmSet$consarCode(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.consarCodeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.consarCodeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.consarCodeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.consarCodeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$curp() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.curpIndex);
    }

    @Override
    public void realmSet$curp(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.curpIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.curpIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.curpIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.curpIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$creationDate() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.creationDateIndex);
    }

    @Override
    public void realmSet$creationDate(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.creationDateIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.creationDateIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.creationDateIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.creationDateIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$assignedBranchId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.assignedBranchIdIndex);
    }

    @Override
    public void realmSet$assignedBranchId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.assignedBranchIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.assignedBranchIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.assignedBranchIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.assignedBranchIdIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$assignedBranchName() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.assignedBranchNameIndex);
    }

    @Override
    public void realmSet$assignedBranchName(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.assignedBranchNameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.assignedBranchNameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.assignedBranchNameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.assignedBranchNameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idLogin() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idLoginIndex);
    }

    @Override
    public void realmSet$idLogin(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idLoginIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idLoginIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idLoginIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idLoginIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("AgentSession", 10, 0);
        builder.addPersistedProperty("agentNumber", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("surName", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("lastName", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("name", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("consarCode", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("curp", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("creationDate", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("assignedBranchId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("assignedBranchName", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idLogin", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static AgentSessionColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new AgentSessionColumnInfo(schemaInfo);
    }

    public static String getTableName() {
        return "class_AgentSession";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static app.profuturo.reclasificacion.com.model.AgentSession createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        app.profuturo.reclasificacion.com.model.AgentSession obj = null;
        if (update) {
            Table table = realm.getTable(app.profuturo.reclasificacion.com.model.AgentSession.class);
            AgentSessionColumnInfo columnInfo = (AgentSessionColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.AgentSession.class);
            long pkColumnIndex = columnInfo.agentNumberIndex;
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("agentNumber")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("agentNumber"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.AgentSession.class), false, Collections.<String> emptyList());
                    obj = new io.realm.AgentSessionRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("agentNumber")) {
                if (json.isNull("agentNumber")) {
                    obj = (io.realm.AgentSessionRealmProxy) realm.createObjectInternal(app.profuturo.reclasificacion.com.model.AgentSession.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.AgentSessionRealmProxy) realm.createObjectInternal(app.profuturo.reclasificacion.com.model.AgentSession.class, json.getString("agentNumber"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'agentNumber'.");
            }
        }

        final AgentSessionRealmProxyInterface objProxy = (AgentSessionRealmProxyInterface) obj;
        if (json.has("surName")) {
            if (json.isNull("surName")) {
                objProxy.realmSet$surName(null);
            } else {
                objProxy.realmSet$surName((String) json.getString("surName"));
            }
        }
        if (json.has("lastName")) {
            if (json.isNull("lastName")) {
                objProxy.realmSet$lastName(null);
            } else {
                objProxy.realmSet$lastName((String) json.getString("lastName"));
            }
        }
        if (json.has("name")) {
            if (json.isNull("name")) {
                objProxy.realmSet$name(null);
            } else {
                objProxy.realmSet$name((String) json.getString("name"));
            }
        }
        if (json.has("consarCode")) {
            if (json.isNull("consarCode")) {
                objProxy.realmSet$consarCode(null);
            } else {
                objProxy.realmSet$consarCode((String) json.getString("consarCode"));
            }
        }
        if (json.has("curp")) {
            if (json.isNull("curp")) {
                objProxy.realmSet$curp(null);
            } else {
                objProxy.realmSet$curp((String) json.getString("curp"));
            }
        }
        if (json.has("creationDate")) {
            if (json.isNull("creationDate")) {
                objProxy.realmSet$creationDate(null);
            } else {
                objProxy.realmSet$creationDate((String) json.getString("creationDate"));
            }
        }
        if (json.has("assignedBranchId")) {
            if (json.isNull("assignedBranchId")) {
                objProxy.realmSet$assignedBranchId(null);
            } else {
                objProxy.realmSet$assignedBranchId((String) json.getString("assignedBranchId"));
            }
        }
        if (json.has("assignedBranchName")) {
            if (json.isNull("assignedBranchName")) {
                objProxy.realmSet$assignedBranchName(null);
            } else {
                objProxy.realmSet$assignedBranchName((String) json.getString("assignedBranchName"));
            }
        }
        if (json.has("idLogin")) {
            if (json.isNull("idLogin")) {
                objProxy.realmSet$idLogin(null);
            } else {
                objProxy.realmSet$idLogin((String) json.getString("idLogin"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static app.profuturo.reclasificacion.com.model.AgentSession createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final app.profuturo.reclasificacion.com.model.AgentSession obj = new app.profuturo.reclasificacion.com.model.AgentSession();
        final AgentSessionRealmProxyInterface objProxy = (AgentSessionRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("agentNumber")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$agentNumber((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$agentNumber(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("surName")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$surName((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$surName(null);
                }
            } else if (name.equals("lastName")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$lastName((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$lastName(null);
                }
            } else if (name.equals("name")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$name((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$name(null);
                }
            } else if (name.equals("consarCode")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$consarCode((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$consarCode(null);
                }
            } else if (name.equals("curp")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$curp((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$curp(null);
                }
            } else if (name.equals("creationDate")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$creationDate((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$creationDate(null);
                }
            } else if (name.equals("assignedBranchId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$assignedBranchId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$assignedBranchId(null);
                }
            } else if (name.equals("assignedBranchName")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$assignedBranchName((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$assignedBranchName(null);
                }
            } else if (name.equals("idLogin")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idLogin((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idLogin(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'agentNumber'.");
        }
        return realm.copyToRealm(obj);
    }

    public static app.profuturo.reclasificacion.com.model.AgentSession copyOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.AgentSession object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.AgentSession) cachedRealmObject;
        }

        app.profuturo.reclasificacion.com.model.AgentSession realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(app.profuturo.reclasificacion.com.model.AgentSession.class);
            AgentSessionColumnInfo columnInfo = (AgentSessionColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.AgentSession.class);
            long pkColumnIndex = columnInfo.agentNumberIndex;
            String value = ((AgentSessionRealmProxyInterface) object).realmGet$agentNumber();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, value);
            }
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.AgentSession.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.AgentSessionRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static app.profuturo.reclasificacion.com.model.AgentSession copy(Realm realm, app.profuturo.reclasificacion.com.model.AgentSession newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.AgentSession) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        app.profuturo.reclasificacion.com.model.AgentSession realmObject = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.AgentSession.class, ((AgentSessionRealmProxyInterface) newObject).realmGet$agentNumber(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        AgentSessionRealmProxyInterface realmObjectSource = (AgentSessionRealmProxyInterface) newObject;
        AgentSessionRealmProxyInterface realmObjectCopy = (AgentSessionRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$surName(realmObjectSource.realmGet$surName());
        realmObjectCopy.realmSet$lastName(realmObjectSource.realmGet$lastName());
        realmObjectCopy.realmSet$name(realmObjectSource.realmGet$name());
        realmObjectCopy.realmSet$consarCode(realmObjectSource.realmGet$consarCode());
        realmObjectCopy.realmSet$curp(realmObjectSource.realmGet$curp());
        realmObjectCopy.realmSet$creationDate(realmObjectSource.realmGet$creationDate());
        realmObjectCopy.realmSet$assignedBranchId(realmObjectSource.realmGet$assignedBranchId());
        realmObjectCopy.realmSet$assignedBranchName(realmObjectSource.realmGet$assignedBranchName());
        realmObjectCopy.realmSet$idLogin(realmObjectSource.realmGet$idLogin());
        return realmObject;
    }

    public static long insert(Realm realm, app.profuturo.reclasificacion.com.model.AgentSession object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.AgentSession.class);
        long tableNativePtr = table.getNativePtr();
        AgentSessionColumnInfo columnInfo = (AgentSessionColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.AgentSession.class);
        long pkColumnIndex = columnInfo.agentNumberIndex;
        String primaryKeyValue = ((AgentSessionRealmProxyInterface) object).realmGet$agentNumber();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$surName = ((AgentSessionRealmProxyInterface) object).realmGet$surName();
        if (realmGet$surName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.surNameIndex, rowIndex, realmGet$surName, false);
        }
        String realmGet$lastName = ((AgentSessionRealmProxyInterface) object).realmGet$lastName();
        if (realmGet$lastName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.lastNameIndex, rowIndex, realmGet$lastName, false);
        }
        String realmGet$name = ((AgentSessionRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
        }
        String realmGet$consarCode = ((AgentSessionRealmProxyInterface) object).realmGet$consarCode();
        if (realmGet$consarCode != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.consarCodeIndex, rowIndex, realmGet$consarCode, false);
        }
        String realmGet$curp = ((AgentSessionRealmProxyInterface) object).realmGet$curp();
        if (realmGet$curp != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.curpIndex, rowIndex, realmGet$curp, false);
        }
        String realmGet$creationDate = ((AgentSessionRealmProxyInterface) object).realmGet$creationDate();
        if (realmGet$creationDate != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.creationDateIndex, rowIndex, realmGet$creationDate, false);
        }
        String realmGet$assignedBranchId = ((AgentSessionRealmProxyInterface) object).realmGet$assignedBranchId();
        if (realmGet$assignedBranchId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.assignedBranchIdIndex, rowIndex, realmGet$assignedBranchId, false);
        }
        String realmGet$assignedBranchName = ((AgentSessionRealmProxyInterface) object).realmGet$assignedBranchName();
        if (realmGet$assignedBranchName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.assignedBranchNameIndex, rowIndex, realmGet$assignedBranchName, false);
        }
        String realmGet$idLogin = ((AgentSessionRealmProxyInterface) object).realmGet$idLogin();
        if (realmGet$idLogin != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idLoginIndex, rowIndex, realmGet$idLogin, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.AgentSession.class);
        long tableNativePtr = table.getNativePtr();
        AgentSessionColumnInfo columnInfo = (AgentSessionColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.AgentSession.class);
        long pkColumnIndex = columnInfo.agentNumberIndex;
        app.profuturo.reclasificacion.com.model.AgentSession object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.AgentSession) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((AgentSessionRealmProxyInterface) object).realmGet$agentNumber();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$surName = ((AgentSessionRealmProxyInterface) object).realmGet$surName();
            if (realmGet$surName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.surNameIndex, rowIndex, realmGet$surName, false);
            }
            String realmGet$lastName = ((AgentSessionRealmProxyInterface) object).realmGet$lastName();
            if (realmGet$lastName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.lastNameIndex, rowIndex, realmGet$lastName, false);
            }
            String realmGet$name = ((AgentSessionRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
            }
            String realmGet$consarCode = ((AgentSessionRealmProxyInterface) object).realmGet$consarCode();
            if (realmGet$consarCode != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.consarCodeIndex, rowIndex, realmGet$consarCode, false);
            }
            String realmGet$curp = ((AgentSessionRealmProxyInterface) object).realmGet$curp();
            if (realmGet$curp != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.curpIndex, rowIndex, realmGet$curp, false);
            }
            String realmGet$creationDate = ((AgentSessionRealmProxyInterface) object).realmGet$creationDate();
            if (realmGet$creationDate != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.creationDateIndex, rowIndex, realmGet$creationDate, false);
            }
            String realmGet$assignedBranchId = ((AgentSessionRealmProxyInterface) object).realmGet$assignedBranchId();
            if (realmGet$assignedBranchId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.assignedBranchIdIndex, rowIndex, realmGet$assignedBranchId, false);
            }
            String realmGet$assignedBranchName = ((AgentSessionRealmProxyInterface) object).realmGet$assignedBranchName();
            if (realmGet$assignedBranchName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.assignedBranchNameIndex, rowIndex, realmGet$assignedBranchName, false);
            }
            String realmGet$idLogin = ((AgentSessionRealmProxyInterface) object).realmGet$idLogin();
            if (realmGet$idLogin != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idLoginIndex, rowIndex, realmGet$idLogin, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.AgentSession object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.AgentSession.class);
        long tableNativePtr = table.getNativePtr();
        AgentSessionColumnInfo columnInfo = (AgentSessionColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.AgentSession.class);
        long pkColumnIndex = columnInfo.agentNumberIndex;
        String primaryKeyValue = ((AgentSessionRealmProxyInterface) object).realmGet$agentNumber();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$surName = ((AgentSessionRealmProxyInterface) object).realmGet$surName();
        if (realmGet$surName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.surNameIndex, rowIndex, realmGet$surName, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.surNameIndex, rowIndex, false);
        }
        String realmGet$lastName = ((AgentSessionRealmProxyInterface) object).realmGet$lastName();
        if (realmGet$lastName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.lastNameIndex, rowIndex, realmGet$lastName, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.lastNameIndex, rowIndex, false);
        }
        String realmGet$name = ((AgentSessionRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.nameIndex, rowIndex, false);
        }
        String realmGet$consarCode = ((AgentSessionRealmProxyInterface) object).realmGet$consarCode();
        if (realmGet$consarCode != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.consarCodeIndex, rowIndex, realmGet$consarCode, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.consarCodeIndex, rowIndex, false);
        }
        String realmGet$curp = ((AgentSessionRealmProxyInterface) object).realmGet$curp();
        if (realmGet$curp != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.curpIndex, rowIndex, realmGet$curp, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.curpIndex, rowIndex, false);
        }
        String realmGet$creationDate = ((AgentSessionRealmProxyInterface) object).realmGet$creationDate();
        if (realmGet$creationDate != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.creationDateIndex, rowIndex, realmGet$creationDate, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.creationDateIndex, rowIndex, false);
        }
        String realmGet$assignedBranchId = ((AgentSessionRealmProxyInterface) object).realmGet$assignedBranchId();
        if (realmGet$assignedBranchId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.assignedBranchIdIndex, rowIndex, realmGet$assignedBranchId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.assignedBranchIdIndex, rowIndex, false);
        }
        String realmGet$assignedBranchName = ((AgentSessionRealmProxyInterface) object).realmGet$assignedBranchName();
        if (realmGet$assignedBranchName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.assignedBranchNameIndex, rowIndex, realmGet$assignedBranchName, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.assignedBranchNameIndex, rowIndex, false);
        }
        String realmGet$idLogin = ((AgentSessionRealmProxyInterface) object).realmGet$idLogin();
        if (realmGet$idLogin != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idLoginIndex, rowIndex, realmGet$idLogin, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idLoginIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.AgentSession.class);
        long tableNativePtr = table.getNativePtr();
        AgentSessionColumnInfo columnInfo = (AgentSessionColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.AgentSession.class);
        long pkColumnIndex = columnInfo.agentNumberIndex;
        app.profuturo.reclasificacion.com.model.AgentSession object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.AgentSession) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((AgentSessionRealmProxyInterface) object).realmGet$agentNumber();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$surName = ((AgentSessionRealmProxyInterface) object).realmGet$surName();
            if (realmGet$surName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.surNameIndex, rowIndex, realmGet$surName, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.surNameIndex, rowIndex, false);
            }
            String realmGet$lastName = ((AgentSessionRealmProxyInterface) object).realmGet$lastName();
            if (realmGet$lastName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.lastNameIndex, rowIndex, realmGet$lastName, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.lastNameIndex, rowIndex, false);
            }
            String realmGet$name = ((AgentSessionRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.nameIndex, rowIndex, false);
            }
            String realmGet$consarCode = ((AgentSessionRealmProxyInterface) object).realmGet$consarCode();
            if (realmGet$consarCode != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.consarCodeIndex, rowIndex, realmGet$consarCode, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.consarCodeIndex, rowIndex, false);
            }
            String realmGet$curp = ((AgentSessionRealmProxyInterface) object).realmGet$curp();
            if (realmGet$curp != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.curpIndex, rowIndex, realmGet$curp, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.curpIndex, rowIndex, false);
            }
            String realmGet$creationDate = ((AgentSessionRealmProxyInterface) object).realmGet$creationDate();
            if (realmGet$creationDate != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.creationDateIndex, rowIndex, realmGet$creationDate, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.creationDateIndex, rowIndex, false);
            }
            String realmGet$assignedBranchId = ((AgentSessionRealmProxyInterface) object).realmGet$assignedBranchId();
            if (realmGet$assignedBranchId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.assignedBranchIdIndex, rowIndex, realmGet$assignedBranchId, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.assignedBranchIdIndex, rowIndex, false);
            }
            String realmGet$assignedBranchName = ((AgentSessionRealmProxyInterface) object).realmGet$assignedBranchName();
            if (realmGet$assignedBranchName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.assignedBranchNameIndex, rowIndex, realmGet$assignedBranchName, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.assignedBranchNameIndex, rowIndex, false);
            }
            String realmGet$idLogin = ((AgentSessionRealmProxyInterface) object).realmGet$idLogin();
            if (realmGet$idLogin != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idLoginIndex, rowIndex, realmGet$idLogin, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idLoginIndex, rowIndex, false);
            }
        }
    }

    public static app.profuturo.reclasificacion.com.model.AgentSession createDetachedCopy(app.profuturo.reclasificacion.com.model.AgentSession realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        app.profuturo.reclasificacion.com.model.AgentSession unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new app.profuturo.reclasificacion.com.model.AgentSession();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (app.profuturo.reclasificacion.com.model.AgentSession) cachedObject.object;
            }
            unmanagedObject = (app.profuturo.reclasificacion.com.model.AgentSession) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        AgentSessionRealmProxyInterface unmanagedCopy = (AgentSessionRealmProxyInterface) unmanagedObject;
        AgentSessionRealmProxyInterface realmSource = (AgentSessionRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$agentNumber(realmSource.realmGet$agentNumber());
        unmanagedCopy.realmSet$surName(realmSource.realmGet$surName());
        unmanagedCopy.realmSet$lastName(realmSource.realmGet$lastName());
        unmanagedCopy.realmSet$name(realmSource.realmGet$name());
        unmanagedCopy.realmSet$consarCode(realmSource.realmGet$consarCode());
        unmanagedCopy.realmSet$curp(realmSource.realmGet$curp());
        unmanagedCopy.realmSet$creationDate(realmSource.realmGet$creationDate());
        unmanagedCopy.realmSet$assignedBranchId(realmSource.realmGet$assignedBranchId());
        unmanagedCopy.realmSet$assignedBranchName(realmSource.realmGet$assignedBranchName());
        unmanagedCopy.realmSet$idLogin(realmSource.realmGet$idLogin());

        return unmanagedObject;
    }

    static app.profuturo.reclasificacion.com.model.AgentSession update(Realm realm, app.profuturo.reclasificacion.com.model.AgentSession realmObject, app.profuturo.reclasificacion.com.model.AgentSession newObject, Map<RealmModel, RealmObjectProxy> cache) {
        AgentSessionRealmProxyInterface realmObjectTarget = (AgentSessionRealmProxyInterface) realmObject;
        AgentSessionRealmProxyInterface realmObjectSource = (AgentSessionRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$surName(realmObjectSource.realmGet$surName());
        realmObjectTarget.realmSet$lastName(realmObjectSource.realmGet$lastName());
        realmObjectTarget.realmSet$name(realmObjectSource.realmGet$name());
        realmObjectTarget.realmSet$consarCode(realmObjectSource.realmGet$consarCode());
        realmObjectTarget.realmSet$curp(realmObjectSource.realmGet$curp());
        realmObjectTarget.realmSet$creationDate(realmObjectSource.realmGet$creationDate());
        realmObjectTarget.realmSet$assignedBranchId(realmObjectSource.realmGet$assignedBranchId());
        realmObjectTarget.realmSet$assignedBranchName(realmObjectSource.realmGet$assignedBranchName());
        realmObjectTarget.realmSet$idLogin(realmObjectSource.realmGet$idLogin());
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("AgentSession = proxy[");
        stringBuilder.append("{agentNumber:");
        stringBuilder.append(realmGet$agentNumber() != null ? realmGet$agentNumber() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{surName:");
        stringBuilder.append(realmGet$surName() != null ? realmGet$surName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{lastName:");
        stringBuilder.append(realmGet$lastName() != null ? realmGet$lastName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{name:");
        stringBuilder.append(realmGet$name() != null ? realmGet$name() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{consarCode:");
        stringBuilder.append(realmGet$consarCode() != null ? realmGet$consarCode() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{curp:");
        stringBuilder.append(realmGet$curp() != null ? realmGet$curp() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{creationDate:");
        stringBuilder.append(realmGet$creationDate() != null ? realmGet$creationDate() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{assignedBranchId:");
        stringBuilder.append(realmGet$assignedBranchId() != null ? realmGet$assignedBranchId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{assignedBranchName:");
        stringBuilder.append(realmGet$assignedBranchName() != null ? realmGet$assignedBranchName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idLogin:");
        stringBuilder.append(realmGet$idLogin() != null ? realmGet$idLogin() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AgentSessionRealmProxy aAgentSession = (AgentSessionRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aAgentSession.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aAgentSession.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aAgentSession.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
