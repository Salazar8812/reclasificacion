package io.realm;


public interface SavingsAccountRealmProxyInterface {
    public app.profuturo.reclasificacion.com.model.Shares realmGet$shares();
    public void realmSet$shares(app.profuturo.reclasificacion.com.model.Shares value);
    public Double realmGet$totalHousingInPesos();
    public void realmSet$totalHousingInPesos(Double value);
    public Double realmGet$totalNotHousingInPesos();
    public void realmSet$totalNotHousingInPesos(Double value);
    public Double realmGet$totalAllAccountsInPesos();
    public void realmSet$totalAllAccountsInPesos(Double value);
}
