package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.ProxyUtils;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class SolicitanteRealmProxy extends app.profuturo.reclasificacion.com.model.Solicitante
    implements RealmObjectProxy, SolicitanteRealmProxyInterface {

    static final class SolicitanteColumnInfo extends ColumnInfo {
        long apMaternoCteIndex;
        long apPaternoCteIndex;
        long celularIndex;
        long correoElecIndex;
        long curpIndex;
        long nombreCteIndex;
        long nssIndex;
        long numCtaInvdualIndex;
        long rfcCteIndex;
        long usuCreIndex;
        long calleIndex;
        long codigoPostalIndex;
        long coloniaIndex;
        long estadoIndex;
        long municipioDelegacionIndex;
        long numeroExteriorIndex;
        long numeroInteriorIndex;
        long telefonoIndex;

        SolicitanteColumnInfo(OsSchemaInfo schemaInfo) {
            super(18);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Solicitante");
            this.apMaternoCteIndex = addColumnDetails("apMaternoCte", objectSchemaInfo);
            this.apPaternoCteIndex = addColumnDetails("apPaternoCte", objectSchemaInfo);
            this.celularIndex = addColumnDetails("celular", objectSchemaInfo);
            this.correoElecIndex = addColumnDetails("correoElec", objectSchemaInfo);
            this.curpIndex = addColumnDetails("curp", objectSchemaInfo);
            this.nombreCteIndex = addColumnDetails("nombreCte", objectSchemaInfo);
            this.nssIndex = addColumnDetails("nss", objectSchemaInfo);
            this.numCtaInvdualIndex = addColumnDetails("numCtaInvdual", objectSchemaInfo);
            this.rfcCteIndex = addColumnDetails("rfcCte", objectSchemaInfo);
            this.usuCreIndex = addColumnDetails("usuCre", objectSchemaInfo);
            this.calleIndex = addColumnDetails("calle", objectSchemaInfo);
            this.codigoPostalIndex = addColumnDetails("codigoPostal", objectSchemaInfo);
            this.coloniaIndex = addColumnDetails("colonia", objectSchemaInfo);
            this.estadoIndex = addColumnDetails("estado", objectSchemaInfo);
            this.municipioDelegacionIndex = addColumnDetails("municipioDelegacion", objectSchemaInfo);
            this.numeroExteriorIndex = addColumnDetails("numeroExterior", objectSchemaInfo);
            this.numeroInteriorIndex = addColumnDetails("numeroInterior", objectSchemaInfo);
            this.telefonoIndex = addColumnDetails("telefono", objectSchemaInfo);
        }

        SolicitanteColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new SolicitanteColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final SolicitanteColumnInfo src = (SolicitanteColumnInfo) rawSrc;
            final SolicitanteColumnInfo dst = (SolicitanteColumnInfo) rawDst;
            dst.apMaternoCteIndex = src.apMaternoCteIndex;
            dst.apPaternoCteIndex = src.apPaternoCteIndex;
            dst.celularIndex = src.celularIndex;
            dst.correoElecIndex = src.correoElecIndex;
            dst.curpIndex = src.curpIndex;
            dst.nombreCteIndex = src.nombreCteIndex;
            dst.nssIndex = src.nssIndex;
            dst.numCtaInvdualIndex = src.numCtaInvdualIndex;
            dst.rfcCteIndex = src.rfcCteIndex;
            dst.usuCreIndex = src.usuCreIndex;
            dst.calleIndex = src.calleIndex;
            dst.codigoPostalIndex = src.codigoPostalIndex;
            dst.coloniaIndex = src.coloniaIndex;
            dst.estadoIndex = src.estadoIndex;
            dst.municipioDelegacionIndex = src.municipioDelegacionIndex;
            dst.numeroExteriorIndex = src.numeroExteriorIndex;
            dst.numeroInteriorIndex = src.numeroInteriorIndex;
            dst.telefonoIndex = src.telefonoIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>(18);
        fieldNames.add("apMaternoCte");
        fieldNames.add("apPaternoCte");
        fieldNames.add("celular");
        fieldNames.add("correoElec");
        fieldNames.add("curp");
        fieldNames.add("nombreCte");
        fieldNames.add("nss");
        fieldNames.add("numCtaInvdual");
        fieldNames.add("rfcCte");
        fieldNames.add("usuCre");
        fieldNames.add("calle");
        fieldNames.add("codigoPostal");
        fieldNames.add("colonia");
        fieldNames.add("estado");
        fieldNames.add("municipioDelegacion");
        fieldNames.add("numeroExterior");
        fieldNames.add("numeroInterior");
        fieldNames.add("telefono");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    private SolicitanteColumnInfo columnInfo;
    private ProxyState<app.profuturo.reclasificacion.com.model.Solicitante> proxyState;

    SolicitanteRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (SolicitanteColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<app.profuturo.reclasificacion.com.model.Solicitante>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$apMaternoCte() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.apMaternoCteIndex);
    }

    @Override
    public void realmSet$apMaternoCte(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.apMaternoCteIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.apMaternoCteIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.apMaternoCteIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.apMaternoCteIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$apPaternoCte() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.apPaternoCteIndex);
    }

    @Override
    public void realmSet$apPaternoCte(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.apPaternoCteIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.apPaternoCteIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.apPaternoCteIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.apPaternoCteIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$celular() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.celularIndex);
    }

    @Override
    public void realmSet$celular(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.celularIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.celularIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.celularIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.celularIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$correoElec() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.correoElecIndex);
    }

    @Override
    public void realmSet$correoElec(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.correoElecIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.correoElecIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.correoElecIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.correoElecIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$curp() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.curpIndex);
    }

    @Override
    public void realmSet$curp(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.curpIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.curpIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.curpIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.curpIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$nombreCte() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.nombreCteIndex);
    }

    @Override
    public void realmSet$nombreCte(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.nombreCteIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.nombreCteIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.nombreCteIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.nombreCteIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$nss() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.nssIndex);
    }

    @Override
    public void realmSet$nss(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.nssIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.nssIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.nssIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.nssIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$numCtaInvdual() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.numCtaInvdualIndex);
    }

    @Override
    public void realmSet$numCtaInvdual(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.numCtaInvdualIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.numCtaInvdualIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.numCtaInvdualIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.numCtaInvdualIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$rfcCte() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.rfcCteIndex);
    }

    @Override
    public void realmSet$rfcCte(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.rfcCteIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.rfcCteIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.rfcCteIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.rfcCteIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$usuCre() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.usuCreIndex);
    }

    @Override
    public void realmSet$usuCre(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.usuCreIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.usuCreIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.usuCreIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.usuCreIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$calle() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.calleIndex);
    }

    @Override
    public void realmSet$calle(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.calleIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.calleIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.calleIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.calleIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$codigoPostal() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.codigoPostalIndex);
    }

    @Override
    public void realmSet$codigoPostal(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.codigoPostalIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.codigoPostalIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.codigoPostalIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.codigoPostalIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$colonia() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.coloniaIndex);
    }

    @Override
    public void realmSet$colonia(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.coloniaIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.coloniaIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.coloniaIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.coloniaIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$estado() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.estadoIndex);
    }

    @Override
    public void realmSet$estado(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.estadoIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.estadoIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.estadoIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.estadoIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$municipioDelegacion() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.municipioDelegacionIndex);
    }

    @Override
    public void realmSet$municipioDelegacion(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.municipioDelegacionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.municipioDelegacionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.municipioDelegacionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.municipioDelegacionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$numeroExterior() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.numeroExteriorIndex);
    }

    @Override
    public void realmSet$numeroExterior(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.numeroExteriorIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.numeroExteriorIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.numeroExteriorIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.numeroExteriorIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$numeroInterior() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.numeroInteriorIndex);
    }

    @Override
    public void realmSet$numeroInterior(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.numeroInteriorIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.numeroInteriorIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.numeroInteriorIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.numeroInteriorIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$telefono() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.telefonoIndex);
    }

    @Override
    public void realmSet$telefono(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.telefonoIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.telefonoIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.telefonoIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.telefonoIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Solicitante", 18, 0);
        builder.addPersistedProperty("apMaternoCte", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("apPaternoCte", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("celular", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("correoElec", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("curp", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("nombreCte", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("nss", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("numCtaInvdual", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("rfcCte", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("usuCre", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("calle", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("codigoPostal", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("colonia", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("estado", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("municipioDelegacion", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("numeroExterior", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("numeroInterior", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("telefono", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static SolicitanteColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new SolicitanteColumnInfo(schemaInfo);
    }

    public static String getTableName() {
        return "class_Solicitante";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static app.profuturo.reclasificacion.com.model.Solicitante createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        app.profuturo.reclasificacion.com.model.Solicitante obj = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.Solicitante.class, true, excludeFields);

        final SolicitanteRealmProxyInterface objProxy = (SolicitanteRealmProxyInterface) obj;
        if (json.has("apMaternoCte")) {
            if (json.isNull("apMaternoCte")) {
                objProxy.realmSet$apMaternoCte(null);
            } else {
                objProxy.realmSet$apMaternoCte((String) json.getString("apMaternoCte"));
            }
        }
        if (json.has("apPaternoCte")) {
            if (json.isNull("apPaternoCte")) {
                objProxy.realmSet$apPaternoCte(null);
            } else {
                objProxy.realmSet$apPaternoCte((String) json.getString("apPaternoCte"));
            }
        }
        if (json.has("celular")) {
            if (json.isNull("celular")) {
                objProxy.realmSet$celular(null);
            } else {
                objProxy.realmSet$celular((String) json.getString("celular"));
            }
        }
        if (json.has("correoElec")) {
            if (json.isNull("correoElec")) {
                objProxy.realmSet$correoElec(null);
            } else {
                objProxy.realmSet$correoElec((String) json.getString("correoElec"));
            }
        }
        if (json.has("curp")) {
            if (json.isNull("curp")) {
                objProxy.realmSet$curp(null);
            } else {
                objProxy.realmSet$curp((String) json.getString("curp"));
            }
        }
        if (json.has("nombreCte")) {
            if (json.isNull("nombreCte")) {
                objProxy.realmSet$nombreCte(null);
            } else {
                objProxy.realmSet$nombreCte((String) json.getString("nombreCte"));
            }
        }
        if (json.has("nss")) {
            if (json.isNull("nss")) {
                objProxy.realmSet$nss(null);
            } else {
                objProxy.realmSet$nss((String) json.getString("nss"));
            }
        }
        if (json.has("numCtaInvdual")) {
            if (json.isNull("numCtaInvdual")) {
                objProxy.realmSet$numCtaInvdual(null);
            } else {
                objProxy.realmSet$numCtaInvdual((String) json.getString("numCtaInvdual"));
            }
        }
        if (json.has("rfcCte")) {
            if (json.isNull("rfcCte")) {
                objProxy.realmSet$rfcCte(null);
            } else {
                objProxy.realmSet$rfcCte((String) json.getString("rfcCte"));
            }
        }
        if (json.has("usuCre")) {
            if (json.isNull("usuCre")) {
                objProxy.realmSet$usuCre(null);
            } else {
                objProxy.realmSet$usuCre((String) json.getString("usuCre"));
            }
        }
        if (json.has("calle")) {
            if (json.isNull("calle")) {
                objProxy.realmSet$calle(null);
            } else {
                objProxy.realmSet$calle((String) json.getString("calle"));
            }
        }
        if (json.has("codigoPostal")) {
            if (json.isNull("codigoPostal")) {
                objProxy.realmSet$codigoPostal(null);
            } else {
                objProxy.realmSet$codigoPostal((String) json.getString("codigoPostal"));
            }
        }
        if (json.has("colonia")) {
            if (json.isNull("colonia")) {
                objProxy.realmSet$colonia(null);
            } else {
                objProxy.realmSet$colonia((String) json.getString("colonia"));
            }
        }
        if (json.has("estado")) {
            if (json.isNull("estado")) {
                objProxy.realmSet$estado(null);
            } else {
                objProxy.realmSet$estado((String) json.getString("estado"));
            }
        }
        if (json.has("municipioDelegacion")) {
            if (json.isNull("municipioDelegacion")) {
                objProxy.realmSet$municipioDelegacion(null);
            } else {
                objProxy.realmSet$municipioDelegacion((String) json.getString("municipioDelegacion"));
            }
        }
        if (json.has("numeroExterior")) {
            if (json.isNull("numeroExterior")) {
                objProxy.realmSet$numeroExterior(null);
            } else {
                objProxy.realmSet$numeroExterior((String) json.getString("numeroExterior"));
            }
        }
        if (json.has("numeroInterior")) {
            if (json.isNull("numeroInterior")) {
                objProxy.realmSet$numeroInterior(null);
            } else {
                objProxy.realmSet$numeroInterior((String) json.getString("numeroInterior"));
            }
        }
        if (json.has("telefono")) {
            if (json.isNull("telefono")) {
                objProxy.realmSet$telefono(null);
            } else {
                objProxy.realmSet$telefono((String) json.getString("telefono"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static app.profuturo.reclasificacion.com.model.Solicitante createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        final app.profuturo.reclasificacion.com.model.Solicitante obj = new app.profuturo.reclasificacion.com.model.Solicitante();
        final SolicitanteRealmProxyInterface objProxy = (SolicitanteRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("apMaternoCte")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$apMaternoCte((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$apMaternoCte(null);
                }
            } else if (name.equals("apPaternoCte")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$apPaternoCte((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$apPaternoCte(null);
                }
            } else if (name.equals("celular")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$celular((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$celular(null);
                }
            } else if (name.equals("correoElec")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$correoElec((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$correoElec(null);
                }
            } else if (name.equals("curp")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$curp((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$curp(null);
                }
            } else if (name.equals("nombreCte")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$nombreCte((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$nombreCte(null);
                }
            } else if (name.equals("nss")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$nss((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$nss(null);
                }
            } else if (name.equals("numCtaInvdual")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$numCtaInvdual((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$numCtaInvdual(null);
                }
            } else if (name.equals("rfcCte")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$rfcCte((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$rfcCte(null);
                }
            } else if (name.equals("usuCre")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$usuCre((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$usuCre(null);
                }
            } else if (name.equals("calle")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$calle((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$calle(null);
                }
            } else if (name.equals("codigoPostal")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$codigoPostal((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$codigoPostal(null);
                }
            } else if (name.equals("colonia")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$colonia((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$colonia(null);
                }
            } else if (name.equals("estado")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$estado((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$estado(null);
                }
            } else if (name.equals("municipioDelegacion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$municipioDelegacion((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$municipioDelegacion(null);
                }
            } else if (name.equals("numeroExterior")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$numeroExterior((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$numeroExterior(null);
                }
            } else if (name.equals("numeroInterior")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$numeroInterior((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$numeroInterior(null);
                }
            } else if (name.equals("telefono")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$telefono((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$telefono(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return realm.copyToRealm(obj);
    }

    public static app.profuturo.reclasificacion.com.model.Solicitante copyOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.Solicitante object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.Solicitante) cachedRealmObject;
        }

        return copy(realm, object, update, cache);
    }

    public static app.profuturo.reclasificacion.com.model.Solicitante copy(Realm realm, app.profuturo.reclasificacion.com.model.Solicitante newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.Solicitante) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        app.profuturo.reclasificacion.com.model.Solicitante realmObject = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.Solicitante.class, false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        SolicitanteRealmProxyInterface realmObjectSource = (SolicitanteRealmProxyInterface) newObject;
        SolicitanteRealmProxyInterface realmObjectCopy = (SolicitanteRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$apMaternoCte(realmObjectSource.realmGet$apMaternoCte());
        realmObjectCopy.realmSet$apPaternoCte(realmObjectSource.realmGet$apPaternoCte());
        realmObjectCopy.realmSet$celular(realmObjectSource.realmGet$celular());
        realmObjectCopy.realmSet$correoElec(realmObjectSource.realmGet$correoElec());
        realmObjectCopy.realmSet$curp(realmObjectSource.realmGet$curp());
        realmObjectCopy.realmSet$nombreCte(realmObjectSource.realmGet$nombreCte());
        realmObjectCopy.realmSet$nss(realmObjectSource.realmGet$nss());
        realmObjectCopy.realmSet$numCtaInvdual(realmObjectSource.realmGet$numCtaInvdual());
        realmObjectCopy.realmSet$rfcCte(realmObjectSource.realmGet$rfcCte());
        realmObjectCopy.realmSet$usuCre(realmObjectSource.realmGet$usuCre());
        realmObjectCopy.realmSet$calle(realmObjectSource.realmGet$calle());
        realmObjectCopy.realmSet$codigoPostal(realmObjectSource.realmGet$codigoPostal());
        realmObjectCopy.realmSet$colonia(realmObjectSource.realmGet$colonia());
        realmObjectCopy.realmSet$estado(realmObjectSource.realmGet$estado());
        realmObjectCopy.realmSet$municipioDelegacion(realmObjectSource.realmGet$municipioDelegacion());
        realmObjectCopy.realmSet$numeroExterior(realmObjectSource.realmGet$numeroExterior());
        realmObjectCopy.realmSet$numeroInterior(realmObjectSource.realmGet$numeroInterior());
        realmObjectCopy.realmSet$telefono(realmObjectSource.realmGet$telefono());
        return realmObject;
    }

    public static long insert(Realm realm, app.profuturo.reclasificacion.com.model.Solicitante object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Solicitante.class);
        long tableNativePtr = table.getNativePtr();
        SolicitanteColumnInfo columnInfo = (SolicitanteColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Solicitante.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        String realmGet$apMaternoCte = ((SolicitanteRealmProxyInterface) object).realmGet$apMaternoCte();
        if (realmGet$apMaternoCte != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.apMaternoCteIndex, rowIndex, realmGet$apMaternoCte, false);
        }
        String realmGet$apPaternoCte = ((SolicitanteRealmProxyInterface) object).realmGet$apPaternoCte();
        if (realmGet$apPaternoCte != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.apPaternoCteIndex, rowIndex, realmGet$apPaternoCte, false);
        }
        String realmGet$celular = ((SolicitanteRealmProxyInterface) object).realmGet$celular();
        if (realmGet$celular != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.celularIndex, rowIndex, realmGet$celular, false);
        }
        String realmGet$correoElec = ((SolicitanteRealmProxyInterface) object).realmGet$correoElec();
        if (realmGet$correoElec != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.correoElecIndex, rowIndex, realmGet$correoElec, false);
        }
        String realmGet$curp = ((SolicitanteRealmProxyInterface) object).realmGet$curp();
        if (realmGet$curp != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.curpIndex, rowIndex, realmGet$curp, false);
        }
        String realmGet$nombreCte = ((SolicitanteRealmProxyInterface) object).realmGet$nombreCte();
        if (realmGet$nombreCte != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nombreCteIndex, rowIndex, realmGet$nombreCte, false);
        }
        String realmGet$nss = ((SolicitanteRealmProxyInterface) object).realmGet$nss();
        if (realmGet$nss != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nssIndex, rowIndex, realmGet$nss, false);
        }
        String realmGet$numCtaInvdual = ((SolicitanteRealmProxyInterface) object).realmGet$numCtaInvdual();
        if (realmGet$numCtaInvdual != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.numCtaInvdualIndex, rowIndex, realmGet$numCtaInvdual, false);
        }
        String realmGet$rfcCte = ((SolicitanteRealmProxyInterface) object).realmGet$rfcCte();
        if (realmGet$rfcCte != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.rfcCteIndex, rowIndex, realmGet$rfcCte, false);
        }
        String realmGet$usuCre = ((SolicitanteRealmProxyInterface) object).realmGet$usuCre();
        if (realmGet$usuCre != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.usuCreIndex, rowIndex, realmGet$usuCre, false);
        }
        String realmGet$calle = ((SolicitanteRealmProxyInterface) object).realmGet$calle();
        if (realmGet$calle != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.calleIndex, rowIndex, realmGet$calle, false);
        }
        String realmGet$codigoPostal = ((SolicitanteRealmProxyInterface) object).realmGet$codigoPostal();
        if (realmGet$codigoPostal != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.codigoPostalIndex, rowIndex, realmGet$codigoPostal, false);
        }
        String realmGet$colonia = ((SolicitanteRealmProxyInterface) object).realmGet$colonia();
        if (realmGet$colonia != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.coloniaIndex, rowIndex, realmGet$colonia, false);
        }
        String realmGet$estado = ((SolicitanteRealmProxyInterface) object).realmGet$estado();
        if (realmGet$estado != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estadoIndex, rowIndex, realmGet$estado, false);
        }
        String realmGet$municipioDelegacion = ((SolicitanteRealmProxyInterface) object).realmGet$municipioDelegacion();
        if (realmGet$municipioDelegacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.municipioDelegacionIndex, rowIndex, realmGet$municipioDelegacion, false);
        }
        String realmGet$numeroExterior = ((SolicitanteRealmProxyInterface) object).realmGet$numeroExterior();
        if (realmGet$numeroExterior != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.numeroExteriorIndex, rowIndex, realmGet$numeroExterior, false);
        }
        String realmGet$numeroInterior = ((SolicitanteRealmProxyInterface) object).realmGet$numeroInterior();
        if (realmGet$numeroInterior != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.numeroInteriorIndex, rowIndex, realmGet$numeroInterior, false);
        }
        String realmGet$telefono = ((SolicitanteRealmProxyInterface) object).realmGet$telefono();
        if (realmGet$telefono != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.telefonoIndex, rowIndex, realmGet$telefono, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Solicitante.class);
        long tableNativePtr = table.getNativePtr();
        SolicitanteColumnInfo columnInfo = (SolicitanteColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Solicitante.class);
        app.profuturo.reclasificacion.com.model.Solicitante object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.Solicitante) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            String realmGet$apMaternoCte = ((SolicitanteRealmProxyInterface) object).realmGet$apMaternoCte();
            if (realmGet$apMaternoCte != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.apMaternoCteIndex, rowIndex, realmGet$apMaternoCte, false);
            }
            String realmGet$apPaternoCte = ((SolicitanteRealmProxyInterface) object).realmGet$apPaternoCte();
            if (realmGet$apPaternoCte != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.apPaternoCteIndex, rowIndex, realmGet$apPaternoCte, false);
            }
            String realmGet$celular = ((SolicitanteRealmProxyInterface) object).realmGet$celular();
            if (realmGet$celular != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.celularIndex, rowIndex, realmGet$celular, false);
            }
            String realmGet$correoElec = ((SolicitanteRealmProxyInterface) object).realmGet$correoElec();
            if (realmGet$correoElec != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.correoElecIndex, rowIndex, realmGet$correoElec, false);
            }
            String realmGet$curp = ((SolicitanteRealmProxyInterface) object).realmGet$curp();
            if (realmGet$curp != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.curpIndex, rowIndex, realmGet$curp, false);
            }
            String realmGet$nombreCte = ((SolicitanteRealmProxyInterface) object).realmGet$nombreCte();
            if (realmGet$nombreCte != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nombreCteIndex, rowIndex, realmGet$nombreCte, false);
            }
            String realmGet$nss = ((SolicitanteRealmProxyInterface) object).realmGet$nss();
            if (realmGet$nss != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nssIndex, rowIndex, realmGet$nss, false);
            }
            String realmGet$numCtaInvdual = ((SolicitanteRealmProxyInterface) object).realmGet$numCtaInvdual();
            if (realmGet$numCtaInvdual != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.numCtaInvdualIndex, rowIndex, realmGet$numCtaInvdual, false);
            }
            String realmGet$rfcCte = ((SolicitanteRealmProxyInterface) object).realmGet$rfcCte();
            if (realmGet$rfcCte != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.rfcCteIndex, rowIndex, realmGet$rfcCte, false);
            }
            String realmGet$usuCre = ((SolicitanteRealmProxyInterface) object).realmGet$usuCre();
            if (realmGet$usuCre != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.usuCreIndex, rowIndex, realmGet$usuCre, false);
            }
            String realmGet$calle = ((SolicitanteRealmProxyInterface) object).realmGet$calle();
            if (realmGet$calle != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.calleIndex, rowIndex, realmGet$calle, false);
            }
            String realmGet$codigoPostal = ((SolicitanteRealmProxyInterface) object).realmGet$codigoPostal();
            if (realmGet$codigoPostal != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.codigoPostalIndex, rowIndex, realmGet$codigoPostal, false);
            }
            String realmGet$colonia = ((SolicitanteRealmProxyInterface) object).realmGet$colonia();
            if (realmGet$colonia != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.coloniaIndex, rowIndex, realmGet$colonia, false);
            }
            String realmGet$estado = ((SolicitanteRealmProxyInterface) object).realmGet$estado();
            if (realmGet$estado != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estadoIndex, rowIndex, realmGet$estado, false);
            }
            String realmGet$municipioDelegacion = ((SolicitanteRealmProxyInterface) object).realmGet$municipioDelegacion();
            if (realmGet$municipioDelegacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.municipioDelegacionIndex, rowIndex, realmGet$municipioDelegacion, false);
            }
            String realmGet$numeroExterior = ((SolicitanteRealmProxyInterface) object).realmGet$numeroExterior();
            if (realmGet$numeroExterior != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.numeroExteriorIndex, rowIndex, realmGet$numeroExterior, false);
            }
            String realmGet$numeroInterior = ((SolicitanteRealmProxyInterface) object).realmGet$numeroInterior();
            if (realmGet$numeroInterior != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.numeroInteriorIndex, rowIndex, realmGet$numeroInterior, false);
            }
            String realmGet$telefono = ((SolicitanteRealmProxyInterface) object).realmGet$telefono();
            if (realmGet$telefono != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.telefonoIndex, rowIndex, realmGet$telefono, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.Solicitante object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Solicitante.class);
        long tableNativePtr = table.getNativePtr();
        SolicitanteColumnInfo columnInfo = (SolicitanteColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Solicitante.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        String realmGet$apMaternoCte = ((SolicitanteRealmProxyInterface) object).realmGet$apMaternoCte();
        if (realmGet$apMaternoCte != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.apMaternoCteIndex, rowIndex, realmGet$apMaternoCte, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.apMaternoCteIndex, rowIndex, false);
        }
        String realmGet$apPaternoCte = ((SolicitanteRealmProxyInterface) object).realmGet$apPaternoCte();
        if (realmGet$apPaternoCte != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.apPaternoCteIndex, rowIndex, realmGet$apPaternoCte, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.apPaternoCteIndex, rowIndex, false);
        }
        String realmGet$celular = ((SolicitanteRealmProxyInterface) object).realmGet$celular();
        if (realmGet$celular != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.celularIndex, rowIndex, realmGet$celular, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.celularIndex, rowIndex, false);
        }
        String realmGet$correoElec = ((SolicitanteRealmProxyInterface) object).realmGet$correoElec();
        if (realmGet$correoElec != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.correoElecIndex, rowIndex, realmGet$correoElec, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.correoElecIndex, rowIndex, false);
        }
        String realmGet$curp = ((SolicitanteRealmProxyInterface) object).realmGet$curp();
        if (realmGet$curp != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.curpIndex, rowIndex, realmGet$curp, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.curpIndex, rowIndex, false);
        }
        String realmGet$nombreCte = ((SolicitanteRealmProxyInterface) object).realmGet$nombreCte();
        if (realmGet$nombreCte != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nombreCteIndex, rowIndex, realmGet$nombreCte, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.nombreCteIndex, rowIndex, false);
        }
        String realmGet$nss = ((SolicitanteRealmProxyInterface) object).realmGet$nss();
        if (realmGet$nss != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nssIndex, rowIndex, realmGet$nss, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.nssIndex, rowIndex, false);
        }
        String realmGet$numCtaInvdual = ((SolicitanteRealmProxyInterface) object).realmGet$numCtaInvdual();
        if (realmGet$numCtaInvdual != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.numCtaInvdualIndex, rowIndex, realmGet$numCtaInvdual, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.numCtaInvdualIndex, rowIndex, false);
        }
        String realmGet$rfcCte = ((SolicitanteRealmProxyInterface) object).realmGet$rfcCte();
        if (realmGet$rfcCte != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.rfcCteIndex, rowIndex, realmGet$rfcCte, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.rfcCteIndex, rowIndex, false);
        }
        String realmGet$usuCre = ((SolicitanteRealmProxyInterface) object).realmGet$usuCre();
        if (realmGet$usuCre != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.usuCreIndex, rowIndex, realmGet$usuCre, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.usuCreIndex, rowIndex, false);
        }
        String realmGet$calle = ((SolicitanteRealmProxyInterface) object).realmGet$calle();
        if (realmGet$calle != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.calleIndex, rowIndex, realmGet$calle, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.calleIndex, rowIndex, false);
        }
        String realmGet$codigoPostal = ((SolicitanteRealmProxyInterface) object).realmGet$codigoPostal();
        if (realmGet$codigoPostal != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.codigoPostalIndex, rowIndex, realmGet$codigoPostal, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.codigoPostalIndex, rowIndex, false);
        }
        String realmGet$colonia = ((SolicitanteRealmProxyInterface) object).realmGet$colonia();
        if (realmGet$colonia != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.coloniaIndex, rowIndex, realmGet$colonia, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.coloniaIndex, rowIndex, false);
        }
        String realmGet$estado = ((SolicitanteRealmProxyInterface) object).realmGet$estado();
        if (realmGet$estado != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estadoIndex, rowIndex, realmGet$estado, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.estadoIndex, rowIndex, false);
        }
        String realmGet$municipioDelegacion = ((SolicitanteRealmProxyInterface) object).realmGet$municipioDelegacion();
        if (realmGet$municipioDelegacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.municipioDelegacionIndex, rowIndex, realmGet$municipioDelegacion, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.municipioDelegacionIndex, rowIndex, false);
        }
        String realmGet$numeroExterior = ((SolicitanteRealmProxyInterface) object).realmGet$numeroExterior();
        if (realmGet$numeroExterior != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.numeroExteriorIndex, rowIndex, realmGet$numeroExterior, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.numeroExteriorIndex, rowIndex, false);
        }
        String realmGet$numeroInterior = ((SolicitanteRealmProxyInterface) object).realmGet$numeroInterior();
        if (realmGet$numeroInterior != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.numeroInteriorIndex, rowIndex, realmGet$numeroInterior, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.numeroInteriorIndex, rowIndex, false);
        }
        String realmGet$telefono = ((SolicitanteRealmProxyInterface) object).realmGet$telefono();
        if (realmGet$telefono != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.telefonoIndex, rowIndex, realmGet$telefono, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.telefonoIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Solicitante.class);
        long tableNativePtr = table.getNativePtr();
        SolicitanteColumnInfo columnInfo = (SolicitanteColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Solicitante.class);
        app.profuturo.reclasificacion.com.model.Solicitante object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.Solicitante) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            String realmGet$apMaternoCte = ((SolicitanteRealmProxyInterface) object).realmGet$apMaternoCte();
            if (realmGet$apMaternoCte != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.apMaternoCteIndex, rowIndex, realmGet$apMaternoCte, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.apMaternoCteIndex, rowIndex, false);
            }
            String realmGet$apPaternoCte = ((SolicitanteRealmProxyInterface) object).realmGet$apPaternoCte();
            if (realmGet$apPaternoCte != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.apPaternoCteIndex, rowIndex, realmGet$apPaternoCte, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.apPaternoCteIndex, rowIndex, false);
            }
            String realmGet$celular = ((SolicitanteRealmProxyInterface) object).realmGet$celular();
            if (realmGet$celular != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.celularIndex, rowIndex, realmGet$celular, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.celularIndex, rowIndex, false);
            }
            String realmGet$correoElec = ((SolicitanteRealmProxyInterface) object).realmGet$correoElec();
            if (realmGet$correoElec != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.correoElecIndex, rowIndex, realmGet$correoElec, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.correoElecIndex, rowIndex, false);
            }
            String realmGet$curp = ((SolicitanteRealmProxyInterface) object).realmGet$curp();
            if (realmGet$curp != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.curpIndex, rowIndex, realmGet$curp, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.curpIndex, rowIndex, false);
            }
            String realmGet$nombreCte = ((SolicitanteRealmProxyInterface) object).realmGet$nombreCte();
            if (realmGet$nombreCte != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nombreCteIndex, rowIndex, realmGet$nombreCte, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.nombreCteIndex, rowIndex, false);
            }
            String realmGet$nss = ((SolicitanteRealmProxyInterface) object).realmGet$nss();
            if (realmGet$nss != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nssIndex, rowIndex, realmGet$nss, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.nssIndex, rowIndex, false);
            }
            String realmGet$numCtaInvdual = ((SolicitanteRealmProxyInterface) object).realmGet$numCtaInvdual();
            if (realmGet$numCtaInvdual != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.numCtaInvdualIndex, rowIndex, realmGet$numCtaInvdual, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.numCtaInvdualIndex, rowIndex, false);
            }
            String realmGet$rfcCte = ((SolicitanteRealmProxyInterface) object).realmGet$rfcCte();
            if (realmGet$rfcCte != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.rfcCteIndex, rowIndex, realmGet$rfcCte, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.rfcCteIndex, rowIndex, false);
            }
            String realmGet$usuCre = ((SolicitanteRealmProxyInterface) object).realmGet$usuCre();
            if (realmGet$usuCre != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.usuCreIndex, rowIndex, realmGet$usuCre, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.usuCreIndex, rowIndex, false);
            }
            String realmGet$calle = ((SolicitanteRealmProxyInterface) object).realmGet$calle();
            if (realmGet$calle != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.calleIndex, rowIndex, realmGet$calle, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.calleIndex, rowIndex, false);
            }
            String realmGet$codigoPostal = ((SolicitanteRealmProxyInterface) object).realmGet$codigoPostal();
            if (realmGet$codigoPostal != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.codigoPostalIndex, rowIndex, realmGet$codigoPostal, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.codigoPostalIndex, rowIndex, false);
            }
            String realmGet$colonia = ((SolicitanteRealmProxyInterface) object).realmGet$colonia();
            if (realmGet$colonia != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.coloniaIndex, rowIndex, realmGet$colonia, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.coloniaIndex, rowIndex, false);
            }
            String realmGet$estado = ((SolicitanteRealmProxyInterface) object).realmGet$estado();
            if (realmGet$estado != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estadoIndex, rowIndex, realmGet$estado, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.estadoIndex, rowIndex, false);
            }
            String realmGet$municipioDelegacion = ((SolicitanteRealmProxyInterface) object).realmGet$municipioDelegacion();
            if (realmGet$municipioDelegacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.municipioDelegacionIndex, rowIndex, realmGet$municipioDelegacion, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.municipioDelegacionIndex, rowIndex, false);
            }
            String realmGet$numeroExterior = ((SolicitanteRealmProxyInterface) object).realmGet$numeroExterior();
            if (realmGet$numeroExterior != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.numeroExteriorIndex, rowIndex, realmGet$numeroExterior, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.numeroExteriorIndex, rowIndex, false);
            }
            String realmGet$numeroInterior = ((SolicitanteRealmProxyInterface) object).realmGet$numeroInterior();
            if (realmGet$numeroInterior != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.numeroInteriorIndex, rowIndex, realmGet$numeroInterior, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.numeroInteriorIndex, rowIndex, false);
            }
            String realmGet$telefono = ((SolicitanteRealmProxyInterface) object).realmGet$telefono();
            if (realmGet$telefono != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.telefonoIndex, rowIndex, realmGet$telefono, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.telefonoIndex, rowIndex, false);
            }
        }
    }

    public static app.profuturo.reclasificacion.com.model.Solicitante createDetachedCopy(app.profuturo.reclasificacion.com.model.Solicitante realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        app.profuturo.reclasificacion.com.model.Solicitante unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new app.profuturo.reclasificacion.com.model.Solicitante();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (app.profuturo.reclasificacion.com.model.Solicitante) cachedObject.object;
            }
            unmanagedObject = (app.profuturo.reclasificacion.com.model.Solicitante) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        SolicitanteRealmProxyInterface unmanagedCopy = (SolicitanteRealmProxyInterface) unmanagedObject;
        SolicitanteRealmProxyInterface realmSource = (SolicitanteRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$apMaternoCte(realmSource.realmGet$apMaternoCte());
        unmanagedCopy.realmSet$apPaternoCte(realmSource.realmGet$apPaternoCte());
        unmanagedCopy.realmSet$celular(realmSource.realmGet$celular());
        unmanagedCopy.realmSet$correoElec(realmSource.realmGet$correoElec());
        unmanagedCopy.realmSet$curp(realmSource.realmGet$curp());
        unmanagedCopy.realmSet$nombreCte(realmSource.realmGet$nombreCte());
        unmanagedCopy.realmSet$nss(realmSource.realmGet$nss());
        unmanagedCopy.realmSet$numCtaInvdual(realmSource.realmGet$numCtaInvdual());
        unmanagedCopy.realmSet$rfcCte(realmSource.realmGet$rfcCte());
        unmanagedCopy.realmSet$usuCre(realmSource.realmGet$usuCre());
        unmanagedCopy.realmSet$calle(realmSource.realmGet$calle());
        unmanagedCopy.realmSet$codigoPostal(realmSource.realmGet$codigoPostal());
        unmanagedCopy.realmSet$colonia(realmSource.realmGet$colonia());
        unmanagedCopy.realmSet$estado(realmSource.realmGet$estado());
        unmanagedCopy.realmSet$municipioDelegacion(realmSource.realmGet$municipioDelegacion());
        unmanagedCopy.realmSet$numeroExterior(realmSource.realmGet$numeroExterior());
        unmanagedCopy.realmSet$numeroInterior(realmSource.realmGet$numeroInterior());
        unmanagedCopy.realmSet$telefono(realmSource.realmGet$telefono());

        return unmanagedObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Solicitante = proxy[");
        stringBuilder.append("{apMaternoCte:");
        stringBuilder.append(realmGet$apMaternoCte() != null ? realmGet$apMaternoCte() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{apPaternoCte:");
        stringBuilder.append(realmGet$apPaternoCte() != null ? realmGet$apPaternoCte() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{celular:");
        stringBuilder.append(realmGet$celular() != null ? realmGet$celular() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{correoElec:");
        stringBuilder.append(realmGet$correoElec() != null ? realmGet$correoElec() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{curp:");
        stringBuilder.append(realmGet$curp() != null ? realmGet$curp() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{nombreCte:");
        stringBuilder.append(realmGet$nombreCte() != null ? realmGet$nombreCte() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{nss:");
        stringBuilder.append(realmGet$nss() != null ? realmGet$nss() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{numCtaInvdual:");
        stringBuilder.append(realmGet$numCtaInvdual() != null ? realmGet$numCtaInvdual() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{rfcCte:");
        stringBuilder.append(realmGet$rfcCte() != null ? realmGet$rfcCte() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{usuCre:");
        stringBuilder.append(realmGet$usuCre() != null ? realmGet$usuCre() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{calle:");
        stringBuilder.append(realmGet$calle() != null ? realmGet$calle() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{codigoPostal:");
        stringBuilder.append(realmGet$codigoPostal() != null ? realmGet$codigoPostal() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{colonia:");
        stringBuilder.append(realmGet$colonia() != null ? realmGet$colonia() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{estado:");
        stringBuilder.append(realmGet$estado() != null ? realmGet$estado() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{municipioDelegacion:");
        stringBuilder.append(realmGet$municipioDelegacion() != null ? realmGet$municipioDelegacion() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{numeroExterior:");
        stringBuilder.append(realmGet$numeroExterior() != null ? realmGet$numeroExterior() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{numeroInterior:");
        stringBuilder.append(realmGet$numeroInterior() != null ? realmGet$numeroInterior() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{telefono:");
        stringBuilder.append(realmGet$telefono() != null ? realmGet$telefono() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SolicitanteRealmProxy aSolicitante = (SolicitanteRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aSolicitante.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aSolicitante.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aSolicitante.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
