package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.ProxyUtils;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class SaldosOriginalesRealmProxy extends app.profuturo.reclasificacion.com.model.SaldosOriginales
    implements RealmObjectProxy, SaldosOriginalesRealmProxyInterface {

    static final class SaldosOriginalesColumnInfo extends ColumnInfo {
        long saldoIndex;

        SaldosOriginalesColumnInfo(OsSchemaInfo schemaInfo) {
            super(1);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("SaldosOriginales");
            this.saldoIndex = addColumnDetails("saldo", objectSchemaInfo);
        }

        SaldosOriginalesColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new SaldosOriginalesColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final SaldosOriginalesColumnInfo src = (SaldosOriginalesColumnInfo) rawSrc;
            final SaldosOriginalesColumnInfo dst = (SaldosOriginalesColumnInfo) rawDst;
            dst.saldoIndex = src.saldoIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>(1);
        fieldNames.add("saldo");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    private SaldosOriginalesColumnInfo columnInfo;
    private ProxyState<app.profuturo.reclasificacion.com.model.SaldosOriginales> proxyState;

    SaldosOriginalesRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (SaldosOriginalesColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<app.profuturo.reclasificacion.com.model.SaldosOriginales>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    public app.profuturo.reclasificacion.com.model.Saldo realmGet$saldo() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNullLink(columnInfo.saldoIndex)) {
            return null;
        }
        return proxyState.getRealm$realm().get(app.profuturo.reclasificacion.com.model.Saldo.class, proxyState.getRow$realm().getLink(columnInfo.saldoIndex), false, Collections.<String>emptyList());
    }

    @Override
    public void realmSet$saldo(app.profuturo.reclasificacion.com.model.Saldo value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("saldo")) {
                return;
            }
            if (value != null && !RealmObject.isManaged(value)) {
                value = ((Realm) proxyState.getRealm$realm()).copyToRealm(value);
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                // Table#nullifyLink() does not support default value. Just using Row.
                row.nullifyLink(columnInfo.saldoIndex);
                return;
            }
            proxyState.checkValidObject(value);
            row.getTable().setLink(columnInfo.saldoIndex, row.getIndex(), ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex(), true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().nullifyLink(columnInfo.saldoIndex);
            return;
        }
        proxyState.checkValidObject(value);
        proxyState.getRow$realm().setLink(columnInfo.saldoIndex, ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex());
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("SaldosOriginales", 1, 0);
        builder.addPersistedLinkProperty("saldo", RealmFieldType.OBJECT, "Saldo");
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static SaldosOriginalesColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new SaldosOriginalesColumnInfo(schemaInfo);
    }

    public static String getTableName() {
        return "class_SaldosOriginales";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static app.profuturo.reclasificacion.com.model.SaldosOriginales createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = new ArrayList<String>(1);
        if (json.has("saldo")) {
            excludeFields.add("saldo");
        }
        app.profuturo.reclasificacion.com.model.SaldosOriginales obj = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.SaldosOriginales.class, true, excludeFields);

        final SaldosOriginalesRealmProxyInterface objProxy = (SaldosOriginalesRealmProxyInterface) obj;
        if (json.has("saldo")) {
            if (json.isNull("saldo")) {
                objProxy.realmSet$saldo(null);
            } else {
                app.profuturo.reclasificacion.com.model.Saldo saldoObj = SaldoRealmProxy.createOrUpdateUsingJsonObject(realm, json.getJSONObject("saldo"), update);
                objProxy.realmSet$saldo(saldoObj);
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static app.profuturo.reclasificacion.com.model.SaldosOriginales createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        final app.profuturo.reclasificacion.com.model.SaldosOriginales obj = new app.profuturo.reclasificacion.com.model.SaldosOriginales();
        final SaldosOriginalesRealmProxyInterface objProxy = (SaldosOriginalesRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("saldo")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$saldo(null);
                } else {
                    app.profuturo.reclasificacion.com.model.Saldo saldoObj = SaldoRealmProxy.createUsingJsonStream(realm, reader);
                    objProxy.realmSet$saldo(saldoObj);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return realm.copyToRealm(obj);
    }

    public static app.profuturo.reclasificacion.com.model.SaldosOriginales copyOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.SaldosOriginales object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.SaldosOriginales) cachedRealmObject;
        }

        return copy(realm, object, update, cache);
    }

    public static app.profuturo.reclasificacion.com.model.SaldosOriginales copy(Realm realm, app.profuturo.reclasificacion.com.model.SaldosOriginales newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.SaldosOriginales) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        app.profuturo.reclasificacion.com.model.SaldosOriginales realmObject = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.SaldosOriginales.class, false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        SaldosOriginalesRealmProxyInterface realmObjectSource = (SaldosOriginalesRealmProxyInterface) newObject;
        SaldosOriginalesRealmProxyInterface realmObjectCopy = (SaldosOriginalesRealmProxyInterface) realmObject;


        app.profuturo.reclasificacion.com.model.Saldo saldoObj = realmObjectSource.realmGet$saldo();
        if (saldoObj == null) {
            realmObjectCopy.realmSet$saldo(null);
        } else {
            app.profuturo.reclasificacion.com.model.Saldo cachesaldo = (app.profuturo.reclasificacion.com.model.Saldo) cache.get(saldoObj);
            if (cachesaldo != null) {
                realmObjectCopy.realmSet$saldo(cachesaldo);
            } else {
                realmObjectCopy.realmSet$saldo(SaldoRealmProxy.copyOrUpdate(realm, saldoObj, update, cache));
            }
        }
        return realmObject;
    }

    public static long insert(Realm realm, app.profuturo.reclasificacion.com.model.SaldosOriginales object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SaldosOriginales.class);
        long tableNativePtr = table.getNativePtr();
        SaldosOriginalesColumnInfo columnInfo = (SaldosOriginalesColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SaldosOriginales.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);

        app.profuturo.reclasificacion.com.model.Saldo saldoObj = ((SaldosOriginalesRealmProxyInterface) object).realmGet$saldo();
        if (saldoObj != null) {
            Long cachesaldo = cache.get(saldoObj);
            if (cachesaldo == null) {
                cachesaldo = SaldoRealmProxy.insert(realm, saldoObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.saldoIndex, rowIndex, cachesaldo, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SaldosOriginales.class);
        long tableNativePtr = table.getNativePtr();
        SaldosOriginalesColumnInfo columnInfo = (SaldosOriginalesColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SaldosOriginales.class);
        app.profuturo.reclasificacion.com.model.SaldosOriginales object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.SaldosOriginales) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);

            app.profuturo.reclasificacion.com.model.Saldo saldoObj = ((SaldosOriginalesRealmProxyInterface) object).realmGet$saldo();
            if (saldoObj != null) {
                Long cachesaldo = cache.get(saldoObj);
                if (cachesaldo == null) {
                    cachesaldo = SaldoRealmProxy.insert(realm, saldoObj, cache);
                }
                table.setLink(columnInfo.saldoIndex, rowIndex, cachesaldo, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.SaldosOriginales object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SaldosOriginales.class);
        long tableNativePtr = table.getNativePtr();
        SaldosOriginalesColumnInfo columnInfo = (SaldosOriginalesColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SaldosOriginales.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);

        app.profuturo.reclasificacion.com.model.Saldo saldoObj = ((SaldosOriginalesRealmProxyInterface) object).realmGet$saldo();
        if (saldoObj != null) {
            Long cachesaldo = cache.get(saldoObj);
            if (cachesaldo == null) {
                cachesaldo = SaldoRealmProxy.insertOrUpdate(realm, saldoObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.saldoIndex, rowIndex, cachesaldo, false);
        } else {
            Table.nativeNullifyLink(tableNativePtr, columnInfo.saldoIndex, rowIndex);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SaldosOriginales.class);
        long tableNativePtr = table.getNativePtr();
        SaldosOriginalesColumnInfo columnInfo = (SaldosOriginalesColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SaldosOriginales.class);
        app.profuturo.reclasificacion.com.model.SaldosOriginales object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.SaldosOriginales) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);

            app.profuturo.reclasificacion.com.model.Saldo saldoObj = ((SaldosOriginalesRealmProxyInterface) object).realmGet$saldo();
            if (saldoObj != null) {
                Long cachesaldo = cache.get(saldoObj);
                if (cachesaldo == null) {
                    cachesaldo = SaldoRealmProxy.insertOrUpdate(realm, saldoObj, cache);
                }
                Table.nativeSetLink(tableNativePtr, columnInfo.saldoIndex, rowIndex, cachesaldo, false);
            } else {
                Table.nativeNullifyLink(tableNativePtr, columnInfo.saldoIndex, rowIndex);
            }
        }
    }

    public static app.profuturo.reclasificacion.com.model.SaldosOriginales createDetachedCopy(app.profuturo.reclasificacion.com.model.SaldosOriginales realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        app.profuturo.reclasificacion.com.model.SaldosOriginales unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new app.profuturo.reclasificacion.com.model.SaldosOriginales();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (app.profuturo.reclasificacion.com.model.SaldosOriginales) cachedObject.object;
            }
            unmanagedObject = (app.profuturo.reclasificacion.com.model.SaldosOriginales) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        SaldosOriginalesRealmProxyInterface unmanagedCopy = (SaldosOriginalesRealmProxyInterface) unmanagedObject;
        SaldosOriginalesRealmProxyInterface realmSource = (SaldosOriginalesRealmProxyInterface) realmObject;

        // Deep copy of saldo
        unmanagedCopy.realmSet$saldo(SaldoRealmProxy.createDetachedCopy(realmSource.realmGet$saldo(), currentDepth + 1, maxDepth, cache));

        return unmanagedObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("SaldosOriginales = proxy[");
        stringBuilder.append("{saldo:");
        stringBuilder.append(realmGet$saldo() != null ? "Saldo" : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaldosOriginalesRealmProxy aSaldosOriginales = (SaldosOriginalesRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aSaldosOriginales.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aSaldosOriginales.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aSaldosOriginales.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
