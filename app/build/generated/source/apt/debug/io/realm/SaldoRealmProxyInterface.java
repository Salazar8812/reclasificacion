package io.realm;


public interface SaldoRealmProxyInterface {
    public String realmGet$acciones();
    public void realmSet$acciones(String value);
    public String realmGet$idSiefore();
    public void realmSet$idSiefore(String value);
    public String realmGet$idSubcta();
    public void realmSet$idSubcta(String value);
    public String realmGet$idTipoMov();
    public void realmSet$idTipoMov(String value);
    public String realmGet$idValorAccion();
    public void realmSet$idValorAccion(String value);
    public String realmGet$pesos();
    public void realmSet$pesos(String value);
    public String realmGet$porcentaje();
    public void realmSet$porcentaje(String value);
}
