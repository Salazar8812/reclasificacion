package io.realm;


public interface ListaIndicadoreRealmProxyInterface {
    public String realmGet$id();
    public void realmSet$id(String value);
    public String realmGet$descripcion();
    public void realmSet$descripcion(String value);
}
