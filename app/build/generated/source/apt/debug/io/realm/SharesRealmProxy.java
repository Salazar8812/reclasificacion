package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.ProxyUtils;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class SharesRealmProxy extends app.profuturo.reclasificacion.com.model.Shares
    implements RealmObjectProxy, SharesRealmProxyInterface {

    static final class SharesColumnInfo extends ColumnInfo {
        long sharesCVIndex;
        long sharesCSIndex;
        long sharesRIndex;
        long sharesRetiro92Index;
        long sharesVivienda92Index;
        long sharesVivienda97Index;

        SharesColumnInfo(OsSchemaInfo schemaInfo) {
            super(6);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Shares");
            this.sharesCVIndex = addColumnDetails("sharesCV", objectSchemaInfo);
            this.sharesCSIndex = addColumnDetails("sharesCS", objectSchemaInfo);
            this.sharesRIndex = addColumnDetails("sharesR", objectSchemaInfo);
            this.sharesRetiro92Index = addColumnDetails("sharesRetiro92", objectSchemaInfo);
            this.sharesVivienda92Index = addColumnDetails("sharesVivienda92", objectSchemaInfo);
            this.sharesVivienda97Index = addColumnDetails("sharesVivienda97", objectSchemaInfo);
        }

        SharesColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new SharesColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final SharesColumnInfo src = (SharesColumnInfo) rawSrc;
            final SharesColumnInfo dst = (SharesColumnInfo) rawDst;
            dst.sharesCVIndex = src.sharesCVIndex;
            dst.sharesCSIndex = src.sharesCSIndex;
            dst.sharesRIndex = src.sharesRIndex;
            dst.sharesRetiro92Index = src.sharesRetiro92Index;
            dst.sharesVivienda92Index = src.sharesVivienda92Index;
            dst.sharesVivienda97Index = src.sharesVivienda97Index;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>(6);
        fieldNames.add("sharesCV");
        fieldNames.add("sharesCS");
        fieldNames.add("sharesR");
        fieldNames.add("sharesRetiro92");
        fieldNames.add("sharesVivienda92");
        fieldNames.add("sharesVivienda97");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    private SharesColumnInfo columnInfo;
    private ProxyState<app.profuturo.reclasificacion.com.model.Shares> proxyState;

    SharesRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (SharesColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<app.profuturo.reclasificacion.com.model.Shares>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public Integer realmGet$sharesCV() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.sharesCVIndex)) {
            return null;
        }
        return (int) proxyState.getRow$realm().getLong(columnInfo.sharesCVIndex);
    }

    @Override
    public void realmSet$sharesCV(Integer value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.sharesCVIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setLong(columnInfo.sharesCVIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.sharesCVIndex);
            return;
        }
        proxyState.getRow$realm().setLong(columnInfo.sharesCVIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Integer realmGet$sharesCS() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.sharesCSIndex)) {
            return null;
        }
        return (int) proxyState.getRow$realm().getLong(columnInfo.sharesCSIndex);
    }

    @Override
    public void realmSet$sharesCS(Integer value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.sharesCSIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setLong(columnInfo.sharesCSIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.sharesCSIndex);
            return;
        }
        proxyState.getRow$realm().setLong(columnInfo.sharesCSIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Integer realmGet$sharesR() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.sharesRIndex)) {
            return null;
        }
        return (int) proxyState.getRow$realm().getLong(columnInfo.sharesRIndex);
    }

    @Override
    public void realmSet$sharesR(Integer value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.sharesRIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setLong(columnInfo.sharesRIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.sharesRIndex);
            return;
        }
        proxyState.getRow$realm().setLong(columnInfo.sharesRIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Integer realmGet$sharesRetiro92() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.sharesRetiro92Index)) {
            return null;
        }
        return (int) proxyState.getRow$realm().getLong(columnInfo.sharesRetiro92Index);
    }

    @Override
    public void realmSet$sharesRetiro92(Integer value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.sharesRetiro92Index, row.getIndex(), true);
                return;
            }
            row.getTable().setLong(columnInfo.sharesRetiro92Index, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.sharesRetiro92Index);
            return;
        }
        proxyState.getRow$realm().setLong(columnInfo.sharesRetiro92Index, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Integer realmGet$sharesVivienda92() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.sharesVivienda92Index)) {
            return null;
        }
        return (int) proxyState.getRow$realm().getLong(columnInfo.sharesVivienda92Index);
    }

    @Override
    public void realmSet$sharesVivienda92(Integer value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.sharesVivienda92Index, row.getIndex(), true);
                return;
            }
            row.getTable().setLong(columnInfo.sharesVivienda92Index, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.sharesVivienda92Index);
            return;
        }
        proxyState.getRow$realm().setLong(columnInfo.sharesVivienda92Index, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Integer realmGet$sharesVivienda97() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.sharesVivienda97Index)) {
            return null;
        }
        return (int) proxyState.getRow$realm().getLong(columnInfo.sharesVivienda97Index);
    }

    @Override
    public void realmSet$sharesVivienda97(Integer value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.sharesVivienda97Index, row.getIndex(), true);
                return;
            }
            row.getTable().setLong(columnInfo.sharesVivienda97Index, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.sharesVivienda97Index);
            return;
        }
        proxyState.getRow$realm().setLong(columnInfo.sharesVivienda97Index, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Shares", 6, 0);
        builder.addPersistedProperty("sharesCV", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("sharesCS", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("sharesR", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("sharesRetiro92", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("sharesVivienda92", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("sharesVivienda97", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static SharesColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new SharesColumnInfo(schemaInfo);
    }

    public static String getTableName() {
        return "class_Shares";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static app.profuturo.reclasificacion.com.model.Shares createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        app.profuturo.reclasificacion.com.model.Shares obj = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.Shares.class, true, excludeFields);

        final SharesRealmProxyInterface objProxy = (SharesRealmProxyInterface) obj;
        if (json.has("sharesCV")) {
            if (json.isNull("sharesCV")) {
                objProxy.realmSet$sharesCV(null);
            } else {
                objProxy.realmSet$sharesCV((int) json.getInt("sharesCV"));
            }
        }
        if (json.has("sharesCS")) {
            if (json.isNull("sharesCS")) {
                objProxy.realmSet$sharesCS(null);
            } else {
                objProxy.realmSet$sharesCS((int) json.getInt("sharesCS"));
            }
        }
        if (json.has("sharesR")) {
            if (json.isNull("sharesR")) {
                objProxy.realmSet$sharesR(null);
            } else {
                objProxy.realmSet$sharesR((int) json.getInt("sharesR"));
            }
        }
        if (json.has("sharesRetiro92")) {
            if (json.isNull("sharesRetiro92")) {
                objProxy.realmSet$sharesRetiro92(null);
            } else {
                objProxy.realmSet$sharesRetiro92((int) json.getInt("sharesRetiro92"));
            }
        }
        if (json.has("sharesVivienda92")) {
            if (json.isNull("sharesVivienda92")) {
                objProxy.realmSet$sharesVivienda92(null);
            } else {
                objProxy.realmSet$sharesVivienda92((int) json.getInt("sharesVivienda92"));
            }
        }
        if (json.has("sharesVivienda97")) {
            if (json.isNull("sharesVivienda97")) {
                objProxy.realmSet$sharesVivienda97(null);
            } else {
                objProxy.realmSet$sharesVivienda97((int) json.getInt("sharesVivienda97"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static app.profuturo.reclasificacion.com.model.Shares createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        final app.profuturo.reclasificacion.com.model.Shares obj = new app.profuturo.reclasificacion.com.model.Shares();
        final SharesRealmProxyInterface objProxy = (SharesRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("sharesCV")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$sharesCV((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$sharesCV(null);
                }
            } else if (name.equals("sharesCS")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$sharesCS((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$sharesCS(null);
                }
            } else if (name.equals("sharesR")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$sharesR((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$sharesR(null);
                }
            } else if (name.equals("sharesRetiro92")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$sharesRetiro92((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$sharesRetiro92(null);
                }
            } else if (name.equals("sharesVivienda92")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$sharesVivienda92((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$sharesVivienda92(null);
                }
            } else if (name.equals("sharesVivienda97")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$sharesVivienda97((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$sharesVivienda97(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return realm.copyToRealm(obj);
    }

    public static app.profuturo.reclasificacion.com.model.Shares copyOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.Shares object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.Shares) cachedRealmObject;
        }

        return copy(realm, object, update, cache);
    }

    public static app.profuturo.reclasificacion.com.model.Shares copy(Realm realm, app.profuturo.reclasificacion.com.model.Shares newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.Shares) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        app.profuturo.reclasificacion.com.model.Shares realmObject = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.Shares.class, false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        SharesRealmProxyInterface realmObjectSource = (SharesRealmProxyInterface) newObject;
        SharesRealmProxyInterface realmObjectCopy = (SharesRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$sharesCV(realmObjectSource.realmGet$sharesCV());
        realmObjectCopy.realmSet$sharesCS(realmObjectSource.realmGet$sharesCS());
        realmObjectCopy.realmSet$sharesR(realmObjectSource.realmGet$sharesR());
        realmObjectCopy.realmSet$sharesRetiro92(realmObjectSource.realmGet$sharesRetiro92());
        realmObjectCopy.realmSet$sharesVivienda92(realmObjectSource.realmGet$sharesVivienda92());
        realmObjectCopy.realmSet$sharesVivienda97(realmObjectSource.realmGet$sharesVivienda97());
        return realmObject;
    }

    public static long insert(Realm realm, app.profuturo.reclasificacion.com.model.Shares object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Shares.class);
        long tableNativePtr = table.getNativePtr();
        SharesColumnInfo columnInfo = (SharesColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Shares.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        Number realmGet$sharesCV = ((SharesRealmProxyInterface) object).realmGet$sharesCV();
        if (realmGet$sharesCV != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.sharesCVIndex, rowIndex, realmGet$sharesCV.longValue(), false);
        }
        Number realmGet$sharesCS = ((SharesRealmProxyInterface) object).realmGet$sharesCS();
        if (realmGet$sharesCS != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.sharesCSIndex, rowIndex, realmGet$sharesCS.longValue(), false);
        }
        Number realmGet$sharesR = ((SharesRealmProxyInterface) object).realmGet$sharesR();
        if (realmGet$sharesR != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.sharesRIndex, rowIndex, realmGet$sharesR.longValue(), false);
        }
        Number realmGet$sharesRetiro92 = ((SharesRealmProxyInterface) object).realmGet$sharesRetiro92();
        if (realmGet$sharesRetiro92 != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.sharesRetiro92Index, rowIndex, realmGet$sharesRetiro92.longValue(), false);
        }
        Number realmGet$sharesVivienda92 = ((SharesRealmProxyInterface) object).realmGet$sharesVivienda92();
        if (realmGet$sharesVivienda92 != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.sharesVivienda92Index, rowIndex, realmGet$sharesVivienda92.longValue(), false);
        }
        Number realmGet$sharesVivienda97 = ((SharesRealmProxyInterface) object).realmGet$sharesVivienda97();
        if (realmGet$sharesVivienda97 != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.sharesVivienda97Index, rowIndex, realmGet$sharesVivienda97.longValue(), false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Shares.class);
        long tableNativePtr = table.getNativePtr();
        SharesColumnInfo columnInfo = (SharesColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Shares.class);
        app.profuturo.reclasificacion.com.model.Shares object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.Shares) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            Number realmGet$sharesCV = ((SharesRealmProxyInterface) object).realmGet$sharesCV();
            if (realmGet$sharesCV != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.sharesCVIndex, rowIndex, realmGet$sharesCV.longValue(), false);
            }
            Number realmGet$sharesCS = ((SharesRealmProxyInterface) object).realmGet$sharesCS();
            if (realmGet$sharesCS != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.sharesCSIndex, rowIndex, realmGet$sharesCS.longValue(), false);
            }
            Number realmGet$sharesR = ((SharesRealmProxyInterface) object).realmGet$sharesR();
            if (realmGet$sharesR != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.sharesRIndex, rowIndex, realmGet$sharesR.longValue(), false);
            }
            Number realmGet$sharesRetiro92 = ((SharesRealmProxyInterface) object).realmGet$sharesRetiro92();
            if (realmGet$sharesRetiro92 != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.sharesRetiro92Index, rowIndex, realmGet$sharesRetiro92.longValue(), false);
            }
            Number realmGet$sharesVivienda92 = ((SharesRealmProxyInterface) object).realmGet$sharesVivienda92();
            if (realmGet$sharesVivienda92 != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.sharesVivienda92Index, rowIndex, realmGet$sharesVivienda92.longValue(), false);
            }
            Number realmGet$sharesVivienda97 = ((SharesRealmProxyInterface) object).realmGet$sharesVivienda97();
            if (realmGet$sharesVivienda97 != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.sharesVivienda97Index, rowIndex, realmGet$sharesVivienda97.longValue(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.Shares object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Shares.class);
        long tableNativePtr = table.getNativePtr();
        SharesColumnInfo columnInfo = (SharesColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Shares.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        Number realmGet$sharesCV = ((SharesRealmProxyInterface) object).realmGet$sharesCV();
        if (realmGet$sharesCV != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.sharesCVIndex, rowIndex, realmGet$sharesCV.longValue(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.sharesCVIndex, rowIndex, false);
        }
        Number realmGet$sharesCS = ((SharesRealmProxyInterface) object).realmGet$sharesCS();
        if (realmGet$sharesCS != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.sharesCSIndex, rowIndex, realmGet$sharesCS.longValue(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.sharesCSIndex, rowIndex, false);
        }
        Number realmGet$sharesR = ((SharesRealmProxyInterface) object).realmGet$sharesR();
        if (realmGet$sharesR != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.sharesRIndex, rowIndex, realmGet$sharesR.longValue(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.sharesRIndex, rowIndex, false);
        }
        Number realmGet$sharesRetiro92 = ((SharesRealmProxyInterface) object).realmGet$sharesRetiro92();
        if (realmGet$sharesRetiro92 != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.sharesRetiro92Index, rowIndex, realmGet$sharesRetiro92.longValue(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.sharesRetiro92Index, rowIndex, false);
        }
        Number realmGet$sharesVivienda92 = ((SharesRealmProxyInterface) object).realmGet$sharesVivienda92();
        if (realmGet$sharesVivienda92 != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.sharesVivienda92Index, rowIndex, realmGet$sharesVivienda92.longValue(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.sharesVivienda92Index, rowIndex, false);
        }
        Number realmGet$sharesVivienda97 = ((SharesRealmProxyInterface) object).realmGet$sharesVivienda97();
        if (realmGet$sharesVivienda97 != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.sharesVivienda97Index, rowIndex, realmGet$sharesVivienda97.longValue(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.sharesVivienda97Index, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Shares.class);
        long tableNativePtr = table.getNativePtr();
        SharesColumnInfo columnInfo = (SharesColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Shares.class);
        app.profuturo.reclasificacion.com.model.Shares object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.Shares) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            Number realmGet$sharesCV = ((SharesRealmProxyInterface) object).realmGet$sharesCV();
            if (realmGet$sharesCV != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.sharesCVIndex, rowIndex, realmGet$sharesCV.longValue(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.sharesCVIndex, rowIndex, false);
            }
            Number realmGet$sharesCS = ((SharesRealmProxyInterface) object).realmGet$sharesCS();
            if (realmGet$sharesCS != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.sharesCSIndex, rowIndex, realmGet$sharesCS.longValue(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.sharesCSIndex, rowIndex, false);
            }
            Number realmGet$sharesR = ((SharesRealmProxyInterface) object).realmGet$sharesR();
            if (realmGet$sharesR != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.sharesRIndex, rowIndex, realmGet$sharesR.longValue(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.sharesRIndex, rowIndex, false);
            }
            Number realmGet$sharesRetiro92 = ((SharesRealmProxyInterface) object).realmGet$sharesRetiro92();
            if (realmGet$sharesRetiro92 != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.sharesRetiro92Index, rowIndex, realmGet$sharesRetiro92.longValue(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.sharesRetiro92Index, rowIndex, false);
            }
            Number realmGet$sharesVivienda92 = ((SharesRealmProxyInterface) object).realmGet$sharesVivienda92();
            if (realmGet$sharesVivienda92 != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.sharesVivienda92Index, rowIndex, realmGet$sharesVivienda92.longValue(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.sharesVivienda92Index, rowIndex, false);
            }
            Number realmGet$sharesVivienda97 = ((SharesRealmProxyInterface) object).realmGet$sharesVivienda97();
            if (realmGet$sharesVivienda97 != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.sharesVivienda97Index, rowIndex, realmGet$sharesVivienda97.longValue(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.sharesVivienda97Index, rowIndex, false);
            }
        }
    }

    public static app.profuturo.reclasificacion.com.model.Shares createDetachedCopy(app.profuturo.reclasificacion.com.model.Shares realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        app.profuturo.reclasificacion.com.model.Shares unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new app.profuturo.reclasificacion.com.model.Shares();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (app.profuturo.reclasificacion.com.model.Shares) cachedObject.object;
            }
            unmanagedObject = (app.profuturo.reclasificacion.com.model.Shares) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        SharesRealmProxyInterface unmanagedCopy = (SharesRealmProxyInterface) unmanagedObject;
        SharesRealmProxyInterface realmSource = (SharesRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$sharesCV(realmSource.realmGet$sharesCV());
        unmanagedCopy.realmSet$sharesCS(realmSource.realmGet$sharesCS());
        unmanagedCopy.realmSet$sharesR(realmSource.realmGet$sharesR());
        unmanagedCopy.realmSet$sharesRetiro92(realmSource.realmGet$sharesRetiro92());
        unmanagedCopy.realmSet$sharesVivienda92(realmSource.realmGet$sharesVivienda92());
        unmanagedCopy.realmSet$sharesVivienda97(realmSource.realmGet$sharesVivienda97());

        return unmanagedObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Shares = proxy[");
        stringBuilder.append("{sharesCV:");
        stringBuilder.append(realmGet$sharesCV() != null ? realmGet$sharesCV() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{sharesCS:");
        stringBuilder.append(realmGet$sharesCS() != null ? realmGet$sharesCS() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{sharesR:");
        stringBuilder.append(realmGet$sharesR() != null ? realmGet$sharesR() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{sharesRetiro92:");
        stringBuilder.append(realmGet$sharesRetiro92() != null ? realmGet$sharesRetiro92() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{sharesVivienda92:");
        stringBuilder.append(realmGet$sharesVivienda92() != null ? realmGet$sharesVivienda92() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{sharesVivienda97:");
        stringBuilder.append(realmGet$sharesVivienda97() != null ? realmGet$sharesVivienda97() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SharesRealmProxy aShares = (SharesRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aShares.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aShares.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aShares.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
