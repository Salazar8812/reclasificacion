package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.ProxyUtils;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class ListaIndicadoreRealmProxy extends app.profuturo.reclasificacion.com.model.ListaIndicadore
    implements RealmObjectProxy, ListaIndicadoreRealmProxyInterface {

    static final class ListaIndicadoreColumnInfo extends ColumnInfo {
        long idIndex;
        long descripcionIndex;

        ListaIndicadoreColumnInfo(OsSchemaInfo schemaInfo) {
            super(2);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("ListaIndicadore");
            this.idIndex = addColumnDetails("id", objectSchemaInfo);
            this.descripcionIndex = addColumnDetails("descripcion", objectSchemaInfo);
        }

        ListaIndicadoreColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new ListaIndicadoreColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final ListaIndicadoreColumnInfo src = (ListaIndicadoreColumnInfo) rawSrc;
            final ListaIndicadoreColumnInfo dst = (ListaIndicadoreColumnInfo) rawDst;
            dst.idIndex = src.idIndex;
            dst.descripcionIndex = src.descripcionIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>(2);
        fieldNames.add("id");
        fieldNames.add("descripcion");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    private ListaIndicadoreColumnInfo columnInfo;
    private ProxyState<app.profuturo.reclasificacion.com.model.ListaIndicadore> proxyState;

    ListaIndicadoreRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (ListaIndicadoreColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<app.profuturo.reclasificacion.com.model.ListaIndicadore>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$id() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idIndex);
    }

    @Override
    public void realmSet$id(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$descripcion() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.descripcionIndex);
    }

    @Override
    public void realmSet$descripcion(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.descripcionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.descripcionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.descripcionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.descripcionIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("ListaIndicadore", 2, 0);
        builder.addPersistedProperty("id", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("descripcion", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static ListaIndicadoreColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new ListaIndicadoreColumnInfo(schemaInfo);
    }

    public static String getTableName() {
        return "class_ListaIndicadore";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static app.profuturo.reclasificacion.com.model.ListaIndicadore createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        app.profuturo.reclasificacion.com.model.ListaIndicadore obj = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.ListaIndicadore.class, true, excludeFields);

        final ListaIndicadoreRealmProxyInterface objProxy = (ListaIndicadoreRealmProxyInterface) obj;
        if (json.has("id")) {
            if (json.isNull("id")) {
                objProxy.realmSet$id(null);
            } else {
                objProxy.realmSet$id((String) json.getString("id"));
            }
        }
        if (json.has("descripcion")) {
            if (json.isNull("descripcion")) {
                objProxy.realmSet$descripcion(null);
            } else {
                objProxy.realmSet$descripcion((String) json.getString("descripcion"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static app.profuturo.reclasificacion.com.model.ListaIndicadore createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        final app.profuturo.reclasificacion.com.model.ListaIndicadore obj = new app.profuturo.reclasificacion.com.model.ListaIndicadore();
        final ListaIndicadoreRealmProxyInterface objProxy = (ListaIndicadoreRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$id((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$id(null);
                }
            } else if (name.equals("descripcion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$descripcion((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$descripcion(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return realm.copyToRealm(obj);
    }

    public static app.profuturo.reclasificacion.com.model.ListaIndicadore copyOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.ListaIndicadore object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.ListaIndicadore) cachedRealmObject;
        }

        return copy(realm, object, update, cache);
    }

    public static app.profuturo.reclasificacion.com.model.ListaIndicadore copy(Realm realm, app.profuturo.reclasificacion.com.model.ListaIndicadore newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.ListaIndicadore) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        app.profuturo.reclasificacion.com.model.ListaIndicadore realmObject = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.ListaIndicadore.class, false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        ListaIndicadoreRealmProxyInterface realmObjectSource = (ListaIndicadoreRealmProxyInterface) newObject;
        ListaIndicadoreRealmProxyInterface realmObjectCopy = (ListaIndicadoreRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$id(realmObjectSource.realmGet$id());
        realmObjectCopy.realmSet$descripcion(realmObjectSource.realmGet$descripcion());
        return realmObject;
    }

    public static long insert(Realm realm, app.profuturo.reclasificacion.com.model.ListaIndicadore object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.ListaIndicadore.class);
        long tableNativePtr = table.getNativePtr();
        ListaIndicadoreColumnInfo columnInfo = (ListaIndicadoreColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.ListaIndicadore.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        String realmGet$id = ((ListaIndicadoreRealmProxyInterface) object).realmGet$id();
        if (realmGet$id != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idIndex, rowIndex, realmGet$id, false);
        }
        String realmGet$descripcion = ((ListaIndicadoreRealmProxyInterface) object).realmGet$descripcion();
        if (realmGet$descripcion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.descripcionIndex, rowIndex, realmGet$descripcion, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.ListaIndicadore.class);
        long tableNativePtr = table.getNativePtr();
        ListaIndicadoreColumnInfo columnInfo = (ListaIndicadoreColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.ListaIndicadore.class);
        app.profuturo.reclasificacion.com.model.ListaIndicadore object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.ListaIndicadore) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            String realmGet$id = ((ListaIndicadoreRealmProxyInterface) object).realmGet$id();
            if (realmGet$id != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idIndex, rowIndex, realmGet$id, false);
            }
            String realmGet$descripcion = ((ListaIndicadoreRealmProxyInterface) object).realmGet$descripcion();
            if (realmGet$descripcion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.descripcionIndex, rowIndex, realmGet$descripcion, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.ListaIndicadore object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.ListaIndicadore.class);
        long tableNativePtr = table.getNativePtr();
        ListaIndicadoreColumnInfo columnInfo = (ListaIndicadoreColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.ListaIndicadore.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        String realmGet$id = ((ListaIndicadoreRealmProxyInterface) object).realmGet$id();
        if (realmGet$id != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idIndex, rowIndex, realmGet$id, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idIndex, rowIndex, false);
        }
        String realmGet$descripcion = ((ListaIndicadoreRealmProxyInterface) object).realmGet$descripcion();
        if (realmGet$descripcion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.descripcionIndex, rowIndex, realmGet$descripcion, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.descripcionIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.ListaIndicadore.class);
        long tableNativePtr = table.getNativePtr();
        ListaIndicadoreColumnInfo columnInfo = (ListaIndicadoreColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.ListaIndicadore.class);
        app.profuturo.reclasificacion.com.model.ListaIndicadore object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.ListaIndicadore) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            String realmGet$id = ((ListaIndicadoreRealmProxyInterface) object).realmGet$id();
            if (realmGet$id != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idIndex, rowIndex, realmGet$id, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idIndex, rowIndex, false);
            }
            String realmGet$descripcion = ((ListaIndicadoreRealmProxyInterface) object).realmGet$descripcion();
            if (realmGet$descripcion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.descripcionIndex, rowIndex, realmGet$descripcion, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.descripcionIndex, rowIndex, false);
            }
        }
    }

    public static app.profuturo.reclasificacion.com.model.ListaIndicadore createDetachedCopy(app.profuturo.reclasificacion.com.model.ListaIndicadore realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        app.profuturo.reclasificacion.com.model.ListaIndicadore unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new app.profuturo.reclasificacion.com.model.ListaIndicadore();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (app.profuturo.reclasificacion.com.model.ListaIndicadore) cachedObject.object;
            }
            unmanagedObject = (app.profuturo.reclasificacion.com.model.ListaIndicadore) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        ListaIndicadoreRealmProxyInterface unmanagedCopy = (ListaIndicadoreRealmProxyInterface) unmanagedObject;
        ListaIndicadoreRealmProxyInterface realmSource = (ListaIndicadoreRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$id(realmSource.realmGet$id());
        unmanagedCopy.realmSet$descripcion(realmSource.realmGet$descripcion());

        return unmanagedObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("ListaIndicadore = proxy[");
        stringBuilder.append("{id:");
        stringBuilder.append(realmGet$id() != null ? realmGet$id() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{descripcion:");
        stringBuilder.append(realmGet$descripcion() != null ? realmGet$descripcion() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListaIndicadoreRealmProxy aListaIndicadore = (ListaIndicadoreRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aListaIndicadore.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aListaIndicadore.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aListaIndicadore.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
