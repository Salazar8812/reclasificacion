package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.ProxyUtils;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class ClientRealmProxy extends app.profuturo.reclasificacion.com.model.Client
    implements RealmObjectProxy, ClientRealmProxyInterface {

    static final class ClientColumnInfo extends ColumnInfo {
        long accountNumberIndex;
        long nameIndex;
        long surNameIndex;
        long lastNameIndex;
        long idPersonaIndex;
        long nssIndex;
        long curpIndex;
        long rfcIndex;
        long affiliationRegimeIndex;
        long accountValidIndex;
        long affiliationOriginIndex;
        long birthdayIndex;
        long genderIndex;
        long streetIndex;
        long neighborhoodIndex;
        long zipCodeIndex;
        long stateIndex;
        long expedientIndicatorDescriptionIndex;
        long estatusIndicadorExpedienteIndex;
        long expedientIndicatorValueIndex;
        long biometricIndicatorDescriptionIndex;
        long estatusIndicadorBiometricoIndex;
        long biometricIndicatorValueIndex;
        long regimentIndicatorIdIndex;
        long estatusTipoClienteIndex;
        long fechaCertificacionIndex;
        long estatusIndex;
        long affiliationStatusIndex;
        long outerNumberIndex;
        long innerNumberIndex;
        long districtIndex;
        long facebookUserIndex;
        long twitterUserIndex;
        long expedientStatusIndex;
        long biotmetricStatusIndex;
        long savingsAccountIndex;
        long emailIndex;
        long secondaryEmailIndex;
        long landlineNumberIndex;
        long cellphoneNumberIndex;

        ClientColumnInfo(OsSchemaInfo schemaInfo) {
            super(40);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Client");
            this.accountNumberIndex = addColumnDetails("accountNumber", objectSchemaInfo);
            this.nameIndex = addColumnDetails("name", objectSchemaInfo);
            this.surNameIndex = addColumnDetails("surName", objectSchemaInfo);
            this.lastNameIndex = addColumnDetails("lastName", objectSchemaInfo);
            this.idPersonaIndex = addColumnDetails("idPersona", objectSchemaInfo);
            this.nssIndex = addColumnDetails("nss", objectSchemaInfo);
            this.curpIndex = addColumnDetails("curp", objectSchemaInfo);
            this.rfcIndex = addColumnDetails("rfc", objectSchemaInfo);
            this.affiliationRegimeIndex = addColumnDetails("affiliationRegime", objectSchemaInfo);
            this.accountValidIndex = addColumnDetails("accountValid", objectSchemaInfo);
            this.affiliationOriginIndex = addColumnDetails("affiliationOrigin", objectSchemaInfo);
            this.birthdayIndex = addColumnDetails("birthday", objectSchemaInfo);
            this.genderIndex = addColumnDetails("gender", objectSchemaInfo);
            this.streetIndex = addColumnDetails("street", objectSchemaInfo);
            this.neighborhoodIndex = addColumnDetails("neighborhood", objectSchemaInfo);
            this.zipCodeIndex = addColumnDetails("zipCode", objectSchemaInfo);
            this.stateIndex = addColumnDetails("state", objectSchemaInfo);
            this.expedientIndicatorDescriptionIndex = addColumnDetails("expedientIndicatorDescription", objectSchemaInfo);
            this.estatusIndicadorExpedienteIndex = addColumnDetails("estatusIndicadorExpediente", objectSchemaInfo);
            this.expedientIndicatorValueIndex = addColumnDetails("expedientIndicatorValue", objectSchemaInfo);
            this.biometricIndicatorDescriptionIndex = addColumnDetails("biometricIndicatorDescription", objectSchemaInfo);
            this.estatusIndicadorBiometricoIndex = addColumnDetails("estatusIndicadorBiometrico", objectSchemaInfo);
            this.biometricIndicatorValueIndex = addColumnDetails("biometricIndicatorValue", objectSchemaInfo);
            this.regimentIndicatorIdIndex = addColumnDetails("regimentIndicatorId", objectSchemaInfo);
            this.estatusTipoClienteIndex = addColumnDetails("estatusTipoCliente", objectSchemaInfo);
            this.fechaCertificacionIndex = addColumnDetails("fechaCertificacion", objectSchemaInfo);
            this.estatusIndex = addColumnDetails("estatus", objectSchemaInfo);
            this.affiliationStatusIndex = addColumnDetails("affiliationStatus", objectSchemaInfo);
            this.outerNumberIndex = addColumnDetails("outerNumber", objectSchemaInfo);
            this.innerNumberIndex = addColumnDetails("innerNumber", objectSchemaInfo);
            this.districtIndex = addColumnDetails("district", objectSchemaInfo);
            this.facebookUserIndex = addColumnDetails("facebookUser", objectSchemaInfo);
            this.twitterUserIndex = addColumnDetails("twitterUser", objectSchemaInfo);
            this.expedientStatusIndex = addColumnDetails("expedientStatus", objectSchemaInfo);
            this.biotmetricStatusIndex = addColumnDetails("biotmetricStatus", objectSchemaInfo);
            this.savingsAccountIndex = addColumnDetails("savingsAccount", objectSchemaInfo);
            this.emailIndex = addColumnDetails("email", objectSchemaInfo);
            this.secondaryEmailIndex = addColumnDetails("secondaryEmail", objectSchemaInfo);
            this.landlineNumberIndex = addColumnDetails("landlineNumber", objectSchemaInfo);
            this.cellphoneNumberIndex = addColumnDetails("cellphoneNumber", objectSchemaInfo);
        }

        ClientColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new ClientColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final ClientColumnInfo src = (ClientColumnInfo) rawSrc;
            final ClientColumnInfo dst = (ClientColumnInfo) rawDst;
            dst.accountNumberIndex = src.accountNumberIndex;
            dst.nameIndex = src.nameIndex;
            dst.surNameIndex = src.surNameIndex;
            dst.lastNameIndex = src.lastNameIndex;
            dst.idPersonaIndex = src.idPersonaIndex;
            dst.nssIndex = src.nssIndex;
            dst.curpIndex = src.curpIndex;
            dst.rfcIndex = src.rfcIndex;
            dst.affiliationRegimeIndex = src.affiliationRegimeIndex;
            dst.accountValidIndex = src.accountValidIndex;
            dst.affiliationOriginIndex = src.affiliationOriginIndex;
            dst.birthdayIndex = src.birthdayIndex;
            dst.genderIndex = src.genderIndex;
            dst.streetIndex = src.streetIndex;
            dst.neighborhoodIndex = src.neighborhoodIndex;
            dst.zipCodeIndex = src.zipCodeIndex;
            dst.stateIndex = src.stateIndex;
            dst.expedientIndicatorDescriptionIndex = src.expedientIndicatorDescriptionIndex;
            dst.estatusIndicadorExpedienteIndex = src.estatusIndicadorExpedienteIndex;
            dst.expedientIndicatorValueIndex = src.expedientIndicatorValueIndex;
            dst.biometricIndicatorDescriptionIndex = src.biometricIndicatorDescriptionIndex;
            dst.estatusIndicadorBiometricoIndex = src.estatusIndicadorBiometricoIndex;
            dst.biometricIndicatorValueIndex = src.biometricIndicatorValueIndex;
            dst.regimentIndicatorIdIndex = src.regimentIndicatorIdIndex;
            dst.estatusTipoClienteIndex = src.estatusTipoClienteIndex;
            dst.fechaCertificacionIndex = src.fechaCertificacionIndex;
            dst.estatusIndex = src.estatusIndex;
            dst.affiliationStatusIndex = src.affiliationStatusIndex;
            dst.outerNumberIndex = src.outerNumberIndex;
            dst.innerNumberIndex = src.innerNumberIndex;
            dst.districtIndex = src.districtIndex;
            dst.facebookUserIndex = src.facebookUserIndex;
            dst.twitterUserIndex = src.twitterUserIndex;
            dst.expedientStatusIndex = src.expedientStatusIndex;
            dst.biotmetricStatusIndex = src.biotmetricStatusIndex;
            dst.savingsAccountIndex = src.savingsAccountIndex;
            dst.emailIndex = src.emailIndex;
            dst.secondaryEmailIndex = src.secondaryEmailIndex;
            dst.landlineNumberIndex = src.landlineNumberIndex;
            dst.cellphoneNumberIndex = src.cellphoneNumberIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>(40);
        fieldNames.add("accountNumber");
        fieldNames.add("name");
        fieldNames.add("surName");
        fieldNames.add("lastName");
        fieldNames.add("idPersona");
        fieldNames.add("nss");
        fieldNames.add("curp");
        fieldNames.add("rfc");
        fieldNames.add("affiliationRegime");
        fieldNames.add("accountValid");
        fieldNames.add("affiliationOrigin");
        fieldNames.add("birthday");
        fieldNames.add("gender");
        fieldNames.add("street");
        fieldNames.add("neighborhood");
        fieldNames.add("zipCode");
        fieldNames.add("state");
        fieldNames.add("expedientIndicatorDescription");
        fieldNames.add("estatusIndicadorExpediente");
        fieldNames.add("expedientIndicatorValue");
        fieldNames.add("biometricIndicatorDescription");
        fieldNames.add("estatusIndicadorBiometrico");
        fieldNames.add("biometricIndicatorValue");
        fieldNames.add("regimentIndicatorId");
        fieldNames.add("estatusTipoCliente");
        fieldNames.add("fechaCertificacion");
        fieldNames.add("estatus");
        fieldNames.add("affiliationStatus");
        fieldNames.add("outerNumber");
        fieldNames.add("innerNumber");
        fieldNames.add("district");
        fieldNames.add("facebookUser");
        fieldNames.add("twitterUser");
        fieldNames.add("expedientStatus");
        fieldNames.add("biotmetricStatus");
        fieldNames.add("savingsAccount");
        fieldNames.add("email");
        fieldNames.add("secondaryEmail");
        fieldNames.add("landlineNumber");
        fieldNames.add("cellphoneNumber");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    private ClientColumnInfo columnInfo;
    private ProxyState<app.profuturo.reclasificacion.com.model.Client> proxyState;

    ClientRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (ClientColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<app.profuturo.reclasificacion.com.model.Client>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$accountNumber() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.accountNumberIndex);
    }

    @Override
    public void realmSet$accountNumber(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'accountNumber' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$name() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.nameIndex);
    }

    @Override
    public void realmSet$name(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.nameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.nameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.nameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.nameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$surName() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.surNameIndex);
    }

    @Override
    public void realmSet$surName(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.surNameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.surNameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.surNameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.surNameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$lastName() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.lastNameIndex);
    }

    @Override
    public void realmSet$lastName(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.lastNameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.lastNameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.lastNameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.lastNameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idPersona() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idPersonaIndex);
    }

    @Override
    public void realmSet$idPersona(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idPersonaIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idPersonaIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idPersonaIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idPersonaIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$nss() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.nssIndex);
    }

    @Override
    public void realmSet$nss(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.nssIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.nssIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.nssIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.nssIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$curp() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.curpIndex);
    }

    @Override
    public void realmSet$curp(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.curpIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.curpIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.curpIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.curpIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$rfc() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.rfcIndex);
    }

    @Override
    public void realmSet$rfc(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.rfcIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.rfcIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.rfcIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.rfcIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$affiliationRegime() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.affiliationRegimeIndex);
    }

    @Override
    public void realmSet$affiliationRegime(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.affiliationRegimeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.affiliationRegimeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.affiliationRegimeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.affiliationRegimeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$accountValid() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.accountValidIndex);
    }

    @Override
    public void realmSet$accountValid(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.accountValidIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.accountValidIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.accountValidIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.accountValidIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$affiliationOrigin() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.affiliationOriginIndex);
    }

    @Override
    public void realmSet$affiliationOrigin(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.affiliationOriginIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.affiliationOriginIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.affiliationOriginIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.affiliationOriginIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$birthday() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.birthdayIndex);
    }

    @Override
    public void realmSet$birthday(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.birthdayIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.birthdayIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.birthdayIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.birthdayIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$gender() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.genderIndex);
    }

    @Override
    public void realmSet$gender(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.genderIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.genderIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.genderIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.genderIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$street() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.streetIndex);
    }

    @Override
    public void realmSet$street(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.streetIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.streetIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.streetIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.streetIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$neighborhood() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.neighborhoodIndex);
    }

    @Override
    public void realmSet$neighborhood(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.neighborhoodIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.neighborhoodIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.neighborhoodIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.neighborhoodIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$zipCode() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.zipCodeIndex);
    }

    @Override
    public void realmSet$zipCode(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.zipCodeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.zipCodeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.zipCodeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.zipCodeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$state() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.stateIndex);
    }

    @Override
    public void realmSet$state(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.stateIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.stateIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.stateIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.stateIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$expedientIndicatorDescription() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.expedientIndicatorDescriptionIndex);
    }

    @Override
    public void realmSet$expedientIndicatorDescription(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.expedientIndicatorDescriptionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.expedientIndicatorDescriptionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.expedientIndicatorDescriptionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.expedientIndicatorDescriptionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$estatusIndicadorExpediente() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.estatusIndicadorExpedienteIndex);
    }

    @Override
    public void realmSet$estatusIndicadorExpediente(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.estatusIndicadorExpedienteIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.estatusIndicadorExpedienteIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.estatusIndicadorExpedienteIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.estatusIndicadorExpedienteIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$expedientIndicatorValue() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.expedientIndicatorValueIndex);
    }

    @Override
    public void realmSet$expedientIndicatorValue(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.expedientIndicatorValueIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.expedientIndicatorValueIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$biometricIndicatorDescription() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.biometricIndicatorDescriptionIndex);
    }

    @Override
    public void realmSet$biometricIndicatorDescription(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.biometricIndicatorDescriptionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.biometricIndicatorDescriptionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.biometricIndicatorDescriptionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.biometricIndicatorDescriptionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$estatusIndicadorBiometrico() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.estatusIndicadorBiometricoIndex);
    }

    @Override
    public void realmSet$estatusIndicadorBiometrico(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.estatusIndicadorBiometricoIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.estatusIndicadorBiometricoIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.estatusIndicadorBiometricoIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.estatusIndicadorBiometricoIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$biometricIndicatorValue() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.biometricIndicatorValueIndex);
    }

    @Override
    public void realmSet$biometricIndicatorValue(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.biometricIndicatorValueIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.biometricIndicatorValueIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$regimentIndicatorId() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.regimentIndicatorIdIndex);
    }

    @Override
    public void realmSet$regimentIndicatorId(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.regimentIndicatorIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.regimentIndicatorIdIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$estatusTipoCliente() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.estatusTipoClienteIndex);
    }

    @Override
    public void realmSet$estatusTipoCliente(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.estatusTipoClienteIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.estatusTipoClienteIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.estatusTipoClienteIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.estatusTipoClienteIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$fechaCertificacion() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.fechaCertificacionIndex);
    }

    @Override
    public void realmSet$fechaCertificacion(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.fechaCertificacionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.fechaCertificacionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.fechaCertificacionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.fechaCertificacionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$estatus() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.estatusIndex);
    }

    @Override
    public void realmSet$estatus(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.estatusIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.estatusIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.estatusIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.estatusIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$affiliationStatus() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.affiliationStatusIndex);
    }

    @Override
    public void realmSet$affiliationStatus(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.affiliationStatusIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.affiliationStatusIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.affiliationStatusIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.affiliationStatusIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$outerNumber() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.outerNumberIndex);
    }

    @Override
    public void realmSet$outerNumber(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.outerNumberIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.outerNumberIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.outerNumberIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.outerNumberIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$innerNumber() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.innerNumberIndex);
    }

    @Override
    public void realmSet$innerNumber(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.innerNumberIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.innerNumberIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.innerNumberIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.innerNumberIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$district() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.districtIndex);
    }

    @Override
    public void realmSet$district(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.districtIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.districtIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.districtIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.districtIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$facebookUser() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.facebookUserIndex);
    }

    @Override
    public void realmSet$facebookUser(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.facebookUserIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.facebookUserIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.facebookUserIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.facebookUserIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$twitterUser() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.twitterUserIndex);
    }

    @Override
    public void realmSet$twitterUser(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.twitterUserIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.twitterUserIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.twitterUserIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.twitterUserIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$expedientStatus() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.expedientStatusIndex);
    }

    @Override
    public void realmSet$expedientStatus(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.expedientStatusIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.expedientStatusIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.expedientStatusIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.expedientStatusIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$biotmetricStatus() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.biotmetricStatusIndex);
    }

    @Override
    public void realmSet$biotmetricStatus(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.biotmetricStatusIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.biotmetricStatusIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.biotmetricStatusIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.biotmetricStatusIndex, value);
    }

    @Override
    public app.profuturo.reclasificacion.com.model.SavingsAccount realmGet$savingsAccount() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNullLink(columnInfo.savingsAccountIndex)) {
            return null;
        }
        return proxyState.getRealm$realm().get(app.profuturo.reclasificacion.com.model.SavingsAccount.class, proxyState.getRow$realm().getLink(columnInfo.savingsAccountIndex), false, Collections.<String>emptyList());
    }

    @Override
    public void realmSet$savingsAccount(app.profuturo.reclasificacion.com.model.SavingsAccount value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("savingsAccount")) {
                return;
            }
            if (value != null && !RealmObject.isManaged(value)) {
                value = ((Realm) proxyState.getRealm$realm()).copyToRealm(value);
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                // Table#nullifyLink() does not support default value. Just using Row.
                row.nullifyLink(columnInfo.savingsAccountIndex);
                return;
            }
            proxyState.checkValidObject(value);
            row.getTable().setLink(columnInfo.savingsAccountIndex, row.getIndex(), ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex(), true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().nullifyLink(columnInfo.savingsAccountIndex);
            return;
        }
        proxyState.checkValidObject(value);
        proxyState.getRow$realm().setLink(columnInfo.savingsAccountIndex, ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$email() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.emailIndex);
    }

    @Override
    public void realmSet$email(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.emailIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.emailIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.emailIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.emailIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$secondaryEmail() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.secondaryEmailIndex);
    }

    @Override
    public void realmSet$secondaryEmail(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.secondaryEmailIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.secondaryEmailIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.secondaryEmailIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.secondaryEmailIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$landlineNumber() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.landlineNumberIndex);
    }

    @Override
    public void realmSet$landlineNumber(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.landlineNumberIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.landlineNumberIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.landlineNumberIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.landlineNumberIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$cellphoneNumber() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.cellphoneNumberIndex);
    }

    @Override
    public void realmSet$cellphoneNumber(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.cellphoneNumberIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.cellphoneNumberIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.cellphoneNumberIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.cellphoneNumberIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Client", 40, 0);
        builder.addPersistedProperty("accountNumber", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("name", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("surName", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("lastName", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idPersona", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("nss", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("curp", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("rfc", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("affiliationRegime", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("accountValid", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("affiliationOrigin", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("birthday", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("gender", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("street", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("neighborhood", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("zipCode", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("state", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("expedientIndicatorDescription", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("estatusIndicadorExpediente", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("expedientIndicatorValue", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("biometricIndicatorDescription", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("estatusIndicadorBiometrico", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("biometricIndicatorValue", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("regimentIndicatorId", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("estatusTipoCliente", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("fechaCertificacion", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("estatus", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("affiliationStatus", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("outerNumber", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("innerNumber", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("district", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("facebookUser", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("twitterUser", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("expedientStatus", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("biotmetricStatus", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedLinkProperty("savingsAccount", RealmFieldType.OBJECT, "SavingsAccount");
        builder.addPersistedProperty("email", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("secondaryEmail", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("landlineNumber", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("cellphoneNumber", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static ClientColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new ClientColumnInfo(schemaInfo);
    }

    public static String getTableName() {
        return "class_Client";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static app.profuturo.reclasificacion.com.model.Client createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = new ArrayList<String>(1);
        app.profuturo.reclasificacion.com.model.Client obj = null;
        if (update) {
            Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Client.class);
            ClientColumnInfo columnInfo = (ClientColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Client.class);
            long pkColumnIndex = columnInfo.accountNumberIndex;
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("accountNumber")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("accountNumber"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Client.class), false, Collections.<String> emptyList());
                    obj = new io.realm.ClientRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("savingsAccount")) {
                excludeFields.add("savingsAccount");
            }
            if (json.has("accountNumber")) {
                if (json.isNull("accountNumber")) {
                    obj = (io.realm.ClientRealmProxy) realm.createObjectInternal(app.profuturo.reclasificacion.com.model.Client.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.ClientRealmProxy) realm.createObjectInternal(app.profuturo.reclasificacion.com.model.Client.class, json.getString("accountNumber"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'accountNumber'.");
            }
        }

        final ClientRealmProxyInterface objProxy = (ClientRealmProxyInterface) obj;
        if (json.has("name")) {
            if (json.isNull("name")) {
                objProxy.realmSet$name(null);
            } else {
                objProxy.realmSet$name((String) json.getString("name"));
            }
        }
        if (json.has("surName")) {
            if (json.isNull("surName")) {
                objProxy.realmSet$surName(null);
            } else {
                objProxy.realmSet$surName((String) json.getString("surName"));
            }
        }
        if (json.has("lastName")) {
            if (json.isNull("lastName")) {
                objProxy.realmSet$lastName(null);
            } else {
                objProxy.realmSet$lastName((String) json.getString("lastName"));
            }
        }
        if (json.has("idPersona")) {
            if (json.isNull("idPersona")) {
                objProxy.realmSet$idPersona(null);
            } else {
                objProxy.realmSet$idPersona((String) json.getString("idPersona"));
            }
        }
        if (json.has("nss")) {
            if (json.isNull("nss")) {
                objProxy.realmSet$nss(null);
            } else {
                objProxy.realmSet$nss((String) json.getString("nss"));
            }
        }
        if (json.has("curp")) {
            if (json.isNull("curp")) {
                objProxy.realmSet$curp(null);
            } else {
                objProxy.realmSet$curp((String) json.getString("curp"));
            }
        }
        if (json.has("rfc")) {
            if (json.isNull("rfc")) {
                objProxy.realmSet$rfc(null);
            } else {
                objProxy.realmSet$rfc((String) json.getString("rfc"));
            }
        }
        if (json.has("affiliationRegime")) {
            if (json.isNull("affiliationRegime")) {
                objProxy.realmSet$affiliationRegime(null);
            } else {
                objProxy.realmSet$affiliationRegime((String) json.getString("affiliationRegime"));
            }
        }
        if (json.has("accountValid")) {
            if (json.isNull("accountValid")) {
                objProxy.realmSet$accountValid(null);
            } else {
                objProxy.realmSet$accountValid((String) json.getString("accountValid"));
            }
        }
        if (json.has("affiliationOrigin")) {
            if (json.isNull("affiliationOrigin")) {
                objProxy.realmSet$affiliationOrigin(null);
            } else {
                objProxy.realmSet$affiliationOrigin((String) json.getString("affiliationOrigin"));
            }
        }
        if (json.has("birthday")) {
            if (json.isNull("birthday")) {
                objProxy.realmSet$birthday(null);
            } else {
                objProxy.realmSet$birthday((String) json.getString("birthday"));
            }
        }
        if (json.has("gender")) {
            if (json.isNull("gender")) {
                objProxy.realmSet$gender(null);
            } else {
                objProxy.realmSet$gender((String) json.getString("gender"));
            }
        }
        if (json.has("street")) {
            if (json.isNull("street")) {
                objProxy.realmSet$street(null);
            } else {
                objProxy.realmSet$street((String) json.getString("street"));
            }
        }
        if (json.has("neighborhood")) {
            if (json.isNull("neighborhood")) {
                objProxy.realmSet$neighborhood(null);
            } else {
                objProxy.realmSet$neighborhood((String) json.getString("neighborhood"));
            }
        }
        if (json.has("zipCode")) {
            if (json.isNull("zipCode")) {
                objProxy.realmSet$zipCode(null);
            } else {
                objProxy.realmSet$zipCode((String) json.getString("zipCode"));
            }
        }
        if (json.has("state")) {
            if (json.isNull("state")) {
                objProxy.realmSet$state(null);
            } else {
                objProxy.realmSet$state((String) json.getString("state"));
            }
        }
        if (json.has("expedientIndicatorDescription")) {
            if (json.isNull("expedientIndicatorDescription")) {
                objProxy.realmSet$expedientIndicatorDescription(null);
            } else {
                objProxy.realmSet$expedientIndicatorDescription((String) json.getString("expedientIndicatorDescription"));
            }
        }
        if (json.has("estatusIndicadorExpediente")) {
            if (json.isNull("estatusIndicadorExpediente")) {
                objProxy.realmSet$estatusIndicadorExpediente(null);
            } else {
                objProxy.realmSet$estatusIndicadorExpediente((String) json.getString("estatusIndicadorExpediente"));
            }
        }
        if (json.has("expedientIndicatorValue")) {
            if (json.isNull("expedientIndicatorValue")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'expedientIndicatorValue' to null.");
            } else {
                objProxy.realmSet$expedientIndicatorValue((int) json.getInt("expedientIndicatorValue"));
            }
        }
        if (json.has("biometricIndicatorDescription")) {
            if (json.isNull("biometricIndicatorDescription")) {
                objProxy.realmSet$biometricIndicatorDescription(null);
            } else {
                objProxy.realmSet$biometricIndicatorDescription((String) json.getString("biometricIndicatorDescription"));
            }
        }
        if (json.has("estatusIndicadorBiometrico")) {
            if (json.isNull("estatusIndicadorBiometrico")) {
                objProxy.realmSet$estatusIndicadorBiometrico(null);
            } else {
                objProxy.realmSet$estatusIndicadorBiometrico((String) json.getString("estatusIndicadorBiometrico"));
            }
        }
        if (json.has("biometricIndicatorValue")) {
            if (json.isNull("biometricIndicatorValue")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'biometricIndicatorValue' to null.");
            } else {
                objProxy.realmSet$biometricIndicatorValue((int) json.getInt("biometricIndicatorValue"));
            }
        }
        if (json.has("regimentIndicatorId")) {
            if (json.isNull("regimentIndicatorId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'regimentIndicatorId' to null.");
            } else {
                objProxy.realmSet$regimentIndicatorId((int) json.getInt("regimentIndicatorId"));
            }
        }
        if (json.has("estatusTipoCliente")) {
            if (json.isNull("estatusTipoCliente")) {
                objProxy.realmSet$estatusTipoCliente(null);
            } else {
                objProxy.realmSet$estatusTipoCliente((String) json.getString("estatusTipoCliente"));
            }
        }
        if (json.has("fechaCertificacion")) {
            if (json.isNull("fechaCertificacion")) {
                objProxy.realmSet$fechaCertificacion(null);
            } else {
                objProxy.realmSet$fechaCertificacion((String) json.getString("fechaCertificacion"));
            }
        }
        if (json.has("estatus")) {
            if (json.isNull("estatus")) {
                objProxy.realmSet$estatus(null);
            } else {
                objProxy.realmSet$estatus((String) json.getString("estatus"));
            }
        }
        if (json.has("affiliationStatus")) {
            if (json.isNull("affiliationStatus")) {
                objProxy.realmSet$affiliationStatus(null);
            } else {
                objProxy.realmSet$affiliationStatus((String) json.getString("affiliationStatus"));
            }
        }
        if (json.has("outerNumber")) {
            if (json.isNull("outerNumber")) {
                objProxy.realmSet$outerNumber(null);
            } else {
                objProxy.realmSet$outerNumber((String) json.getString("outerNumber"));
            }
        }
        if (json.has("innerNumber")) {
            if (json.isNull("innerNumber")) {
                objProxy.realmSet$innerNumber(null);
            } else {
                objProxy.realmSet$innerNumber((String) json.getString("innerNumber"));
            }
        }
        if (json.has("district")) {
            if (json.isNull("district")) {
                objProxy.realmSet$district(null);
            } else {
                objProxy.realmSet$district((String) json.getString("district"));
            }
        }
        if (json.has("facebookUser")) {
            if (json.isNull("facebookUser")) {
                objProxy.realmSet$facebookUser(null);
            } else {
                objProxy.realmSet$facebookUser((String) json.getString("facebookUser"));
            }
        }
        if (json.has("twitterUser")) {
            if (json.isNull("twitterUser")) {
                objProxy.realmSet$twitterUser(null);
            } else {
                objProxy.realmSet$twitterUser((String) json.getString("twitterUser"));
            }
        }
        if (json.has("expedientStatus")) {
            if (json.isNull("expedientStatus")) {
                objProxy.realmSet$expedientStatus(null);
            } else {
                objProxy.realmSet$expedientStatus((String) json.getString("expedientStatus"));
            }
        }
        if (json.has("biotmetricStatus")) {
            if (json.isNull("biotmetricStatus")) {
                objProxy.realmSet$biotmetricStatus(null);
            } else {
                objProxy.realmSet$biotmetricStatus((String) json.getString("biotmetricStatus"));
            }
        }
        if (json.has("savingsAccount")) {
            if (json.isNull("savingsAccount")) {
                objProxy.realmSet$savingsAccount(null);
            } else {
                app.profuturo.reclasificacion.com.model.SavingsAccount savingsAccountObj = SavingsAccountRealmProxy.createOrUpdateUsingJsonObject(realm, json.getJSONObject("savingsAccount"), update);
                objProxy.realmSet$savingsAccount(savingsAccountObj);
            }
        }
        if (json.has("email")) {
            if (json.isNull("email")) {
                objProxy.realmSet$email(null);
            } else {
                objProxy.realmSet$email((String) json.getString("email"));
            }
        }
        if (json.has("secondaryEmail")) {
            if (json.isNull("secondaryEmail")) {
                objProxy.realmSet$secondaryEmail(null);
            } else {
                objProxy.realmSet$secondaryEmail((String) json.getString("secondaryEmail"));
            }
        }
        if (json.has("landlineNumber")) {
            if (json.isNull("landlineNumber")) {
                objProxy.realmSet$landlineNumber(null);
            } else {
                objProxy.realmSet$landlineNumber((String) json.getString("landlineNumber"));
            }
        }
        if (json.has("cellphoneNumber")) {
            if (json.isNull("cellphoneNumber")) {
                objProxy.realmSet$cellphoneNumber(null);
            } else {
                objProxy.realmSet$cellphoneNumber((String) json.getString("cellphoneNumber"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static app.profuturo.reclasificacion.com.model.Client createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final app.profuturo.reclasificacion.com.model.Client obj = new app.profuturo.reclasificacion.com.model.Client();
        final ClientRealmProxyInterface objProxy = (ClientRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("accountNumber")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$accountNumber((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$accountNumber(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("name")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$name((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$name(null);
                }
            } else if (name.equals("surName")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$surName((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$surName(null);
                }
            } else if (name.equals("lastName")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$lastName((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$lastName(null);
                }
            } else if (name.equals("idPersona")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idPersona((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idPersona(null);
                }
            } else if (name.equals("nss")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$nss((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$nss(null);
                }
            } else if (name.equals("curp")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$curp((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$curp(null);
                }
            } else if (name.equals("rfc")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$rfc((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$rfc(null);
                }
            } else if (name.equals("affiliationRegime")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$affiliationRegime((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$affiliationRegime(null);
                }
            } else if (name.equals("accountValid")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$accountValid((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$accountValid(null);
                }
            } else if (name.equals("affiliationOrigin")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$affiliationOrigin((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$affiliationOrigin(null);
                }
            } else if (name.equals("birthday")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$birthday((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$birthday(null);
                }
            } else if (name.equals("gender")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$gender((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$gender(null);
                }
            } else if (name.equals("street")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$street((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$street(null);
                }
            } else if (name.equals("neighborhood")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$neighborhood((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$neighborhood(null);
                }
            } else if (name.equals("zipCode")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$zipCode((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$zipCode(null);
                }
            } else if (name.equals("state")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$state((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$state(null);
                }
            } else if (name.equals("expedientIndicatorDescription")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$expedientIndicatorDescription((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$expedientIndicatorDescription(null);
                }
            } else if (name.equals("estatusIndicadorExpediente")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$estatusIndicadorExpediente((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$estatusIndicadorExpediente(null);
                }
            } else if (name.equals("expedientIndicatorValue")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$expedientIndicatorValue((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'expedientIndicatorValue' to null.");
                }
            } else if (name.equals("biometricIndicatorDescription")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$biometricIndicatorDescription((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$biometricIndicatorDescription(null);
                }
            } else if (name.equals("estatusIndicadorBiometrico")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$estatusIndicadorBiometrico((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$estatusIndicadorBiometrico(null);
                }
            } else if (name.equals("biometricIndicatorValue")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$biometricIndicatorValue((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'biometricIndicatorValue' to null.");
                }
            } else if (name.equals("regimentIndicatorId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$regimentIndicatorId((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'regimentIndicatorId' to null.");
                }
            } else if (name.equals("estatusTipoCliente")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$estatusTipoCliente((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$estatusTipoCliente(null);
                }
            } else if (name.equals("fechaCertificacion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$fechaCertificacion((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$fechaCertificacion(null);
                }
            } else if (name.equals("estatus")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$estatus((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$estatus(null);
                }
            } else if (name.equals("affiliationStatus")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$affiliationStatus((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$affiliationStatus(null);
                }
            } else if (name.equals("outerNumber")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$outerNumber((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$outerNumber(null);
                }
            } else if (name.equals("innerNumber")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$innerNumber((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$innerNumber(null);
                }
            } else if (name.equals("district")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$district((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$district(null);
                }
            } else if (name.equals("facebookUser")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$facebookUser((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$facebookUser(null);
                }
            } else if (name.equals("twitterUser")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$twitterUser((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$twitterUser(null);
                }
            } else if (name.equals("expedientStatus")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$expedientStatus((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$expedientStatus(null);
                }
            } else if (name.equals("biotmetricStatus")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$biotmetricStatus((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$biotmetricStatus(null);
                }
            } else if (name.equals("savingsAccount")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$savingsAccount(null);
                } else {
                    app.profuturo.reclasificacion.com.model.SavingsAccount savingsAccountObj = SavingsAccountRealmProxy.createUsingJsonStream(realm, reader);
                    objProxy.realmSet$savingsAccount(savingsAccountObj);
                }
            } else if (name.equals("email")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$email((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$email(null);
                }
            } else if (name.equals("secondaryEmail")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$secondaryEmail((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$secondaryEmail(null);
                }
            } else if (name.equals("landlineNumber")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$landlineNumber((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$landlineNumber(null);
                }
            } else if (name.equals("cellphoneNumber")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$cellphoneNumber((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$cellphoneNumber(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'accountNumber'.");
        }
        return realm.copyToRealm(obj);
    }

    public static app.profuturo.reclasificacion.com.model.Client copyOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.Client object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.Client) cachedRealmObject;
        }

        app.profuturo.reclasificacion.com.model.Client realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Client.class);
            ClientColumnInfo columnInfo = (ClientColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Client.class);
            long pkColumnIndex = columnInfo.accountNumberIndex;
            String value = ((ClientRealmProxyInterface) object).realmGet$accountNumber();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, value);
            }
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Client.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.ClientRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static app.profuturo.reclasificacion.com.model.Client copy(Realm realm, app.profuturo.reclasificacion.com.model.Client newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.Client) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        app.profuturo.reclasificacion.com.model.Client realmObject = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.Client.class, ((ClientRealmProxyInterface) newObject).realmGet$accountNumber(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        ClientRealmProxyInterface realmObjectSource = (ClientRealmProxyInterface) newObject;
        ClientRealmProxyInterface realmObjectCopy = (ClientRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$name(realmObjectSource.realmGet$name());
        realmObjectCopy.realmSet$surName(realmObjectSource.realmGet$surName());
        realmObjectCopy.realmSet$lastName(realmObjectSource.realmGet$lastName());
        realmObjectCopy.realmSet$idPersona(realmObjectSource.realmGet$idPersona());
        realmObjectCopy.realmSet$nss(realmObjectSource.realmGet$nss());
        realmObjectCopy.realmSet$curp(realmObjectSource.realmGet$curp());
        realmObjectCopy.realmSet$rfc(realmObjectSource.realmGet$rfc());
        realmObjectCopy.realmSet$affiliationRegime(realmObjectSource.realmGet$affiliationRegime());
        realmObjectCopy.realmSet$accountValid(realmObjectSource.realmGet$accountValid());
        realmObjectCopy.realmSet$affiliationOrigin(realmObjectSource.realmGet$affiliationOrigin());
        realmObjectCopy.realmSet$birthday(realmObjectSource.realmGet$birthday());
        realmObjectCopy.realmSet$gender(realmObjectSource.realmGet$gender());
        realmObjectCopy.realmSet$street(realmObjectSource.realmGet$street());
        realmObjectCopy.realmSet$neighborhood(realmObjectSource.realmGet$neighborhood());
        realmObjectCopy.realmSet$zipCode(realmObjectSource.realmGet$zipCode());
        realmObjectCopy.realmSet$state(realmObjectSource.realmGet$state());
        realmObjectCopy.realmSet$expedientIndicatorDescription(realmObjectSource.realmGet$expedientIndicatorDescription());
        realmObjectCopy.realmSet$estatusIndicadorExpediente(realmObjectSource.realmGet$estatusIndicadorExpediente());
        realmObjectCopy.realmSet$expedientIndicatorValue(realmObjectSource.realmGet$expedientIndicatorValue());
        realmObjectCopy.realmSet$biometricIndicatorDescription(realmObjectSource.realmGet$biometricIndicatorDescription());
        realmObjectCopy.realmSet$estatusIndicadorBiometrico(realmObjectSource.realmGet$estatusIndicadorBiometrico());
        realmObjectCopy.realmSet$biometricIndicatorValue(realmObjectSource.realmGet$biometricIndicatorValue());
        realmObjectCopy.realmSet$regimentIndicatorId(realmObjectSource.realmGet$regimentIndicatorId());
        realmObjectCopy.realmSet$estatusTipoCliente(realmObjectSource.realmGet$estatusTipoCliente());
        realmObjectCopy.realmSet$fechaCertificacion(realmObjectSource.realmGet$fechaCertificacion());
        realmObjectCopy.realmSet$estatus(realmObjectSource.realmGet$estatus());
        realmObjectCopy.realmSet$affiliationStatus(realmObjectSource.realmGet$affiliationStatus());
        realmObjectCopy.realmSet$outerNumber(realmObjectSource.realmGet$outerNumber());
        realmObjectCopy.realmSet$innerNumber(realmObjectSource.realmGet$innerNumber());
        realmObjectCopy.realmSet$district(realmObjectSource.realmGet$district());
        realmObjectCopy.realmSet$facebookUser(realmObjectSource.realmGet$facebookUser());
        realmObjectCopy.realmSet$twitterUser(realmObjectSource.realmGet$twitterUser());
        realmObjectCopy.realmSet$expedientStatus(realmObjectSource.realmGet$expedientStatus());
        realmObjectCopy.realmSet$biotmetricStatus(realmObjectSource.realmGet$biotmetricStatus());

        app.profuturo.reclasificacion.com.model.SavingsAccount savingsAccountObj = realmObjectSource.realmGet$savingsAccount();
        if (savingsAccountObj == null) {
            realmObjectCopy.realmSet$savingsAccount(null);
        } else {
            app.profuturo.reclasificacion.com.model.SavingsAccount cachesavingsAccount = (app.profuturo.reclasificacion.com.model.SavingsAccount) cache.get(savingsAccountObj);
            if (cachesavingsAccount != null) {
                realmObjectCopy.realmSet$savingsAccount(cachesavingsAccount);
            } else {
                realmObjectCopy.realmSet$savingsAccount(SavingsAccountRealmProxy.copyOrUpdate(realm, savingsAccountObj, update, cache));
            }
        }
        realmObjectCopy.realmSet$email(realmObjectSource.realmGet$email());
        realmObjectCopy.realmSet$secondaryEmail(realmObjectSource.realmGet$secondaryEmail());
        realmObjectCopy.realmSet$landlineNumber(realmObjectSource.realmGet$landlineNumber());
        realmObjectCopy.realmSet$cellphoneNumber(realmObjectSource.realmGet$cellphoneNumber());
        return realmObject;
    }

    public static long insert(Realm realm, app.profuturo.reclasificacion.com.model.Client object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Client.class);
        long tableNativePtr = table.getNativePtr();
        ClientColumnInfo columnInfo = (ClientColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Client.class);
        long pkColumnIndex = columnInfo.accountNumberIndex;
        String primaryKeyValue = ((ClientRealmProxyInterface) object).realmGet$accountNumber();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$name = ((ClientRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
        }
        String realmGet$surName = ((ClientRealmProxyInterface) object).realmGet$surName();
        if (realmGet$surName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.surNameIndex, rowIndex, realmGet$surName, false);
        }
        String realmGet$lastName = ((ClientRealmProxyInterface) object).realmGet$lastName();
        if (realmGet$lastName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.lastNameIndex, rowIndex, realmGet$lastName, false);
        }
        String realmGet$idPersona = ((ClientRealmProxyInterface) object).realmGet$idPersona();
        if (realmGet$idPersona != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idPersonaIndex, rowIndex, realmGet$idPersona, false);
        }
        String realmGet$nss = ((ClientRealmProxyInterface) object).realmGet$nss();
        if (realmGet$nss != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nssIndex, rowIndex, realmGet$nss, false);
        }
        String realmGet$curp = ((ClientRealmProxyInterface) object).realmGet$curp();
        if (realmGet$curp != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.curpIndex, rowIndex, realmGet$curp, false);
        }
        String realmGet$rfc = ((ClientRealmProxyInterface) object).realmGet$rfc();
        if (realmGet$rfc != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.rfcIndex, rowIndex, realmGet$rfc, false);
        }
        String realmGet$affiliationRegime = ((ClientRealmProxyInterface) object).realmGet$affiliationRegime();
        if (realmGet$affiliationRegime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.affiliationRegimeIndex, rowIndex, realmGet$affiliationRegime, false);
        }
        String realmGet$accountValid = ((ClientRealmProxyInterface) object).realmGet$accountValid();
        if (realmGet$accountValid != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.accountValidIndex, rowIndex, realmGet$accountValid, false);
        }
        String realmGet$affiliationOrigin = ((ClientRealmProxyInterface) object).realmGet$affiliationOrigin();
        if (realmGet$affiliationOrigin != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.affiliationOriginIndex, rowIndex, realmGet$affiliationOrigin, false);
        }
        String realmGet$birthday = ((ClientRealmProxyInterface) object).realmGet$birthday();
        if (realmGet$birthday != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.birthdayIndex, rowIndex, realmGet$birthday, false);
        }
        String realmGet$gender = ((ClientRealmProxyInterface) object).realmGet$gender();
        if (realmGet$gender != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.genderIndex, rowIndex, realmGet$gender, false);
        }
        String realmGet$street = ((ClientRealmProxyInterface) object).realmGet$street();
        if (realmGet$street != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.streetIndex, rowIndex, realmGet$street, false);
        }
        String realmGet$neighborhood = ((ClientRealmProxyInterface) object).realmGet$neighborhood();
        if (realmGet$neighborhood != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.neighborhoodIndex, rowIndex, realmGet$neighborhood, false);
        }
        String realmGet$zipCode = ((ClientRealmProxyInterface) object).realmGet$zipCode();
        if (realmGet$zipCode != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.zipCodeIndex, rowIndex, realmGet$zipCode, false);
        }
        String realmGet$state = ((ClientRealmProxyInterface) object).realmGet$state();
        if (realmGet$state != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.stateIndex, rowIndex, realmGet$state, false);
        }
        String realmGet$expedientIndicatorDescription = ((ClientRealmProxyInterface) object).realmGet$expedientIndicatorDescription();
        if (realmGet$expedientIndicatorDescription != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.expedientIndicatorDescriptionIndex, rowIndex, realmGet$expedientIndicatorDescription, false);
        }
        String realmGet$estatusIndicadorExpediente = ((ClientRealmProxyInterface) object).realmGet$estatusIndicadorExpediente();
        if (realmGet$estatusIndicadorExpediente != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estatusIndicadorExpedienteIndex, rowIndex, realmGet$estatusIndicadorExpediente, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.expedientIndicatorValueIndex, rowIndex, ((ClientRealmProxyInterface) object).realmGet$expedientIndicatorValue(), false);
        String realmGet$biometricIndicatorDescription = ((ClientRealmProxyInterface) object).realmGet$biometricIndicatorDescription();
        if (realmGet$biometricIndicatorDescription != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.biometricIndicatorDescriptionIndex, rowIndex, realmGet$biometricIndicatorDescription, false);
        }
        String realmGet$estatusIndicadorBiometrico = ((ClientRealmProxyInterface) object).realmGet$estatusIndicadorBiometrico();
        if (realmGet$estatusIndicadorBiometrico != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estatusIndicadorBiometricoIndex, rowIndex, realmGet$estatusIndicadorBiometrico, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.biometricIndicatorValueIndex, rowIndex, ((ClientRealmProxyInterface) object).realmGet$biometricIndicatorValue(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.regimentIndicatorIdIndex, rowIndex, ((ClientRealmProxyInterface) object).realmGet$regimentIndicatorId(), false);
        String realmGet$estatusTipoCliente = ((ClientRealmProxyInterface) object).realmGet$estatusTipoCliente();
        if (realmGet$estatusTipoCliente != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estatusTipoClienteIndex, rowIndex, realmGet$estatusTipoCliente, false);
        }
        String realmGet$fechaCertificacion = ((ClientRealmProxyInterface) object).realmGet$fechaCertificacion();
        if (realmGet$fechaCertificacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fechaCertificacionIndex, rowIndex, realmGet$fechaCertificacion, false);
        }
        String realmGet$estatus = ((ClientRealmProxyInterface) object).realmGet$estatus();
        if (realmGet$estatus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estatusIndex, rowIndex, realmGet$estatus, false);
        }
        String realmGet$affiliationStatus = ((ClientRealmProxyInterface) object).realmGet$affiliationStatus();
        if (realmGet$affiliationStatus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.affiliationStatusIndex, rowIndex, realmGet$affiliationStatus, false);
        }
        String realmGet$outerNumber = ((ClientRealmProxyInterface) object).realmGet$outerNumber();
        if (realmGet$outerNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.outerNumberIndex, rowIndex, realmGet$outerNumber, false);
        }
        String realmGet$innerNumber = ((ClientRealmProxyInterface) object).realmGet$innerNumber();
        if (realmGet$innerNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.innerNumberIndex, rowIndex, realmGet$innerNumber, false);
        }
        String realmGet$district = ((ClientRealmProxyInterface) object).realmGet$district();
        if (realmGet$district != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.districtIndex, rowIndex, realmGet$district, false);
        }
        String realmGet$facebookUser = ((ClientRealmProxyInterface) object).realmGet$facebookUser();
        if (realmGet$facebookUser != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.facebookUserIndex, rowIndex, realmGet$facebookUser, false);
        }
        String realmGet$twitterUser = ((ClientRealmProxyInterface) object).realmGet$twitterUser();
        if (realmGet$twitterUser != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.twitterUserIndex, rowIndex, realmGet$twitterUser, false);
        }
        String realmGet$expedientStatus = ((ClientRealmProxyInterface) object).realmGet$expedientStatus();
        if (realmGet$expedientStatus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.expedientStatusIndex, rowIndex, realmGet$expedientStatus, false);
        }
        String realmGet$biotmetricStatus = ((ClientRealmProxyInterface) object).realmGet$biotmetricStatus();
        if (realmGet$biotmetricStatus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.biotmetricStatusIndex, rowIndex, realmGet$biotmetricStatus, false);
        }

        app.profuturo.reclasificacion.com.model.SavingsAccount savingsAccountObj = ((ClientRealmProxyInterface) object).realmGet$savingsAccount();
        if (savingsAccountObj != null) {
            Long cachesavingsAccount = cache.get(savingsAccountObj);
            if (cachesavingsAccount == null) {
                cachesavingsAccount = SavingsAccountRealmProxy.insert(realm, savingsAccountObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.savingsAccountIndex, rowIndex, cachesavingsAccount, false);
        }
        String realmGet$email = ((ClientRealmProxyInterface) object).realmGet$email();
        if (realmGet$email != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.emailIndex, rowIndex, realmGet$email, false);
        }
        String realmGet$secondaryEmail = ((ClientRealmProxyInterface) object).realmGet$secondaryEmail();
        if (realmGet$secondaryEmail != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.secondaryEmailIndex, rowIndex, realmGet$secondaryEmail, false);
        }
        String realmGet$landlineNumber = ((ClientRealmProxyInterface) object).realmGet$landlineNumber();
        if (realmGet$landlineNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.landlineNumberIndex, rowIndex, realmGet$landlineNumber, false);
        }
        String realmGet$cellphoneNumber = ((ClientRealmProxyInterface) object).realmGet$cellphoneNumber();
        if (realmGet$cellphoneNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.cellphoneNumberIndex, rowIndex, realmGet$cellphoneNumber, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Client.class);
        long tableNativePtr = table.getNativePtr();
        ClientColumnInfo columnInfo = (ClientColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Client.class);
        long pkColumnIndex = columnInfo.accountNumberIndex;
        app.profuturo.reclasificacion.com.model.Client object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.Client) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((ClientRealmProxyInterface) object).realmGet$accountNumber();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$name = ((ClientRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
            }
            String realmGet$surName = ((ClientRealmProxyInterface) object).realmGet$surName();
            if (realmGet$surName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.surNameIndex, rowIndex, realmGet$surName, false);
            }
            String realmGet$lastName = ((ClientRealmProxyInterface) object).realmGet$lastName();
            if (realmGet$lastName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.lastNameIndex, rowIndex, realmGet$lastName, false);
            }
            String realmGet$idPersona = ((ClientRealmProxyInterface) object).realmGet$idPersona();
            if (realmGet$idPersona != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idPersonaIndex, rowIndex, realmGet$idPersona, false);
            }
            String realmGet$nss = ((ClientRealmProxyInterface) object).realmGet$nss();
            if (realmGet$nss != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nssIndex, rowIndex, realmGet$nss, false);
            }
            String realmGet$curp = ((ClientRealmProxyInterface) object).realmGet$curp();
            if (realmGet$curp != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.curpIndex, rowIndex, realmGet$curp, false);
            }
            String realmGet$rfc = ((ClientRealmProxyInterface) object).realmGet$rfc();
            if (realmGet$rfc != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.rfcIndex, rowIndex, realmGet$rfc, false);
            }
            String realmGet$affiliationRegime = ((ClientRealmProxyInterface) object).realmGet$affiliationRegime();
            if (realmGet$affiliationRegime != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.affiliationRegimeIndex, rowIndex, realmGet$affiliationRegime, false);
            }
            String realmGet$accountValid = ((ClientRealmProxyInterface) object).realmGet$accountValid();
            if (realmGet$accountValid != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.accountValidIndex, rowIndex, realmGet$accountValid, false);
            }
            String realmGet$affiliationOrigin = ((ClientRealmProxyInterface) object).realmGet$affiliationOrigin();
            if (realmGet$affiliationOrigin != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.affiliationOriginIndex, rowIndex, realmGet$affiliationOrigin, false);
            }
            String realmGet$birthday = ((ClientRealmProxyInterface) object).realmGet$birthday();
            if (realmGet$birthday != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.birthdayIndex, rowIndex, realmGet$birthday, false);
            }
            String realmGet$gender = ((ClientRealmProxyInterface) object).realmGet$gender();
            if (realmGet$gender != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.genderIndex, rowIndex, realmGet$gender, false);
            }
            String realmGet$street = ((ClientRealmProxyInterface) object).realmGet$street();
            if (realmGet$street != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.streetIndex, rowIndex, realmGet$street, false);
            }
            String realmGet$neighborhood = ((ClientRealmProxyInterface) object).realmGet$neighborhood();
            if (realmGet$neighborhood != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.neighborhoodIndex, rowIndex, realmGet$neighborhood, false);
            }
            String realmGet$zipCode = ((ClientRealmProxyInterface) object).realmGet$zipCode();
            if (realmGet$zipCode != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.zipCodeIndex, rowIndex, realmGet$zipCode, false);
            }
            String realmGet$state = ((ClientRealmProxyInterface) object).realmGet$state();
            if (realmGet$state != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.stateIndex, rowIndex, realmGet$state, false);
            }
            String realmGet$expedientIndicatorDescription = ((ClientRealmProxyInterface) object).realmGet$expedientIndicatorDescription();
            if (realmGet$expedientIndicatorDescription != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.expedientIndicatorDescriptionIndex, rowIndex, realmGet$expedientIndicatorDescription, false);
            }
            String realmGet$estatusIndicadorExpediente = ((ClientRealmProxyInterface) object).realmGet$estatusIndicadorExpediente();
            if (realmGet$estatusIndicadorExpediente != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estatusIndicadorExpedienteIndex, rowIndex, realmGet$estatusIndicadorExpediente, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.expedientIndicatorValueIndex, rowIndex, ((ClientRealmProxyInterface) object).realmGet$expedientIndicatorValue(), false);
            String realmGet$biometricIndicatorDescription = ((ClientRealmProxyInterface) object).realmGet$biometricIndicatorDescription();
            if (realmGet$biometricIndicatorDescription != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.biometricIndicatorDescriptionIndex, rowIndex, realmGet$biometricIndicatorDescription, false);
            }
            String realmGet$estatusIndicadorBiometrico = ((ClientRealmProxyInterface) object).realmGet$estatusIndicadorBiometrico();
            if (realmGet$estatusIndicadorBiometrico != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estatusIndicadorBiometricoIndex, rowIndex, realmGet$estatusIndicadorBiometrico, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.biometricIndicatorValueIndex, rowIndex, ((ClientRealmProxyInterface) object).realmGet$biometricIndicatorValue(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.regimentIndicatorIdIndex, rowIndex, ((ClientRealmProxyInterface) object).realmGet$regimentIndicatorId(), false);
            String realmGet$estatusTipoCliente = ((ClientRealmProxyInterface) object).realmGet$estatusTipoCliente();
            if (realmGet$estatusTipoCliente != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estatusTipoClienteIndex, rowIndex, realmGet$estatusTipoCliente, false);
            }
            String realmGet$fechaCertificacion = ((ClientRealmProxyInterface) object).realmGet$fechaCertificacion();
            if (realmGet$fechaCertificacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.fechaCertificacionIndex, rowIndex, realmGet$fechaCertificacion, false);
            }
            String realmGet$estatus = ((ClientRealmProxyInterface) object).realmGet$estatus();
            if (realmGet$estatus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estatusIndex, rowIndex, realmGet$estatus, false);
            }
            String realmGet$affiliationStatus = ((ClientRealmProxyInterface) object).realmGet$affiliationStatus();
            if (realmGet$affiliationStatus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.affiliationStatusIndex, rowIndex, realmGet$affiliationStatus, false);
            }
            String realmGet$outerNumber = ((ClientRealmProxyInterface) object).realmGet$outerNumber();
            if (realmGet$outerNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.outerNumberIndex, rowIndex, realmGet$outerNumber, false);
            }
            String realmGet$innerNumber = ((ClientRealmProxyInterface) object).realmGet$innerNumber();
            if (realmGet$innerNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.innerNumberIndex, rowIndex, realmGet$innerNumber, false);
            }
            String realmGet$district = ((ClientRealmProxyInterface) object).realmGet$district();
            if (realmGet$district != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.districtIndex, rowIndex, realmGet$district, false);
            }
            String realmGet$facebookUser = ((ClientRealmProxyInterface) object).realmGet$facebookUser();
            if (realmGet$facebookUser != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.facebookUserIndex, rowIndex, realmGet$facebookUser, false);
            }
            String realmGet$twitterUser = ((ClientRealmProxyInterface) object).realmGet$twitterUser();
            if (realmGet$twitterUser != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.twitterUserIndex, rowIndex, realmGet$twitterUser, false);
            }
            String realmGet$expedientStatus = ((ClientRealmProxyInterface) object).realmGet$expedientStatus();
            if (realmGet$expedientStatus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.expedientStatusIndex, rowIndex, realmGet$expedientStatus, false);
            }
            String realmGet$biotmetricStatus = ((ClientRealmProxyInterface) object).realmGet$biotmetricStatus();
            if (realmGet$biotmetricStatus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.biotmetricStatusIndex, rowIndex, realmGet$biotmetricStatus, false);
            }

            app.profuturo.reclasificacion.com.model.SavingsAccount savingsAccountObj = ((ClientRealmProxyInterface) object).realmGet$savingsAccount();
            if (savingsAccountObj != null) {
                Long cachesavingsAccount = cache.get(savingsAccountObj);
                if (cachesavingsAccount == null) {
                    cachesavingsAccount = SavingsAccountRealmProxy.insert(realm, savingsAccountObj, cache);
                }
                table.setLink(columnInfo.savingsAccountIndex, rowIndex, cachesavingsAccount, false);
            }
            String realmGet$email = ((ClientRealmProxyInterface) object).realmGet$email();
            if (realmGet$email != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.emailIndex, rowIndex, realmGet$email, false);
            }
            String realmGet$secondaryEmail = ((ClientRealmProxyInterface) object).realmGet$secondaryEmail();
            if (realmGet$secondaryEmail != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.secondaryEmailIndex, rowIndex, realmGet$secondaryEmail, false);
            }
            String realmGet$landlineNumber = ((ClientRealmProxyInterface) object).realmGet$landlineNumber();
            if (realmGet$landlineNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.landlineNumberIndex, rowIndex, realmGet$landlineNumber, false);
            }
            String realmGet$cellphoneNumber = ((ClientRealmProxyInterface) object).realmGet$cellphoneNumber();
            if (realmGet$cellphoneNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.cellphoneNumberIndex, rowIndex, realmGet$cellphoneNumber, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.Client object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Client.class);
        long tableNativePtr = table.getNativePtr();
        ClientColumnInfo columnInfo = (ClientColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Client.class);
        long pkColumnIndex = columnInfo.accountNumberIndex;
        String primaryKeyValue = ((ClientRealmProxyInterface) object).realmGet$accountNumber();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$name = ((ClientRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.nameIndex, rowIndex, false);
        }
        String realmGet$surName = ((ClientRealmProxyInterface) object).realmGet$surName();
        if (realmGet$surName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.surNameIndex, rowIndex, realmGet$surName, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.surNameIndex, rowIndex, false);
        }
        String realmGet$lastName = ((ClientRealmProxyInterface) object).realmGet$lastName();
        if (realmGet$lastName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.lastNameIndex, rowIndex, realmGet$lastName, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.lastNameIndex, rowIndex, false);
        }
        String realmGet$idPersona = ((ClientRealmProxyInterface) object).realmGet$idPersona();
        if (realmGet$idPersona != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idPersonaIndex, rowIndex, realmGet$idPersona, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idPersonaIndex, rowIndex, false);
        }
        String realmGet$nss = ((ClientRealmProxyInterface) object).realmGet$nss();
        if (realmGet$nss != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nssIndex, rowIndex, realmGet$nss, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.nssIndex, rowIndex, false);
        }
        String realmGet$curp = ((ClientRealmProxyInterface) object).realmGet$curp();
        if (realmGet$curp != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.curpIndex, rowIndex, realmGet$curp, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.curpIndex, rowIndex, false);
        }
        String realmGet$rfc = ((ClientRealmProxyInterface) object).realmGet$rfc();
        if (realmGet$rfc != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.rfcIndex, rowIndex, realmGet$rfc, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.rfcIndex, rowIndex, false);
        }
        String realmGet$affiliationRegime = ((ClientRealmProxyInterface) object).realmGet$affiliationRegime();
        if (realmGet$affiliationRegime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.affiliationRegimeIndex, rowIndex, realmGet$affiliationRegime, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.affiliationRegimeIndex, rowIndex, false);
        }
        String realmGet$accountValid = ((ClientRealmProxyInterface) object).realmGet$accountValid();
        if (realmGet$accountValid != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.accountValidIndex, rowIndex, realmGet$accountValid, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.accountValidIndex, rowIndex, false);
        }
        String realmGet$affiliationOrigin = ((ClientRealmProxyInterface) object).realmGet$affiliationOrigin();
        if (realmGet$affiliationOrigin != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.affiliationOriginIndex, rowIndex, realmGet$affiliationOrigin, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.affiliationOriginIndex, rowIndex, false);
        }
        String realmGet$birthday = ((ClientRealmProxyInterface) object).realmGet$birthday();
        if (realmGet$birthday != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.birthdayIndex, rowIndex, realmGet$birthday, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.birthdayIndex, rowIndex, false);
        }
        String realmGet$gender = ((ClientRealmProxyInterface) object).realmGet$gender();
        if (realmGet$gender != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.genderIndex, rowIndex, realmGet$gender, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.genderIndex, rowIndex, false);
        }
        String realmGet$street = ((ClientRealmProxyInterface) object).realmGet$street();
        if (realmGet$street != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.streetIndex, rowIndex, realmGet$street, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.streetIndex, rowIndex, false);
        }
        String realmGet$neighborhood = ((ClientRealmProxyInterface) object).realmGet$neighborhood();
        if (realmGet$neighborhood != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.neighborhoodIndex, rowIndex, realmGet$neighborhood, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.neighborhoodIndex, rowIndex, false);
        }
        String realmGet$zipCode = ((ClientRealmProxyInterface) object).realmGet$zipCode();
        if (realmGet$zipCode != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.zipCodeIndex, rowIndex, realmGet$zipCode, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.zipCodeIndex, rowIndex, false);
        }
        String realmGet$state = ((ClientRealmProxyInterface) object).realmGet$state();
        if (realmGet$state != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.stateIndex, rowIndex, realmGet$state, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.stateIndex, rowIndex, false);
        }
        String realmGet$expedientIndicatorDescription = ((ClientRealmProxyInterface) object).realmGet$expedientIndicatorDescription();
        if (realmGet$expedientIndicatorDescription != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.expedientIndicatorDescriptionIndex, rowIndex, realmGet$expedientIndicatorDescription, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.expedientIndicatorDescriptionIndex, rowIndex, false);
        }
        String realmGet$estatusIndicadorExpediente = ((ClientRealmProxyInterface) object).realmGet$estatusIndicadorExpediente();
        if (realmGet$estatusIndicadorExpediente != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estatusIndicadorExpedienteIndex, rowIndex, realmGet$estatusIndicadorExpediente, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.estatusIndicadorExpedienteIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.expedientIndicatorValueIndex, rowIndex, ((ClientRealmProxyInterface) object).realmGet$expedientIndicatorValue(), false);
        String realmGet$biometricIndicatorDescription = ((ClientRealmProxyInterface) object).realmGet$biometricIndicatorDescription();
        if (realmGet$biometricIndicatorDescription != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.biometricIndicatorDescriptionIndex, rowIndex, realmGet$biometricIndicatorDescription, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.biometricIndicatorDescriptionIndex, rowIndex, false);
        }
        String realmGet$estatusIndicadorBiometrico = ((ClientRealmProxyInterface) object).realmGet$estatusIndicadorBiometrico();
        if (realmGet$estatusIndicadorBiometrico != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estatusIndicadorBiometricoIndex, rowIndex, realmGet$estatusIndicadorBiometrico, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.estatusIndicadorBiometricoIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.biometricIndicatorValueIndex, rowIndex, ((ClientRealmProxyInterface) object).realmGet$biometricIndicatorValue(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.regimentIndicatorIdIndex, rowIndex, ((ClientRealmProxyInterface) object).realmGet$regimentIndicatorId(), false);
        String realmGet$estatusTipoCliente = ((ClientRealmProxyInterface) object).realmGet$estatusTipoCliente();
        if (realmGet$estatusTipoCliente != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estatusTipoClienteIndex, rowIndex, realmGet$estatusTipoCliente, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.estatusTipoClienteIndex, rowIndex, false);
        }
        String realmGet$fechaCertificacion = ((ClientRealmProxyInterface) object).realmGet$fechaCertificacion();
        if (realmGet$fechaCertificacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fechaCertificacionIndex, rowIndex, realmGet$fechaCertificacion, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.fechaCertificacionIndex, rowIndex, false);
        }
        String realmGet$estatus = ((ClientRealmProxyInterface) object).realmGet$estatus();
        if (realmGet$estatus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estatusIndex, rowIndex, realmGet$estatus, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.estatusIndex, rowIndex, false);
        }
        String realmGet$affiliationStatus = ((ClientRealmProxyInterface) object).realmGet$affiliationStatus();
        if (realmGet$affiliationStatus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.affiliationStatusIndex, rowIndex, realmGet$affiliationStatus, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.affiliationStatusIndex, rowIndex, false);
        }
        String realmGet$outerNumber = ((ClientRealmProxyInterface) object).realmGet$outerNumber();
        if (realmGet$outerNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.outerNumberIndex, rowIndex, realmGet$outerNumber, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.outerNumberIndex, rowIndex, false);
        }
        String realmGet$innerNumber = ((ClientRealmProxyInterface) object).realmGet$innerNumber();
        if (realmGet$innerNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.innerNumberIndex, rowIndex, realmGet$innerNumber, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.innerNumberIndex, rowIndex, false);
        }
        String realmGet$district = ((ClientRealmProxyInterface) object).realmGet$district();
        if (realmGet$district != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.districtIndex, rowIndex, realmGet$district, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.districtIndex, rowIndex, false);
        }
        String realmGet$facebookUser = ((ClientRealmProxyInterface) object).realmGet$facebookUser();
        if (realmGet$facebookUser != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.facebookUserIndex, rowIndex, realmGet$facebookUser, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.facebookUserIndex, rowIndex, false);
        }
        String realmGet$twitterUser = ((ClientRealmProxyInterface) object).realmGet$twitterUser();
        if (realmGet$twitterUser != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.twitterUserIndex, rowIndex, realmGet$twitterUser, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.twitterUserIndex, rowIndex, false);
        }
        String realmGet$expedientStatus = ((ClientRealmProxyInterface) object).realmGet$expedientStatus();
        if (realmGet$expedientStatus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.expedientStatusIndex, rowIndex, realmGet$expedientStatus, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.expedientStatusIndex, rowIndex, false);
        }
        String realmGet$biotmetricStatus = ((ClientRealmProxyInterface) object).realmGet$biotmetricStatus();
        if (realmGet$biotmetricStatus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.biotmetricStatusIndex, rowIndex, realmGet$biotmetricStatus, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.biotmetricStatusIndex, rowIndex, false);
        }

        app.profuturo.reclasificacion.com.model.SavingsAccount savingsAccountObj = ((ClientRealmProxyInterface) object).realmGet$savingsAccount();
        if (savingsAccountObj != null) {
            Long cachesavingsAccount = cache.get(savingsAccountObj);
            if (cachesavingsAccount == null) {
                cachesavingsAccount = SavingsAccountRealmProxy.insertOrUpdate(realm, savingsAccountObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.savingsAccountIndex, rowIndex, cachesavingsAccount, false);
        } else {
            Table.nativeNullifyLink(tableNativePtr, columnInfo.savingsAccountIndex, rowIndex);
        }
        String realmGet$email = ((ClientRealmProxyInterface) object).realmGet$email();
        if (realmGet$email != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.emailIndex, rowIndex, realmGet$email, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.emailIndex, rowIndex, false);
        }
        String realmGet$secondaryEmail = ((ClientRealmProxyInterface) object).realmGet$secondaryEmail();
        if (realmGet$secondaryEmail != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.secondaryEmailIndex, rowIndex, realmGet$secondaryEmail, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.secondaryEmailIndex, rowIndex, false);
        }
        String realmGet$landlineNumber = ((ClientRealmProxyInterface) object).realmGet$landlineNumber();
        if (realmGet$landlineNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.landlineNumberIndex, rowIndex, realmGet$landlineNumber, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.landlineNumberIndex, rowIndex, false);
        }
        String realmGet$cellphoneNumber = ((ClientRealmProxyInterface) object).realmGet$cellphoneNumber();
        if (realmGet$cellphoneNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.cellphoneNumberIndex, rowIndex, realmGet$cellphoneNumber, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.cellphoneNumberIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.Client.class);
        long tableNativePtr = table.getNativePtr();
        ClientColumnInfo columnInfo = (ClientColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.Client.class);
        long pkColumnIndex = columnInfo.accountNumberIndex;
        app.profuturo.reclasificacion.com.model.Client object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.Client) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((ClientRealmProxyInterface) object).realmGet$accountNumber();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$name = ((ClientRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.nameIndex, rowIndex, false);
            }
            String realmGet$surName = ((ClientRealmProxyInterface) object).realmGet$surName();
            if (realmGet$surName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.surNameIndex, rowIndex, realmGet$surName, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.surNameIndex, rowIndex, false);
            }
            String realmGet$lastName = ((ClientRealmProxyInterface) object).realmGet$lastName();
            if (realmGet$lastName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.lastNameIndex, rowIndex, realmGet$lastName, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.lastNameIndex, rowIndex, false);
            }
            String realmGet$idPersona = ((ClientRealmProxyInterface) object).realmGet$idPersona();
            if (realmGet$idPersona != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idPersonaIndex, rowIndex, realmGet$idPersona, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idPersonaIndex, rowIndex, false);
            }
            String realmGet$nss = ((ClientRealmProxyInterface) object).realmGet$nss();
            if (realmGet$nss != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nssIndex, rowIndex, realmGet$nss, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.nssIndex, rowIndex, false);
            }
            String realmGet$curp = ((ClientRealmProxyInterface) object).realmGet$curp();
            if (realmGet$curp != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.curpIndex, rowIndex, realmGet$curp, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.curpIndex, rowIndex, false);
            }
            String realmGet$rfc = ((ClientRealmProxyInterface) object).realmGet$rfc();
            if (realmGet$rfc != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.rfcIndex, rowIndex, realmGet$rfc, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.rfcIndex, rowIndex, false);
            }
            String realmGet$affiliationRegime = ((ClientRealmProxyInterface) object).realmGet$affiliationRegime();
            if (realmGet$affiliationRegime != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.affiliationRegimeIndex, rowIndex, realmGet$affiliationRegime, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.affiliationRegimeIndex, rowIndex, false);
            }
            String realmGet$accountValid = ((ClientRealmProxyInterface) object).realmGet$accountValid();
            if (realmGet$accountValid != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.accountValidIndex, rowIndex, realmGet$accountValid, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.accountValidIndex, rowIndex, false);
            }
            String realmGet$affiliationOrigin = ((ClientRealmProxyInterface) object).realmGet$affiliationOrigin();
            if (realmGet$affiliationOrigin != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.affiliationOriginIndex, rowIndex, realmGet$affiliationOrigin, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.affiliationOriginIndex, rowIndex, false);
            }
            String realmGet$birthday = ((ClientRealmProxyInterface) object).realmGet$birthday();
            if (realmGet$birthday != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.birthdayIndex, rowIndex, realmGet$birthday, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.birthdayIndex, rowIndex, false);
            }
            String realmGet$gender = ((ClientRealmProxyInterface) object).realmGet$gender();
            if (realmGet$gender != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.genderIndex, rowIndex, realmGet$gender, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.genderIndex, rowIndex, false);
            }
            String realmGet$street = ((ClientRealmProxyInterface) object).realmGet$street();
            if (realmGet$street != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.streetIndex, rowIndex, realmGet$street, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.streetIndex, rowIndex, false);
            }
            String realmGet$neighborhood = ((ClientRealmProxyInterface) object).realmGet$neighborhood();
            if (realmGet$neighborhood != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.neighborhoodIndex, rowIndex, realmGet$neighborhood, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.neighborhoodIndex, rowIndex, false);
            }
            String realmGet$zipCode = ((ClientRealmProxyInterface) object).realmGet$zipCode();
            if (realmGet$zipCode != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.zipCodeIndex, rowIndex, realmGet$zipCode, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.zipCodeIndex, rowIndex, false);
            }
            String realmGet$state = ((ClientRealmProxyInterface) object).realmGet$state();
            if (realmGet$state != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.stateIndex, rowIndex, realmGet$state, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.stateIndex, rowIndex, false);
            }
            String realmGet$expedientIndicatorDescription = ((ClientRealmProxyInterface) object).realmGet$expedientIndicatorDescription();
            if (realmGet$expedientIndicatorDescription != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.expedientIndicatorDescriptionIndex, rowIndex, realmGet$expedientIndicatorDescription, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.expedientIndicatorDescriptionIndex, rowIndex, false);
            }
            String realmGet$estatusIndicadorExpediente = ((ClientRealmProxyInterface) object).realmGet$estatusIndicadorExpediente();
            if (realmGet$estatusIndicadorExpediente != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estatusIndicadorExpedienteIndex, rowIndex, realmGet$estatusIndicadorExpediente, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.estatusIndicadorExpedienteIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.expedientIndicatorValueIndex, rowIndex, ((ClientRealmProxyInterface) object).realmGet$expedientIndicatorValue(), false);
            String realmGet$biometricIndicatorDescription = ((ClientRealmProxyInterface) object).realmGet$biometricIndicatorDescription();
            if (realmGet$biometricIndicatorDescription != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.biometricIndicatorDescriptionIndex, rowIndex, realmGet$biometricIndicatorDescription, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.biometricIndicatorDescriptionIndex, rowIndex, false);
            }
            String realmGet$estatusIndicadorBiometrico = ((ClientRealmProxyInterface) object).realmGet$estatusIndicadorBiometrico();
            if (realmGet$estatusIndicadorBiometrico != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estatusIndicadorBiometricoIndex, rowIndex, realmGet$estatusIndicadorBiometrico, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.estatusIndicadorBiometricoIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.biometricIndicatorValueIndex, rowIndex, ((ClientRealmProxyInterface) object).realmGet$biometricIndicatorValue(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.regimentIndicatorIdIndex, rowIndex, ((ClientRealmProxyInterface) object).realmGet$regimentIndicatorId(), false);
            String realmGet$estatusTipoCliente = ((ClientRealmProxyInterface) object).realmGet$estatusTipoCliente();
            if (realmGet$estatusTipoCliente != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estatusTipoClienteIndex, rowIndex, realmGet$estatusTipoCliente, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.estatusTipoClienteIndex, rowIndex, false);
            }
            String realmGet$fechaCertificacion = ((ClientRealmProxyInterface) object).realmGet$fechaCertificacion();
            if (realmGet$fechaCertificacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.fechaCertificacionIndex, rowIndex, realmGet$fechaCertificacion, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.fechaCertificacionIndex, rowIndex, false);
            }
            String realmGet$estatus = ((ClientRealmProxyInterface) object).realmGet$estatus();
            if (realmGet$estatus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estatusIndex, rowIndex, realmGet$estatus, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.estatusIndex, rowIndex, false);
            }
            String realmGet$affiliationStatus = ((ClientRealmProxyInterface) object).realmGet$affiliationStatus();
            if (realmGet$affiliationStatus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.affiliationStatusIndex, rowIndex, realmGet$affiliationStatus, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.affiliationStatusIndex, rowIndex, false);
            }
            String realmGet$outerNumber = ((ClientRealmProxyInterface) object).realmGet$outerNumber();
            if (realmGet$outerNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.outerNumberIndex, rowIndex, realmGet$outerNumber, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.outerNumberIndex, rowIndex, false);
            }
            String realmGet$innerNumber = ((ClientRealmProxyInterface) object).realmGet$innerNumber();
            if (realmGet$innerNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.innerNumberIndex, rowIndex, realmGet$innerNumber, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.innerNumberIndex, rowIndex, false);
            }
            String realmGet$district = ((ClientRealmProxyInterface) object).realmGet$district();
            if (realmGet$district != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.districtIndex, rowIndex, realmGet$district, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.districtIndex, rowIndex, false);
            }
            String realmGet$facebookUser = ((ClientRealmProxyInterface) object).realmGet$facebookUser();
            if (realmGet$facebookUser != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.facebookUserIndex, rowIndex, realmGet$facebookUser, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.facebookUserIndex, rowIndex, false);
            }
            String realmGet$twitterUser = ((ClientRealmProxyInterface) object).realmGet$twitterUser();
            if (realmGet$twitterUser != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.twitterUserIndex, rowIndex, realmGet$twitterUser, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.twitterUserIndex, rowIndex, false);
            }
            String realmGet$expedientStatus = ((ClientRealmProxyInterface) object).realmGet$expedientStatus();
            if (realmGet$expedientStatus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.expedientStatusIndex, rowIndex, realmGet$expedientStatus, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.expedientStatusIndex, rowIndex, false);
            }
            String realmGet$biotmetricStatus = ((ClientRealmProxyInterface) object).realmGet$biotmetricStatus();
            if (realmGet$biotmetricStatus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.biotmetricStatusIndex, rowIndex, realmGet$biotmetricStatus, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.biotmetricStatusIndex, rowIndex, false);
            }

            app.profuturo.reclasificacion.com.model.SavingsAccount savingsAccountObj = ((ClientRealmProxyInterface) object).realmGet$savingsAccount();
            if (savingsAccountObj != null) {
                Long cachesavingsAccount = cache.get(savingsAccountObj);
                if (cachesavingsAccount == null) {
                    cachesavingsAccount = SavingsAccountRealmProxy.insertOrUpdate(realm, savingsAccountObj, cache);
                }
                Table.nativeSetLink(tableNativePtr, columnInfo.savingsAccountIndex, rowIndex, cachesavingsAccount, false);
            } else {
                Table.nativeNullifyLink(tableNativePtr, columnInfo.savingsAccountIndex, rowIndex);
            }
            String realmGet$email = ((ClientRealmProxyInterface) object).realmGet$email();
            if (realmGet$email != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.emailIndex, rowIndex, realmGet$email, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.emailIndex, rowIndex, false);
            }
            String realmGet$secondaryEmail = ((ClientRealmProxyInterface) object).realmGet$secondaryEmail();
            if (realmGet$secondaryEmail != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.secondaryEmailIndex, rowIndex, realmGet$secondaryEmail, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.secondaryEmailIndex, rowIndex, false);
            }
            String realmGet$landlineNumber = ((ClientRealmProxyInterface) object).realmGet$landlineNumber();
            if (realmGet$landlineNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.landlineNumberIndex, rowIndex, realmGet$landlineNumber, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.landlineNumberIndex, rowIndex, false);
            }
            String realmGet$cellphoneNumber = ((ClientRealmProxyInterface) object).realmGet$cellphoneNumber();
            if (realmGet$cellphoneNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.cellphoneNumberIndex, rowIndex, realmGet$cellphoneNumber, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.cellphoneNumberIndex, rowIndex, false);
            }
        }
    }

    public static app.profuturo.reclasificacion.com.model.Client createDetachedCopy(app.profuturo.reclasificacion.com.model.Client realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        app.profuturo.reclasificacion.com.model.Client unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new app.profuturo.reclasificacion.com.model.Client();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (app.profuturo.reclasificacion.com.model.Client) cachedObject.object;
            }
            unmanagedObject = (app.profuturo.reclasificacion.com.model.Client) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        ClientRealmProxyInterface unmanagedCopy = (ClientRealmProxyInterface) unmanagedObject;
        ClientRealmProxyInterface realmSource = (ClientRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$accountNumber(realmSource.realmGet$accountNumber());
        unmanagedCopy.realmSet$name(realmSource.realmGet$name());
        unmanagedCopy.realmSet$surName(realmSource.realmGet$surName());
        unmanagedCopy.realmSet$lastName(realmSource.realmGet$lastName());
        unmanagedCopy.realmSet$idPersona(realmSource.realmGet$idPersona());
        unmanagedCopy.realmSet$nss(realmSource.realmGet$nss());
        unmanagedCopy.realmSet$curp(realmSource.realmGet$curp());
        unmanagedCopy.realmSet$rfc(realmSource.realmGet$rfc());
        unmanagedCopy.realmSet$affiliationRegime(realmSource.realmGet$affiliationRegime());
        unmanagedCopy.realmSet$accountValid(realmSource.realmGet$accountValid());
        unmanagedCopy.realmSet$affiliationOrigin(realmSource.realmGet$affiliationOrigin());
        unmanagedCopy.realmSet$birthday(realmSource.realmGet$birthday());
        unmanagedCopy.realmSet$gender(realmSource.realmGet$gender());
        unmanagedCopy.realmSet$street(realmSource.realmGet$street());
        unmanagedCopy.realmSet$neighborhood(realmSource.realmGet$neighborhood());
        unmanagedCopy.realmSet$zipCode(realmSource.realmGet$zipCode());
        unmanagedCopy.realmSet$state(realmSource.realmGet$state());
        unmanagedCopy.realmSet$expedientIndicatorDescription(realmSource.realmGet$expedientIndicatorDescription());
        unmanagedCopy.realmSet$estatusIndicadorExpediente(realmSource.realmGet$estatusIndicadorExpediente());
        unmanagedCopy.realmSet$expedientIndicatorValue(realmSource.realmGet$expedientIndicatorValue());
        unmanagedCopy.realmSet$biometricIndicatorDescription(realmSource.realmGet$biometricIndicatorDescription());
        unmanagedCopy.realmSet$estatusIndicadorBiometrico(realmSource.realmGet$estatusIndicadorBiometrico());
        unmanagedCopy.realmSet$biometricIndicatorValue(realmSource.realmGet$biometricIndicatorValue());
        unmanagedCopy.realmSet$regimentIndicatorId(realmSource.realmGet$regimentIndicatorId());
        unmanagedCopy.realmSet$estatusTipoCliente(realmSource.realmGet$estatusTipoCliente());
        unmanagedCopy.realmSet$fechaCertificacion(realmSource.realmGet$fechaCertificacion());
        unmanagedCopy.realmSet$estatus(realmSource.realmGet$estatus());
        unmanagedCopy.realmSet$affiliationStatus(realmSource.realmGet$affiliationStatus());
        unmanagedCopy.realmSet$outerNumber(realmSource.realmGet$outerNumber());
        unmanagedCopy.realmSet$innerNumber(realmSource.realmGet$innerNumber());
        unmanagedCopy.realmSet$district(realmSource.realmGet$district());
        unmanagedCopy.realmSet$facebookUser(realmSource.realmGet$facebookUser());
        unmanagedCopy.realmSet$twitterUser(realmSource.realmGet$twitterUser());
        unmanagedCopy.realmSet$expedientStatus(realmSource.realmGet$expedientStatus());
        unmanagedCopy.realmSet$biotmetricStatus(realmSource.realmGet$biotmetricStatus());

        // Deep copy of savingsAccount
        unmanagedCopy.realmSet$savingsAccount(SavingsAccountRealmProxy.createDetachedCopy(realmSource.realmGet$savingsAccount(), currentDepth + 1, maxDepth, cache));
        unmanagedCopy.realmSet$email(realmSource.realmGet$email());
        unmanagedCopy.realmSet$secondaryEmail(realmSource.realmGet$secondaryEmail());
        unmanagedCopy.realmSet$landlineNumber(realmSource.realmGet$landlineNumber());
        unmanagedCopy.realmSet$cellphoneNumber(realmSource.realmGet$cellphoneNumber());

        return unmanagedObject;
    }

    static app.profuturo.reclasificacion.com.model.Client update(Realm realm, app.profuturo.reclasificacion.com.model.Client realmObject, app.profuturo.reclasificacion.com.model.Client newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ClientRealmProxyInterface realmObjectTarget = (ClientRealmProxyInterface) realmObject;
        ClientRealmProxyInterface realmObjectSource = (ClientRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$name(realmObjectSource.realmGet$name());
        realmObjectTarget.realmSet$surName(realmObjectSource.realmGet$surName());
        realmObjectTarget.realmSet$lastName(realmObjectSource.realmGet$lastName());
        realmObjectTarget.realmSet$idPersona(realmObjectSource.realmGet$idPersona());
        realmObjectTarget.realmSet$nss(realmObjectSource.realmGet$nss());
        realmObjectTarget.realmSet$curp(realmObjectSource.realmGet$curp());
        realmObjectTarget.realmSet$rfc(realmObjectSource.realmGet$rfc());
        realmObjectTarget.realmSet$affiliationRegime(realmObjectSource.realmGet$affiliationRegime());
        realmObjectTarget.realmSet$accountValid(realmObjectSource.realmGet$accountValid());
        realmObjectTarget.realmSet$affiliationOrigin(realmObjectSource.realmGet$affiliationOrigin());
        realmObjectTarget.realmSet$birthday(realmObjectSource.realmGet$birthday());
        realmObjectTarget.realmSet$gender(realmObjectSource.realmGet$gender());
        realmObjectTarget.realmSet$street(realmObjectSource.realmGet$street());
        realmObjectTarget.realmSet$neighborhood(realmObjectSource.realmGet$neighborhood());
        realmObjectTarget.realmSet$zipCode(realmObjectSource.realmGet$zipCode());
        realmObjectTarget.realmSet$state(realmObjectSource.realmGet$state());
        realmObjectTarget.realmSet$expedientIndicatorDescription(realmObjectSource.realmGet$expedientIndicatorDescription());
        realmObjectTarget.realmSet$estatusIndicadorExpediente(realmObjectSource.realmGet$estatusIndicadorExpediente());
        realmObjectTarget.realmSet$expedientIndicatorValue(realmObjectSource.realmGet$expedientIndicatorValue());
        realmObjectTarget.realmSet$biometricIndicatorDescription(realmObjectSource.realmGet$biometricIndicatorDescription());
        realmObjectTarget.realmSet$estatusIndicadorBiometrico(realmObjectSource.realmGet$estatusIndicadorBiometrico());
        realmObjectTarget.realmSet$biometricIndicatorValue(realmObjectSource.realmGet$biometricIndicatorValue());
        realmObjectTarget.realmSet$regimentIndicatorId(realmObjectSource.realmGet$regimentIndicatorId());
        realmObjectTarget.realmSet$estatusTipoCliente(realmObjectSource.realmGet$estatusTipoCliente());
        realmObjectTarget.realmSet$fechaCertificacion(realmObjectSource.realmGet$fechaCertificacion());
        realmObjectTarget.realmSet$estatus(realmObjectSource.realmGet$estatus());
        realmObjectTarget.realmSet$affiliationStatus(realmObjectSource.realmGet$affiliationStatus());
        realmObjectTarget.realmSet$outerNumber(realmObjectSource.realmGet$outerNumber());
        realmObjectTarget.realmSet$innerNumber(realmObjectSource.realmGet$innerNumber());
        realmObjectTarget.realmSet$district(realmObjectSource.realmGet$district());
        realmObjectTarget.realmSet$facebookUser(realmObjectSource.realmGet$facebookUser());
        realmObjectTarget.realmSet$twitterUser(realmObjectSource.realmGet$twitterUser());
        realmObjectTarget.realmSet$expedientStatus(realmObjectSource.realmGet$expedientStatus());
        realmObjectTarget.realmSet$biotmetricStatus(realmObjectSource.realmGet$biotmetricStatus());
        app.profuturo.reclasificacion.com.model.SavingsAccount savingsAccountObj = realmObjectSource.realmGet$savingsAccount();
        if (savingsAccountObj == null) {
            realmObjectTarget.realmSet$savingsAccount(null);
        } else {
            app.profuturo.reclasificacion.com.model.SavingsAccount cachesavingsAccount = (app.profuturo.reclasificacion.com.model.SavingsAccount) cache.get(savingsAccountObj);
            if (cachesavingsAccount != null) {
                realmObjectTarget.realmSet$savingsAccount(cachesavingsAccount);
            } else {
                realmObjectTarget.realmSet$savingsAccount(SavingsAccountRealmProxy.copyOrUpdate(realm, savingsAccountObj, true, cache));
            }
        }
        realmObjectTarget.realmSet$email(realmObjectSource.realmGet$email());
        realmObjectTarget.realmSet$secondaryEmail(realmObjectSource.realmGet$secondaryEmail());
        realmObjectTarget.realmSet$landlineNumber(realmObjectSource.realmGet$landlineNumber());
        realmObjectTarget.realmSet$cellphoneNumber(realmObjectSource.realmGet$cellphoneNumber());
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Client = proxy[");
        stringBuilder.append("{accountNumber:");
        stringBuilder.append(realmGet$accountNumber() != null ? realmGet$accountNumber() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{name:");
        stringBuilder.append(realmGet$name() != null ? realmGet$name() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{surName:");
        stringBuilder.append(realmGet$surName() != null ? realmGet$surName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{lastName:");
        stringBuilder.append(realmGet$lastName() != null ? realmGet$lastName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idPersona:");
        stringBuilder.append(realmGet$idPersona() != null ? realmGet$idPersona() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{nss:");
        stringBuilder.append(realmGet$nss() != null ? realmGet$nss() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{curp:");
        stringBuilder.append(realmGet$curp() != null ? realmGet$curp() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{rfc:");
        stringBuilder.append(realmGet$rfc() != null ? realmGet$rfc() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{affiliationRegime:");
        stringBuilder.append(realmGet$affiliationRegime() != null ? realmGet$affiliationRegime() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{accountValid:");
        stringBuilder.append(realmGet$accountValid() != null ? realmGet$accountValid() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{affiliationOrigin:");
        stringBuilder.append(realmGet$affiliationOrigin() != null ? realmGet$affiliationOrigin() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{birthday:");
        stringBuilder.append(realmGet$birthday() != null ? realmGet$birthday() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{gender:");
        stringBuilder.append(realmGet$gender() != null ? realmGet$gender() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{street:");
        stringBuilder.append(realmGet$street() != null ? realmGet$street() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{neighborhood:");
        stringBuilder.append(realmGet$neighborhood() != null ? realmGet$neighborhood() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zipCode:");
        stringBuilder.append(realmGet$zipCode() != null ? realmGet$zipCode() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{state:");
        stringBuilder.append(realmGet$state() != null ? realmGet$state() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{expedientIndicatorDescription:");
        stringBuilder.append(realmGet$expedientIndicatorDescription() != null ? realmGet$expedientIndicatorDescription() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{estatusIndicadorExpediente:");
        stringBuilder.append(realmGet$estatusIndicadorExpediente() != null ? realmGet$estatusIndicadorExpediente() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{expedientIndicatorValue:");
        stringBuilder.append(realmGet$expedientIndicatorValue());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{biometricIndicatorDescription:");
        stringBuilder.append(realmGet$biometricIndicatorDescription() != null ? realmGet$biometricIndicatorDescription() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{estatusIndicadorBiometrico:");
        stringBuilder.append(realmGet$estatusIndicadorBiometrico() != null ? realmGet$estatusIndicadorBiometrico() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{biometricIndicatorValue:");
        stringBuilder.append(realmGet$biometricIndicatorValue());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{regimentIndicatorId:");
        stringBuilder.append(realmGet$regimentIndicatorId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{estatusTipoCliente:");
        stringBuilder.append(realmGet$estatusTipoCliente() != null ? realmGet$estatusTipoCliente() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{fechaCertificacion:");
        stringBuilder.append(realmGet$fechaCertificacion() != null ? realmGet$fechaCertificacion() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{estatus:");
        stringBuilder.append(realmGet$estatus() != null ? realmGet$estatus() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{affiliationStatus:");
        stringBuilder.append(realmGet$affiliationStatus() != null ? realmGet$affiliationStatus() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{outerNumber:");
        stringBuilder.append(realmGet$outerNumber() != null ? realmGet$outerNumber() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{innerNumber:");
        stringBuilder.append(realmGet$innerNumber() != null ? realmGet$innerNumber() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{district:");
        stringBuilder.append(realmGet$district() != null ? realmGet$district() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{facebookUser:");
        stringBuilder.append(realmGet$facebookUser() != null ? realmGet$facebookUser() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{twitterUser:");
        stringBuilder.append(realmGet$twitterUser() != null ? realmGet$twitterUser() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{expedientStatus:");
        stringBuilder.append(realmGet$expedientStatus() != null ? realmGet$expedientStatus() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{biotmetricStatus:");
        stringBuilder.append(realmGet$biotmetricStatus() != null ? realmGet$biotmetricStatus() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{savingsAccount:");
        stringBuilder.append(realmGet$savingsAccount() != null ? "SavingsAccount" : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{email:");
        stringBuilder.append(realmGet$email() != null ? realmGet$email() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{secondaryEmail:");
        stringBuilder.append(realmGet$secondaryEmail() != null ? realmGet$secondaryEmail() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{landlineNumber:");
        stringBuilder.append(realmGet$landlineNumber() != null ? realmGet$landlineNumber() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{cellphoneNumber:");
        stringBuilder.append(realmGet$cellphoneNumber() != null ? realmGet$cellphoneNumber() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientRealmProxy aClient = (ClientRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aClient.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aClient.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aClient.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
