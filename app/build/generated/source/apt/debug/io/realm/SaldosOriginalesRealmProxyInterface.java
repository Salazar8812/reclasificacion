package io.realm;


public interface SaldosOriginalesRealmProxyInterface {
    public app.profuturo.reclasificacion.com.model.Saldo realmGet$saldo();
    public void realmSet$saldo(app.profuturo.reclasificacion.com.model.Saldo value);
}
