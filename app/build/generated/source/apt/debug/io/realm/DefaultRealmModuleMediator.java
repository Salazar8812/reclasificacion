package io.realm;


import android.util.JsonReader;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmProxyMediator;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

@io.realm.annotations.RealmModule
class DefaultRealmModuleMediator extends RealmProxyMediator {

    private static final Set<Class<? extends RealmModel>> MODEL_CLASSES;
    static {
        Set<Class<? extends RealmModel>> modelClasses = new HashSet<Class<? extends RealmModel>>(10);
        modelClasses.add(app.profuturo.reclasificacion.com.model.Client.class);
        modelClasses.add(app.profuturo.reclasificacion.com.model.SaldosOriginales.class);
        modelClasses.add(app.profuturo.reclasificacion.com.model.Saldo.class);
        modelClasses.add(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class);
        modelClasses.add(app.profuturo.reclasificacion.com.model.ListReclasificacion.class);
        modelClasses.add(app.profuturo.reclasificacion.com.model.Shares.class);
        modelClasses.add(app.profuturo.reclasificacion.com.model.Solicitante.class);
        modelClasses.add(app.profuturo.reclasificacion.com.model.SavingsAccount.class);
        modelClasses.add(app.profuturo.reclasificacion.com.model.AgentSession.class);
        modelClasses.add(app.profuturo.reclasificacion.com.model.ListaIndicadore.class);
        MODEL_CLASSES = Collections.unmodifiableSet(modelClasses);
    }

    @Override
    public Map<Class<? extends RealmModel>, OsObjectSchemaInfo> getExpectedObjectSchemaInfoMap() {
        Map<Class<? extends RealmModel>, OsObjectSchemaInfo> infoMap = new HashMap<Class<? extends RealmModel>, OsObjectSchemaInfo>(10);
        infoMap.put(app.profuturo.reclasificacion.com.model.Client.class, io.realm.ClientRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(app.profuturo.reclasificacion.com.model.SaldosOriginales.class, io.realm.SaldosOriginalesRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(app.profuturo.reclasificacion.com.model.Saldo.class, io.realm.SaldoRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class, io.realm.SaveProcedureRequestRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(app.profuturo.reclasificacion.com.model.ListReclasificacion.class, io.realm.ListReclasificacionRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(app.profuturo.reclasificacion.com.model.Shares.class, io.realm.SharesRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(app.profuturo.reclasificacion.com.model.Solicitante.class, io.realm.SolicitanteRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(app.profuturo.reclasificacion.com.model.SavingsAccount.class, io.realm.SavingsAccountRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(app.profuturo.reclasificacion.com.model.AgentSession.class, io.realm.AgentSessionRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(app.profuturo.reclasificacion.com.model.ListaIndicadore.class, io.realm.ListaIndicadoreRealmProxy.getExpectedObjectSchemaInfo());
        return infoMap;
    }

    @Override
    public ColumnInfo createColumnInfo(Class<? extends RealmModel> clazz, OsSchemaInfo schemaInfo) {
        checkClass(clazz);

        if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
            return io.realm.ClientRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
            return io.realm.SaldosOriginalesRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
            return io.realm.SaldoRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
            return io.realm.SaveProcedureRequestRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
            return io.realm.ListReclasificacionRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
            return io.realm.SharesRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
            return io.realm.SolicitanteRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
            return io.realm.SavingsAccountRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
            return io.realm.AgentSessionRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
            return io.realm.ListaIndicadoreRealmProxy.createColumnInfo(schemaInfo);
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public List<String> getFieldNames(Class<? extends RealmModel> clazz) {
        checkClass(clazz);

        if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
            return io.realm.ClientRealmProxy.getFieldNames();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
            return io.realm.SaldosOriginalesRealmProxy.getFieldNames();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
            return io.realm.SaldoRealmProxy.getFieldNames();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
            return io.realm.SaveProcedureRequestRealmProxy.getFieldNames();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
            return io.realm.ListReclasificacionRealmProxy.getFieldNames();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
            return io.realm.SharesRealmProxy.getFieldNames();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
            return io.realm.SolicitanteRealmProxy.getFieldNames();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
            return io.realm.SavingsAccountRealmProxy.getFieldNames();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
            return io.realm.AgentSessionRealmProxy.getFieldNames();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
            return io.realm.ListaIndicadoreRealmProxy.getFieldNames();
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public String getTableName(Class<? extends RealmModel> clazz) {
        checkClass(clazz);

        if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
            return io.realm.ClientRealmProxy.getTableName();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
            return io.realm.SaldosOriginalesRealmProxy.getTableName();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
            return io.realm.SaldoRealmProxy.getTableName();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
            return io.realm.SaveProcedureRequestRealmProxy.getTableName();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
            return io.realm.ListReclasificacionRealmProxy.getTableName();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
            return io.realm.SharesRealmProxy.getTableName();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
            return io.realm.SolicitanteRealmProxy.getTableName();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
            return io.realm.SavingsAccountRealmProxy.getTableName();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
            return io.realm.AgentSessionRealmProxy.getTableName();
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
            return io.realm.ListaIndicadoreRealmProxy.getTableName();
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E newInstance(Class<E> clazz, Object baseRealm, Row row, ColumnInfo columnInfo, boolean acceptDefaultValue, List<String> excludeFields) {
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        try {
            objectContext.set((BaseRealm) baseRealm, row, columnInfo, acceptDefaultValue, excludeFields);
            checkClass(clazz);

            if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
                return clazz.cast(new io.realm.ClientRealmProxy());
            }
            if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
                return clazz.cast(new io.realm.SaldosOriginalesRealmProxy());
            }
            if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
                return clazz.cast(new io.realm.SaldoRealmProxy());
            }
            if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
                return clazz.cast(new io.realm.SaveProcedureRequestRealmProxy());
            }
            if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
                return clazz.cast(new io.realm.ListReclasificacionRealmProxy());
            }
            if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
                return clazz.cast(new io.realm.SharesRealmProxy());
            }
            if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
                return clazz.cast(new io.realm.SolicitanteRealmProxy());
            }
            if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
                return clazz.cast(new io.realm.SavingsAccountRealmProxy());
            }
            if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
                return clazz.cast(new io.realm.AgentSessionRealmProxy());
            }
            if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
                return clazz.cast(new io.realm.ListaIndicadoreRealmProxy());
            }
            throw getMissingProxyClassException(clazz);
        } finally {
            objectContext.clear();
        }
    }

    @Override
    public Set<Class<? extends RealmModel>> getModelClasses() {
        return MODEL_CLASSES;
    }

    @Override
    public <E extends RealmModel> E copyOrUpdate(Realm realm, E obj, boolean update, Map<RealmModel, RealmObjectProxy> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
            return clazz.cast(io.realm.ClientRealmProxy.copyOrUpdate(realm, (app.profuturo.reclasificacion.com.model.Client) obj, update, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
            return clazz.cast(io.realm.SaldosOriginalesRealmProxy.copyOrUpdate(realm, (app.profuturo.reclasificacion.com.model.SaldosOriginales) obj, update, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
            return clazz.cast(io.realm.SaldoRealmProxy.copyOrUpdate(realm, (app.profuturo.reclasificacion.com.model.Saldo) obj, update, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
            return clazz.cast(io.realm.SaveProcedureRequestRealmProxy.copyOrUpdate(realm, (app.profuturo.reclasificacion.com.model.SaveProcedureRequest) obj, update, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
            return clazz.cast(io.realm.ListReclasificacionRealmProxy.copyOrUpdate(realm, (app.profuturo.reclasificacion.com.model.ListReclasificacion) obj, update, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
            return clazz.cast(io.realm.SharesRealmProxy.copyOrUpdate(realm, (app.profuturo.reclasificacion.com.model.Shares) obj, update, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
            return clazz.cast(io.realm.SolicitanteRealmProxy.copyOrUpdate(realm, (app.profuturo.reclasificacion.com.model.Solicitante) obj, update, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
            return clazz.cast(io.realm.SavingsAccountRealmProxy.copyOrUpdate(realm, (app.profuturo.reclasificacion.com.model.SavingsAccount) obj, update, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
            return clazz.cast(io.realm.AgentSessionRealmProxy.copyOrUpdate(realm, (app.profuturo.reclasificacion.com.model.AgentSession) obj, update, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
            return clazz.cast(io.realm.ListaIndicadoreRealmProxy.copyOrUpdate(realm, (app.profuturo.reclasificacion.com.model.ListaIndicadore) obj, update, cache));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public void insert(Realm realm, RealmModel object, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

        if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
            io.realm.ClientRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.Client) object, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
            io.realm.SaldosOriginalesRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.SaldosOriginales) object, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
            io.realm.SaldoRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.Saldo) object, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
            io.realm.SaveProcedureRequestRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.SaveProcedureRequest) object, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
            io.realm.ListReclasificacionRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.ListReclasificacion) object, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
            io.realm.SharesRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.Shares) object, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
            io.realm.SolicitanteRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.Solicitante) object, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
            io.realm.SavingsAccountRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.SavingsAccount) object, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
            io.realm.AgentSessionRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.AgentSession) object, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
            io.realm.ListaIndicadoreRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.ListaIndicadore) object, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insert(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
                io.realm.ClientRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.Client) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
                io.realm.SaldosOriginalesRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.SaldosOriginales) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
                io.realm.SaldoRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.Saldo) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
                io.realm.SaveProcedureRequestRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.SaveProcedureRequest) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
                io.realm.ListReclasificacionRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.ListReclasificacion) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
                io.realm.SharesRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.Shares) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
                io.realm.SolicitanteRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.Solicitante) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
                io.realm.SavingsAccountRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.SavingsAccount) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
                io.realm.AgentSessionRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.AgentSession) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
                io.realm.ListaIndicadoreRealmProxy.insert(realm, (app.profuturo.reclasificacion.com.model.ListaIndicadore) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
                    io.realm.ClientRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
                    io.realm.SaldosOriginalesRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
                    io.realm.SaldoRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
                    io.realm.SaveProcedureRequestRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
                    io.realm.ListReclasificacionRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
                    io.realm.SharesRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
                    io.realm.SolicitanteRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
                    io.realm.SavingsAccountRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
                    io.realm.AgentSessionRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
                    io.realm.ListaIndicadoreRealmProxy.insert(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, RealmModel obj, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
            io.realm.ClientRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.Client) obj, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
            io.realm.SaldosOriginalesRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.SaldosOriginales) obj, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
            io.realm.SaldoRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.Saldo) obj, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
            io.realm.SaveProcedureRequestRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.SaveProcedureRequest) obj, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
            io.realm.ListReclasificacionRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.ListReclasificacion) obj, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
            io.realm.SharesRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.Shares) obj, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
            io.realm.SolicitanteRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.Solicitante) obj, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
            io.realm.SavingsAccountRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.SavingsAccount) obj, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
            io.realm.AgentSessionRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.AgentSession) obj, cache);
        } else if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
            io.realm.ListaIndicadoreRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.ListaIndicadore) obj, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
                io.realm.ClientRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.Client) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
                io.realm.SaldosOriginalesRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.SaldosOriginales) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
                io.realm.SaldoRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.Saldo) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
                io.realm.SaveProcedureRequestRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.SaveProcedureRequest) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
                io.realm.ListReclasificacionRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.ListReclasificacion) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
                io.realm.SharesRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.Shares) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
                io.realm.SolicitanteRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.Solicitante) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
                io.realm.SavingsAccountRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.SavingsAccount) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
                io.realm.AgentSessionRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.AgentSession) object, cache);
            } else if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
                io.realm.ListaIndicadoreRealmProxy.insertOrUpdate(realm, (app.profuturo.reclasificacion.com.model.ListaIndicadore) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
                    io.realm.ClientRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
                    io.realm.SaldosOriginalesRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
                    io.realm.SaldoRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
                    io.realm.SaveProcedureRequestRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
                    io.realm.ListReclasificacionRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
                    io.realm.SharesRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
                    io.realm.SolicitanteRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
                    io.realm.SavingsAccountRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
                    io.realm.AgentSessionRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
                    io.realm.ListaIndicadoreRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public <E extends RealmModel> E createOrUpdateUsingJsonObject(Class<E> clazz, Realm realm, JSONObject json, boolean update)
        throws JSONException {
        checkClass(clazz);

        if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
            return clazz.cast(io.realm.ClientRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
            return clazz.cast(io.realm.SaldosOriginalesRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
            return clazz.cast(io.realm.SaldoRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
            return clazz.cast(io.realm.SaveProcedureRequestRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
            return clazz.cast(io.realm.ListReclasificacionRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
            return clazz.cast(io.realm.SharesRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
            return clazz.cast(io.realm.SolicitanteRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
            return clazz.cast(io.realm.SavingsAccountRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
            return clazz.cast(io.realm.AgentSessionRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
            return clazz.cast(io.realm.ListaIndicadoreRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E createUsingJsonStream(Class<E> clazz, Realm realm, JsonReader reader)
        throws IOException {
        checkClass(clazz);

        if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
            return clazz.cast(io.realm.ClientRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
            return clazz.cast(io.realm.SaldosOriginalesRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
            return clazz.cast(io.realm.SaldoRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
            return clazz.cast(io.realm.SaveProcedureRequestRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
            return clazz.cast(io.realm.ListReclasificacionRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
            return clazz.cast(io.realm.SharesRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
            return clazz.cast(io.realm.SolicitanteRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
            return clazz.cast(io.realm.SavingsAccountRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
            return clazz.cast(io.realm.AgentSessionRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
            return clazz.cast(io.realm.ListaIndicadoreRealmProxy.createUsingJsonStream(realm, reader));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E createDetachedCopy(E realmObject, int maxDepth, Map<RealmModel, RealmObjectProxy.CacheData<RealmModel>> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) realmObject.getClass().getSuperclass();

        if (clazz.equals(app.profuturo.reclasificacion.com.model.Client.class)) {
            return clazz.cast(io.realm.ClientRealmProxy.createDetachedCopy((app.profuturo.reclasificacion.com.model.Client) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaldosOriginales.class)) {
            return clazz.cast(io.realm.SaldosOriginalesRealmProxy.createDetachedCopy((app.profuturo.reclasificacion.com.model.SaldosOriginales) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Saldo.class)) {
            return clazz.cast(io.realm.SaldoRealmProxy.createDetachedCopy((app.profuturo.reclasificacion.com.model.Saldo) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class)) {
            return clazz.cast(io.realm.SaveProcedureRequestRealmProxy.createDetachedCopy((app.profuturo.reclasificacion.com.model.SaveProcedureRequest) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListReclasificacion.class)) {
            return clazz.cast(io.realm.ListReclasificacionRealmProxy.createDetachedCopy((app.profuturo.reclasificacion.com.model.ListReclasificacion) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Shares.class)) {
            return clazz.cast(io.realm.SharesRealmProxy.createDetachedCopy((app.profuturo.reclasificacion.com.model.Shares) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.Solicitante.class)) {
            return clazz.cast(io.realm.SolicitanteRealmProxy.createDetachedCopy((app.profuturo.reclasificacion.com.model.Solicitante) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.SavingsAccount.class)) {
            return clazz.cast(io.realm.SavingsAccountRealmProxy.createDetachedCopy((app.profuturo.reclasificacion.com.model.SavingsAccount) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.AgentSession.class)) {
            return clazz.cast(io.realm.AgentSessionRealmProxy.createDetachedCopy((app.profuturo.reclasificacion.com.model.AgentSession) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(app.profuturo.reclasificacion.com.model.ListaIndicadore.class)) {
            return clazz.cast(io.realm.ListaIndicadoreRealmProxy.createDetachedCopy((app.profuturo.reclasificacion.com.model.ListaIndicadore) realmObject, 0, maxDepth, cache));
        }
        throw getMissingProxyClassException(clazz);
    }

}
