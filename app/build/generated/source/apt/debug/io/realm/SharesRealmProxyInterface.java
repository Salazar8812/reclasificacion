package io.realm;


public interface SharesRealmProxyInterface {
    public Integer realmGet$sharesCV();
    public void realmSet$sharesCV(Integer value);
    public Integer realmGet$sharesCS();
    public void realmSet$sharesCS(Integer value);
    public Integer realmGet$sharesR();
    public void realmSet$sharesR(Integer value);
    public Integer realmGet$sharesRetiro92();
    public void realmSet$sharesRetiro92(Integer value);
    public Integer realmGet$sharesVivienda92();
    public void realmSet$sharesVivienda92(Integer value);
    public Integer realmGet$sharesVivienda97();
    public void realmSet$sharesVivienda97(Integer value);
}
