package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.ProxyUtils;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class SaveProcedureRequestRealmProxy extends app.profuturo.reclasificacion.com.model.SaveProcedureRequest
    implements RealmObjectProxy, SaveProcedureRequestRealmProxyInterface {

    static final class SaveProcedureRequestColumnInfo extends ColumnInfo {
        long uuidIndex;
        long idEstatusIndex;
        long idSubprocesoIndex;
        long idEtapaIndex;
        long idSubetapaIndex;
        long idResultadoIndex;
        long usuarioIndex;
        long cveSolicitudIndex;
        long destinoNotificacionIndex;
        long fechaCapturaIndex;
        long folioFusIndex;
        long idEstatusSolicitudIndex;
        long idMotivoRechazoIndex;
        long idFondoAppIndex;
        long idMedioNotificacionIndex;
        long idOrigenCapturaIndex;
        long idTipoReclasificacionIndex;
        long idTipoSieforeIndex;
        long listReclasificacionIndex;
        long listaIndicadoresIndex;
        long saldosOriginalesIndex;
        long montoReclasificacionIndex;
        long numCtaInvdualIndex;
        long solicitanteIndex;
        long usuCapturaIndex;
        long usuCreIndex;

        SaveProcedureRequestColumnInfo(OsSchemaInfo schemaInfo) {
            super(26);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("SaveProcedureRequest");
            this.uuidIndex = addColumnDetails("uuid", objectSchemaInfo);
            this.idEstatusIndex = addColumnDetails("idEstatus", objectSchemaInfo);
            this.idSubprocesoIndex = addColumnDetails("idSubproceso", objectSchemaInfo);
            this.idEtapaIndex = addColumnDetails("idEtapa", objectSchemaInfo);
            this.idSubetapaIndex = addColumnDetails("idSubetapa", objectSchemaInfo);
            this.idResultadoIndex = addColumnDetails("idResultado", objectSchemaInfo);
            this.usuarioIndex = addColumnDetails("usuario", objectSchemaInfo);
            this.cveSolicitudIndex = addColumnDetails("cveSolicitud", objectSchemaInfo);
            this.destinoNotificacionIndex = addColumnDetails("destinoNotificacion", objectSchemaInfo);
            this.fechaCapturaIndex = addColumnDetails("fechaCaptura", objectSchemaInfo);
            this.folioFusIndex = addColumnDetails("folioFus", objectSchemaInfo);
            this.idEstatusSolicitudIndex = addColumnDetails("idEstatusSolicitud", objectSchemaInfo);
            this.idMotivoRechazoIndex = addColumnDetails("idMotivoRechazo", objectSchemaInfo);
            this.idFondoAppIndex = addColumnDetails("idFondoApp", objectSchemaInfo);
            this.idMedioNotificacionIndex = addColumnDetails("idMedioNotificacion", objectSchemaInfo);
            this.idOrigenCapturaIndex = addColumnDetails("idOrigenCaptura", objectSchemaInfo);
            this.idTipoReclasificacionIndex = addColumnDetails("idTipoReclasificacion", objectSchemaInfo);
            this.idTipoSieforeIndex = addColumnDetails("idTipoSiefore", objectSchemaInfo);
            this.listReclasificacionIndex = addColumnDetails("listReclasificacion", objectSchemaInfo);
            this.listaIndicadoresIndex = addColumnDetails("listaIndicadores", objectSchemaInfo);
            this.saldosOriginalesIndex = addColumnDetails("saldosOriginales", objectSchemaInfo);
            this.montoReclasificacionIndex = addColumnDetails("montoReclasificacion", objectSchemaInfo);
            this.numCtaInvdualIndex = addColumnDetails("numCtaInvdual", objectSchemaInfo);
            this.solicitanteIndex = addColumnDetails("solicitante", objectSchemaInfo);
            this.usuCapturaIndex = addColumnDetails("usuCaptura", objectSchemaInfo);
            this.usuCreIndex = addColumnDetails("usuCre", objectSchemaInfo);
        }

        SaveProcedureRequestColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new SaveProcedureRequestColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final SaveProcedureRequestColumnInfo src = (SaveProcedureRequestColumnInfo) rawSrc;
            final SaveProcedureRequestColumnInfo dst = (SaveProcedureRequestColumnInfo) rawDst;
            dst.uuidIndex = src.uuidIndex;
            dst.idEstatusIndex = src.idEstatusIndex;
            dst.idSubprocesoIndex = src.idSubprocesoIndex;
            dst.idEtapaIndex = src.idEtapaIndex;
            dst.idSubetapaIndex = src.idSubetapaIndex;
            dst.idResultadoIndex = src.idResultadoIndex;
            dst.usuarioIndex = src.usuarioIndex;
            dst.cveSolicitudIndex = src.cveSolicitudIndex;
            dst.destinoNotificacionIndex = src.destinoNotificacionIndex;
            dst.fechaCapturaIndex = src.fechaCapturaIndex;
            dst.folioFusIndex = src.folioFusIndex;
            dst.idEstatusSolicitudIndex = src.idEstatusSolicitudIndex;
            dst.idMotivoRechazoIndex = src.idMotivoRechazoIndex;
            dst.idFondoAppIndex = src.idFondoAppIndex;
            dst.idMedioNotificacionIndex = src.idMedioNotificacionIndex;
            dst.idOrigenCapturaIndex = src.idOrigenCapturaIndex;
            dst.idTipoReclasificacionIndex = src.idTipoReclasificacionIndex;
            dst.idTipoSieforeIndex = src.idTipoSieforeIndex;
            dst.listReclasificacionIndex = src.listReclasificacionIndex;
            dst.listaIndicadoresIndex = src.listaIndicadoresIndex;
            dst.saldosOriginalesIndex = src.saldosOriginalesIndex;
            dst.montoReclasificacionIndex = src.montoReclasificacionIndex;
            dst.numCtaInvdualIndex = src.numCtaInvdualIndex;
            dst.solicitanteIndex = src.solicitanteIndex;
            dst.usuCapturaIndex = src.usuCapturaIndex;
            dst.usuCreIndex = src.usuCreIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>(26);
        fieldNames.add("uuid");
        fieldNames.add("idEstatus");
        fieldNames.add("idSubproceso");
        fieldNames.add("idEtapa");
        fieldNames.add("idSubetapa");
        fieldNames.add("idResultado");
        fieldNames.add("usuario");
        fieldNames.add("cveSolicitud");
        fieldNames.add("destinoNotificacion");
        fieldNames.add("fechaCaptura");
        fieldNames.add("folioFus");
        fieldNames.add("idEstatusSolicitud");
        fieldNames.add("idMotivoRechazo");
        fieldNames.add("idFondoApp");
        fieldNames.add("idMedioNotificacion");
        fieldNames.add("idOrigenCaptura");
        fieldNames.add("idTipoReclasificacion");
        fieldNames.add("idTipoSiefore");
        fieldNames.add("listReclasificacion");
        fieldNames.add("listaIndicadores");
        fieldNames.add("saldosOriginales");
        fieldNames.add("montoReclasificacion");
        fieldNames.add("numCtaInvdual");
        fieldNames.add("solicitante");
        fieldNames.add("usuCaptura");
        fieldNames.add("usuCre");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    private SaveProcedureRequestColumnInfo columnInfo;
    private ProxyState<app.profuturo.reclasificacion.com.model.SaveProcedureRequest> proxyState;
    private RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> listReclasificacionRealmList;
    private RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> listaIndicadoresRealmList;
    private RealmList<app.profuturo.reclasificacion.com.model.Saldo> saldosOriginalesRealmList;

    SaveProcedureRequestRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (SaveProcedureRequestColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<app.profuturo.reclasificacion.com.model.SaveProcedureRequest>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$uuid() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.uuidIndex);
    }

    @Override
    public void realmSet$uuid(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'uuid' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idEstatus() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idEstatusIndex);
    }

    @Override
    public void realmSet$idEstatus(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idEstatusIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idEstatusIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idEstatusIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idEstatusIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idSubproceso() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idSubprocesoIndex);
    }

    @Override
    public void realmSet$idSubproceso(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idSubprocesoIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idSubprocesoIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idSubprocesoIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idSubprocesoIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idEtapa() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idEtapaIndex);
    }

    @Override
    public void realmSet$idEtapa(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idEtapaIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idEtapaIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idEtapaIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idEtapaIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idSubetapa() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idSubetapaIndex);
    }

    @Override
    public void realmSet$idSubetapa(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idSubetapaIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idSubetapaIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idSubetapaIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idSubetapaIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idResultado() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idResultadoIndex);
    }

    @Override
    public void realmSet$idResultado(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idResultadoIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idResultadoIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idResultadoIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idResultadoIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$usuario() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.usuarioIndex);
    }

    @Override
    public void realmSet$usuario(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.usuarioIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.usuarioIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.usuarioIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.usuarioIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$cveSolicitud() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.cveSolicitudIndex);
    }

    @Override
    public void realmSet$cveSolicitud(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.cveSolicitudIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.cveSolicitudIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.cveSolicitudIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.cveSolicitudIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$destinoNotificacion() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.destinoNotificacionIndex);
    }

    @Override
    public void realmSet$destinoNotificacion(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.destinoNotificacionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.destinoNotificacionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.destinoNotificacionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.destinoNotificacionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$fechaCaptura() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.fechaCapturaIndex);
    }

    @Override
    public void realmSet$fechaCaptura(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.fechaCapturaIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.fechaCapturaIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.fechaCapturaIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.fechaCapturaIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$folioFus() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.folioFusIndex);
    }

    @Override
    public void realmSet$folioFus(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.folioFusIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.folioFusIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.folioFusIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.folioFusIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idEstatusSolicitud() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idEstatusSolicitudIndex);
    }

    @Override
    public void realmSet$idEstatusSolicitud(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idEstatusSolicitudIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idEstatusSolicitudIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idEstatusSolicitudIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idEstatusSolicitudIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idMotivoRechazo() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idMotivoRechazoIndex);
    }

    @Override
    public void realmSet$idMotivoRechazo(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idMotivoRechazoIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idMotivoRechazoIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idMotivoRechazoIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idMotivoRechazoIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idFondoApp() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idFondoAppIndex);
    }

    @Override
    public void realmSet$idFondoApp(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idFondoAppIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idFondoAppIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idFondoAppIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idFondoAppIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idMedioNotificacion() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idMedioNotificacionIndex);
    }

    @Override
    public void realmSet$idMedioNotificacion(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idMedioNotificacionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idMedioNotificacionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idMedioNotificacionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idMedioNotificacionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idOrigenCaptura() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idOrigenCapturaIndex);
    }

    @Override
    public void realmSet$idOrigenCaptura(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idOrigenCapturaIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idOrigenCapturaIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idOrigenCapturaIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idOrigenCapturaIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idTipoReclasificacion() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idTipoReclasificacionIndex);
    }

    @Override
    public void realmSet$idTipoReclasificacion(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idTipoReclasificacionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idTipoReclasificacionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idTipoReclasificacionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idTipoReclasificacionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idTipoSiefore() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idTipoSieforeIndex);
    }

    @Override
    public void realmSet$idTipoSiefore(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idTipoSieforeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idTipoSieforeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idTipoSieforeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idTipoSieforeIndex, value);
    }

    @Override
    public RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> realmGet$listReclasificacion() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (listReclasificacionRealmList != null) {
            return listReclasificacionRealmList;
        } else {
            OsList osList = proxyState.getRow$realm().getModelList(columnInfo.listReclasificacionIndex);
            listReclasificacionRealmList = new RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion>(app.profuturo.reclasificacion.com.model.ListReclasificacion.class, osList, proxyState.getRealm$realm());
            return listReclasificacionRealmList;
        }
    }

    @Override
    public void realmSet$listReclasificacion(RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("listReclasificacion")) {
                return;
            }
            // if the list contains unmanaged RealmObjects, convert them to managed.
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> original = value;
                value = new RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion>();
                for (app.profuturo.reclasificacion.com.model.ListReclasificacion item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        OsList osList = proxyState.getRow$realm().getModelList(columnInfo.listReclasificacionIndex);
        // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
        if (value != null && value.size() == osList.size()) {
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                app.profuturo.reclasificacion.com.model.ListReclasificacion linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.setRow(i, ((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        } else {
            osList.removeAll();
            if (value == null) {
                return;
            }
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                app.profuturo.reclasificacion.com.model.ListReclasificacion linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.addRow(((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        }
    }

    @Override
    public RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> realmGet$listaIndicadores() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (listaIndicadoresRealmList != null) {
            return listaIndicadoresRealmList;
        } else {
            OsList osList = proxyState.getRow$realm().getModelList(columnInfo.listaIndicadoresIndex);
            listaIndicadoresRealmList = new RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore>(app.profuturo.reclasificacion.com.model.ListaIndicadore.class, osList, proxyState.getRealm$realm());
            return listaIndicadoresRealmList;
        }
    }

    @Override
    public void realmSet$listaIndicadores(RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("listaIndicadores")) {
                return;
            }
            // if the list contains unmanaged RealmObjects, convert them to managed.
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> original = value;
                value = new RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore>();
                for (app.profuturo.reclasificacion.com.model.ListaIndicadore item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        OsList osList = proxyState.getRow$realm().getModelList(columnInfo.listaIndicadoresIndex);
        // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
        if (value != null && value.size() == osList.size()) {
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                app.profuturo.reclasificacion.com.model.ListaIndicadore linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.setRow(i, ((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        } else {
            osList.removeAll();
            if (value == null) {
                return;
            }
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                app.profuturo.reclasificacion.com.model.ListaIndicadore linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.addRow(((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        }
    }

    @Override
    public RealmList<app.profuturo.reclasificacion.com.model.Saldo> realmGet$saldosOriginales() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (saldosOriginalesRealmList != null) {
            return saldosOriginalesRealmList;
        } else {
            OsList osList = proxyState.getRow$realm().getModelList(columnInfo.saldosOriginalesIndex);
            saldosOriginalesRealmList = new RealmList<app.profuturo.reclasificacion.com.model.Saldo>(app.profuturo.reclasificacion.com.model.Saldo.class, osList, proxyState.getRealm$realm());
            return saldosOriginalesRealmList;
        }
    }

    @Override
    public void realmSet$saldosOriginales(RealmList<app.profuturo.reclasificacion.com.model.Saldo> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("saldosOriginales")) {
                return;
            }
            // if the list contains unmanaged RealmObjects, convert them to managed.
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<app.profuturo.reclasificacion.com.model.Saldo> original = value;
                value = new RealmList<app.profuturo.reclasificacion.com.model.Saldo>();
                for (app.profuturo.reclasificacion.com.model.Saldo item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        OsList osList = proxyState.getRow$realm().getModelList(columnInfo.saldosOriginalesIndex);
        // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
        if (value != null && value.size() == osList.size()) {
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                app.profuturo.reclasificacion.com.model.Saldo linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.setRow(i, ((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        } else {
            osList.removeAll();
            if (value == null) {
                return;
            }
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                app.profuturo.reclasificacion.com.model.Saldo linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.addRow(((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        }
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$montoReclasificacion() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.montoReclasificacionIndex);
    }

    @Override
    public void realmSet$montoReclasificacion(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.montoReclasificacionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.montoReclasificacionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.montoReclasificacionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.montoReclasificacionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$numCtaInvdual() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.numCtaInvdualIndex);
    }

    @Override
    public void realmSet$numCtaInvdual(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.numCtaInvdualIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.numCtaInvdualIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.numCtaInvdualIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.numCtaInvdualIndex, value);
    }

    @Override
    public app.profuturo.reclasificacion.com.model.Solicitante realmGet$solicitante() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNullLink(columnInfo.solicitanteIndex)) {
            return null;
        }
        return proxyState.getRealm$realm().get(app.profuturo.reclasificacion.com.model.Solicitante.class, proxyState.getRow$realm().getLink(columnInfo.solicitanteIndex), false, Collections.<String>emptyList());
    }

    @Override
    public void realmSet$solicitante(app.profuturo.reclasificacion.com.model.Solicitante value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("solicitante")) {
                return;
            }
            if (value != null && !RealmObject.isManaged(value)) {
                value = ((Realm) proxyState.getRealm$realm()).copyToRealm(value);
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                // Table#nullifyLink() does not support default value. Just using Row.
                row.nullifyLink(columnInfo.solicitanteIndex);
                return;
            }
            proxyState.checkValidObject(value);
            row.getTable().setLink(columnInfo.solicitanteIndex, row.getIndex(), ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex(), true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().nullifyLink(columnInfo.solicitanteIndex);
            return;
        }
        proxyState.checkValidObject(value);
        proxyState.getRow$realm().setLink(columnInfo.solicitanteIndex, ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$usuCaptura() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.usuCapturaIndex);
    }

    @Override
    public void realmSet$usuCaptura(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.usuCapturaIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.usuCapturaIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.usuCapturaIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.usuCapturaIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$usuCre() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.usuCreIndex);
    }

    @Override
    public void realmSet$usuCre(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.usuCreIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.usuCreIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.usuCreIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.usuCreIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("SaveProcedureRequest", 26, 0);
        builder.addPersistedProperty("uuid", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idEstatus", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idSubproceso", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idEtapa", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idSubetapa", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idResultado", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("usuario", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("cveSolicitud", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("destinoNotificacion", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("fechaCaptura", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("folioFus", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idEstatusSolicitud", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idMotivoRechazo", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idFondoApp", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idMedioNotificacion", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idOrigenCaptura", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idTipoReclasificacion", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idTipoSiefore", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedLinkProperty("listReclasificacion", RealmFieldType.LIST, "ListReclasificacion");
        builder.addPersistedLinkProperty("listaIndicadores", RealmFieldType.LIST, "ListaIndicadore");
        builder.addPersistedLinkProperty("saldosOriginales", RealmFieldType.LIST, "Saldo");
        builder.addPersistedProperty("montoReclasificacion", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("numCtaInvdual", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedLinkProperty("solicitante", RealmFieldType.OBJECT, "Solicitante");
        builder.addPersistedProperty("usuCaptura", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("usuCre", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static SaveProcedureRequestColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new SaveProcedureRequestColumnInfo(schemaInfo);
    }

    public static String getTableName() {
        return "class_SaveProcedureRequest";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static app.profuturo.reclasificacion.com.model.SaveProcedureRequest createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = new ArrayList<String>(4);
        app.profuturo.reclasificacion.com.model.SaveProcedureRequest obj = null;
        if (update) {
            Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class);
            SaveProcedureRequestColumnInfo columnInfo = (SaveProcedureRequestColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class);
            long pkColumnIndex = columnInfo.uuidIndex;
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("uuid")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("uuid"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class), false, Collections.<String> emptyList());
                    obj = new io.realm.SaveProcedureRequestRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("listReclasificacion")) {
                excludeFields.add("listReclasificacion");
            }
            if (json.has("listaIndicadores")) {
                excludeFields.add("listaIndicadores");
            }
            if (json.has("saldosOriginales")) {
                excludeFields.add("saldosOriginales");
            }
            if (json.has("solicitante")) {
                excludeFields.add("solicitante");
            }
            if (json.has("uuid")) {
                if (json.isNull("uuid")) {
                    obj = (io.realm.SaveProcedureRequestRealmProxy) realm.createObjectInternal(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.SaveProcedureRequestRealmProxy) realm.createObjectInternal(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class, json.getString("uuid"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'uuid'.");
            }
        }

        final SaveProcedureRequestRealmProxyInterface objProxy = (SaveProcedureRequestRealmProxyInterface) obj;
        if (json.has("idEstatus")) {
            if (json.isNull("idEstatus")) {
                objProxy.realmSet$idEstatus(null);
            } else {
                objProxy.realmSet$idEstatus((String) json.getString("idEstatus"));
            }
        }
        if (json.has("idSubproceso")) {
            if (json.isNull("idSubproceso")) {
                objProxy.realmSet$idSubproceso(null);
            } else {
                objProxy.realmSet$idSubproceso((String) json.getString("idSubproceso"));
            }
        }
        if (json.has("idEtapa")) {
            if (json.isNull("idEtapa")) {
                objProxy.realmSet$idEtapa(null);
            } else {
                objProxy.realmSet$idEtapa((String) json.getString("idEtapa"));
            }
        }
        if (json.has("idSubetapa")) {
            if (json.isNull("idSubetapa")) {
                objProxy.realmSet$idSubetapa(null);
            } else {
                objProxy.realmSet$idSubetapa((String) json.getString("idSubetapa"));
            }
        }
        if (json.has("idResultado")) {
            if (json.isNull("idResultado")) {
                objProxy.realmSet$idResultado(null);
            } else {
                objProxy.realmSet$idResultado((String) json.getString("idResultado"));
            }
        }
        if (json.has("usuario")) {
            if (json.isNull("usuario")) {
                objProxy.realmSet$usuario(null);
            } else {
                objProxy.realmSet$usuario((String) json.getString("usuario"));
            }
        }
        if (json.has("cveSolicitud")) {
            if (json.isNull("cveSolicitud")) {
                objProxy.realmSet$cveSolicitud(null);
            } else {
                objProxy.realmSet$cveSolicitud((String) json.getString("cveSolicitud"));
            }
        }
        if (json.has("destinoNotificacion")) {
            if (json.isNull("destinoNotificacion")) {
                objProxy.realmSet$destinoNotificacion(null);
            } else {
                objProxy.realmSet$destinoNotificacion((String) json.getString("destinoNotificacion"));
            }
        }
        if (json.has("fechaCaptura")) {
            if (json.isNull("fechaCaptura")) {
                objProxy.realmSet$fechaCaptura(null);
            } else {
                objProxy.realmSet$fechaCaptura((String) json.getString("fechaCaptura"));
            }
        }
        if (json.has("folioFus")) {
            if (json.isNull("folioFus")) {
                objProxy.realmSet$folioFus(null);
            } else {
                objProxy.realmSet$folioFus((String) json.getString("folioFus"));
            }
        }
        if (json.has("idEstatusSolicitud")) {
            if (json.isNull("idEstatusSolicitud")) {
                objProxy.realmSet$idEstatusSolicitud(null);
            } else {
                objProxy.realmSet$idEstatusSolicitud((String) json.getString("idEstatusSolicitud"));
            }
        }
        if (json.has("idMotivoRechazo")) {
            if (json.isNull("idMotivoRechazo")) {
                objProxy.realmSet$idMotivoRechazo(null);
            } else {
                objProxy.realmSet$idMotivoRechazo((String) json.getString("idMotivoRechazo"));
            }
        }
        if (json.has("idFondoApp")) {
            if (json.isNull("idFondoApp")) {
                objProxy.realmSet$idFondoApp(null);
            } else {
                objProxy.realmSet$idFondoApp((String) json.getString("idFondoApp"));
            }
        }
        if (json.has("idMedioNotificacion")) {
            if (json.isNull("idMedioNotificacion")) {
                objProxy.realmSet$idMedioNotificacion(null);
            } else {
                objProxy.realmSet$idMedioNotificacion((String) json.getString("idMedioNotificacion"));
            }
        }
        if (json.has("idOrigenCaptura")) {
            if (json.isNull("idOrigenCaptura")) {
                objProxy.realmSet$idOrigenCaptura(null);
            } else {
                objProxy.realmSet$idOrigenCaptura((String) json.getString("idOrigenCaptura"));
            }
        }
        if (json.has("idTipoReclasificacion")) {
            if (json.isNull("idTipoReclasificacion")) {
                objProxy.realmSet$idTipoReclasificacion(null);
            } else {
                objProxy.realmSet$idTipoReclasificacion((String) json.getString("idTipoReclasificacion"));
            }
        }
        if (json.has("idTipoSiefore")) {
            if (json.isNull("idTipoSiefore")) {
                objProxy.realmSet$idTipoSiefore(null);
            } else {
                objProxy.realmSet$idTipoSiefore((String) json.getString("idTipoSiefore"));
            }
        }
        if (json.has("listReclasificacion")) {
            if (json.isNull("listReclasificacion")) {
                objProxy.realmSet$listReclasificacion(null);
            } else {
                objProxy.realmGet$listReclasificacion().clear();
                JSONArray array = json.getJSONArray("listReclasificacion");
                for (int i = 0; i < array.length(); i++) {
                    app.profuturo.reclasificacion.com.model.ListReclasificacion item = ListReclasificacionRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    objProxy.realmGet$listReclasificacion().add(item);
                }
            }
        }
        if (json.has("listaIndicadores")) {
            if (json.isNull("listaIndicadores")) {
                objProxy.realmSet$listaIndicadores(null);
            } else {
                objProxy.realmGet$listaIndicadores().clear();
                JSONArray array = json.getJSONArray("listaIndicadores");
                for (int i = 0; i < array.length(); i++) {
                    app.profuturo.reclasificacion.com.model.ListaIndicadore item = ListaIndicadoreRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    objProxy.realmGet$listaIndicadores().add(item);
                }
            }
        }
        if (json.has("saldosOriginales")) {
            if (json.isNull("saldosOriginales")) {
                objProxy.realmSet$saldosOriginales(null);
            } else {
                objProxy.realmGet$saldosOriginales().clear();
                JSONArray array = json.getJSONArray("saldosOriginales");
                for (int i = 0; i < array.length(); i++) {
                    app.profuturo.reclasificacion.com.model.Saldo item = SaldoRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    objProxy.realmGet$saldosOriginales().add(item);
                }
            }
        }
        if (json.has("montoReclasificacion")) {
            if (json.isNull("montoReclasificacion")) {
                objProxy.realmSet$montoReclasificacion(null);
            } else {
                objProxy.realmSet$montoReclasificacion((String) json.getString("montoReclasificacion"));
            }
        }
        if (json.has("numCtaInvdual")) {
            if (json.isNull("numCtaInvdual")) {
                objProxy.realmSet$numCtaInvdual(null);
            } else {
                objProxy.realmSet$numCtaInvdual((String) json.getString("numCtaInvdual"));
            }
        }
        if (json.has("solicitante")) {
            if (json.isNull("solicitante")) {
                objProxy.realmSet$solicitante(null);
            } else {
                app.profuturo.reclasificacion.com.model.Solicitante solicitanteObj = SolicitanteRealmProxy.createOrUpdateUsingJsonObject(realm, json.getJSONObject("solicitante"), update);
                objProxy.realmSet$solicitante(solicitanteObj);
            }
        }
        if (json.has("usuCaptura")) {
            if (json.isNull("usuCaptura")) {
                objProxy.realmSet$usuCaptura(null);
            } else {
                objProxy.realmSet$usuCaptura((String) json.getString("usuCaptura"));
            }
        }
        if (json.has("usuCre")) {
            if (json.isNull("usuCre")) {
                objProxy.realmSet$usuCre(null);
            } else {
                objProxy.realmSet$usuCre((String) json.getString("usuCre"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static app.profuturo.reclasificacion.com.model.SaveProcedureRequest createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final app.profuturo.reclasificacion.com.model.SaveProcedureRequest obj = new app.profuturo.reclasificacion.com.model.SaveProcedureRequest();
        final SaveProcedureRequestRealmProxyInterface objProxy = (SaveProcedureRequestRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("uuid")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$uuid((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$uuid(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("idEstatus")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idEstatus((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idEstatus(null);
                }
            } else if (name.equals("idSubproceso")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idSubproceso((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idSubproceso(null);
                }
            } else if (name.equals("idEtapa")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idEtapa((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idEtapa(null);
                }
            } else if (name.equals("idSubetapa")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idSubetapa((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idSubetapa(null);
                }
            } else if (name.equals("idResultado")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idResultado((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idResultado(null);
                }
            } else if (name.equals("usuario")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$usuario((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$usuario(null);
                }
            } else if (name.equals("cveSolicitud")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$cveSolicitud((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$cveSolicitud(null);
                }
            } else if (name.equals("destinoNotificacion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$destinoNotificacion((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$destinoNotificacion(null);
                }
            } else if (name.equals("fechaCaptura")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$fechaCaptura((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$fechaCaptura(null);
                }
            } else if (name.equals("folioFus")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$folioFus((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$folioFus(null);
                }
            } else if (name.equals("idEstatusSolicitud")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idEstatusSolicitud((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idEstatusSolicitud(null);
                }
            } else if (name.equals("idMotivoRechazo")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idMotivoRechazo((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idMotivoRechazo(null);
                }
            } else if (name.equals("idFondoApp")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idFondoApp((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idFondoApp(null);
                }
            } else if (name.equals("idMedioNotificacion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idMedioNotificacion((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idMedioNotificacion(null);
                }
            } else if (name.equals("idOrigenCaptura")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idOrigenCaptura((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idOrigenCaptura(null);
                }
            } else if (name.equals("idTipoReclasificacion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idTipoReclasificacion((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idTipoReclasificacion(null);
                }
            } else if (name.equals("idTipoSiefore")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idTipoSiefore((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idTipoSiefore(null);
                }
            } else if (name.equals("listReclasificacion")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$listReclasificacion(null);
                } else {
                    objProxy.realmSet$listReclasificacion(new RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        app.profuturo.reclasificacion.com.model.ListReclasificacion item = ListReclasificacionRealmProxy.createUsingJsonStream(realm, reader);
                        objProxy.realmGet$listReclasificacion().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("listaIndicadores")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$listaIndicadores(null);
                } else {
                    objProxy.realmSet$listaIndicadores(new RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        app.profuturo.reclasificacion.com.model.ListaIndicadore item = ListaIndicadoreRealmProxy.createUsingJsonStream(realm, reader);
                        objProxy.realmGet$listaIndicadores().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("saldosOriginales")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$saldosOriginales(null);
                } else {
                    objProxy.realmSet$saldosOriginales(new RealmList<app.profuturo.reclasificacion.com.model.Saldo>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        app.profuturo.reclasificacion.com.model.Saldo item = SaldoRealmProxy.createUsingJsonStream(realm, reader);
                        objProxy.realmGet$saldosOriginales().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("montoReclasificacion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$montoReclasificacion((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$montoReclasificacion(null);
                }
            } else if (name.equals("numCtaInvdual")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$numCtaInvdual((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$numCtaInvdual(null);
                }
            } else if (name.equals("solicitante")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$solicitante(null);
                } else {
                    app.profuturo.reclasificacion.com.model.Solicitante solicitanteObj = SolicitanteRealmProxy.createUsingJsonStream(realm, reader);
                    objProxy.realmSet$solicitante(solicitanteObj);
                }
            } else if (name.equals("usuCaptura")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$usuCaptura((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$usuCaptura(null);
                }
            } else if (name.equals("usuCre")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$usuCre((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$usuCre(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'uuid'.");
        }
        return realm.copyToRealm(obj);
    }

    public static app.profuturo.reclasificacion.com.model.SaveProcedureRequest copyOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.SaveProcedureRequest object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.SaveProcedureRequest) cachedRealmObject;
        }

        app.profuturo.reclasificacion.com.model.SaveProcedureRequest realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class);
            SaveProcedureRequestColumnInfo columnInfo = (SaveProcedureRequestColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class);
            long pkColumnIndex = columnInfo.uuidIndex;
            String value = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$uuid();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, value);
            }
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.SaveProcedureRequestRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static app.profuturo.reclasificacion.com.model.SaveProcedureRequest copy(Realm realm, app.profuturo.reclasificacion.com.model.SaveProcedureRequest newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.SaveProcedureRequest) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        app.profuturo.reclasificacion.com.model.SaveProcedureRequest realmObject = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class, ((SaveProcedureRequestRealmProxyInterface) newObject).realmGet$uuid(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        SaveProcedureRequestRealmProxyInterface realmObjectSource = (SaveProcedureRequestRealmProxyInterface) newObject;
        SaveProcedureRequestRealmProxyInterface realmObjectCopy = (SaveProcedureRequestRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$idEstatus(realmObjectSource.realmGet$idEstatus());
        realmObjectCopy.realmSet$idSubproceso(realmObjectSource.realmGet$idSubproceso());
        realmObjectCopy.realmSet$idEtapa(realmObjectSource.realmGet$idEtapa());
        realmObjectCopy.realmSet$idSubetapa(realmObjectSource.realmGet$idSubetapa());
        realmObjectCopy.realmSet$idResultado(realmObjectSource.realmGet$idResultado());
        realmObjectCopy.realmSet$usuario(realmObjectSource.realmGet$usuario());
        realmObjectCopy.realmSet$cveSolicitud(realmObjectSource.realmGet$cveSolicitud());
        realmObjectCopy.realmSet$destinoNotificacion(realmObjectSource.realmGet$destinoNotificacion());
        realmObjectCopy.realmSet$fechaCaptura(realmObjectSource.realmGet$fechaCaptura());
        realmObjectCopy.realmSet$folioFus(realmObjectSource.realmGet$folioFus());
        realmObjectCopy.realmSet$idEstatusSolicitud(realmObjectSource.realmGet$idEstatusSolicitud());
        realmObjectCopy.realmSet$idMotivoRechazo(realmObjectSource.realmGet$idMotivoRechazo());
        realmObjectCopy.realmSet$idFondoApp(realmObjectSource.realmGet$idFondoApp());
        realmObjectCopy.realmSet$idMedioNotificacion(realmObjectSource.realmGet$idMedioNotificacion());
        realmObjectCopy.realmSet$idOrigenCaptura(realmObjectSource.realmGet$idOrigenCaptura());
        realmObjectCopy.realmSet$idTipoReclasificacion(realmObjectSource.realmGet$idTipoReclasificacion());
        realmObjectCopy.realmSet$idTipoSiefore(realmObjectSource.realmGet$idTipoSiefore());

        RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> listReclasificacionList = realmObjectSource.realmGet$listReclasificacion();
        if (listReclasificacionList != null) {
            RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> listReclasificacionRealmList = realmObjectCopy.realmGet$listReclasificacion();
            listReclasificacionRealmList.clear();
            for (int i = 0; i < listReclasificacionList.size(); i++) {
                app.profuturo.reclasificacion.com.model.ListReclasificacion listReclasificacionItem = listReclasificacionList.get(i);
                app.profuturo.reclasificacion.com.model.ListReclasificacion cachelistReclasificacion = (app.profuturo.reclasificacion.com.model.ListReclasificacion) cache.get(listReclasificacionItem);
                if (cachelistReclasificacion != null) {
                    listReclasificacionRealmList.add(cachelistReclasificacion);
                } else {
                    listReclasificacionRealmList.add(ListReclasificacionRealmProxy.copyOrUpdate(realm, listReclasificacionItem, update, cache));
                }
            }
        }


        RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> listaIndicadoresList = realmObjectSource.realmGet$listaIndicadores();
        if (listaIndicadoresList != null) {
            RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> listaIndicadoresRealmList = realmObjectCopy.realmGet$listaIndicadores();
            listaIndicadoresRealmList.clear();
            for (int i = 0; i < listaIndicadoresList.size(); i++) {
                app.profuturo.reclasificacion.com.model.ListaIndicadore listaIndicadoresItem = listaIndicadoresList.get(i);
                app.profuturo.reclasificacion.com.model.ListaIndicadore cachelistaIndicadores = (app.profuturo.reclasificacion.com.model.ListaIndicadore) cache.get(listaIndicadoresItem);
                if (cachelistaIndicadores != null) {
                    listaIndicadoresRealmList.add(cachelistaIndicadores);
                } else {
                    listaIndicadoresRealmList.add(ListaIndicadoreRealmProxy.copyOrUpdate(realm, listaIndicadoresItem, update, cache));
                }
            }
        }


        RealmList<app.profuturo.reclasificacion.com.model.Saldo> saldosOriginalesList = realmObjectSource.realmGet$saldosOriginales();
        if (saldosOriginalesList != null) {
            RealmList<app.profuturo.reclasificacion.com.model.Saldo> saldosOriginalesRealmList = realmObjectCopy.realmGet$saldosOriginales();
            saldosOriginalesRealmList.clear();
            for (int i = 0; i < saldosOriginalesList.size(); i++) {
                app.profuturo.reclasificacion.com.model.Saldo saldosOriginalesItem = saldosOriginalesList.get(i);
                app.profuturo.reclasificacion.com.model.Saldo cachesaldosOriginales = (app.profuturo.reclasificacion.com.model.Saldo) cache.get(saldosOriginalesItem);
                if (cachesaldosOriginales != null) {
                    saldosOriginalesRealmList.add(cachesaldosOriginales);
                } else {
                    saldosOriginalesRealmList.add(SaldoRealmProxy.copyOrUpdate(realm, saldosOriginalesItem, update, cache));
                }
            }
        }

        realmObjectCopy.realmSet$montoReclasificacion(realmObjectSource.realmGet$montoReclasificacion());
        realmObjectCopy.realmSet$numCtaInvdual(realmObjectSource.realmGet$numCtaInvdual());

        app.profuturo.reclasificacion.com.model.Solicitante solicitanteObj = realmObjectSource.realmGet$solicitante();
        if (solicitanteObj == null) {
            realmObjectCopy.realmSet$solicitante(null);
        } else {
            app.profuturo.reclasificacion.com.model.Solicitante cachesolicitante = (app.profuturo.reclasificacion.com.model.Solicitante) cache.get(solicitanteObj);
            if (cachesolicitante != null) {
                realmObjectCopy.realmSet$solicitante(cachesolicitante);
            } else {
                realmObjectCopy.realmSet$solicitante(SolicitanteRealmProxy.copyOrUpdate(realm, solicitanteObj, update, cache));
            }
        }
        realmObjectCopy.realmSet$usuCaptura(realmObjectSource.realmGet$usuCaptura());
        realmObjectCopy.realmSet$usuCre(realmObjectSource.realmGet$usuCre());
        return realmObject;
    }

    public static long insert(Realm realm, app.profuturo.reclasificacion.com.model.SaveProcedureRequest object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class);
        long tableNativePtr = table.getNativePtr();
        SaveProcedureRequestColumnInfo columnInfo = (SaveProcedureRequestColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class);
        long pkColumnIndex = columnInfo.uuidIndex;
        String primaryKeyValue = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$uuid();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$idEstatus = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idEstatus();
        if (realmGet$idEstatus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idEstatusIndex, rowIndex, realmGet$idEstatus, false);
        }
        String realmGet$idSubproceso = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idSubproceso();
        if (realmGet$idSubproceso != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idSubprocesoIndex, rowIndex, realmGet$idSubproceso, false);
        }
        String realmGet$idEtapa = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idEtapa();
        if (realmGet$idEtapa != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idEtapaIndex, rowIndex, realmGet$idEtapa, false);
        }
        String realmGet$idSubetapa = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idSubetapa();
        if (realmGet$idSubetapa != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idSubetapaIndex, rowIndex, realmGet$idSubetapa, false);
        }
        String realmGet$idResultado = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idResultado();
        if (realmGet$idResultado != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idResultadoIndex, rowIndex, realmGet$idResultado, false);
        }
        String realmGet$usuario = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$usuario();
        if (realmGet$usuario != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.usuarioIndex, rowIndex, realmGet$usuario, false);
        }
        String realmGet$cveSolicitud = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$cveSolicitud();
        if (realmGet$cveSolicitud != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.cveSolicitudIndex, rowIndex, realmGet$cveSolicitud, false);
        }
        String realmGet$destinoNotificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$destinoNotificacion();
        if (realmGet$destinoNotificacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.destinoNotificacionIndex, rowIndex, realmGet$destinoNotificacion, false);
        }
        String realmGet$fechaCaptura = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$fechaCaptura();
        if (realmGet$fechaCaptura != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fechaCapturaIndex, rowIndex, realmGet$fechaCaptura, false);
        }
        String realmGet$folioFus = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$folioFus();
        if (realmGet$folioFus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.folioFusIndex, rowIndex, realmGet$folioFus, false);
        }
        String realmGet$idEstatusSolicitud = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idEstatusSolicitud();
        if (realmGet$idEstatusSolicitud != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idEstatusSolicitudIndex, rowIndex, realmGet$idEstatusSolicitud, false);
        }
        String realmGet$idMotivoRechazo = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idMotivoRechazo();
        if (realmGet$idMotivoRechazo != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idMotivoRechazoIndex, rowIndex, realmGet$idMotivoRechazo, false);
        }
        String realmGet$idFondoApp = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idFondoApp();
        if (realmGet$idFondoApp != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idFondoAppIndex, rowIndex, realmGet$idFondoApp, false);
        }
        String realmGet$idMedioNotificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idMedioNotificacion();
        if (realmGet$idMedioNotificacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idMedioNotificacionIndex, rowIndex, realmGet$idMedioNotificacion, false);
        }
        String realmGet$idOrigenCaptura = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idOrigenCaptura();
        if (realmGet$idOrigenCaptura != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idOrigenCapturaIndex, rowIndex, realmGet$idOrigenCaptura, false);
        }
        String realmGet$idTipoReclasificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idTipoReclasificacion();
        if (realmGet$idTipoReclasificacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idTipoReclasificacionIndex, rowIndex, realmGet$idTipoReclasificacion, false);
        }
        String realmGet$idTipoSiefore = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idTipoSiefore();
        if (realmGet$idTipoSiefore != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idTipoSieforeIndex, rowIndex, realmGet$idTipoSiefore, false);
        }

        RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> listReclasificacionList = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$listReclasificacion();
        if (listReclasificacionList != null) {
            OsList listReclasificacionOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.listReclasificacionIndex);
            for (app.profuturo.reclasificacion.com.model.ListReclasificacion listReclasificacionItem : listReclasificacionList) {
                Long cacheItemIndexlistReclasificacion = cache.get(listReclasificacionItem);
                if (cacheItemIndexlistReclasificacion == null) {
                    cacheItemIndexlistReclasificacion = ListReclasificacionRealmProxy.insert(realm, listReclasificacionItem, cache);
                }
                listReclasificacionOsList.addRow(cacheItemIndexlistReclasificacion);
            }
        }

        RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> listaIndicadoresList = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$listaIndicadores();
        if (listaIndicadoresList != null) {
            OsList listaIndicadoresOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.listaIndicadoresIndex);
            for (app.profuturo.reclasificacion.com.model.ListaIndicadore listaIndicadoresItem : listaIndicadoresList) {
                Long cacheItemIndexlistaIndicadores = cache.get(listaIndicadoresItem);
                if (cacheItemIndexlistaIndicadores == null) {
                    cacheItemIndexlistaIndicadores = ListaIndicadoreRealmProxy.insert(realm, listaIndicadoresItem, cache);
                }
                listaIndicadoresOsList.addRow(cacheItemIndexlistaIndicadores);
            }
        }

        RealmList<app.profuturo.reclasificacion.com.model.Saldo> saldosOriginalesList = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$saldosOriginales();
        if (saldosOriginalesList != null) {
            OsList saldosOriginalesOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.saldosOriginalesIndex);
            for (app.profuturo.reclasificacion.com.model.Saldo saldosOriginalesItem : saldosOriginalesList) {
                Long cacheItemIndexsaldosOriginales = cache.get(saldosOriginalesItem);
                if (cacheItemIndexsaldosOriginales == null) {
                    cacheItemIndexsaldosOriginales = SaldoRealmProxy.insert(realm, saldosOriginalesItem, cache);
                }
                saldosOriginalesOsList.addRow(cacheItemIndexsaldosOriginales);
            }
        }
        String realmGet$montoReclasificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$montoReclasificacion();
        if (realmGet$montoReclasificacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.montoReclasificacionIndex, rowIndex, realmGet$montoReclasificacion, false);
        }
        String realmGet$numCtaInvdual = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$numCtaInvdual();
        if (realmGet$numCtaInvdual != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.numCtaInvdualIndex, rowIndex, realmGet$numCtaInvdual, false);
        }

        app.profuturo.reclasificacion.com.model.Solicitante solicitanteObj = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$solicitante();
        if (solicitanteObj != null) {
            Long cachesolicitante = cache.get(solicitanteObj);
            if (cachesolicitante == null) {
                cachesolicitante = SolicitanteRealmProxy.insert(realm, solicitanteObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.solicitanteIndex, rowIndex, cachesolicitante, false);
        }
        String realmGet$usuCaptura = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$usuCaptura();
        if (realmGet$usuCaptura != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.usuCapturaIndex, rowIndex, realmGet$usuCaptura, false);
        }
        String realmGet$usuCre = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$usuCre();
        if (realmGet$usuCre != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.usuCreIndex, rowIndex, realmGet$usuCre, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class);
        long tableNativePtr = table.getNativePtr();
        SaveProcedureRequestColumnInfo columnInfo = (SaveProcedureRequestColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class);
        long pkColumnIndex = columnInfo.uuidIndex;
        app.profuturo.reclasificacion.com.model.SaveProcedureRequest object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.SaveProcedureRequest) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$uuid();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$idEstatus = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idEstatus();
            if (realmGet$idEstatus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idEstatusIndex, rowIndex, realmGet$idEstatus, false);
            }
            String realmGet$idSubproceso = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idSubproceso();
            if (realmGet$idSubproceso != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idSubprocesoIndex, rowIndex, realmGet$idSubproceso, false);
            }
            String realmGet$idEtapa = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idEtapa();
            if (realmGet$idEtapa != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idEtapaIndex, rowIndex, realmGet$idEtapa, false);
            }
            String realmGet$idSubetapa = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idSubetapa();
            if (realmGet$idSubetapa != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idSubetapaIndex, rowIndex, realmGet$idSubetapa, false);
            }
            String realmGet$idResultado = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idResultado();
            if (realmGet$idResultado != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idResultadoIndex, rowIndex, realmGet$idResultado, false);
            }
            String realmGet$usuario = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$usuario();
            if (realmGet$usuario != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.usuarioIndex, rowIndex, realmGet$usuario, false);
            }
            String realmGet$cveSolicitud = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$cveSolicitud();
            if (realmGet$cveSolicitud != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.cveSolicitudIndex, rowIndex, realmGet$cveSolicitud, false);
            }
            String realmGet$destinoNotificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$destinoNotificacion();
            if (realmGet$destinoNotificacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.destinoNotificacionIndex, rowIndex, realmGet$destinoNotificacion, false);
            }
            String realmGet$fechaCaptura = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$fechaCaptura();
            if (realmGet$fechaCaptura != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.fechaCapturaIndex, rowIndex, realmGet$fechaCaptura, false);
            }
            String realmGet$folioFus = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$folioFus();
            if (realmGet$folioFus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.folioFusIndex, rowIndex, realmGet$folioFus, false);
            }
            String realmGet$idEstatusSolicitud = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idEstatusSolicitud();
            if (realmGet$idEstatusSolicitud != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idEstatusSolicitudIndex, rowIndex, realmGet$idEstatusSolicitud, false);
            }
            String realmGet$idMotivoRechazo = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idMotivoRechazo();
            if (realmGet$idMotivoRechazo != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idMotivoRechazoIndex, rowIndex, realmGet$idMotivoRechazo, false);
            }
            String realmGet$idFondoApp = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idFondoApp();
            if (realmGet$idFondoApp != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idFondoAppIndex, rowIndex, realmGet$idFondoApp, false);
            }
            String realmGet$idMedioNotificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idMedioNotificacion();
            if (realmGet$idMedioNotificacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idMedioNotificacionIndex, rowIndex, realmGet$idMedioNotificacion, false);
            }
            String realmGet$idOrigenCaptura = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idOrigenCaptura();
            if (realmGet$idOrigenCaptura != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idOrigenCapturaIndex, rowIndex, realmGet$idOrigenCaptura, false);
            }
            String realmGet$idTipoReclasificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idTipoReclasificacion();
            if (realmGet$idTipoReclasificacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idTipoReclasificacionIndex, rowIndex, realmGet$idTipoReclasificacion, false);
            }
            String realmGet$idTipoSiefore = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idTipoSiefore();
            if (realmGet$idTipoSiefore != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idTipoSieforeIndex, rowIndex, realmGet$idTipoSiefore, false);
            }

            RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> listReclasificacionList = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$listReclasificacion();
            if (listReclasificacionList != null) {
                OsList listReclasificacionOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.listReclasificacionIndex);
                for (app.profuturo.reclasificacion.com.model.ListReclasificacion listReclasificacionItem : listReclasificacionList) {
                    Long cacheItemIndexlistReclasificacion = cache.get(listReclasificacionItem);
                    if (cacheItemIndexlistReclasificacion == null) {
                        cacheItemIndexlistReclasificacion = ListReclasificacionRealmProxy.insert(realm, listReclasificacionItem, cache);
                    }
                    listReclasificacionOsList.addRow(cacheItemIndexlistReclasificacion);
                }
            }

            RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> listaIndicadoresList = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$listaIndicadores();
            if (listaIndicadoresList != null) {
                OsList listaIndicadoresOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.listaIndicadoresIndex);
                for (app.profuturo.reclasificacion.com.model.ListaIndicadore listaIndicadoresItem : listaIndicadoresList) {
                    Long cacheItemIndexlistaIndicadores = cache.get(listaIndicadoresItem);
                    if (cacheItemIndexlistaIndicadores == null) {
                        cacheItemIndexlistaIndicadores = ListaIndicadoreRealmProxy.insert(realm, listaIndicadoresItem, cache);
                    }
                    listaIndicadoresOsList.addRow(cacheItemIndexlistaIndicadores);
                }
            }

            RealmList<app.profuturo.reclasificacion.com.model.Saldo> saldosOriginalesList = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$saldosOriginales();
            if (saldosOriginalesList != null) {
                OsList saldosOriginalesOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.saldosOriginalesIndex);
                for (app.profuturo.reclasificacion.com.model.Saldo saldosOriginalesItem : saldosOriginalesList) {
                    Long cacheItemIndexsaldosOriginales = cache.get(saldosOriginalesItem);
                    if (cacheItemIndexsaldosOriginales == null) {
                        cacheItemIndexsaldosOriginales = SaldoRealmProxy.insert(realm, saldosOriginalesItem, cache);
                    }
                    saldosOriginalesOsList.addRow(cacheItemIndexsaldosOriginales);
                }
            }
            String realmGet$montoReclasificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$montoReclasificacion();
            if (realmGet$montoReclasificacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.montoReclasificacionIndex, rowIndex, realmGet$montoReclasificacion, false);
            }
            String realmGet$numCtaInvdual = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$numCtaInvdual();
            if (realmGet$numCtaInvdual != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.numCtaInvdualIndex, rowIndex, realmGet$numCtaInvdual, false);
            }

            app.profuturo.reclasificacion.com.model.Solicitante solicitanteObj = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$solicitante();
            if (solicitanteObj != null) {
                Long cachesolicitante = cache.get(solicitanteObj);
                if (cachesolicitante == null) {
                    cachesolicitante = SolicitanteRealmProxy.insert(realm, solicitanteObj, cache);
                }
                table.setLink(columnInfo.solicitanteIndex, rowIndex, cachesolicitante, false);
            }
            String realmGet$usuCaptura = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$usuCaptura();
            if (realmGet$usuCaptura != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.usuCapturaIndex, rowIndex, realmGet$usuCaptura, false);
            }
            String realmGet$usuCre = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$usuCre();
            if (realmGet$usuCre != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.usuCreIndex, rowIndex, realmGet$usuCre, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.SaveProcedureRequest object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class);
        long tableNativePtr = table.getNativePtr();
        SaveProcedureRequestColumnInfo columnInfo = (SaveProcedureRequestColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class);
        long pkColumnIndex = columnInfo.uuidIndex;
        String primaryKeyValue = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$uuid();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$idEstatus = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idEstatus();
        if (realmGet$idEstatus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idEstatusIndex, rowIndex, realmGet$idEstatus, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idEstatusIndex, rowIndex, false);
        }
        String realmGet$idSubproceso = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idSubproceso();
        if (realmGet$idSubproceso != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idSubprocesoIndex, rowIndex, realmGet$idSubproceso, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idSubprocesoIndex, rowIndex, false);
        }
        String realmGet$idEtapa = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idEtapa();
        if (realmGet$idEtapa != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idEtapaIndex, rowIndex, realmGet$idEtapa, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idEtapaIndex, rowIndex, false);
        }
        String realmGet$idSubetapa = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idSubetapa();
        if (realmGet$idSubetapa != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idSubetapaIndex, rowIndex, realmGet$idSubetapa, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idSubetapaIndex, rowIndex, false);
        }
        String realmGet$idResultado = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idResultado();
        if (realmGet$idResultado != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idResultadoIndex, rowIndex, realmGet$idResultado, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idResultadoIndex, rowIndex, false);
        }
        String realmGet$usuario = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$usuario();
        if (realmGet$usuario != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.usuarioIndex, rowIndex, realmGet$usuario, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.usuarioIndex, rowIndex, false);
        }
        String realmGet$cveSolicitud = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$cveSolicitud();
        if (realmGet$cveSolicitud != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.cveSolicitudIndex, rowIndex, realmGet$cveSolicitud, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.cveSolicitudIndex, rowIndex, false);
        }
        String realmGet$destinoNotificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$destinoNotificacion();
        if (realmGet$destinoNotificacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.destinoNotificacionIndex, rowIndex, realmGet$destinoNotificacion, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.destinoNotificacionIndex, rowIndex, false);
        }
        String realmGet$fechaCaptura = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$fechaCaptura();
        if (realmGet$fechaCaptura != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fechaCapturaIndex, rowIndex, realmGet$fechaCaptura, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.fechaCapturaIndex, rowIndex, false);
        }
        String realmGet$folioFus = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$folioFus();
        if (realmGet$folioFus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.folioFusIndex, rowIndex, realmGet$folioFus, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.folioFusIndex, rowIndex, false);
        }
        String realmGet$idEstatusSolicitud = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idEstatusSolicitud();
        if (realmGet$idEstatusSolicitud != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idEstatusSolicitudIndex, rowIndex, realmGet$idEstatusSolicitud, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idEstatusSolicitudIndex, rowIndex, false);
        }
        String realmGet$idMotivoRechazo = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idMotivoRechazo();
        if (realmGet$idMotivoRechazo != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idMotivoRechazoIndex, rowIndex, realmGet$idMotivoRechazo, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idMotivoRechazoIndex, rowIndex, false);
        }
        String realmGet$idFondoApp = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idFondoApp();
        if (realmGet$idFondoApp != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idFondoAppIndex, rowIndex, realmGet$idFondoApp, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idFondoAppIndex, rowIndex, false);
        }
        String realmGet$idMedioNotificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idMedioNotificacion();
        if (realmGet$idMedioNotificacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idMedioNotificacionIndex, rowIndex, realmGet$idMedioNotificacion, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idMedioNotificacionIndex, rowIndex, false);
        }
        String realmGet$idOrigenCaptura = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idOrigenCaptura();
        if (realmGet$idOrigenCaptura != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idOrigenCapturaIndex, rowIndex, realmGet$idOrigenCaptura, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idOrigenCapturaIndex, rowIndex, false);
        }
        String realmGet$idTipoReclasificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idTipoReclasificacion();
        if (realmGet$idTipoReclasificacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idTipoReclasificacionIndex, rowIndex, realmGet$idTipoReclasificacion, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idTipoReclasificacionIndex, rowIndex, false);
        }
        String realmGet$idTipoSiefore = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idTipoSiefore();
        if (realmGet$idTipoSiefore != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idTipoSieforeIndex, rowIndex, realmGet$idTipoSiefore, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idTipoSieforeIndex, rowIndex, false);
        }

        OsList listReclasificacionOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.listReclasificacionIndex);
        RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> listReclasificacionList = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$listReclasificacion();
        if (listReclasificacionList != null && listReclasificacionList.size() == listReclasificacionOsList.size()) {
            // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
            int objects = listReclasificacionList.size();
            for (int i = 0; i < objects; i++) {
                app.profuturo.reclasificacion.com.model.ListReclasificacion listReclasificacionItem = listReclasificacionList.get(i);
                Long cacheItemIndexlistReclasificacion = cache.get(listReclasificacionItem);
                if (cacheItemIndexlistReclasificacion == null) {
                    cacheItemIndexlistReclasificacion = ListReclasificacionRealmProxy.insertOrUpdate(realm, listReclasificacionItem, cache);
                }
                listReclasificacionOsList.setRow(i, cacheItemIndexlistReclasificacion);
            }
        } else {
            listReclasificacionOsList.removeAll();
            if (listReclasificacionList != null) {
                for (app.profuturo.reclasificacion.com.model.ListReclasificacion listReclasificacionItem : listReclasificacionList) {
                    Long cacheItemIndexlistReclasificacion = cache.get(listReclasificacionItem);
                    if (cacheItemIndexlistReclasificacion == null) {
                        cacheItemIndexlistReclasificacion = ListReclasificacionRealmProxy.insertOrUpdate(realm, listReclasificacionItem, cache);
                    }
                    listReclasificacionOsList.addRow(cacheItemIndexlistReclasificacion);
                }
            }
        }


        OsList listaIndicadoresOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.listaIndicadoresIndex);
        RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> listaIndicadoresList = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$listaIndicadores();
        if (listaIndicadoresList != null && listaIndicadoresList.size() == listaIndicadoresOsList.size()) {
            // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
            int objects = listaIndicadoresList.size();
            for (int i = 0; i < objects; i++) {
                app.profuturo.reclasificacion.com.model.ListaIndicadore listaIndicadoresItem = listaIndicadoresList.get(i);
                Long cacheItemIndexlistaIndicadores = cache.get(listaIndicadoresItem);
                if (cacheItemIndexlistaIndicadores == null) {
                    cacheItemIndexlistaIndicadores = ListaIndicadoreRealmProxy.insertOrUpdate(realm, listaIndicadoresItem, cache);
                }
                listaIndicadoresOsList.setRow(i, cacheItemIndexlistaIndicadores);
            }
        } else {
            listaIndicadoresOsList.removeAll();
            if (listaIndicadoresList != null) {
                for (app.profuturo.reclasificacion.com.model.ListaIndicadore listaIndicadoresItem : listaIndicadoresList) {
                    Long cacheItemIndexlistaIndicadores = cache.get(listaIndicadoresItem);
                    if (cacheItemIndexlistaIndicadores == null) {
                        cacheItemIndexlistaIndicadores = ListaIndicadoreRealmProxy.insertOrUpdate(realm, listaIndicadoresItem, cache);
                    }
                    listaIndicadoresOsList.addRow(cacheItemIndexlistaIndicadores);
                }
            }
        }


        OsList saldosOriginalesOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.saldosOriginalesIndex);
        RealmList<app.profuturo.reclasificacion.com.model.Saldo> saldosOriginalesList = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$saldosOriginales();
        if (saldosOriginalesList != null && saldosOriginalesList.size() == saldosOriginalesOsList.size()) {
            // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
            int objects = saldosOriginalesList.size();
            for (int i = 0; i < objects; i++) {
                app.profuturo.reclasificacion.com.model.Saldo saldosOriginalesItem = saldosOriginalesList.get(i);
                Long cacheItemIndexsaldosOriginales = cache.get(saldosOriginalesItem);
                if (cacheItemIndexsaldosOriginales == null) {
                    cacheItemIndexsaldosOriginales = SaldoRealmProxy.insertOrUpdate(realm, saldosOriginalesItem, cache);
                }
                saldosOriginalesOsList.setRow(i, cacheItemIndexsaldosOriginales);
            }
        } else {
            saldosOriginalesOsList.removeAll();
            if (saldosOriginalesList != null) {
                for (app.profuturo.reclasificacion.com.model.Saldo saldosOriginalesItem : saldosOriginalesList) {
                    Long cacheItemIndexsaldosOriginales = cache.get(saldosOriginalesItem);
                    if (cacheItemIndexsaldosOriginales == null) {
                        cacheItemIndexsaldosOriginales = SaldoRealmProxy.insertOrUpdate(realm, saldosOriginalesItem, cache);
                    }
                    saldosOriginalesOsList.addRow(cacheItemIndexsaldosOriginales);
                }
            }
        }

        String realmGet$montoReclasificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$montoReclasificacion();
        if (realmGet$montoReclasificacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.montoReclasificacionIndex, rowIndex, realmGet$montoReclasificacion, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.montoReclasificacionIndex, rowIndex, false);
        }
        String realmGet$numCtaInvdual = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$numCtaInvdual();
        if (realmGet$numCtaInvdual != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.numCtaInvdualIndex, rowIndex, realmGet$numCtaInvdual, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.numCtaInvdualIndex, rowIndex, false);
        }

        app.profuturo.reclasificacion.com.model.Solicitante solicitanteObj = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$solicitante();
        if (solicitanteObj != null) {
            Long cachesolicitante = cache.get(solicitanteObj);
            if (cachesolicitante == null) {
                cachesolicitante = SolicitanteRealmProxy.insertOrUpdate(realm, solicitanteObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.solicitanteIndex, rowIndex, cachesolicitante, false);
        } else {
            Table.nativeNullifyLink(tableNativePtr, columnInfo.solicitanteIndex, rowIndex);
        }
        String realmGet$usuCaptura = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$usuCaptura();
        if (realmGet$usuCaptura != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.usuCapturaIndex, rowIndex, realmGet$usuCaptura, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.usuCapturaIndex, rowIndex, false);
        }
        String realmGet$usuCre = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$usuCre();
        if (realmGet$usuCre != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.usuCreIndex, rowIndex, realmGet$usuCre, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.usuCreIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class);
        long tableNativePtr = table.getNativePtr();
        SaveProcedureRequestColumnInfo columnInfo = (SaveProcedureRequestColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.SaveProcedureRequest.class);
        long pkColumnIndex = columnInfo.uuidIndex;
        app.profuturo.reclasificacion.com.model.SaveProcedureRequest object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.SaveProcedureRequest) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$uuid();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$idEstatus = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idEstatus();
            if (realmGet$idEstatus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idEstatusIndex, rowIndex, realmGet$idEstatus, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idEstatusIndex, rowIndex, false);
            }
            String realmGet$idSubproceso = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idSubproceso();
            if (realmGet$idSubproceso != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idSubprocesoIndex, rowIndex, realmGet$idSubproceso, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idSubprocesoIndex, rowIndex, false);
            }
            String realmGet$idEtapa = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idEtapa();
            if (realmGet$idEtapa != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idEtapaIndex, rowIndex, realmGet$idEtapa, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idEtapaIndex, rowIndex, false);
            }
            String realmGet$idSubetapa = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idSubetapa();
            if (realmGet$idSubetapa != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idSubetapaIndex, rowIndex, realmGet$idSubetapa, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idSubetapaIndex, rowIndex, false);
            }
            String realmGet$idResultado = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idResultado();
            if (realmGet$idResultado != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idResultadoIndex, rowIndex, realmGet$idResultado, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idResultadoIndex, rowIndex, false);
            }
            String realmGet$usuario = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$usuario();
            if (realmGet$usuario != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.usuarioIndex, rowIndex, realmGet$usuario, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.usuarioIndex, rowIndex, false);
            }
            String realmGet$cveSolicitud = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$cveSolicitud();
            if (realmGet$cveSolicitud != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.cveSolicitudIndex, rowIndex, realmGet$cveSolicitud, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.cveSolicitudIndex, rowIndex, false);
            }
            String realmGet$destinoNotificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$destinoNotificacion();
            if (realmGet$destinoNotificacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.destinoNotificacionIndex, rowIndex, realmGet$destinoNotificacion, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.destinoNotificacionIndex, rowIndex, false);
            }
            String realmGet$fechaCaptura = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$fechaCaptura();
            if (realmGet$fechaCaptura != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.fechaCapturaIndex, rowIndex, realmGet$fechaCaptura, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.fechaCapturaIndex, rowIndex, false);
            }
            String realmGet$folioFus = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$folioFus();
            if (realmGet$folioFus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.folioFusIndex, rowIndex, realmGet$folioFus, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.folioFusIndex, rowIndex, false);
            }
            String realmGet$idEstatusSolicitud = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idEstatusSolicitud();
            if (realmGet$idEstatusSolicitud != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idEstatusSolicitudIndex, rowIndex, realmGet$idEstatusSolicitud, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idEstatusSolicitudIndex, rowIndex, false);
            }
            String realmGet$idMotivoRechazo = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idMotivoRechazo();
            if (realmGet$idMotivoRechazo != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idMotivoRechazoIndex, rowIndex, realmGet$idMotivoRechazo, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idMotivoRechazoIndex, rowIndex, false);
            }
            String realmGet$idFondoApp = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idFondoApp();
            if (realmGet$idFondoApp != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idFondoAppIndex, rowIndex, realmGet$idFondoApp, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idFondoAppIndex, rowIndex, false);
            }
            String realmGet$idMedioNotificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idMedioNotificacion();
            if (realmGet$idMedioNotificacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idMedioNotificacionIndex, rowIndex, realmGet$idMedioNotificacion, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idMedioNotificacionIndex, rowIndex, false);
            }
            String realmGet$idOrigenCaptura = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idOrigenCaptura();
            if (realmGet$idOrigenCaptura != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idOrigenCapturaIndex, rowIndex, realmGet$idOrigenCaptura, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idOrigenCapturaIndex, rowIndex, false);
            }
            String realmGet$idTipoReclasificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idTipoReclasificacion();
            if (realmGet$idTipoReclasificacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idTipoReclasificacionIndex, rowIndex, realmGet$idTipoReclasificacion, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idTipoReclasificacionIndex, rowIndex, false);
            }
            String realmGet$idTipoSiefore = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$idTipoSiefore();
            if (realmGet$idTipoSiefore != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idTipoSieforeIndex, rowIndex, realmGet$idTipoSiefore, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idTipoSieforeIndex, rowIndex, false);
            }

            OsList listReclasificacionOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.listReclasificacionIndex);
            RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> listReclasificacionList = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$listReclasificacion();
            if (listReclasificacionList != null && listReclasificacionList.size() == listReclasificacionOsList.size()) {
                // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
                int objectCount = listReclasificacionList.size();
                for (int i = 0; i < objectCount; i++) {
                    app.profuturo.reclasificacion.com.model.ListReclasificacion listReclasificacionItem = listReclasificacionList.get(i);
                    Long cacheItemIndexlistReclasificacion = cache.get(listReclasificacionItem);
                    if (cacheItemIndexlistReclasificacion == null) {
                        cacheItemIndexlistReclasificacion = ListReclasificacionRealmProxy.insertOrUpdate(realm, listReclasificacionItem, cache);
                    }
                    listReclasificacionOsList.setRow(i, cacheItemIndexlistReclasificacion);
                }
            } else {
                listReclasificacionOsList.removeAll();
                if (listReclasificacionList != null) {
                    for (app.profuturo.reclasificacion.com.model.ListReclasificacion listReclasificacionItem : listReclasificacionList) {
                        Long cacheItemIndexlistReclasificacion = cache.get(listReclasificacionItem);
                        if (cacheItemIndexlistReclasificacion == null) {
                            cacheItemIndexlistReclasificacion = ListReclasificacionRealmProxy.insertOrUpdate(realm, listReclasificacionItem, cache);
                        }
                        listReclasificacionOsList.addRow(cacheItemIndexlistReclasificacion);
                    }
                }
            }


            OsList listaIndicadoresOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.listaIndicadoresIndex);
            RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> listaIndicadoresList = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$listaIndicadores();
            if (listaIndicadoresList != null && listaIndicadoresList.size() == listaIndicadoresOsList.size()) {
                // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
                int objectCount = listaIndicadoresList.size();
                for (int i = 0; i < objectCount; i++) {
                    app.profuturo.reclasificacion.com.model.ListaIndicadore listaIndicadoresItem = listaIndicadoresList.get(i);
                    Long cacheItemIndexlistaIndicadores = cache.get(listaIndicadoresItem);
                    if (cacheItemIndexlistaIndicadores == null) {
                        cacheItemIndexlistaIndicadores = ListaIndicadoreRealmProxy.insertOrUpdate(realm, listaIndicadoresItem, cache);
                    }
                    listaIndicadoresOsList.setRow(i, cacheItemIndexlistaIndicadores);
                }
            } else {
                listaIndicadoresOsList.removeAll();
                if (listaIndicadoresList != null) {
                    for (app.profuturo.reclasificacion.com.model.ListaIndicadore listaIndicadoresItem : listaIndicadoresList) {
                        Long cacheItemIndexlistaIndicadores = cache.get(listaIndicadoresItem);
                        if (cacheItemIndexlistaIndicadores == null) {
                            cacheItemIndexlistaIndicadores = ListaIndicadoreRealmProxy.insertOrUpdate(realm, listaIndicadoresItem, cache);
                        }
                        listaIndicadoresOsList.addRow(cacheItemIndexlistaIndicadores);
                    }
                }
            }


            OsList saldosOriginalesOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.saldosOriginalesIndex);
            RealmList<app.profuturo.reclasificacion.com.model.Saldo> saldosOriginalesList = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$saldosOriginales();
            if (saldosOriginalesList != null && saldosOriginalesList.size() == saldosOriginalesOsList.size()) {
                // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
                int objectCount = saldosOriginalesList.size();
                for (int i = 0; i < objectCount; i++) {
                    app.profuturo.reclasificacion.com.model.Saldo saldosOriginalesItem = saldosOriginalesList.get(i);
                    Long cacheItemIndexsaldosOriginales = cache.get(saldosOriginalesItem);
                    if (cacheItemIndexsaldosOriginales == null) {
                        cacheItemIndexsaldosOriginales = SaldoRealmProxy.insertOrUpdate(realm, saldosOriginalesItem, cache);
                    }
                    saldosOriginalesOsList.setRow(i, cacheItemIndexsaldosOriginales);
                }
            } else {
                saldosOriginalesOsList.removeAll();
                if (saldosOriginalesList != null) {
                    for (app.profuturo.reclasificacion.com.model.Saldo saldosOriginalesItem : saldosOriginalesList) {
                        Long cacheItemIndexsaldosOriginales = cache.get(saldosOriginalesItem);
                        if (cacheItemIndexsaldosOriginales == null) {
                            cacheItemIndexsaldosOriginales = SaldoRealmProxy.insertOrUpdate(realm, saldosOriginalesItem, cache);
                        }
                        saldosOriginalesOsList.addRow(cacheItemIndexsaldosOriginales);
                    }
                }
            }

            String realmGet$montoReclasificacion = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$montoReclasificacion();
            if (realmGet$montoReclasificacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.montoReclasificacionIndex, rowIndex, realmGet$montoReclasificacion, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.montoReclasificacionIndex, rowIndex, false);
            }
            String realmGet$numCtaInvdual = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$numCtaInvdual();
            if (realmGet$numCtaInvdual != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.numCtaInvdualIndex, rowIndex, realmGet$numCtaInvdual, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.numCtaInvdualIndex, rowIndex, false);
            }

            app.profuturo.reclasificacion.com.model.Solicitante solicitanteObj = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$solicitante();
            if (solicitanteObj != null) {
                Long cachesolicitante = cache.get(solicitanteObj);
                if (cachesolicitante == null) {
                    cachesolicitante = SolicitanteRealmProxy.insertOrUpdate(realm, solicitanteObj, cache);
                }
                Table.nativeSetLink(tableNativePtr, columnInfo.solicitanteIndex, rowIndex, cachesolicitante, false);
            } else {
                Table.nativeNullifyLink(tableNativePtr, columnInfo.solicitanteIndex, rowIndex);
            }
            String realmGet$usuCaptura = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$usuCaptura();
            if (realmGet$usuCaptura != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.usuCapturaIndex, rowIndex, realmGet$usuCaptura, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.usuCapturaIndex, rowIndex, false);
            }
            String realmGet$usuCre = ((SaveProcedureRequestRealmProxyInterface) object).realmGet$usuCre();
            if (realmGet$usuCre != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.usuCreIndex, rowIndex, realmGet$usuCre, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.usuCreIndex, rowIndex, false);
            }
        }
    }

    public static app.profuturo.reclasificacion.com.model.SaveProcedureRequest createDetachedCopy(app.profuturo.reclasificacion.com.model.SaveProcedureRequest realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        app.profuturo.reclasificacion.com.model.SaveProcedureRequest unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new app.profuturo.reclasificacion.com.model.SaveProcedureRequest();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (app.profuturo.reclasificacion.com.model.SaveProcedureRequest) cachedObject.object;
            }
            unmanagedObject = (app.profuturo.reclasificacion.com.model.SaveProcedureRequest) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        SaveProcedureRequestRealmProxyInterface unmanagedCopy = (SaveProcedureRequestRealmProxyInterface) unmanagedObject;
        SaveProcedureRequestRealmProxyInterface realmSource = (SaveProcedureRequestRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$uuid(realmSource.realmGet$uuid());
        unmanagedCopy.realmSet$idEstatus(realmSource.realmGet$idEstatus());
        unmanagedCopy.realmSet$idSubproceso(realmSource.realmGet$idSubproceso());
        unmanagedCopy.realmSet$idEtapa(realmSource.realmGet$idEtapa());
        unmanagedCopy.realmSet$idSubetapa(realmSource.realmGet$idSubetapa());
        unmanagedCopy.realmSet$idResultado(realmSource.realmGet$idResultado());
        unmanagedCopy.realmSet$usuario(realmSource.realmGet$usuario());
        unmanagedCopy.realmSet$cveSolicitud(realmSource.realmGet$cveSolicitud());
        unmanagedCopy.realmSet$destinoNotificacion(realmSource.realmGet$destinoNotificacion());
        unmanagedCopy.realmSet$fechaCaptura(realmSource.realmGet$fechaCaptura());
        unmanagedCopy.realmSet$folioFus(realmSource.realmGet$folioFus());
        unmanagedCopy.realmSet$idEstatusSolicitud(realmSource.realmGet$idEstatusSolicitud());
        unmanagedCopy.realmSet$idMotivoRechazo(realmSource.realmGet$idMotivoRechazo());
        unmanagedCopy.realmSet$idFondoApp(realmSource.realmGet$idFondoApp());
        unmanagedCopy.realmSet$idMedioNotificacion(realmSource.realmGet$idMedioNotificacion());
        unmanagedCopy.realmSet$idOrigenCaptura(realmSource.realmGet$idOrigenCaptura());
        unmanagedCopy.realmSet$idTipoReclasificacion(realmSource.realmGet$idTipoReclasificacion());
        unmanagedCopy.realmSet$idTipoSiefore(realmSource.realmGet$idTipoSiefore());

        // Deep copy of listReclasificacion
        if (currentDepth == maxDepth) {
            unmanagedCopy.realmSet$listReclasificacion(null);
        } else {
            RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> managedlistReclasificacionList = realmSource.realmGet$listReclasificacion();
            RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> unmanagedlistReclasificacionList = new RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion>();
            unmanagedCopy.realmSet$listReclasificacion(unmanagedlistReclasificacionList);
            int nextDepth = currentDepth + 1;
            int size = managedlistReclasificacionList.size();
            for (int i = 0; i < size; i++) {
                app.profuturo.reclasificacion.com.model.ListReclasificacion item = ListReclasificacionRealmProxy.createDetachedCopy(managedlistReclasificacionList.get(i), nextDepth, maxDepth, cache);
                unmanagedlistReclasificacionList.add(item);
            }
        }

        // Deep copy of listaIndicadores
        if (currentDepth == maxDepth) {
            unmanagedCopy.realmSet$listaIndicadores(null);
        } else {
            RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> managedlistaIndicadoresList = realmSource.realmGet$listaIndicadores();
            RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> unmanagedlistaIndicadoresList = new RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore>();
            unmanagedCopy.realmSet$listaIndicadores(unmanagedlistaIndicadoresList);
            int nextDepth = currentDepth + 1;
            int size = managedlistaIndicadoresList.size();
            for (int i = 0; i < size; i++) {
                app.profuturo.reclasificacion.com.model.ListaIndicadore item = ListaIndicadoreRealmProxy.createDetachedCopy(managedlistaIndicadoresList.get(i), nextDepth, maxDepth, cache);
                unmanagedlistaIndicadoresList.add(item);
            }
        }

        // Deep copy of saldosOriginales
        if (currentDepth == maxDepth) {
            unmanagedCopy.realmSet$saldosOriginales(null);
        } else {
            RealmList<app.profuturo.reclasificacion.com.model.Saldo> managedsaldosOriginalesList = realmSource.realmGet$saldosOriginales();
            RealmList<app.profuturo.reclasificacion.com.model.Saldo> unmanagedsaldosOriginalesList = new RealmList<app.profuturo.reclasificacion.com.model.Saldo>();
            unmanagedCopy.realmSet$saldosOriginales(unmanagedsaldosOriginalesList);
            int nextDepth = currentDepth + 1;
            int size = managedsaldosOriginalesList.size();
            for (int i = 0; i < size; i++) {
                app.profuturo.reclasificacion.com.model.Saldo item = SaldoRealmProxy.createDetachedCopy(managedsaldosOriginalesList.get(i), nextDepth, maxDepth, cache);
                unmanagedsaldosOriginalesList.add(item);
            }
        }
        unmanagedCopy.realmSet$montoReclasificacion(realmSource.realmGet$montoReclasificacion());
        unmanagedCopy.realmSet$numCtaInvdual(realmSource.realmGet$numCtaInvdual());

        // Deep copy of solicitante
        unmanagedCopy.realmSet$solicitante(SolicitanteRealmProxy.createDetachedCopy(realmSource.realmGet$solicitante(), currentDepth + 1, maxDepth, cache));
        unmanagedCopy.realmSet$usuCaptura(realmSource.realmGet$usuCaptura());
        unmanagedCopy.realmSet$usuCre(realmSource.realmGet$usuCre());

        return unmanagedObject;
    }

    static app.profuturo.reclasificacion.com.model.SaveProcedureRequest update(Realm realm, app.profuturo.reclasificacion.com.model.SaveProcedureRequest realmObject, app.profuturo.reclasificacion.com.model.SaveProcedureRequest newObject, Map<RealmModel, RealmObjectProxy> cache) {
        SaveProcedureRequestRealmProxyInterface realmObjectTarget = (SaveProcedureRequestRealmProxyInterface) realmObject;
        SaveProcedureRequestRealmProxyInterface realmObjectSource = (SaveProcedureRequestRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$idEstatus(realmObjectSource.realmGet$idEstatus());
        realmObjectTarget.realmSet$idSubproceso(realmObjectSource.realmGet$idSubproceso());
        realmObjectTarget.realmSet$idEtapa(realmObjectSource.realmGet$idEtapa());
        realmObjectTarget.realmSet$idSubetapa(realmObjectSource.realmGet$idSubetapa());
        realmObjectTarget.realmSet$idResultado(realmObjectSource.realmGet$idResultado());
        realmObjectTarget.realmSet$usuario(realmObjectSource.realmGet$usuario());
        realmObjectTarget.realmSet$cveSolicitud(realmObjectSource.realmGet$cveSolicitud());
        realmObjectTarget.realmSet$destinoNotificacion(realmObjectSource.realmGet$destinoNotificacion());
        realmObjectTarget.realmSet$fechaCaptura(realmObjectSource.realmGet$fechaCaptura());
        realmObjectTarget.realmSet$folioFus(realmObjectSource.realmGet$folioFus());
        realmObjectTarget.realmSet$idEstatusSolicitud(realmObjectSource.realmGet$idEstatusSolicitud());
        realmObjectTarget.realmSet$idMotivoRechazo(realmObjectSource.realmGet$idMotivoRechazo());
        realmObjectTarget.realmSet$idFondoApp(realmObjectSource.realmGet$idFondoApp());
        realmObjectTarget.realmSet$idMedioNotificacion(realmObjectSource.realmGet$idMedioNotificacion());
        realmObjectTarget.realmSet$idOrigenCaptura(realmObjectSource.realmGet$idOrigenCaptura());
        realmObjectTarget.realmSet$idTipoReclasificacion(realmObjectSource.realmGet$idTipoReclasificacion());
        realmObjectTarget.realmSet$idTipoSiefore(realmObjectSource.realmGet$idTipoSiefore());
        RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> listReclasificacionList = realmObjectSource.realmGet$listReclasificacion();
        RealmList<app.profuturo.reclasificacion.com.model.ListReclasificacion> listReclasificacionRealmList = realmObjectTarget.realmGet$listReclasificacion();
        if (listReclasificacionList != null && listReclasificacionList.size() == listReclasificacionRealmList.size()) {
            // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
            int objects = listReclasificacionList.size();
            for (int i = 0; i < objects; i++) {
                app.profuturo.reclasificacion.com.model.ListReclasificacion listReclasificacionItem = listReclasificacionList.get(i);
                app.profuturo.reclasificacion.com.model.ListReclasificacion cachelistReclasificacion = (app.profuturo.reclasificacion.com.model.ListReclasificacion) cache.get(listReclasificacionItem);
                if (cachelistReclasificacion != null) {
                    listReclasificacionRealmList.set(i, cachelistReclasificacion);
                } else {
                    listReclasificacionRealmList.set(i, ListReclasificacionRealmProxy.copyOrUpdate(realm, listReclasificacionItem, true, cache));
                }
            }
        } else {
            listReclasificacionRealmList.clear();
            if (listReclasificacionList != null) {
                for (int i = 0; i < listReclasificacionList.size(); i++) {
                    app.profuturo.reclasificacion.com.model.ListReclasificacion listReclasificacionItem = listReclasificacionList.get(i);
                    app.profuturo.reclasificacion.com.model.ListReclasificacion cachelistReclasificacion = (app.profuturo.reclasificacion.com.model.ListReclasificacion) cache.get(listReclasificacionItem);
                    if (cachelistReclasificacion != null) {
                        listReclasificacionRealmList.add(cachelistReclasificacion);
                    } else {
                        listReclasificacionRealmList.add(ListReclasificacionRealmProxy.copyOrUpdate(realm, listReclasificacionItem, true, cache));
                    }
                }
            }
        }
        RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> listaIndicadoresList = realmObjectSource.realmGet$listaIndicadores();
        RealmList<app.profuturo.reclasificacion.com.model.ListaIndicadore> listaIndicadoresRealmList = realmObjectTarget.realmGet$listaIndicadores();
        if (listaIndicadoresList != null && listaIndicadoresList.size() == listaIndicadoresRealmList.size()) {
            // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
            int objects = listaIndicadoresList.size();
            for (int i = 0; i < objects; i++) {
                app.profuturo.reclasificacion.com.model.ListaIndicadore listaIndicadoresItem = listaIndicadoresList.get(i);
                app.profuturo.reclasificacion.com.model.ListaIndicadore cachelistaIndicadores = (app.profuturo.reclasificacion.com.model.ListaIndicadore) cache.get(listaIndicadoresItem);
                if (cachelistaIndicadores != null) {
                    listaIndicadoresRealmList.set(i, cachelistaIndicadores);
                } else {
                    listaIndicadoresRealmList.set(i, ListaIndicadoreRealmProxy.copyOrUpdate(realm, listaIndicadoresItem, true, cache));
                }
            }
        } else {
            listaIndicadoresRealmList.clear();
            if (listaIndicadoresList != null) {
                for (int i = 0; i < listaIndicadoresList.size(); i++) {
                    app.profuturo.reclasificacion.com.model.ListaIndicadore listaIndicadoresItem = listaIndicadoresList.get(i);
                    app.profuturo.reclasificacion.com.model.ListaIndicadore cachelistaIndicadores = (app.profuturo.reclasificacion.com.model.ListaIndicadore) cache.get(listaIndicadoresItem);
                    if (cachelistaIndicadores != null) {
                        listaIndicadoresRealmList.add(cachelistaIndicadores);
                    } else {
                        listaIndicadoresRealmList.add(ListaIndicadoreRealmProxy.copyOrUpdate(realm, listaIndicadoresItem, true, cache));
                    }
                }
            }
        }
        RealmList<app.profuturo.reclasificacion.com.model.Saldo> saldosOriginalesList = realmObjectSource.realmGet$saldosOriginales();
        RealmList<app.profuturo.reclasificacion.com.model.Saldo> saldosOriginalesRealmList = realmObjectTarget.realmGet$saldosOriginales();
        if (saldosOriginalesList != null && saldosOriginalesList.size() == saldosOriginalesRealmList.size()) {
            // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
            int objects = saldosOriginalesList.size();
            for (int i = 0; i < objects; i++) {
                app.profuturo.reclasificacion.com.model.Saldo saldosOriginalesItem = saldosOriginalesList.get(i);
                app.profuturo.reclasificacion.com.model.Saldo cachesaldosOriginales = (app.profuturo.reclasificacion.com.model.Saldo) cache.get(saldosOriginalesItem);
                if (cachesaldosOriginales != null) {
                    saldosOriginalesRealmList.set(i, cachesaldosOriginales);
                } else {
                    saldosOriginalesRealmList.set(i, SaldoRealmProxy.copyOrUpdate(realm, saldosOriginalesItem, true, cache));
                }
            }
        } else {
            saldosOriginalesRealmList.clear();
            if (saldosOriginalesList != null) {
                for (int i = 0; i < saldosOriginalesList.size(); i++) {
                    app.profuturo.reclasificacion.com.model.Saldo saldosOriginalesItem = saldosOriginalesList.get(i);
                    app.profuturo.reclasificacion.com.model.Saldo cachesaldosOriginales = (app.profuturo.reclasificacion.com.model.Saldo) cache.get(saldosOriginalesItem);
                    if (cachesaldosOriginales != null) {
                        saldosOriginalesRealmList.add(cachesaldosOriginales);
                    } else {
                        saldosOriginalesRealmList.add(SaldoRealmProxy.copyOrUpdate(realm, saldosOriginalesItem, true, cache));
                    }
                }
            }
        }
        realmObjectTarget.realmSet$montoReclasificacion(realmObjectSource.realmGet$montoReclasificacion());
        realmObjectTarget.realmSet$numCtaInvdual(realmObjectSource.realmGet$numCtaInvdual());
        app.profuturo.reclasificacion.com.model.Solicitante solicitanteObj = realmObjectSource.realmGet$solicitante();
        if (solicitanteObj == null) {
            realmObjectTarget.realmSet$solicitante(null);
        } else {
            app.profuturo.reclasificacion.com.model.Solicitante cachesolicitante = (app.profuturo.reclasificacion.com.model.Solicitante) cache.get(solicitanteObj);
            if (cachesolicitante != null) {
                realmObjectTarget.realmSet$solicitante(cachesolicitante);
            } else {
                realmObjectTarget.realmSet$solicitante(SolicitanteRealmProxy.copyOrUpdate(realm, solicitanteObj, true, cache));
            }
        }
        realmObjectTarget.realmSet$usuCaptura(realmObjectSource.realmGet$usuCaptura());
        realmObjectTarget.realmSet$usuCre(realmObjectSource.realmGet$usuCre());
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("SaveProcedureRequest = proxy[");
        stringBuilder.append("{uuid:");
        stringBuilder.append(realmGet$uuid() != null ? realmGet$uuid() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idEstatus:");
        stringBuilder.append(realmGet$idEstatus() != null ? realmGet$idEstatus() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idSubproceso:");
        stringBuilder.append(realmGet$idSubproceso() != null ? realmGet$idSubproceso() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idEtapa:");
        stringBuilder.append(realmGet$idEtapa() != null ? realmGet$idEtapa() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idSubetapa:");
        stringBuilder.append(realmGet$idSubetapa() != null ? realmGet$idSubetapa() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idResultado:");
        stringBuilder.append(realmGet$idResultado() != null ? realmGet$idResultado() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{usuario:");
        stringBuilder.append(realmGet$usuario() != null ? realmGet$usuario() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{cveSolicitud:");
        stringBuilder.append(realmGet$cveSolicitud() != null ? realmGet$cveSolicitud() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{destinoNotificacion:");
        stringBuilder.append(realmGet$destinoNotificacion() != null ? realmGet$destinoNotificacion() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{fechaCaptura:");
        stringBuilder.append(realmGet$fechaCaptura() != null ? realmGet$fechaCaptura() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{folioFus:");
        stringBuilder.append(realmGet$folioFus() != null ? realmGet$folioFus() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idEstatusSolicitud:");
        stringBuilder.append(realmGet$idEstatusSolicitud() != null ? realmGet$idEstatusSolicitud() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idMotivoRechazo:");
        stringBuilder.append(realmGet$idMotivoRechazo() != null ? realmGet$idMotivoRechazo() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idFondoApp:");
        stringBuilder.append(realmGet$idFondoApp() != null ? realmGet$idFondoApp() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idMedioNotificacion:");
        stringBuilder.append(realmGet$idMedioNotificacion() != null ? realmGet$idMedioNotificacion() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idOrigenCaptura:");
        stringBuilder.append(realmGet$idOrigenCaptura() != null ? realmGet$idOrigenCaptura() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idTipoReclasificacion:");
        stringBuilder.append(realmGet$idTipoReclasificacion() != null ? realmGet$idTipoReclasificacion() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idTipoSiefore:");
        stringBuilder.append(realmGet$idTipoSiefore() != null ? realmGet$idTipoSiefore() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{listReclasificacion:");
        stringBuilder.append("RealmList<ListReclasificacion>[").append(realmGet$listReclasificacion().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{listaIndicadores:");
        stringBuilder.append("RealmList<ListaIndicadore>[").append(realmGet$listaIndicadores().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{saldosOriginales:");
        stringBuilder.append("RealmList<Saldo>[").append(realmGet$saldosOriginales().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{montoReclasificacion:");
        stringBuilder.append(realmGet$montoReclasificacion() != null ? realmGet$montoReclasificacion() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{numCtaInvdual:");
        stringBuilder.append(realmGet$numCtaInvdual() != null ? realmGet$numCtaInvdual() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{solicitante:");
        stringBuilder.append(realmGet$solicitante() != null ? "Solicitante" : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{usuCaptura:");
        stringBuilder.append(realmGet$usuCaptura() != null ? realmGet$usuCaptura() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{usuCre:");
        stringBuilder.append(realmGet$usuCre() != null ? realmGet$usuCre() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaveProcedureRequestRealmProxy aSaveProcedureRequest = (SaveProcedureRequestRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aSaveProcedureRequest.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aSaveProcedureRequest.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aSaveProcedureRequest.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
