package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.ProxyUtils;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class ListReclasificacionRealmProxy extends app.profuturo.reclasificacion.com.model.ListReclasificacion
    implements RealmObjectProxy, ListReclasificacionRealmProxyInterface {

    static final class ListReclasificacionColumnInfo extends ColumnInfo {
        long accionesIndex;
        long idSieforeIndex;
        long idSubctaIndex;
        long idTipoMovIndex;
        long idValorAccionIndex;
        long pesosIndex;
        long porcentajeIndex;
        long idFondoAppIndex;

        ListReclasificacionColumnInfo(OsSchemaInfo schemaInfo) {
            super(8);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("ListReclasificacion");
            this.accionesIndex = addColumnDetails("acciones", objectSchemaInfo);
            this.idSieforeIndex = addColumnDetails("idSiefore", objectSchemaInfo);
            this.idSubctaIndex = addColumnDetails("idSubcta", objectSchemaInfo);
            this.idTipoMovIndex = addColumnDetails("idTipoMov", objectSchemaInfo);
            this.idValorAccionIndex = addColumnDetails("idValorAccion", objectSchemaInfo);
            this.pesosIndex = addColumnDetails("pesos", objectSchemaInfo);
            this.porcentajeIndex = addColumnDetails("porcentaje", objectSchemaInfo);
            this.idFondoAppIndex = addColumnDetails("idFondoApp", objectSchemaInfo);
        }

        ListReclasificacionColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new ListReclasificacionColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final ListReclasificacionColumnInfo src = (ListReclasificacionColumnInfo) rawSrc;
            final ListReclasificacionColumnInfo dst = (ListReclasificacionColumnInfo) rawDst;
            dst.accionesIndex = src.accionesIndex;
            dst.idSieforeIndex = src.idSieforeIndex;
            dst.idSubctaIndex = src.idSubctaIndex;
            dst.idTipoMovIndex = src.idTipoMovIndex;
            dst.idValorAccionIndex = src.idValorAccionIndex;
            dst.pesosIndex = src.pesosIndex;
            dst.porcentajeIndex = src.porcentajeIndex;
            dst.idFondoAppIndex = src.idFondoAppIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>(8);
        fieldNames.add("acciones");
        fieldNames.add("idSiefore");
        fieldNames.add("idSubcta");
        fieldNames.add("idTipoMov");
        fieldNames.add("idValorAccion");
        fieldNames.add("pesos");
        fieldNames.add("porcentaje");
        fieldNames.add("idFondoApp");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    private ListReclasificacionColumnInfo columnInfo;
    private ProxyState<app.profuturo.reclasificacion.com.model.ListReclasificacion> proxyState;

    ListReclasificacionRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (ListReclasificacionColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<app.profuturo.reclasificacion.com.model.ListReclasificacion>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$acciones() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.accionesIndex);
    }

    @Override
    public void realmSet$acciones(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.accionesIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.accionesIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.accionesIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.accionesIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idSiefore() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idSieforeIndex);
    }

    @Override
    public void realmSet$idSiefore(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idSieforeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idSieforeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idSieforeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idSieforeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idSubcta() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idSubctaIndex);
    }

    @Override
    public void realmSet$idSubcta(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idSubctaIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idSubctaIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idSubctaIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idSubctaIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idTipoMov() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idTipoMovIndex);
    }

    @Override
    public void realmSet$idTipoMov(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idTipoMovIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idTipoMovIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idTipoMovIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idTipoMovIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idValorAccion() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idValorAccionIndex);
    }

    @Override
    public void realmSet$idValorAccion(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idValorAccionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idValorAccionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idValorAccionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idValorAccionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$pesos() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.pesosIndex);
    }

    @Override
    public void realmSet$pesos(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.pesosIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.pesosIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.pesosIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.pesosIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$porcentaje() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.porcentajeIndex);
    }

    @Override
    public void realmSet$porcentaje(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.porcentajeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.porcentajeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.porcentajeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.porcentajeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$idFondoApp() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idFondoAppIndex);
    }

    @Override
    public void realmSet$idFondoApp(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.idFondoAppIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.idFondoAppIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.idFondoAppIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.idFondoAppIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("ListReclasificacion", 8, 0);
        builder.addPersistedProperty("acciones", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idSiefore", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idSubcta", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idTipoMov", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idValorAccion", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("pesos", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("porcentaje", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("idFondoApp", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static ListReclasificacionColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new ListReclasificacionColumnInfo(schemaInfo);
    }

    public static String getTableName() {
        return "class_ListReclasificacion";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static app.profuturo.reclasificacion.com.model.ListReclasificacion createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        app.profuturo.reclasificacion.com.model.ListReclasificacion obj = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.ListReclasificacion.class, true, excludeFields);

        final ListReclasificacionRealmProxyInterface objProxy = (ListReclasificacionRealmProxyInterface) obj;
        if (json.has("acciones")) {
            if (json.isNull("acciones")) {
                objProxy.realmSet$acciones(null);
            } else {
                objProxy.realmSet$acciones((String) json.getString("acciones"));
            }
        }
        if (json.has("idSiefore")) {
            if (json.isNull("idSiefore")) {
                objProxy.realmSet$idSiefore(null);
            } else {
                objProxy.realmSet$idSiefore((String) json.getString("idSiefore"));
            }
        }
        if (json.has("idSubcta")) {
            if (json.isNull("idSubcta")) {
                objProxy.realmSet$idSubcta(null);
            } else {
                objProxy.realmSet$idSubcta((String) json.getString("idSubcta"));
            }
        }
        if (json.has("idTipoMov")) {
            if (json.isNull("idTipoMov")) {
                objProxy.realmSet$idTipoMov(null);
            } else {
                objProxy.realmSet$idTipoMov((String) json.getString("idTipoMov"));
            }
        }
        if (json.has("idValorAccion")) {
            if (json.isNull("idValorAccion")) {
                objProxy.realmSet$idValorAccion(null);
            } else {
                objProxy.realmSet$idValorAccion((String) json.getString("idValorAccion"));
            }
        }
        if (json.has("pesos")) {
            if (json.isNull("pesos")) {
                objProxy.realmSet$pesos(null);
            } else {
                objProxy.realmSet$pesos((String) json.getString("pesos"));
            }
        }
        if (json.has("porcentaje")) {
            if (json.isNull("porcentaje")) {
                objProxy.realmSet$porcentaje(null);
            } else {
                objProxy.realmSet$porcentaje((String) json.getString("porcentaje"));
            }
        }
        if (json.has("idFondoApp")) {
            if (json.isNull("idFondoApp")) {
                objProxy.realmSet$idFondoApp(null);
            } else {
                objProxy.realmSet$idFondoApp((String) json.getString("idFondoApp"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static app.profuturo.reclasificacion.com.model.ListReclasificacion createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        final app.profuturo.reclasificacion.com.model.ListReclasificacion obj = new app.profuturo.reclasificacion.com.model.ListReclasificacion();
        final ListReclasificacionRealmProxyInterface objProxy = (ListReclasificacionRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("acciones")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$acciones((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$acciones(null);
                }
            } else if (name.equals("idSiefore")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idSiefore((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idSiefore(null);
                }
            } else if (name.equals("idSubcta")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idSubcta((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idSubcta(null);
                }
            } else if (name.equals("idTipoMov")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idTipoMov((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idTipoMov(null);
                }
            } else if (name.equals("idValorAccion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idValorAccion((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idValorAccion(null);
                }
            } else if (name.equals("pesos")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$pesos((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$pesos(null);
                }
            } else if (name.equals("porcentaje")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$porcentaje((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$porcentaje(null);
                }
            } else if (name.equals("idFondoApp")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$idFondoApp((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$idFondoApp(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return realm.copyToRealm(obj);
    }

    public static app.profuturo.reclasificacion.com.model.ListReclasificacion copyOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.ListReclasificacion object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.ListReclasificacion) cachedRealmObject;
        }

        return copy(realm, object, update, cache);
    }

    public static app.profuturo.reclasificacion.com.model.ListReclasificacion copy(Realm realm, app.profuturo.reclasificacion.com.model.ListReclasificacion newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (app.profuturo.reclasificacion.com.model.ListReclasificacion) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        app.profuturo.reclasificacion.com.model.ListReclasificacion realmObject = realm.createObjectInternal(app.profuturo.reclasificacion.com.model.ListReclasificacion.class, false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        ListReclasificacionRealmProxyInterface realmObjectSource = (ListReclasificacionRealmProxyInterface) newObject;
        ListReclasificacionRealmProxyInterface realmObjectCopy = (ListReclasificacionRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$acciones(realmObjectSource.realmGet$acciones());
        realmObjectCopy.realmSet$idSiefore(realmObjectSource.realmGet$idSiefore());
        realmObjectCopy.realmSet$idSubcta(realmObjectSource.realmGet$idSubcta());
        realmObjectCopy.realmSet$idTipoMov(realmObjectSource.realmGet$idTipoMov());
        realmObjectCopy.realmSet$idValorAccion(realmObjectSource.realmGet$idValorAccion());
        realmObjectCopy.realmSet$pesos(realmObjectSource.realmGet$pesos());
        realmObjectCopy.realmSet$porcentaje(realmObjectSource.realmGet$porcentaje());
        realmObjectCopy.realmSet$idFondoApp(realmObjectSource.realmGet$idFondoApp());
        return realmObject;
    }

    public static long insert(Realm realm, app.profuturo.reclasificacion.com.model.ListReclasificacion object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.ListReclasificacion.class);
        long tableNativePtr = table.getNativePtr();
        ListReclasificacionColumnInfo columnInfo = (ListReclasificacionColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.ListReclasificacion.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        String realmGet$acciones = ((ListReclasificacionRealmProxyInterface) object).realmGet$acciones();
        if (realmGet$acciones != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.accionesIndex, rowIndex, realmGet$acciones, false);
        }
        String realmGet$idSiefore = ((ListReclasificacionRealmProxyInterface) object).realmGet$idSiefore();
        if (realmGet$idSiefore != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idSieforeIndex, rowIndex, realmGet$idSiefore, false);
        }
        String realmGet$idSubcta = ((ListReclasificacionRealmProxyInterface) object).realmGet$idSubcta();
        if (realmGet$idSubcta != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idSubctaIndex, rowIndex, realmGet$idSubcta, false);
        }
        String realmGet$idTipoMov = ((ListReclasificacionRealmProxyInterface) object).realmGet$idTipoMov();
        if (realmGet$idTipoMov != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idTipoMovIndex, rowIndex, realmGet$idTipoMov, false);
        }
        String realmGet$idValorAccion = ((ListReclasificacionRealmProxyInterface) object).realmGet$idValorAccion();
        if (realmGet$idValorAccion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idValorAccionIndex, rowIndex, realmGet$idValorAccion, false);
        }
        String realmGet$pesos = ((ListReclasificacionRealmProxyInterface) object).realmGet$pesos();
        if (realmGet$pesos != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.pesosIndex, rowIndex, realmGet$pesos, false);
        }
        String realmGet$porcentaje = ((ListReclasificacionRealmProxyInterface) object).realmGet$porcentaje();
        if (realmGet$porcentaje != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.porcentajeIndex, rowIndex, realmGet$porcentaje, false);
        }
        String realmGet$idFondoApp = ((ListReclasificacionRealmProxyInterface) object).realmGet$idFondoApp();
        if (realmGet$idFondoApp != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idFondoAppIndex, rowIndex, realmGet$idFondoApp, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.ListReclasificacion.class);
        long tableNativePtr = table.getNativePtr();
        ListReclasificacionColumnInfo columnInfo = (ListReclasificacionColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.ListReclasificacion.class);
        app.profuturo.reclasificacion.com.model.ListReclasificacion object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.ListReclasificacion) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            String realmGet$acciones = ((ListReclasificacionRealmProxyInterface) object).realmGet$acciones();
            if (realmGet$acciones != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.accionesIndex, rowIndex, realmGet$acciones, false);
            }
            String realmGet$idSiefore = ((ListReclasificacionRealmProxyInterface) object).realmGet$idSiefore();
            if (realmGet$idSiefore != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idSieforeIndex, rowIndex, realmGet$idSiefore, false);
            }
            String realmGet$idSubcta = ((ListReclasificacionRealmProxyInterface) object).realmGet$idSubcta();
            if (realmGet$idSubcta != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idSubctaIndex, rowIndex, realmGet$idSubcta, false);
            }
            String realmGet$idTipoMov = ((ListReclasificacionRealmProxyInterface) object).realmGet$idTipoMov();
            if (realmGet$idTipoMov != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idTipoMovIndex, rowIndex, realmGet$idTipoMov, false);
            }
            String realmGet$idValorAccion = ((ListReclasificacionRealmProxyInterface) object).realmGet$idValorAccion();
            if (realmGet$idValorAccion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idValorAccionIndex, rowIndex, realmGet$idValorAccion, false);
            }
            String realmGet$pesos = ((ListReclasificacionRealmProxyInterface) object).realmGet$pesos();
            if (realmGet$pesos != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.pesosIndex, rowIndex, realmGet$pesos, false);
            }
            String realmGet$porcentaje = ((ListReclasificacionRealmProxyInterface) object).realmGet$porcentaje();
            if (realmGet$porcentaje != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.porcentajeIndex, rowIndex, realmGet$porcentaje, false);
            }
            String realmGet$idFondoApp = ((ListReclasificacionRealmProxyInterface) object).realmGet$idFondoApp();
            if (realmGet$idFondoApp != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idFondoAppIndex, rowIndex, realmGet$idFondoApp, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, app.profuturo.reclasificacion.com.model.ListReclasificacion object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.ListReclasificacion.class);
        long tableNativePtr = table.getNativePtr();
        ListReclasificacionColumnInfo columnInfo = (ListReclasificacionColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.ListReclasificacion.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        String realmGet$acciones = ((ListReclasificacionRealmProxyInterface) object).realmGet$acciones();
        if (realmGet$acciones != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.accionesIndex, rowIndex, realmGet$acciones, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.accionesIndex, rowIndex, false);
        }
        String realmGet$idSiefore = ((ListReclasificacionRealmProxyInterface) object).realmGet$idSiefore();
        if (realmGet$idSiefore != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idSieforeIndex, rowIndex, realmGet$idSiefore, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idSieforeIndex, rowIndex, false);
        }
        String realmGet$idSubcta = ((ListReclasificacionRealmProxyInterface) object).realmGet$idSubcta();
        if (realmGet$idSubcta != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idSubctaIndex, rowIndex, realmGet$idSubcta, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idSubctaIndex, rowIndex, false);
        }
        String realmGet$idTipoMov = ((ListReclasificacionRealmProxyInterface) object).realmGet$idTipoMov();
        if (realmGet$idTipoMov != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idTipoMovIndex, rowIndex, realmGet$idTipoMov, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idTipoMovIndex, rowIndex, false);
        }
        String realmGet$idValorAccion = ((ListReclasificacionRealmProxyInterface) object).realmGet$idValorAccion();
        if (realmGet$idValorAccion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idValorAccionIndex, rowIndex, realmGet$idValorAccion, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idValorAccionIndex, rowIndex, false);
        }
        String realmGet$pesos = ((ListReclasificacionRealmProxyInterface) object).realmGet$pesos();
        if (realmGet$pesos != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.pesosIndex, rowIndex, realmGet$pesos, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.pesosIndex, rowIndex, false);
        }
        String realmGet$porcentaje = ((ListReclasificacionRealmProxyInterface) object).realmGet$porcentaje();
        if (realmGet$porcentaje != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.porcentajeIndex, rowIndex, realmGet$porcentaje, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.porcentajeIndex, rowIndex, false);
        }
        String realmGet$idFondoApp = ((ListReclasificacionRealmProxyInterface) object).realmGet$idFondoApp();
        if (realmGet$idFondoApp != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.idFondoAppIndex, rowIndex, realmGet$idFondoApp, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.idFondoAppIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(app.profuturo.reclasificacion.com.model.ListReclasificacion.class);
        long tableNativePtr = table.getNativePtr();
        ListReclasificacionColumnInfo columnInfo = (ListReclasificacionColumnInfo) realm.getSchema().getColumnInfo(app.profuturo.reclasificacion.com.model.ListReclasificacion.class);
        app.profuturo.reclasificacion.com.model.ListReclasificacion object = null;
        while (objects.hasNext()) {
            object = (app.profuturo.reclasificacion.com.model.ListReclasificacion) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            String realmGet$acciones = ((ListReclasificacionRealmProxyInterface) object).realmGet$acciones();
            if (realmGet$acciones != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.accionesIndex, rowIndex, realmGet$acciones, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.accionesIndex, rowIndex, false);
            }
            String realmGet$idSiefore = ((ListReclasificacionRealmProxyInterface) object).realmGet$idSiefore();
            if (realmGet$idSiefore != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idSieforeIndex, rowIndex, realmGet$idSiefore, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idSieforeIndex, rowIndex, false);
            }
            String realmGet$idSubcta = ((ListReclasificacionRealmProxyInterface) object).realmGet$idSubcta();
            if (realmGet$idSubcta != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idSubctaIndex, rowIndex, realmGet$idSubcta, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idSubctaIndex, rowIndex, false);
            }
            String realmGet$idTipoMov = ((ListReclasificacionRealmProxyInterface) object).realmGet$idTipoMov();
            if (realmGet$idTipoMov != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idTipoMovIndex, rowIndex, realmGet$idTipoMov, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idTipoMovIndex, rowIndex, false);
            }
            String realmGet$idValorAccion = ((ListReclasificacionRealmProxyInterface) object).realmGet$idValorAccion();
            if (realmGet$idValorAccion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idValorAccionIndex, rowIndex, realmGet$idValorAccion, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idValorAccionIndex, rowIndex, false);
            }
            String realmGet$pesos = ((ListReclasificacionRealmProxyInterface) object).realmGet$pesos();
            if (realmGet$pesos != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.pesosIndex, rowIndex, realmGet$pesos, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.pesosIndex, rowIndex, false);
            }
            String realmGet$porcentaje = ((ListReclasificacionRealmProxyInterface) object).realmGet$porcentaje();
            if (realmGet$porcentaje != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.porcentajeIndex, rowIndex, realmGet$porcentaje, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.porcentajeIndex, rowIndex, false);
            }
            String realmGet$idFondoApp = ((ListReclasificacionRealmProxyInterface) object).realmGet$idFondoApp();
            if (realmGet$idFondoApp != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.idFondoAppIndex, rowIndex, realmGet$idFondoApp, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.idFondoAppIndex, rowIndex, false);
            }
        }
    }

    public static app.profuturo.reclasificacion.com.model.ListReclasificacion createDetachedCopy(app.profuturo.reclasificacion.com.model.ListReclasificacion realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        app.profuturo.reclasificacion.com.model.ListReclasificacion unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new app.profuturo.reclasificacion.com.model.ListReclasificacion();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (app.profuturo.reclasificacion.com.model.ListReclasificacion) cachedObject.object;
            }
            unmanagedObject = (app.profuturo.reclasificacion.com.model.ListReclasificacion) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        ListReclasificacionRealmProxyInterface unmanagedCopy = (ListReclasificacionRealmProxyInterface) unmanagedObject;
        ListReclasificacionRealmProxyInterface realmSource = (ListReclasificacionRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$acciones(realmSource.realmGet$acciones());
        unmanagedCopy.realmSet$idSiefore(realmSource.realmGet$idSiefore());
        unmanagedCopy.realmSet$idSubcta(realmSource.realmGet$idSubcta());
        unmanagedCopy.realmSet$idTipoMov(realmSource.realmGet$idTipoMov());
        unmanagedCopy.realmSet$idValorAccion(realmSource.realmGet$idValorAccion());
        unmanagedCopy.realmSet$pesos(realmSource.realmGet$pesos());
        unmanagedCopy.realmSet$porcentaje(realmSource.realmGet$porcentaje());
        unmanagedCopy.realmSet$idFondoApp(realmSource.realmGet$idFondoApp());

        return unmanagedObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("ListReclasificacion = proxy[");
        stringBuilder.append("{acciones:");
        stringBuilder.append(realmGet$acciones() != null ? realmGet$acciones() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idSiefore:");
        stringBuilder.append(realmGet$idSiefore() != null ? realmGet$idSiefore() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idSubcta:");
        stringBuilder.append(realmGet$idSubcta() != null ? realmGet$idSubcta() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idTipoMov:");
        stringBuilder.append(realmGet$idTipoMov() != null ? realmGet$idTipoMov() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idValorAccion:");
        stringBuilder.append(realmGet$idValorAccion() != null ? realmGet$idValorAccion() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{pesos:");
        stringBuilder.append(realmGet$pesos() != null ? realmGet$pesos() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{porcentaje:");
        stringBuilder.append(realmGet$porcentaje() != null ? realmGet$porcentaje() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{idFondoApp:");
        stringBuilder.append(realmGet$idFondoApp() != null ? realmGet$idFondoApp() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListReclasificacionRealmProxy aListReclasificacion = (ListReclasificacionRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aListReclasificacion.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aListReclasificacion.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aListReclasificacion.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
