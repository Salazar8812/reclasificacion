package io.realm;


public interface SolicitanteRealmProxyInterface {
    public String realmGet$apMaternoCte();
    public void realmSet$apMaternoCte(String value);
    public String realmGet$apPaternoCte();
    public void realmSet$apPaternoCte(String value);
    public String realmGet$celular();
    public void realmSet$celular(String value);
    public String realmGet$correoElec();
    public void realmSet$correoElec(String value);
    public String realmGet$curp();
    public void realmSet$curp(String value);
    public String realmGet$nombreCte();
    public void realmSet$nombreCte(String value);
    public String realmGet$nss();
    public void realmSet$nss(String value);
    public String realmGet$numCtaInvdual();
    public void realmSet$numCtaInvdual(String value);
    public String realmGet$rfcCte();
    public void realmSet$rfcCte(String value);
    public String realmGet$usuCre();
    public void realmSet$usuCre(String value);
    public String realmGet$calle();
    public void realmSet$calle(String value);
    public String realmGet$codigoPostal();
    public void realmSet$codigoPostal(String value);
    public String realmGet$colonia();
    public void realmSet$colonia(String value);
    public String realmGet$estado();
    public void realmSet$estado(String value);
    public String realmGet$municipioDelegacion();
    public void realmSet$municipioDelegacion(String value);
    public String realmGet$numeroExterior();
    public void realmSet$numeroExterior(String value);
    public String realmGet$numeroInterior();
    public void realmSet$numeroInterior(String value);
    public String realmGet$telefono();
    public void realmSet$telefono(String value);
}
