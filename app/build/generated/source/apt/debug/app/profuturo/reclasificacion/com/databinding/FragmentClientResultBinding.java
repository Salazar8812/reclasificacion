package app.profuturo.reclasificacion.com.databinding;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentClientResultBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.procedure_title, 1);
        sViewsWithIds.put(R.id.header_group, 2);
        sViewsWithIds.put(R.id.account_number_header, 3);
        sViewsWithIds.put(R.id.account_name_header, 4);
        sViewsWithIds.put(R.id.nss_header, 5);
        sViewsWithIds.put(R.id.curp_header, 6);
        sViewsWithIds.put(R.id.rfc_header, 7);
        sViewsWithIds.put(R.id.results_list, 8);
        sViewsWithIds.put(R.id.new_search, 9);
        sViewsWithIds.put(R.id.cancel, 10);
    }
    // views
    @NonNull
    public final android.widget.TextView accountNameHeader;
    @NonNull
    public final android.widget.TextView accountNumberHeader;
    @NonNull
    public final android.widget.Button cancel;
    @NonNull
    public final android.widget.TextView curpHeader;
    @NonNull
    public final android.support.constraint.Group headerGroup;
    @NonNull
    private final android.support.constraint.ConstraintLayout mboundView0;
    @NonNull
    public final android.widget.Button newSearch;
    @NonNull
    public final android.widget.TextView nssHeader;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.ProcedureHeader procedureTitle;
    @NonNull
    public final android.support.v7.widget.RecyclerView resultsList;
    @NonNull
    public final android.widget.TextView rfcHeader;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentClientResultBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds);
        this.accountNameHeader = (android.widget.TextView) bindings[4];
        this.accountNumberHeader = (android.widget.TextView) bindings[3];
        this.cancel = (android.widget.Button) bindings[10];
        this.curpHeader = (android.widget.TextView) bindings[6];
        this.headerGroup = (android.support.constraint.Group) bindings[2];
        this.mboundView0 = (android.support.constraint.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.newSearch = (android.widget.Button) bindings[9];
        this.nssHeader = (android.widget.TextView) bindings[5];
        this.procedureTitle = (app.profuturo.reclasificacion.com.widget.ProcedureHeader) bindings[1];
        this.resultsList = (android.support.v7.widget.RecyclerView) bindings[8];
        this.rfcHeader = (android.widget.TextView) bindings[7];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static FragmentClientResultBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentClientResultBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<FragmentClientResultBinding>inflate(inflater, app.profuturo.reclasificacion.com.R.layout.fragment_client_result, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static FragmentClientResultBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentClientResultBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(app.profuturo.reclasificacion.com.R.layout.fragment_client_result, null, false), bindingComponent);
    }
    @NonNull
    public static FragmentClientResultBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentClientResultBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/fragment_client_result_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new FragmentClientResultBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}