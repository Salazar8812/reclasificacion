package app.profuturo.reclasificacion.com.databinding;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityProcedureBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.lateral_toolbar, 3);
        sViewsWithIds.put(R.id.toolbar, 4);
        sViewsWithIds.put(R.id.procedure_content, 5);
        sViewsWithIds.put(R.id.left_drawer, 6);
        sViewsWithIds.put(R.id.profuturo_logo, 7);
        sViewsWithIds.put(R.id.welcome_title, 8);
    }
    // views
    @NonNull
    public final android.widget.TextView agentName;
    @NonNull
    public final android.support.v4.widget.DrawerLayout drawerLayout;
    @NonNull
    public final android.view.View lateralToolbar;
    @NonNull
    public final android.support.constraint.ConstraintLayout leftDrawer;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.NavigationViewPager procedureContent;
    @NonNull
    public final android.widget.ImageView profuturoLogo;
    @NonNull
    public final android.support.v7.widget.Toolbar toolbar;
    @NonNull
    public final android.widget.TextView version;
    @NonNull
    public final android.widget.TextView welcomeTitle;
    // variables
    @Nullable
    private java.lang.String mVersionNumber;
    @Nullable
    private app.profuturo.reclasificacion.com.model.AgentSession mAgentSession;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityProcedureBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 9, sIncludes, sViewsWithIds);
        this.agentName = (android.widget.TextView) bindings[1];
        this.agentName.setTag(null);
        this.drawerLayout = (android.support.v4.widget.DrawerLayout) bindings[0];
        this.drawerLayout.setTag(null);
        this.lateralToolbar = (android.view.View) bindings[3];
        this.leftDrawer = (android.support.constraint.ConstraintLayout) bindings[6];
        this.procedureContent = (app.profuturo.reclasificacion.com.widget.NavigationViewPager) bindings[5];
        this.profuturoLogo = (android.widget.ImageView) bindings[7];
        this.toolbar = (android.support.v7.widget.Toolbar) bindings[4];
        this.version = (android.widget.TextView) bindings[2];
        this.version.setTag(null);
        this.welcomeTitle = (android.widget.TextView) bindings[8];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.versionNumber == variableId) {
            setVersionNumber((java.lang.String) variable);
        }
        else if (BR.agentSession == variableId) {
            setAgentSession((app.profuturo.reclasificacion.com.model.AgentSession) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVersionNumber(@Nullable java.lang.String VersionNumber) {
        this.mVersionNumber = VersionNumber;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.versionNumber);
        super.requestRebind();
    }
    @Nullable
    public java.lang.String getVersionNumber() {
        return mVersionNumber;
    }
    public void setAgentSession(@Nullable app.profuturo.reclasificacion.com.model.AgentSession AgentSession) {
        this.mAgentSession = AgentSession;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.agentSession);
        super.requestRebind();
    }
    @Nullable
    public app.profuturo.reclasificacion.com.model.AgentSession getAgentSession() {
        return mAgentSession;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String agentSessionGetDisplayName = null;
        java.lang.String versionNumber = mVersionNumber;
        app.profuturo.reclasificacion.com.model.AgentSession agentSession = mAgentSession;
        java.lang.String versionAndroidStringVersionVersionNumber = null;

        if ((dirtyFlags & 0x5L) != 0) {



                // read @android:string/version
                versionAndroidStringVersionVersionNumber = version.getResources().getString(R.string.version, versionNumber);
        }
        if ((dirtyFlags & 0x6L) != 0) {



                if (agentSession != null) {
                    // read agentSession.getDisplayName
                    agentSessionGetDisplayName = agentSession.getDisplayName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.agentName, agentSessionGetDisplayName);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.version, versionAndroidStringVersionVersionNumber);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static ActivityProcedureBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivityProcedureBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<ActivityProcedureBinding>inflate(inflater, app.profuturo.reclasificacion.com.R.layout.activity_procedure, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static ActivityProcedureBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivityProcedureBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(app.profuturo.reclasificacion.com.R.layout.activity_procedure, null, false), bindingComponent);
    }
    @NonNull
    public static ActivityProcedureBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivityProcedureBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/activity_procedure_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new ActivityProcedureBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): versionNumber
        flag 1 (0x2L): agentSession
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}