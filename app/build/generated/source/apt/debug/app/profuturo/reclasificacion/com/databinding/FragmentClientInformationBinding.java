package app.profuturo.reclasificacion.com.databinding;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentClientInformationBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.distribution_guideline, 23);
        sViewsWithIds.put(R.id.vertical_distribution_guideline, 24);
        sViewsWithIds.put(R.id.basic_info_group, 25);
        sViewsWithIds.put(R.id.current_account_title, 26);
        sViewsWithIds.put(R.id.client_photo, 27);
        sViewsWithIds.put(R.id.vertical_seamless_guideline, 28);
        sViewsWithIds.put(R.id.client_demographic_group, 29);
        sViewsWithIds.put(R.id.facebook_icon, 30);
        sViewsWithIds.put(R.id.twitter_icon, 31);
        sViewsWithIds.put(R.id.client_identification_group, 32);
        sViewsWithIds.put(R.id.identification_transact_title, 33);
        sViewsWithIds.put(R.id.identification_transact_description, 34);
        sViewsWithIds.put(R.id.identification_transact_status_client_affiliate, 35);
        sViewsWithIds.put(R.id.biometric_transact_title, 36);
        sViewsWithIds.put(R.id.biometric_transact_description, 37);
        sViewsWithIds.put(R.id.identification_transact_status_img, 38);
        sViewsWithIds.put(R.id.indicator_compelement_group, 39);
        sViewsWithIds.put(R.id.identification_certification_description, 40);
        sViewsWithIds.put(R.id.identification_certification_status_date, 41);
        sViewsWithIds.put(R.id.client_join_title, 42);
        sViewsWithIds.put(R.id.clinet_join_description, 43);
        sViewsWithIds.put(R.id.identification_biometric_, 44);
        sViewsWithIds.put(R.id.client_next_group, 45);
        sViewsWithIds.put(R.id.mProcedureContinueButton, 46);
    }
    // views
    @NonNull
    public final android.support.constraint.Group basicInfoGroup;
    @NonNull
    public final android.widget.TextView biometricTransactDescription;
    @NonNull
    public final android.widget.TextView biometricTransactTitle;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientAccountNumber;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientBirthDate;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientCellPhone;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientCurp;
    @NonNull
    public final android.support.constraint.Group clientDemographicGroup;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientDistrict;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientExteriorNumber;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientGender;
    @NonNull
    public final android.support.constraint.Group clientIdentificationGroup;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientInteriorNumber;
    @NonNull
    public final android.widget.TextView clientJoinTitle;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientLandLine;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientLastName;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientName;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientNeighborhood;
    @NonNull
    public final android.support.constraint.Group clientNextGroup;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientNss;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.Base64ImageView clientPhoto;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientPostalCode;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientPrimaryEmail;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientRfc;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientSecondaryEmail;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientState;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientStreet;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView clientSurName;
    @NonNull
    public final android.widget.TextView clinetJoinDescription;
    @NonNull
    public final android.widget.TextView currentAccountTitle;
    @NonNull
    public final android.support.constraint.Guideline distributionGuideline;
    @NonNull
    public final android.widget.TextView facebookHandle;
    @NonNull
    public final android.widget.ImageView facebookIcon;
    @NonNull
    public final android.widget.ImageView identificationBiometric;
    @NonNull
    public final android.widget.TextView identificationCertificationDescription;
    @NonNull
    public final android.widget.ImageView identificationCertificationStatusDate;
    @NonNull
    public final android.widget.TextView identificationTransactDescription;
    @NonNull
    public final android.widget.ImageView identificationTransactStatusClientAffiliate;
    @NonNull
    public final android.widget.ImageView identificationTransactStatusImg;
    @NonNull
    public final android.widget.TextView identificationTransactTitle;
    @NonNull
    public final android.support.constraint.Group indicatorCompelementGroup;
    @NonNull
    public final android.widget.Button mProcedureContinueButton;
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    public final android.widget.TextView twitterHandle;
    @NonNull
    public final android.widget.ImageView twitterIcon;
    @NonNull
    public final android.support.constraint.Guideline verticalDistributionGuideline;
    @NonNull
    public final android.support.constraint.Guideline verticalSeamlessGuideline;
    // variables
    @Nullable
    private app.profuturo.reclasificacion.com.model.Client mClient;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentClientInformationBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 47, sIncludes, sViewsWithIds);
        this.basicInfoGroup = (android.support.constraint.Group) bindings[25];
        this.biometricTransactDescription = (android.widget.TextView) bindings[37];
        this.biometricTransactTitle = (android.widget.TextView) bindings[36];
        this.clientAccountNumber = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[4];
        this.clientAccountNumber.setTag(null);
        this.clientBirthDate = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[9];
        this.clientBirthDate.setTag(null);
        this.clientCellPhone = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[17];
        this.clientCellPhone.setTag(null);
        this.clientCurp = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[6];
        this.clientCurp.setTag(null);
        this.clientDemographicGroup = (android.support.constraint.Group) bindings[29];
        this.clientDistrict = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[15];
        this.clientDistrict.setTag(null);
        this.clientExteriorNumber = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[11];
        this.clientExteriorNumber.setTag(null);
        this.clientGender = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[8];
        this.clientGender.setTag(null);
        this.clientIdentificationGroup = (android.support.constraint.Group) bindings[32];
        this.clientInteriorNumber = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[12];
        this.clientInteriorNumber.setTag(null);
        this.clientJoinTitle = (android.widget.TextView) bindings[42];
        this.clientLandLine = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[18];
        this.clientLandLine.setTag(null);
        this.clientLastName = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[3];
        this.clientLastName.setTag(null);
        this.clientName = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[1];
        this.clientName.setTag(null);
        this.clientNeighborhood = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[13];
        this.clientNeighborhood.setTag(null);
        this.clientNextGroup = (android.support.constraint.Group) bindings[45];
        this.clientNss = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[5];
        this.clientNss.setTag(null);
        this.clientPhoto = (app.profuturo.reclasificacion.com.widget.Base64ImageView) bindings[27];
        this.clientPostalCode = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[14];
        this.clientPostalCode.setTag(null);
        this.clientPrimaryEmail = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[19];
        this.clientPrimaryEmail.setTag(null);
        this.clientRfc = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[7];
        this.clientRfc.setTag(null);
        this.clientSecondaryEmail = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[20];
        this.clientSecondaryEmail.setTag(null);
        this.clientState = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[16];
        this.clientState.setTag(null);
        this.clientStreet = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[10];
        this.clientStreet.setTag(null);
        this.clientSurName = (app.profuturo.reclasificacion.com.widget.HorizontalLabeledTextView) bindings[2];
        this.clientSurName.setTag(null);
        this.clinetJoinDescription = (android.widget.TextView) bindings[43];
        this.currentAccountTitle = (android.widget.TextView) bindings[26];
        this.distributionGuideline = (android.support.constraint.Guideline) bindings[23];
        this.facebookHandle = (android.widget.TextView) bindings[21];
        this.facebookHandle.setTag(null);
        this.facebookIcon = (android.widget.ImageView) bindings[30];
        this.identificationBiometric = (android.widget.ImageView) bindings[44];
        this.identificationCertificationDescription = (android.widget.TextView) bindings[40];
        this.identificationCertificationStatusDate = (android.widget.ImageView) bindings[41];
        this.identificationTransactDescription = (android.widget.TextView) bindings[34];
        this.identificationTransactStatusClientAffiliate = (android.widget.ImageView) bindings[35];
        this.identificationTransactStatusImg = (android.widget.ImageView) bindings[38];
        this.identificationTransactTitle = (android.widget.TextView) bindings[33];
        this.indicatorCompelementGroup = (android.support.constraint.Group) bindings[39];
        this.mProcedureContinueButton = (android.widget.Button) bindings[46];
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.twitterHandle = (android.widget.TextView) bindings[22];
        this.twitterHandle.setTag(null);
        this.twitterIcon = (android.widget.ImageView) bindings[31];
        this.verticalDistributionGuideline = (android.support.constraint.Guideline) bindings[24];
        this.verticalSeamlessGuideline = (android.support.constraint.Guideline) bindings[28];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.client == variableId) {
            setClient((app.profuturo.reclasificacion.com.model.Client) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setClient(@Nullable app.profuturo.reclasificacion.com.model.Client Client) {
        this.mClient = Client;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.client);
        super.requestRebind();
    }
    @Nullable
    public app.profuturo.reclasificacion.com.model.Client getClient() {
        return mClient;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String clientOuterNumber = null;
        java.lang.String ClientLastName1 = null;
        java.lang.String clientFacebookUser = null;
        java.lang.String ClientName1 = null;
        java.lang.String twitterHandleAndroidStringTwitterHandleClientTwitterUser = null;
        java.lang.String clientInnerNumber = null;
        java.lang.String clientGetDisplayDate = null;
        java.lang.String ClientAccountNumber1 = null;
        java.lang.String ClientRfc1 = null;
        java.lang.String ClientState1 = null;
        java.lang.String clientZipCode = null;
        java.lang.String ClientNeighborhood1 = null;
        java.lang.String ClientSecondaryEmail1 = null;
        java.lang.String ClientGender1 = null;
        java.lang.String clientLandlineNumber = null;
        java.lang.String ClientDistrict1 = null;
        java.lang.String clientEmail = null;
        java.lang.String ClientStreet1 = null;
        java.lang.String ClientSurName1 = null;
        java.lang.String clientTwitterUser = null;
        java.lang.String ClientNss1 = null;
        java.lang.String clientCellphoneNumber = null;
        app.profuturo.reclasificacion.com.model.Client client = mClient;
        java.lang.String ClientCurp1 = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (client != null) {
                    // read client.outerNumber
                    clientOuterNumber = client.getOuterNumber();
                    // read client.lastName
                    ClientLastName1 = client.getLastName();
                    // read client.facebookUser
                    clientFacebookUser = client.getFacebookUser();
                    // read client.name
                    ClientName1 = client.getName();
                    // read client.innerNumber
                    clientInnerNumber = client.getInnerNumber();
                    // read client.getDisplayDate()
                    clientGetDisplayDate = client.getDisplayDate();
                    // read client.accountNumber
                    ClientAccountNumber1 = client.getAccountNumber();
                    // read client.rfc
                    ClientRfc1 = client.getRfc();
                    // read client.state
                    ClientState1 = client.getState();
                    // read client.zipCode
                    clientZipCode = client.getZipCode();
                    // read client.neighborhood
                    ClientNeighborhood1 = client.getNeighborhood();
                    // read client.secondaryEmail
                    ClientSecondaryEmail1 = client.getSecondaryEmail();
                    // read client.gender
                    ClientGender1 = client.getGender();
                    // read client.landlineNumber
                    clientLandlineNumber = client.getLandlineNumber();
                    // read client.district
                    ClientDistrict1 = client.getDistrict();
                    // read client.email
                    clientEmail = client.getEmail();
                    // read client.street
                    ClientStreet1 = client.getStreet();
                    // read client.surName
                    ClientSurName1 = client.getSurName();
                    // read client.twitterUser
                    clientTwitterUser = client.getTwitterUser();
                    // read client.nss
                    ClientNss1 = client.getNss();
                    // read client.cellphoneNumber
                    clientCellphoneNumber = client.getCellphoneNumber();
                    // read client.curp
                    ClientCurp1 = client.getCurp();
                }


                // read @android:string/twitter_handle
                twitterHandleAndroidStringTwitterHandleClientTwitterUser = twitterHandle.getResources().getString(R.string.twitter_handle, clientTwitterUser);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.clientAccountNumber.setClientDataContent(ClientAccountNumber1);
            this.clientBirthDate.setClientDataContent(clientGetDisplayDate);
            this.clientCellPhone.setClientDataContent(clientCellphoneNumber);
            this.clientCurp.setClientDataContent(ClientCurp1);
            this.clientDistrict.setClientDataContent(ClientDistrict1);
            this.clientExteriorNumber.setClientDataContent(clientOuterNumber);
            this.clientGender.setClientDataContent(ClientGender1);
            this.clientInteriorNumber.setClientDataContent(clientInnerNumber);
            this.clientLandLine.setClientDataContent(clientLandlineNumber);
            this.clientLastName.setClientDataContent(ClientSurName1);
            this.clientName.setClientDataContent(ClientName1);
            this.clientNeighborhood.setClientDataContent(ClientNeighborhood1);
            this.clientNss.setClientDataContent(ClientNss1);
            this.clientPostalCode.setClientDataContent(clientZipCode);
            this.clientPrimaryEmail.setClientDataContent(clientEmail);
            this.clientRfc.setClientDataContent(ClientRfc1);
            this.clientSecondaryEmail.setClientDataContent(ClientSecondaryEmail1);
            this.clientState.setClientDataContent(ClientState1);
            this.clientStreet.setClientDataContent(ClientStreet1);
            this.clientSurName.setClientDataContent(ClientLastName1);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.facebookHandle, clientFacebookUser);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.twitterHandle, twitterHandleAndroidStringTwitterHandleClientTwitterUser);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static FragmentClientInformationBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentClientInformationBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<FragmentClientInformationBinding>inflate(inflater, app.profuturo.reclasificacion.com.R.layout.fragment_client_information, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static FragmentClientInformationBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentClientInformationBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(app.profuturo.reclasificacion.com.R.layout.fragment_client_information, null, false), bindingComponent);
    }
    @NonNull
    public static FragmentClientInformationBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentClientInformationBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/fragment_client_information_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new FragmentClientInformationBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): client
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}