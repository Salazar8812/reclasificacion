package app.profuturo.reclasificacion.com.databinding;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivitySplashBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.profuturo_logo, 2);
        sViewsWithIds.put(R.id.welcome, 3);
        sViewsWithIds.put(R.id.branch_spinner, 4);
        sViewsWithIds.put(R.id.search, 5);
        sViewsWithIds.put(R.id.search_button, 6);
        sViewsWithIds.put(R.id.exit, 7);
        sViewsWithIds.put(R.id.version, 8);
    }
    // views
    @NonNull
    public final android.widget.Spinner branchSpinner;
    @NonNull
    public final android.widget.TextView exit;
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    public final android.widget.TextView name;
    @NonNull
    public final android.widget.ImageView profuturoLogo;
    @NonNull
    public final android.widget.ImageView search;
    @NonNull
    public final android.widget.Button searchButton;
    @NonNull
    public final android.widget.TextView version;
    @NonNull
    public final android.widget.TextView welcome;
    // variables
    @Nullable
    private app.profuturo.reclasificacion.com.presenter.BranchesPresenter mPresenter;
    @Nullable
    private java.lang.String mAgentName;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivitySplashBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 9, sIncludes, sViewsWithIds);
        this.branchSpinner = (android.widget.Spinner) bindings[4];
        this.exit = (android.widget.TextView) bindings[7];
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.name = (android.widget.TextView) bindings[1];
        this.name.setTag(null);
        this.profuturoLogo = (android.widget.ImageView) bindings[2];
        this.search = (android.widget.ImageView) bindings[5];
        this.searchButton = (android.widget.Button) bindings[6];
        this.version = (android.widget.TextView) bindings[8];
        this.welcome = (android.widget.TextView) bindings[3];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.presenter == variableId) {
            setPresenter((app.profuturo.reclasificacion.com.presenter.BranchesPresenter) variable);
        }
        else if (BR.agentName == variableId) {
            setAgentName((java.lang.String) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setPresenter(@Nullable app.profuturo.reclasificacion.com.presenter.BranchesPresenter Presenter) {
        this.mPresenter = Presenter;
    }
    @Nullable
    public app.profuturo.reclasificacion.com.presenter.BranchesPresenter getPresenter() {
        return mPresenter;
    }
    public void setAgentName(@Nullable java.lang.String AgentName) {
        this.mAgentName = AgentName;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.agentName);
        super.requestRebind();
    }
    @Nullable
    public java.lang.String getAgentName() {
        return mAgentName;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String agentName = mAgentName;

        if ((dirtyFlags & 0x6L) != 0) {
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.name, agentName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static ActivitySplashBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivitySplashBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<ActivitySplashBinding>inflate(inflater, app.profuturo.reclasificacion.com.R.layout.activity_splash, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static ActivitySplashBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivitySplashBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(app.profuturo.reclasificacion.com.R.layout.activity_splash, null, false), bindingComponent);
    }
    @NonNull
    public static ActivitySplashBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivitySplashBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/activity_splash_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new ActivitySplashBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): presenter
        flag 1 (0x2L): agentName
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}