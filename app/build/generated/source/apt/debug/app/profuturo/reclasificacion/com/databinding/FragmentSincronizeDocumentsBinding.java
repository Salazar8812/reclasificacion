package app.profuturo.reclasificacion.com.databinding;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentSincronizeDocumentsBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.documents_capture_title, 1);
        sViewsWithIds.put(R.id.client_name_account_number, 2);
        sViewsWithIds.put(R.id.documents_zoom_division, 3);
        sViewsWithIds.put(R.id.cancel, 4);
        sViewsWithIds.put(R.id.mfinishProcedureButton, 5);
        sViewsWithIds.put(R.id.mSmsRB, 6);
        sViewsWithIds.put(R.id.mSMSRadio, 7);
        sViewsWithIds.put(R.id.mEmailRB, 8);
        sViewsWithIds.put(R.id.mEmailRadio, 9);
        sViewsWithIds.put(R.id.mSocialRB, 10);
        sViewsWithIds.put(R.id.mSocialRadio, 11);
        sViewsWithIds.put(R.id.mNoneRB, 12);
        sViewsWithIds.put(R.id.mNoneRadio, 13);
        sViewsWithIds.put(R.id.mChangeNotification, 14);
        sViewsWithIds.put(R.id.documents_list, 15);
        sViewsWithIds.put(R.id.progressBar, 16);
        sViewsWithIds.put(R.id.mListDocumentsSincronizaRecyclerView, 17);
        sViewsWithIds.put(R.id.documents_compulsory_label, 18);
        sViewsWithIds.put(R.id.main_content_cancel_button_division, 19);
    }
    // views
    @NonNull
    public final android.widget.Button cancel;
    @NonNull
    public final android.widget.TextView clientNameAccountNumber;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.ProcedureHeader documentsCaptureTitle;
    @NonNull
    public final android.widget.TextView documentsCompulsoryLabel;
    @NonNull
    public final android.support.v7.widget.RecyclerView documentsList;
    @NonNull
    public final android.support.constraint.Guideline documentsZoomDivision;
    @NonNull
    public final android.widget.Button mChangeNotification;
    @NonNull
    public final android.widget.LinearLayout mEmailRB;
    @NonNull
    public final android.widget.RadioButton mEmailRadio;
    @NonNull
    public final android.support.v7.widget.RecyclerView mListDocumentsSincronizaRecyclerView;
    @NonNull
    public final android.widget.LinearLayout mNoneRB;
    @NonNull
    public final android.widget.RadioButton mNoneRadio;
    @NonNull
    public final android.widget.RadioButton mSMSRadio;
    @NonNull
    public final android.widget.LinearLayout mSmsRB;
    @NonNull
    public final android.widget.LinearLayout mSocialRB;
    @NonNull
    public final android.widget.RadioButton mSocialRadio;
    @NonNull
    public final android.support.constraint.Guideline mainContentCancelButtonDivision;
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    public final android.widget.Button mfinishProcedureButton;
    @NonNull
    public final android.widget.ProgressBar progressBar;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentSincronizeDocumentsBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 20, sIncludes, sViewsWithIds);
        this.cancel = (android.widget.Button) bindings[4];
        this.clientNameAccountNumber = (android.widget.TextView) bindings[2];
        this.documentsCaptureTitle = (app.profuturo.reclasificacion.com.widget.ProcedureHeader) bindings[1];
        this.documentsCompulsoryLabel = (android.widget.TextView) bindings[18];
        this.documentsList = (android.support.v7.widget.RecyclerView) bindings[15];
        this.documentsZoomDivision = (android.support.constraint.Guideline) bindings[3];
        this.mChangeNotification = (android.widget.Button) bindings[14];
        this.mEmailRB = (android.widget.LinearLayout) bindings[8];
        this.mEmailRadio = (android.widget.RadioButton) bindings[9];
        this.mListDocumentsSincronizaRecyclerView = (android.support.v7.widget.RecyclerView) bindings[17];
        this.mNoneRB = (android.widget.LinearLayout) bindings[12];
        this.mNoneRadio = (android.widget.RadioButton) bindings[13];
        this.mSMSRadio = (android.widget.RadioButton) bindings[7];
        this.mSmsRB = (android.widget.LinearLayout) bindings[6];
        this.mSocialRB = (android.widget.LinearLayout) bindings[10];
        this.mSocialRadio = (android.widget.RadioButton) bindings[11];
        this.mainContentCancelButtonDivision = (android.support.constraint.Guideline) bindings[19];
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mfinishProcedureButton = (android.widget.Button) bindings[5];
        this.progressBar = (android.widget.ProgressBar) bindings[16];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static FragmentSincronizeDocumentsBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentSincronizeDocumentsBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<FragmentSincronizeDocumentsBinding>inflate(inflater, app.profuturo.reclasificacion.com.R.layout.fragment_sincronize_documents, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static FragmentSincronizeDocumentsBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentSincronizeDocumentsBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(app.profuturo.reclasificacion.com.R.layout.fragment_sincronize_documents, null, false), bindingComponent);
    }
    @NonNull
    public static FragmentSincronizeDocumentsBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentSincronizeDocumentsBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/fragment_sincronize_documents_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new FragmentSincronizeDocumentsBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}