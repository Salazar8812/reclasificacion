package app.profuturo.reclasificacion.com.databinding;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentCaptureReclasificacionBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.documents_capture_title, 1);
        sViewsWithIds.put(R.id.client_name_account_number, 2);
        sViewsWithIds.put(R.id.client_description_ammount, 3);
        sViewsWithIds.put(R.id.mContentHeaderCheck, 4);
        sViewsWithIds.put(R.id.documents_list, 5);
        sViewsWithIds.put(R.id.mParcialLinearLayout, 6);
        sViewsWithIds.put(R.id.mParcialAmmountImageView, 7);
        sViewsWithIds.put(R.id.mTotalLinearLayout, 8);
        sViewsWithIds.put(R.id.mTotalAmmountImageView, 9);
        sViewsWithIds.put(R.id.mAmmountParcialET, 10);
        sViewsWithIds.put(R.id.mPorcentRB, 11);
        sViewsWithIds.put(R.id.mPorcentImageView, 12);
        sViewsWithIds.put(R.id.mAmmountRB, 13);
        sViewsWithIds.put(R.id.mAmountlImageView, 14);
        sViewsWithIds.put(R.id.mTypeSubAccountOneTextView, 15);
        sViewsWithIds.put(R.id.mSimbolTextOneView, 16);
        sViewsWithIds.put(R.id.mAmmountSubAccountOneTextView, 17);
        sViewsWithIds.put(R.id.mTypeSubAccountTwoTextView, 18);
        sViewsWithIds.put(R.id.mSimbolTwoTextView, 19);
        sViewsWithIds.put(R.id.mAmmountSubAccountTwoTextView, 20);
        sViewsWithIds.put(R.id.mTypeSubAccountThreeTextView, 21);
        sViewsWithIds.put(R.id.mSimbolThreeTextView, 22);
        sViewsWithIds.put(R.id.mAmmountSubAccountThreeTextView, 23);
        sViewsWithIds.put(R.id.mTypeSubAccountFourTextView, 24);
        sViewsWithIds.put(R.id.mSimbolFourTextView, 25);
        sViewsWithIds.put(R.id.mAmmountSubAccountFourTextView, 26);
        sViewsWithIds.put(R.id.mIndicatorFive, 27);
        sViewsWithIds.put(R.id.mSimbolFive, 28);
        sViewsWithIds.put(R.id.mAmmountFive, 29);
        sViewsWithIds.put(R.id.cancel, 30);
        sViewsWithIds.put(R.id.mNextButton, 31);
    }
    // views
    @NonNull
    public final android.widget.Button cancel;
    @NonNull
    public final android.widget.TextView clientDescriptionAmmount;
    @NonNull
    public final android.widget.TextView clientNameAccountNumber;
    @NonNull
    public final app.profuturo.reclasificacion.com.widget.ProcedureHeader documentsCaptureTitle;
    @NonNull
    public final android.support.v7.widget.RecyclerView documentsList;
    @NonNull
    public final android.widget.EditText mAmmountFive;
    @NonNull
    public final android.widget.EditText mAmmountParcialET;
    @NonNull
    public final android.widget.LinearLayout mAmmountRB;
    @NonNull
    public final android.widget.EditText mAmmountSubAccountFourTextView;
    @NonNull
    public final android.widget.EditText mAmmountSubAccountOneTextView;
    @NonNull
    public final android.widget.EditText mAmmountSubAccountThreeTextView;
    @NonNull
    public final android.widget.EditText mAmmountSubAccountTwoTextView;
    @NonNull
    public final android.widget.ImageView mAmountlImageView;
    @NonNull
    public final android.widget.LinearLayout mContentHeaderCheck;
    @NonNull
    public final android.widget.TextView mIndicatorFive;
    @NonNull
    public final android.widget.Button mNextButton;
    @NonNull
    public final android.widget.ImageView mParcialAmmountImageView;
    @NonNull
    public final android.widget.LinearLayout mParcialLinearLayout;
    @NonNull
    public final android.widget.ImageView mPorcentImageView;
    @NonNull
    public final android.widget.LinearLayout mPorcentRB;
    @NonNull
    public final android.widget.TextView mSimbolFive;
    @NonNull
    public final android.widget.TextView mSimbolFourTextView;
    @NonNull
    public final android.widget.TextView mSimbolTextOneView;
    @NonNull
    public final android.widget.TextView mSimbolThreeTextView;
    @NonNull
    public final android.widget.TextView mSimbolTwoTextView;
    @NonNull
    public final android.widget.ImageView mTotalAmmountImageView;
    @NonNull
    public final android.widget.LinearLayout mTotalLinearLayout;
    @NonNull
    public final android.widget.TextView mTypeSubAccountFourTextView;
    @NonNull
    public final android.widget.TextView mTypeSubAccountOneTextView;
    @NonNull
    public final android.widget.TextView mTypeSubAccountThreeTextView;
    @NonNull
    public final android.widget.TextView mTypeSubAccountTwoTextView;
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentCaptureReclasificacionBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 32, sIncludes, sViewsWithIds);
        this.cancel = (android.widget.Button) bindings[30];
        this.clientDescriptionAmmount = (android.widget.TextView) bindings[3];
        this.clientNameAccountNumber = (android.widget.TextView) bindings[2];
        this.documentsCaptureTitle = (app.profuturo.reclasificacion.com.widget.ProcedureHeader) bindings[1];
        this.documentsList = (android.support.v7.widget.RecyclerView) bindings[5];
        this.mAmmountFive = (android.widget.EditText) bindings[29];
        this.mAmmountParcialET = (android.widget.EditText) bindings[10];
        this.mAmmountRB = (android.widget.LinearLayout) bindings[13];
        this.mAmmountSubAccountFourTextView = (android.widget.EditText) bindings[26];
        this.mAmmountSubAccountOneTextView = (android.widget.EditText) bindings[17];
        this.mAmmountSubAccountThreeTextView = (android.widget.EditText) bindings[23];
        this.mAmmountSubAccountTwoTextView = (android.widget.EditText) bindings[20];
        this.mAmountlImageView = (android.widget.ImageView) bindings[14];
        this.mContentHeaderCheck = (android.widget.LinearLayout) bindings[4];
        this.mIndicatorFive = (android.widget.TextView) bindings[27];
        this.mNextButton = (android.widget.Button) bindings[31];
        this.mParcialAmmountImageView = (android.widget.ImageView) bindings[7];
        this.mParcialLinearLayout = (android.widget.LinearLayout) bindings[6];
        this.mPorcentImageView = (android.widget.ImageView) bindings[12];
        this.mPorcentRB = (android.widget.LinearLayout) bindings[11];
        this.mSimbolFive = (android.widget.TextView) bindings[28];
        this.mSimbolFourTextView = (android.widget.TextView) bindings[25];
        this.mSimbolTextOneView = (android.widget.TextView) bindings[16];
        this.mSimbolThreeTextView = (android.widget.TextView) bindings[22];
        this.mSimbolTwoTextView = (android.widget.TextView) bindings[19];
        this.mTotalAmmountImageView = (android.widget.ImageView) bindings[9];
        this.mTotalLinearLayout = (android.widget.LinearLayout) bindings[8];
        this.mTypeSubAccountFourTextView = (android.widget.TextView) bindings[24];
        this.mTypeSubAccountOneTextView = (android.widget.TextView) bindings[15];
        this.mTypeSubAccountThreeTextView = (android.widget.TextView) bindings[21];
        this.mTypeSubAccountTwoTextView = (android.widget.TextView) bindings[18];
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static FragmentCaptureReclasificacionBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentCaptureReclasificacionBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<FragmentCaptureReclasificacionBinding>inflate(inflater, app.profuturo.reclasificacion.com.R.layout.fragment_capture_reclasificacion, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static FragmentCaptureReclasificacionBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentCaptureReclasificacionBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(app.profuturo.reclasificacion.com.R.layout.fragment_capture_reclasificacion, null, false), bindingComponent);
    }
    @NonNull
    public static FragmentCaptureReclasificacionBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentCaptureReclasificacionBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/fragment_capture_reclasificacion_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new FragmentCaptureReclasificacionBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}