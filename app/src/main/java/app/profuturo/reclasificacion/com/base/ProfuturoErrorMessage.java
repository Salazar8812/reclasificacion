package app.profuturo.reclasificacion.com.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfuturoErrorMessage {
    @Expose
    @SerializedName("message")
    private String message;

    public ProfuturoErrorMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
