package app.profuturo.reclasificacion.com.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;

public class SavingsAccount extends RealmObject implements Parcelable {

    public static final Creator<SavingsAccount> CREATOR = new Creator<SavingsAccount>() {
        @Override
        public SavingsAccount createFromParcel(Parcel in) {
            return new SavingsAccount(in);
        }

        @Override
        public SavingsAccount[] newArray(int size) {
            return new SavingsAccount[size];
        }
    };
    private Shares shares;
    private Double totalHousingInPesos;
    private Double totalNotHousingInPesos;
    private Double totalAllAccountsInPesos;

    public SavingsAccount() {
    }

    protected SavingsAccount(Parcel in) {
        if (in.readByte() == 0) {
            totalHousingInPesos = null;
        } else {
            totalHousingInPesos = in.readDouble();
        }
        if (in.readByte() == 0) {
            totalNotHousingInPesos = null;
        } else {
            totalNotHousingInPesos = in.readDouble();
        }
        if (in.readByte() == 0) {
            totalAllAccountsInPesos = null;
        } else {
            totalAllAccountsInPesos = in.readDouble();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (totalHousingInPesos == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(totalHousingInPesos);
        }
        if (totalNotHousingInPesos == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(totalNotHousingInPesos);
        }
        if (totalAllAccountsInPesos == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(totalAllAccountsInPesos);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Shares getShares() {
        return shares;
    }

    public void setShares(Shares shares) {
        this.shares = shares;
    }

    public Double getTotalHousingInPesos() {
        return totalHousingInPesos;
    }

    public void setTotalHousingInPesos(Double totalHousingInPesos) {
        this.totalHousingInPesos = totalHousingInPesos;
    }

    public Double getTotalNotHousingInPesos() {
        return totalNotHousingInPesos;
    }

    public void setTotalNotHousingInPesos(Double totalNotHousingInPesos) {
        this.totalNotHousingInPesos = totalNotHousingInPesos;
    }

    public Double getTotalAllAccountsInPesos() {
        return totalAllAccountsInPesos;
    }

    public void setTotalAllAccountsInPesos(Double totalAllAccountsInPesos) {
        this.totalAllAccountsInPesos = totalAllAccountsInPesos;
    }
}

