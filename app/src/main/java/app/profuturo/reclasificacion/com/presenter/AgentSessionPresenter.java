package app.profuturo.reclasificacion.com.presenter;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import app.profuturo.reclasificacion.com.BuildConfig;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.Utils.PrefsReclasification;
import app.profuturo.reclasificacion.com.base.View.BaseView;
import app.profuturo.reclasificacion.com.base.utils.JsonMocker;
import app.profuturo.reclasificacion.com.model.AgentData;
import app.profuturo.reclasificacion.com.model.AgentNumber;
import app.profuturo.reclasificacion.com.model.AgentSession;
import app.profuturo.reclasificacion.com.model.AgentUser;
import app.profuturo.reclasificacion.com.model.AssignedBranch;
import app.profuturo.reclasificacion.com.model.ProfuturoRequest;
import retrofit2.Call;

public class AgentSessionPresenter extends NetworkPresenter {
    /**
     * Constant key that defines the agent number within an {@link Intent} object.
     */
    public static final String AGENT_NUMBER = "agent_number";

    /**
     * Reference to the retrieved session to be stored locally.
     */
    private AgentSession mAgentSession;

    /**
     * Constructor for this presenter class that assigns a View.
     * This view can be null, this is useful in Unit Test cases.
     *
     * @param view The view for this presenter.
     */
    public AgentSessionPresenter(@NonNull BaseView view) {
        super(view);
    }

    /**
     * Retrieves the current agent session.
     *
     * @return The agent session that has been worked with.
     */
    @NonNull
    public AgentSession getAgentSession() {
        return mAgentSession;
    }

    /**
     * Extracts a the agent number string value from a given intent.
     *
     * @param intent The intent containing the Agent Number value.
     * @return The agent number value or empty if the intent came null.
     */
    public String agentNumberFromIntent(@Nullable Intent intent) {
        String agentNumber = "";
        // Check if the intent is not null, its action correspond to ACTION_VIEW and its deep link scheme correspond to this app.
        if (intent != null && intent.hasExtra(AGENT_NUMBER)) {
            // Return the retrieved agent number (it should came in the host).
            agentNumber = intent.getStringExtra(AGENT_NUMBER);
        }
        return agentNumber;
    }

    /**
     * Builds a request to retrieve the user agent session and enqueues it.
     *
     * @param agentNumberValue The agent number value to make the request.
     */
    public void getAgentInformation(final String agentNumberValue, PrefsReclasification prefsReclasification) {
        runOnWorkerThread(() -> {
            // Create the agent number class wrapping the agent number value.
            AgentUser agentUser = new AgentUser("112082"/*agentNumberValue*/);//new AgentUser(agentNumberValue);
            prefsReclasification.saveData("idAgent", agentUser.getValue());

            // Build the request object for retrieving the agent information.
            Call<AgentData> request = routes.getAgentInformation(new ProfuturoRequest<>(agentUser));
            // Enqueue the agent session request.
            enqueueRequest(request, payload -> {
                // Reference the retrieved session for further modifications.
                mAgentSession = payload.getEmployee();
                // The retrieved session can't be null, if that happens report that something wrong happened.
                if (mAgentSession == null) {
                    setStatus(Status.STATUS_FAILED);
                } else {
                    // Now, retrieve the assigned branch for this agent.
                    getAgentAssignedBranch();
                }
            });
        });
    }

    /**
     * Retrieves the assigned agent branch from a login.
     */
    private void getAgentAssignedBranch() {
        if (mAgentSession != null) {
            AgentNumber agentNumber = new AgentNumber(mAgentSession.getAgentNumber());
            Call<AssignedBranch> request = routes.getAgentBranchInformation(new ProfuturoRequest<>(agentNumber));
            enqueueRequest(request, payload -> {
                // Assign assigned branch data to the agent session.
                mAgentSession.setAssignedBranchId(payload.getNumber());
                mAgentSession.setAssignedBranchName(payload.getName());
                // Now, perform a login attempt with this agent session.
                setStatus(Status.STATUS_DONE);
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getBaseURL() {
        return BuildConfig.PROFUTURO_BASE_URL;
    }
}

