package app.profuturo.reclasificacion.com.Repository.local;

import android.support.annotation.NonNull;

import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.base.BaseDao;
import io.realm.Realm;
import io.realm.RealmResults;

public class ClientDAO extends BaseDao {

    /**
     * Inserts a {@link Client} into Realm
     * Realm proceeds to delete all of them and then insert the new one
     *
     * @param client the client to insert
     */

    public static void createClient(final Client client) {
        performTransaction(realm -> {
            // Retrieve all the records from the client table.
            RealmResults<Client> clientes = realm.where(Client.class).findAll();
            // Check if the list is not empty.
            if (!clientes.isEmpty()) {
                // If the list is not empty, delete the records from realm.
                clientes.deleteAllFromRealm();
            }
            // Then, proceed to insert the client object.
            realm.insertOrUpdate(client);
        });
    }

    /**
     * Retrieves the first Client object from Realm.
     *
     * @return The Client object from realm.
     */
    public static Client readClient() {
        // Create a new ClientTransaction object.
        ClientDAO.ClientTransaction transaction = new ClientTransaction();
        // Execute the transaction.
        performTransaction(transaction);
        // Then, proceed to retrieve the transaction.
        return transaction.retrievedClient;
    }

    /**
     * Delete all the records from the Client table.
     */
    public static void deleteClient() {
        performTransaction(realm -> {
            // Retrieve the records from the client table.
            RealmResults<Client> clients = realm.where(Client.class).findAll();
            // Delete all those records from realm.
            clients.deleteAllFromRealm();
        });
    }

    /**
     * This ClientTransaction class holds a reference of
     * a retrieved Client object from the local database.
     */
    static class ClientTransaction implements Realm.Transaction {
        /**
         * Reference to the Client object copied from Realm.
         */
        private Client retrievedClient;

        /**
         * Execute the transaction.
         *
         * @param realm Realm instance executing the transaction.
         */
        @Override
        public void execute(@NonNull Realm realm) {
            // Get the client from Realm.
            Client client = realm.where(Client.class).findFirst();
            // If the client is not null, copy the realm object to this transaction client.
            if (client != null) {
                // Copy the value from Realm.
                retrievedClient = realm.copyFromRealm(client);
            }
        }
    }

}
