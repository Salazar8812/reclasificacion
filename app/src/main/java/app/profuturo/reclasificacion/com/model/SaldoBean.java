
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaldoBean {

    @SerializedName("idSubcuenta")
    @Expose
    private String idSubcuenta;

    @SerializedName("numCuentaIndividual")
    @Expose
    private String numCuentaIndividual;
    @SerializedName("tablaNciMov")
    @Expose
    private Object tablaNciMov;
    @SerializedName("tipoSubcuenta")
    @Expose
    private TipoSubcuenta tipoSubcuenta;
    @SerializedName("siefore")
    @Expose
    private Siefore siefore;
    @SerializedName("saldoAlDiaAcciones")
    @Expose
    private String saldoAlDiaAcciones;
    @SerializedName("saldoAlDiaPesos")
    @Expose
    private String saldoAlDiaPesos;
    @SerializedName("saldoDisponibleAcciones")
    @Expose
    private String saldoDisponibleAcciones;
    @SerializedName("saldoComprometidoAcciones")
    @Expose
    private String saldoComprometidoAcciones;
    @SerializedName("saldoComprometidoPesos")
    @Expose
    private String saldoComprometidoPesos;
    @SerializedName("saldoPendienteAcciones")
    @Expose
    private String saldoPendienteAcciones;
    @SerializedName("saldoPendientePesos")
    @Expose
    private String saldoPendientePesos;

    @SerializedName("plazo")
    @Expose
    private String plazo;

    @SerializedName("deducible")
    @Expose
    private String deducible;

    @SerializedName("idValorAccion")
    private String idValorAccion;

    @SerializedName("valorAccion")
    private String valorAccion;

    @SerializedName("descripcionPlazo")
    private String descripcionPlazo;


    public String getIdSubcuenta() {
        return idSubcuenta;
    }

    public void setIdSubcuenta(String idSubcuenta) {
        this.idSubcuenta = idSubcuenta;
    }

    public String getNumCuentaIndividual() {
        return numCuentaIndividual;
    }

    public void setNumCuentaIndividual(String numCuentaIndividual) {
        this.numCuentaIndividual = numCuentaIndividual;
    }

    public Object getTablaNciMov() {
        return tablaNciMov;
    }

    public void setTablaNciMov(Object tablaNciMov) {
        this.tablaNciMov = tablaNciMov;
    }

    public TipoSubcuenta getTipoSubcuenta() {
        return tipoSubcuenta;
    }

    public void setTipoSubcuenta(TipoSubcuenta tipoSubcuenta) {
        this.tipoSubcuenta = tipoSubcuenta;
    }

    public Siefore getSiefore() {
        return siefore;
    }

    public void setSiefore(Siefore siefore) {
        this.siefore = siefore;
    }

    public String getSaldoAlDiaAcciones() {
        return saldoAlDiaAcciones;
    }

    public void setSaldoAlDiaAcciones(String saldoAlDiaAcciones) {
        this.saldoAlDiaAcciones = saldoAlDiaAcciones;
    }

    public String getSaldoAlDiaPesos() {
        return saldoAlDiaPesos;
    }

    public void setSaldoAlDiaPesos(String saldoAlDiaPesos) {
        this.saldoAlDiaPesos = saldoAlDiaPesos;
    }

    public String getSaldoDisponibleAcciones() {
        return saldoDisponibleAcciones;
    }

    public void setSaldoDisponibleAcciones(String saldoDisponibleAcciones) {
        this.saldoDisponibleAcciones = saldoDisponibleAcciones;
    }

    public String getSaldoComprometidoAcciones() {
        return saldoComprometidoAcciones;
    }

    public void setSaldoComprometidoAcciones(String saldoComprometidoAcciones) {
        this.saldoComprometidoAcciones = saldoComprometidoAcciones;
    }

    public String getSaldoComprometidoPesos() {
        return saldoComprometidoPesos;
    }

    public void setSaldoComprometidoPesos(String saldoComprometidoPesos) {
        this.saldoComprometidoPesos = saldoComprometidoPesos;
    }

    public String getSaldoPendienteAcciones() {
        return saldoPendienteAcciones;
    }

    public void setSaldoPendienteAcciones(String saldoPendienteAcciones) {
        this.saldoPendienteAcciones = saldoPendienteAcciones;
    }

    public String getSaldoPendientePesos() {
        return saldoPendientePesos;
    }

    public void setSaldoPendientePesos(String saldoPendientePesos) {
        this.saldoPendientePesos = saldoPendientePesos;
    }

    public String getPlazo() {
        return plazo;
    }

    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    public String getDeducible() {
        return deducible;
    }

    public void setDeducible(String deducible) {
        this.deducible = deducible;
    }

    public String getIdValorAccion() {
        return idValorAccion;
    }

    public void setIdValorAccion(String idValorAccion) {
        this.idValorAccion = idValorAccion;
    }

    public String getValorAccion() {
        return valorAccion;
    }

    public void setValorAccion(String valorAccion) {
        this.valorAccion = valorAccion;
    }

    public String getDescripcionPlazo() {
        return descripcionPlazo;
    }

    public void setDescripcionPlazo(String descripcionPlazo) {
        this.descripcionPlazo = descripcionPlazo;
    }

}
