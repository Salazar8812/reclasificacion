
package app.profuturo.reclasificacion.com.model;
import java.util.List;
import java.util.UUID;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseRequestInterface;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SaveProcedureRequest extends RealmObject implements WSBaseRequestInterface {

    @PrimaryKey
    @Expose(serialize = false, deserialize = false)
    public String uuid = UUID.randomUUID().toString();

    @SerializedName("idEstatus")
    @Expose
    public String idEstatus;

    @SerializedName("idSubproceso")
    @Expose
    public String idSubproceso;

    @SerializedName("idEtapa")
    public String idEtapa;

    @SerializedName("idSubetapa")
    public String idSubetapa;

    @SerializedName("idResultado")
    public String idResultado;

    @SerializedName("usuario")
    public String usuario;

    @SerializedName("cveSolicitud")
    @Expose
    public String cveSolicitud;
    @SerializedName("destinoNotificacion")
    @Expose
    public String destinoNotificacion;
    @SerializedName("fechaCaptura")
    @Expose
    public String fechaCaptura;
    @SerializedName("folioFus")
    @Expose
    public String folioFus;
    @SerializedName("idEstatusSolicitud")
    @Expose
    public String idEstatusSolicitud;

    @SerializedName("idMotivoRechazo")
    public String idMotivoRechazo;

    @SerializedName("idFondoApp")
    @Expose
    public String idFondoApp;
    @SerializedName("idMedioNotificacion")
    @Expose
    public String idMedioNotificacion;
    @SerializedName("idOrigenCaptura")
    @Expose
    public String idOrigenCaptura;
    @SerializedName("idTipoReclasificacion")
    @Expose
    public String idTipoReclasificacion;
    @SerializedName("idTipoSiefore")
    @Expose
    public String idTipoSiefore;
    @SerializedName("listReclasificacion")
    @Expose
    public RealmList<ListReclasificacion> listReclasificacion = new RealmList<>();
    @SerializedName("listaIndicadores")
    @Expose
    public RealmList<ListaIndicadore> listaIndicadores = new RealmList<>();
    @SerializedName("saldosOriginales")
    @Expose
    public RealmList<Saldo> saldosOriginales;
    @SerializedName("montoReclasificacion")
    @Expose
    public String montoReclasificacion;
    @SerializedName("numCtaInvdual")
    @Expose
    public String numCtaInvdual;
    @SerializedName("solicitante")
    @Expose
    public Solicitante solicitante;
    @SerializedName("usuCaptura")
    @Expose
    public String usuCaptura;
    @SerializedName("usuCre")
    @Expose
    public String usuCre;

}
