package app.profuturo.reclasificacion.com.Implementation;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import app.profuturo.reclasificacion.com.Background.BaseWSManager;
import app.profuturo.reclasificacion.com.Background.WSManager;
import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;
import app.profuturo.reclasificacion.com.Callbacks.WSCallback;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.Repository.local.ClientDAO;
import app.profuturo.reclasificacion.com.Utils.MessageUtils;
import app.profuturo.reclasificacion.com.base.Utils;
import app.profuturo.reclasificacion.com.base.View.BaseView;
import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.model.IdcRequest;
import app.profuturo.reclasificacion.com.model.SaleResponse;

public class ClientResultPresenter extends ReclasificationPresenter implements WSCallback {


    public interface ClientCallback{
        void OnSuccessGetClients(Client client);
    }

    public ClientCallback mClientCallback;
    /**
     * Constant value that defines the length of an account number.
     */
    public static final int CURP_LENGTH = 18;
    /**
     * Constant that defines that a client is independent.
     */
    private static final Integer INDEPENDENT_CLIENT = 68;
    /**
     * Constant value that defines the key for the NSS number.
     */
    private static final String NSS = "nss";
    /**
     * Constant value that defines the key for the CURP number.
     */
    private static final String CURP = "curp";
    /**
     * Constant value that defines the key for an account number.
     */
    private static final String ACCOUNT_NUMBER = "numeroCuenta";
    /**
     * Constant value that defines the length of a NSS number.
     */
    private static final int NSS_LENGTH = 11;
    /**
     * Defines the results from the client search.
     */

    /**
     * Validates that a given parameter matches the necessary formats.
     * If so it gets added to the map.
     *
     * @param queryParams Map containing the query params keys and values.
     * @param value       The value to be added to the given query params map.
     */
    private void validateQueryParam(Map<String, String> queryParams, String value) {
        // Create a reference that defines the name of the key for the given value.
        String parameterKey;
        // Validate the given parameter, if it matches any of the necessary formats.
        if (Utils.validateNumericCriteriaByLength(value, NSS_LENGTH)) {
            // Assign the key value for a Social Security Number.
            parameterKey = NSS;
        } else if (Utils.validateCriteriaByLength(value, CURP_LENGTH)) {
            // Assign the key value for a CURP value.
            parameterKey = CURP;
        } else {
            // Assign the key value for an account number.
            parameterKey = ACCOUNT_NUMBER;
        }
        // Check that the parameter key is not null.
        if (parameterKey != null) {
            // If not, add the value with its key to the query params map.
            queryParams.put(parameterKey, value);
        }
    }

    /**
     * Builds the query params for performing a client search.
     *
     * @param criteria The criteria for searching clients (NSS, Curp or account number).
     * @return The Map with the generated query params.
     */
    @NonNull
    private Map<String, String> generateQueryParams(String criteria) {
        // Build a new Map, it will define the query params.
        Map<String, String> queryParams = new HashMap<>();
        // Validate the given criteria and add it to the query params.
        validateQueryParam(queryParams, criteria);
        // Return the built query params.
        return queryParams;
    }

    /**
     * Reports that no matches were found for a search query.
     */
    private void noClientsFound() {
        // Notify that no clients were found.
        // Notify why it failed.
        sendMessage(R.string.no_valid_clients);
        // Notify that the presenter failed doing the main operation.
    }

    /**
     * Method to get the first criteria valid, return null if any is valid
     *
     * @param accountNumber The account number of the client
     * @param nss           the security number of the client
     * @param curp          the curp of the client
     * @return a valid criteria , null if any is valid
     */
    public String getFirstValidCriteria(String accountNumber, String nss, String curp) {
        // Initializes the criteria as null
        String criteria = null;
        // Check the account number has 10 digits exactly
        if (!TextUtils.isEmpty(accountNumber)) {
            // Assigns the criteria as the account number
            criteria = accountNumber;
            // Check the account number has 11 digits exactly
        } else if (Utils.validateNumericCriteriaByLength(nss, NSS_LENGTH)) {
            // Assigns the criteria as the account number
            criteria = nss;
        } else if (Utils.isCURPvalid(curp.toUpperCase(Locale.getDefault()))) {
            criteria = curp;
        }
        return criteria;
    }

    /**
     * Method to save the client into the database
     *
     * @param client The client to be stored
     */
    public void saveClient(Client client) {
        // Run this operation in a worker thread
        //client.setAccountNumber(validateClientAccount(client));
        new Thread(new Runnable() {
            @Override
            public void run() {
                ClientDAO.createClient(client);
            }
        }).start();
    }

    /**
     * Method to delete the client from database
     */
    public void deleteClient() {

        new Thread(new Runnable() {
            @Override
            public void run() {
               ClientDAO.deleteClient();
            }
        }).start();
    }

    /**
     * Retrieves the client stored in the local storage database
     * and reports the value to a MutableLiveData property.
     */
    public void fetchStoredClient() {
        // Execute this code in a worker thread.
        /*runOnWorkerThread(() -> {
            // Report that the presenter is doing work.
            setStatus(BasePresenter.Status.STATUS_BUSY);
            // Read the client using the ClientDAO.
            Client c = ClientDAO.readClient();
            // Post the value of the results in the MutableLiveData property.
            mStoredClient.postValue(c);
            // Report the finish status of the presenter depending on the client result.
            if (c == null) {
                setStatus(BasePresenter.Status.STATUS_FAILED);
            } else {
                setStatus(BasePresenter.Status.STATUS_DONE);
            }
        });*/
    }


    public ClientResultPresenter(AppCompatActivity appCompatActivity, ClientCallback mClientCallback, BaseView view) {
        super(appCompatActivity, view);
        this.mClientCallback = mClientCallback;
    }



    public void searchByCriteria(String criteria){
        IdcRequest mIdcRequest = new IdcRequest(generateQueryParams(criteria));
        mWSManager.requestWs(Client.class, WSManager.WS.GET_IDC, mIdcRequest);
    }


    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if(requestUrl.equals(WSManager.WS.GET_IDC)){
            OnSuccessLoadClient((Client) baseResponse);
        }
    }

    public void OnSuccessLoadClient(Client client){
        MessageUtils.stopProgress();
        if (client != null && client.isAccountNotNull()) {
            // Check if the client is valid and enabled
            if (client.isAccountValid()) {
                // Generate the client contact data
                client.digestContactData();
                // Check if the client is independent, if so, notify to the agent.
                if (INDEPENDENT_CLIENT == client.getRegimentIndicatorId()) {
                    sendMessage(R.string.total_withdrawal_unavailable);
                }
                // Notify the change to the view.
                mClientCallback.OnSuccessGetClients(client);
                // Notify that the presenter has finished doing work.
            } else {
                sendMessage(R.string.no_valid_clients);
            }
        } else {
            sendMessage(R.string.no_matches_found);
        }
    }

}
