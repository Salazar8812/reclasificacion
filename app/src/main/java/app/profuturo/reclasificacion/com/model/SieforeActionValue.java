package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.SerializedName;

public class SieforeActionValue {
    @SerializedName("idSiefore")
    public String idSiefore;

    @SerializedName("descSiefore")
    public String descSiefore ;

}
