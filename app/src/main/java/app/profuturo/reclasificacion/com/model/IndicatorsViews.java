package app.profuturo.reclasificacion.com.model;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class IndicatorsViews {
    public TextView mTitle;
    public EditText mBoxEdith;
    public TextView mSimbol;

    public IndicatorsViews(TextView mTitle, EditText mBoxEdith, TextView mSimbol) {
        this.mTitle = mTitle;
        this.mBoxEdith = mBoxEdith;
        this.mSimbol = mSimbol;
    }
}
