package app.profuturo.reclasificacion.com.base;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.HashMap;

import app.profuturo.reclasificacion.com.Implementation.BasePresenterC;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.base.View.BaseView;
import app.profuturo.reclasificacion.com.base.View.SnackBar;
import app.profuturo.reclasificacion.com.presenter.BasePresenter;

/**
 * Simple {@link Fragment} subclass that implements {@link BaseView}.
 *
 * @param <VDB> Data class that this fragment will render.
 * @author Alfredo Bejarano
 * @version 1.0
 * @since August 3rd, 2018 - 05:27 PM
 */
public abstract class BaseFragment<VDB extends ViewDataBinding> extends Fragment implements BaseView {
    /**
     * Presenters for this activity.
     */
    private HashMap<String, BasePresenter> mPresenters = new HashMap<>();
    /**
     * ViewDataBinding reference for this view.
     */
    private VDB mDataBinding;

    /**
     * Loading layout for this view.
     */
    private FrameLayout mLoadingView;
    /**
     * Reference to the content view of this fragment.
     */
    private View mContent;

    /**
     * Observer for all the attached presenters, this observer will display or hide the loading view
     * if any of the attached presenters is busy or has done doing work, respectively.
     */
    private Observer<BasePresenter.Status> mPresenterStatusObserver = status -> {
        // Show the loading view if the status has changed to STATUS_BUSY.
        displayLoadingView(status == BasePresenter.Status.STATUS_BUSY);
    };

    /**
     * Must return the Id of the layout file for this fragment.
     *
     * @return ResId of this fragment layout.
     */
    protected abstract Integer getLayoutId();

    /**
     * Returns the data binding object for this view.
     *
     * @return This data binding object as the necessary class for this view.
     */
    protected VDB getDataBinding() {
        return mDataBinding;
    }

    /**
     * Initializes the message Snackbar class and its DataBinding references.
     */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = getPresenterC();
        mArrayPresenters = getmArrayPresenters();

        if (mArrayPresenters != null) {
            for (BasePresenterC basePresenter : mArrayPresenters) {
                if (basePresenter != null) {
                    basePresenter.onCreate();
                }
            }
        }
        if (mPresenter != null) mPresenter.onCreate();
    }


    private void startView() {
        // Initialize the loading view.
        mLoadingView = mContent.findViewById(R.id.loading);
        // Setup this view.

        setup();
    }


    /**
     * Charls Salazar
     */

    protected BasePresenterC mPresenter;
    private BasePresenterC[] mArrayPresenters;

    /**
     * Destroys this view references from this view
     * presenters and clears the presenters Map.
     */
    private void detachPresenters() {
        // Check if the presenters HashMap is not null or empty.
        if (Utils.isNotNullOrEmpty(mPresenters)) {
            // Get every single key from the mPresenters HashMap.
            for (String key : mPresenters.keySet()) {
                // Remove all the observer for the presenter status.
                mPresenters.get(key).getStatus().removeObservers(this);
                // Proceed to destroy the view reference for said presenter.
                mPresenters.get(key).destroyView();
            }
            // Now clear the presenters HashMap.
            mPresenters.clear();
        }
    }

    /**
     * Renders this fragment.
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate this fragment root.
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.layout_base, container, false);
        // Now, inflate the layout content that will make the ViewDataBinding object.
        mDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(),
                root.findViewById(R.id.content), true);
        // Set the binding reference LifeCycleOwner.
        if (getDataBinding() != null) {
            mDataBinding.setLifecycleOwner(this);
        }
        // Assign the mContent value as the root of the fragment.
        mContent = root;
        // Return the inflated view.


        return root;
    }

    public BasePresenterC[] getmArrayPresenters() {
        return null;
    }

    public BasePresenterC getPresenterC() {
        return null;
    }
    /**
     * After the fragment renders, initialize this view elements.
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        startView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayMessage(@NonNull Object payload) {
        displayLoadingView(false);
        if (payload instanceof Integer) {
            SnackBar.show(mContent, (Integer) payload);
        } else if (payload instanceof String) {
            SnackBar.show(mContent, (String) payload);
        } else if (payload instanceof Throwable) {
            SnackBar.show(mContent, ((Throwable) payload).getLocalizedMessage());
        } else {
            SnackBar.show(mContent, String.valueOf(payload));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayLoadingView(@NonNull Boolean displayView) {
        if (mLoadingView != null) {
            mLoadingView.setVisibility(displayView ? View.VISIBLE : View.GONE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Context asContext() {
        if (mContent != null)
            return mContent.getContext();
        else return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mArrayPresenters != null) {
            for (BasePresenterC basePresenter : mArrayPresenters) {
                if (basePresenter != null) {
                    basePresenter.onDestroy();
                }
            }
        }
        if (mPresenter != null) mPresenter.onDestroy();
        mPresenter = null;

        detachPresenters();
    }

    /**
     * Adds an array of BasePresenter objects to the presenters for this view.
     *
     * @param presenters The presenters to be attached.
     */
    protected void attachPresenters(BasePresenter... presenters) {
        // Check if the presenters varargs is not null or empty.
        if (presenters != null && presenters.length > 0) {
            // If it isn't, observe all the attaching presenter status.
            for (BasePresenter p : presenters) {
                // Observe the current presenter status.
                p.getStatus().observe(this, mPresenterStatusObserver);
                // Add the presenter to the presenters list.
                mPresenters.put(p.getClass().getSimpleName(), p);
            }
        }
    }

    /**
     * Returns a presenter that has  been attached.
     *
     * @param clazz The presenter class to be retrieved.
     * @param <P>   Generic defining the type of presenter to be retrieved.
     * @return The expected presenter object.
     */
    protected <P extends BasePresenter> P getPresenter(@NonNull Class<P> clazz) {
        // Retrieve the simple name of the expected presenter class.
        String className = clazz.getSimpleName();
        // Check if the desired presenter has been attached.
        if (mPresenters.containsKey(className)) {
            // Return the casted presenter class, if found.
            return clazz.cast(mPresenters.get(className));
        } else {
            // If it hasn't been attached, return null.
            return null;
        }
    }

    /**
     * Observes changes from a {@link MutableLiveData} object.
     *
     * @param liveData The object to be observed.
     * @param observer How the changed values are going to behave.
     * @param <T>      Generic defining the type of data the liveData object has.
     */
    protected <T> void observe(MutableLiveData<T> liveData, Observer<T> observer) {
        liveData.observe(this, observer);
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (mArrayPresenters != null) {
            for (BasePresenterC basePresenter : mArrayPresenters) {
                if (basePresenter != null) {
                    basePresenter.onResume();
                }
            }
        }
        if (mPresenter != null) mPresenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mArrayPresenters != null) {
            for (BasePresenterC basePresenter : mArrayPresenters) {
                if (basePresenter != null) {
                    basePresenter.onPause();
                }
            }
        }
        if (mPresenter != null) mPresenter.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mArrayPresenters != null) {
            for (BasePresenterC basePresenter : mArrayPresenters) {
                if (basePresenter != null) {
                    basePresenter.onStop();
                }
            }
        }
        if (mPresenter != null) mPresenter.onStop();
    }


}