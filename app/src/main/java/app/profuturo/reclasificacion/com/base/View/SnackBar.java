package app.profuturo.reclasificacion.com.base.View;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;

import app.profuturo.reclasificacion.com.R;

/**
 * Custom version of the google's design SnackBar class
 * that applies custom padding and colors from the app resources.
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 29/08/2018 - 11:56 AM
 */
public class SnackBar {
    /**
     * Creates a SnackBar, applies a custom set of parameters
     * to the SnackBar and then proceeds to display the SnackBar.
     *
     * @param view      The view that is displaying the SnackBar.
     * @param stringRes Resource Id for the message.
     */
    public static void show(@NonNull View view, @NonNull @StringRes Integer stringRes) {
        // Retrieve the string value for the message string resource.
        String message = view.getContext().getString(stringRes);
        // Show the SnackBar with the given message.
        show(view, message);
    }

    /**
     * Creates a SnackBar, applies a custom set of parameters
     * to the SnackBar and then proceeds to display the SnackBar.
     *
     * @param view    The view that is displaying the SnackBar.
     * @param message The message that is going to be displayed.
     */
    public static void show(@NonNull View view, @NonNull String message) {
        // Create a SnackBar object.
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        // Get the view of said SnackBar.
        View snackBarView = snackbar.getView();
        // Change the background color of the SnackBar.
        snackBarView.setBackgroundColor(ContextCompat.getColor(view.getContext(),
                R.color.colorSnackBarError));
        // Retrieve the SnackBar padding dimension value.
        Integer snackBarPadding = (int) view.getResources()
                .getDimension(R.dimen.error_snack_bar_dimen);
        // Apply the retrieved SnackBar padding.
        snackBarView.setPadding(snackBarPadding, snackBarPadding, snackBarPadding, snackBarPadding);
        // Show the SnackBar.
        snackbar.show();
    }
}
