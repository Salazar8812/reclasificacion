
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Obligatorio {

    @SerializedName("procesar")
    @Expose
    public String procesar;
    @SerializedName("profuturo")
    @Expose
    public String profuturo;

}
