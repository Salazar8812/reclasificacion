package app.profuturo.reclasificacion.com.Adapter;


import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.base.Utils;
import app.profuturo.reclasificacion.com.model.BranchResponse;

/**
 * Adapter class that will fill a Spinner with a list of
 * {@link app.profuturo.reclasificacion.com.model.BranchResponse.Branch branch} objects.
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 15/08/2018 - 11:33 AM
 */
public class BranchSpinnerAdapter extends ArrayAdapter<BranchResponse.Branch> implements
        AdapterView.OnItemSelectedListener {
    /**
     * Property that holds a reference to an array of branches.
     */
    private List<BranchResponse.Branch> mBranches;
    /**
     * Property defining the layout for this adapter items.
     */
    private Integer mItemLayout;
    /**
     * Reference to this Adapter inflater.
     */
    private LayoutInflater mInflater;
    /**
     * Holds a reference to the selected branch by the user.
     */
    private BranchResponse.Branch mSelectedBranch;

    /**
     * Constructor for this adapter.
     *
     * @param context  The context creating this adapter.
     * @param resource The layout ID for this adapter elements.
     * @param objects  Array of objects that is going to fill this adapter.
     */
    public BranchSpinnerAdapter(@NonNull Context context, @NonNull @LayoutRes Integer resource,
                                @NonNull List<BranchResponse.Branch> objects) {
        super(context, resource, objects);
        mBranches = objects;
        mItemLayout = resource;
        if (Utils.isNotNullOrEmpty(mBranches)) {
            mSelectedBranch = mBranches.get(0);
        }
        mInflater = LayoutInflater.from(context);
    }

    /**
     * @return The selected branch by the user.
     */
    public BranchResponse.Branch getSelectedBranch() {
        return mSelectedBranch;
    }

    /**
     * {@inheritDoc}
     */
    @NonNull
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createViewHolder(position, parent);
    }

    /**
     * {@inheritDoc}
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createViewHolder(position, parent);
    }

    /**
     * Creates a {@link View} representing the root for a given spinner item.
     *
     * @param position The position of the current spinner item.
     * @param parent   The parent of the spinner.
     * @return The created view for the spinner item.
     */
    @NonNull
    private View createViewHolder(int position, @NonNull ViewGroup parent) {
        View root = mInflater.inflate(mItemLayout, parent, false);
        BranchResponse.Branch currentBranch = mBranches.get(position);
        TextView branchNameView = root.findViewById(R.id.branch_name);
        branchNameView.setText(currentBranch.getBranchName());
        return root;
    }

    /**
     * Changes the value of the selected branch.
     * {@inheritDoc}
     */
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        mSelectedBranch = mBranches.get(position);
    }

    /**
     * Do nothing.
     */
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        // Empty method stub!
    }
}

