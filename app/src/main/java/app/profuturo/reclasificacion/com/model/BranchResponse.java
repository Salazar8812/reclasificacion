package app.profuturo.reclasificacion.com.model;

import android.location.Location;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BranchResponse {

    @Expose
    @SerializedName("sucursales")
    private List<Branch> branchList;
    @Expose
    @SerializedName("encontrada")
    private Boolean found;
    @Expose
    @SerializedName("mensaje")
    private String menssage;

    public BranchResponse() {
    }

    public BranchResponse(List<Branch> branchList, Boolean found, String menssage) {
        this.branchList = branchList;
        this.found = found;
        this.menssage = menssage;
    }

    public List<Branch> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<Branch> branchList) {
        this.branchList = branchList;
    }

    public Boolean isFound() {
        return found;
    }

    public void setFound(Boolean found) {
        this.found = found;
    }

    public String getMenssage() {
        return menssage;
    }

    public void setMenssage(String menssage) {
        this.menssage = menssage;
    }

    public static class Branch {

        @Expose
        @SerializedName("calle")
        private String Street;
        @Expose
        @SerializedName("ciudad")
        private String city;
        @Expose
        @SerializedName("delegacionMunicipio")
        private String delegation;
        @Expose
        @SerializedName("distancia")
        private Double distance;
        @Expose
        @SerializedName("entidadFederativa")
        private String entity;
        @Expose
        @SerializedName("estado")
        private String state;
        @Expose
        @SerializedName("idSucursal")
        private String branchOffice;
        @Expose
        @SerializedName("latitud")
        private Double latitude;
        @Expose
        @SerializedName("longitud")
        private Double longitude;
        @Expose
        @SerializedName("nombreSucursal")
        private String branchName;

        public Branch() {
        }

        public Branch(String street, String city, String delegation, Double distance, String entity, String state, String branchOffice, Double latitude, Double longitude, String branchName) {
            Street = street;
            this.city = city;
            this.delegation = delegation;
            this.distance = distance;
            this.entity = entity;
            this.state = state;
            this.branchOffice = branchOffice;
            this.latitude = latitude;
            this.longitude = longitude;
            this.branchName = branchName;
        }

        public String getStreet() {
            return Street;
        }

        public void setStreet(String street) {
            Street = street;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getDelegation() {
            return delegation;
        }

        public void setDelegation(String delegation) {
            this.delegation = delegation;
        }

        public Double getDistance() {
            return distance;
        }

        public void setDistance(Double distance) {
            this.distance = distance;
        }

        public String getEntity() {
            return entity;
        }

        public void setEntity(String entity) {
            this.entity = entity;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getBranchOffice() {
            return branchOffice;
        }

        public void setBranchOffice(String branchOffice) {
            this.branchOffice = branchOffice;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getBranchName() {
            return branchName;
        }

        public void setBranchName(String branchName) {
            this.branchName = branchName;
        }

        /**
         * Parses the necessary data for this branch to become a Location object.
         *
         * @return This branch instance as a Location object.
         * @see Location
         */
        @NonNull
        public Location toLocation() {
            // Create a new Location l object being the branch name the location provider.
            Location l = new Location(branchName);
            // Set the branch location coordinates as the location coordinates.
            l.setLatitude(latitude);
            l.setLongitude(longitude);
            // Return the built location.
            return l;
        }
    }
}
