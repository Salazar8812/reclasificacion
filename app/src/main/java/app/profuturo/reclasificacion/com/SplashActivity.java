package app.profuturo.reclasificacion.com;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

import app.profuturo.reclasificacion.com.Adapter.BranchSpinnerAdapter;
import app.profuturo.reclasificacion.com.Utils.PrefsReclasification;
import app.profuturo.reclasificacion.com.base.BaseActivity;
import app.profuturo.reclasificacion.com.base.Utils;
import app.profuturo.reclasificacion.com.databinding.ActivitySplashBinding;
import app.profuturo.reclasificacion.com.model.AgentSession;
import app.profuturo.reclasificacion.com.model.BranchResponse;
import app.profuturo.reclasificacion.com.presenter.AgentSessionPresenter;
import app.profuturo.reclasificacion.com.presenter.BasePresenter;
import app.profuturo.reclasificacion.com.presenter.BranchesPresenter;
import app.profuturo.reclasificacion.com.presenter.LoginPresenter;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class SplashActivity extends BaseActivity<ActivitySplashBinding>{
    // Code for requesting location permission.
    private static final int LOCATION_CODE = 777;
    // Reference to the branch spinner.
    Spinner mBranchesSpinner;
    private PrefsReclasification prefsReclasification;

    /**
     * {@inheritDoc}
     */

    @Override
    public Integer getLayoutId() {
        return R.layout.activity_splash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setup() {
        requestPermission();
        prefsReclasification= new PrefsReclasification(this);
        displayLoadingView(true);
        new Thread(() -> {
            // Initialization of SDKs must execute first.
            initializeSDKs();
            // Create the necessary presenters for this view.
            LoginPresenter lp = new LoginPresenter(this);
            BranchesPresenter bp = new BranchesPresenter(this);
            AgentSessionPresenter ap = new AgentSessionPresenter(this);
            // Proceed to attach them.
            attachPresenters(lp, bp, ap);
            // Set the binding's presenter variable.
            getDataBinding().setPresenter(bp);
            // If it became null, initialize it again.
            mBranchesSpinner = getDataBinding().branchSpinner;
            // Creates a listener to evaluates distance between the user and the selected branch
            mBranchesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                /**
                 * {@inheritDoc}
                 */
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    // Check if the location is valid, it being a Location object and being inside a branch.
                    Boolean isLocationValid = getPresenter(BranchesPresenter.class)
                            .isAgentWithinBranchDistance(adapterView.getSelectedItem());
                    if (!isLocationValid) {
                        // Build alert dialog.
                        displayMessage(R.string.dialog_wrong_branch);
                    }
                }

                /**
                 * {@inheritDoc}
                 */
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    // Empty inherited method, not necessary
                }
            });
            // Start observing the branches value from the presenter.
            subscribeToBranchesService(bp);
            // Observe the status of the AgentSessionPresenter.
            observe(ap.getStatus(), status -> {
                if (status == BasePresenter.Status.STATUS_DONE) {
                    AgentSession agentSession = ap.getAgentSession();
                    if (agentSession != null) {
                        getDataBinding().name.setText(agentSession.getName() +" "+agentSession.getSurName());
                        lp.createLogin(agentSession);
                    } else {
                        killApp();
                    }
                } else if (status != BasePresenter.Status.STATUS_BUSY) {
                    killApp();
                }
            });
            // Observe the status of the LoginPresenter.
            observe(lp.getStatus(), status -> {
                if (status == BasePresenter.Status.STATUS_DONE) {
                    // Check if the ACCESS_FINE_LOCATION permission has been given. - TODO - access after agent retrieval.
                    checkForLocationPermission();
                } else if (status != BasePresenter.Status.STATUS_BUSY) {
                    killApp();
                }
            });
            // Retrieve the AgentNumber from the intent.
            String agentNumber = ap.agentNumberFromIntent(getIntent());
            // Get the agent information using the retrieved agent number.
            ap.getAgentInformation(agentNumber, prefsReclasification);
        }).start();
    }

    /**
     * Method to listen to changes in the list of branches
     */
    private void subscribeToBranchesService(@NonNull BranchesPresenter bp) {
        // Observe changes in the user location.
        observe(bp.getUserLocation(), location -> {
            // Get the AgentSession from its presenter.
            AgentSession agentSession = getPresenter(AgentSessionPresenter.class).getAgentSession();
            // If the session is not null, proceed to the branch selection.
            if (agentSession != null) {
                // Request the branches.
                bp.callBranchesService(agentSession);
            } else {
                // If no session was found, close the app.
                killApp();
            }
        });

        // In case list of branches changes, update the branches spinner.
        observe(bp.getBranches(), branches -> {
            // Check if the new branches list is not null.
            if (Utils.isNotNullOrEmpty(branches)) {
                // Remove the spinner adapter.
                mBranchesSpinner.setAdapter(null);
                // Assign a new spinner.
                BranchSpinnerAdapter adapter = new BranchSpinnerAdapter(asContext(),
                        R.layout.item_branch, branches);
                // Assign the adapter to the branches spinner.
                mBranchesSpinner.setAdapter(adapter);
            } else {
                // Display a message notifying to the agent that his branch doesn't exists.
                displayMessage(R.string.assigned_branch_not_found);
            }
        });

        // Observe changes in the assigned branch.
        observe(bp.getAssignedBranch(), branchId -> {
            // Get the spinner adapter of the BranchSpinner.
            BranchSpinnerAdapter adapter = (BranchSpinnerAdapter) getDataBinding()
                    .branchSpinner.getAdapter();
            // Iterate through all the adapter items.
            for (int i = 0; i <= adapter.getCount(); i++) {
                // Retrieve the current item.
                BranchResponse.Branch item = adapter.getItem(i);
                // If the item matches the selected branch id.
                if (item.getBranchOffice().equals(branchId)) {
                    // Set the selection to said item.
                    getDataBinding().branchSpinner.setSelection(i, true);
                    // Break the loop.
                    break;
                }
            }
        });
    }

    /**
     * Closes the app completely and kills the app process.
     */
    private void killApp() {
        //finishAffinity();
        //System.exit(0);
    }

    /**
     * Checks if fine location permission is granted otherwise request it
     */
    private void checkForLocationPermission() {
        BranchesPresenter p = getPresenter(BranchesPresenter.class);
        // Check if the device is running on Android 6.0 or later.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // If it is, check for the necessary permission.
            if (Utils.isPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Get last known location if the permission has been granted.
                p.getLastKnownLocation();
            } else {
                // If not, request the necessary location.
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_CODE);
            }
        } else {
            // If the device is running on a version lower than 6.0, get the last known location.
            p.getLastKnownLocation();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Get last known location
                getPresenter(BranchesPresenter.class).getLastKnownLocation();
            } else {
                // Prompt the user to grant permission
                displayMessage(R.string.we_need_location_access);
                // Get last known location
                checkForLocationPermission();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Dismisses a given dialog interface and then proceeds to kill the app process if desired.
     *
     * @param dialogInterface The dialog to be dismissed.
     * @param exitApp         Boolean value indicating if the app will be closed.
     */
    private void dismissDialog(DialogInterface dialogInterface, Boolean exitApp) {
        // Dismiss the dialog.
        dialogInterface.dismiss();
        // Check if its desired to close the app.
        if (exitApp) {
            // Finish the app.
            finish();
            // Shutdown the app process.
            //System.exit(0);
        }
    }

    /**
     * Displays a dialog prompting the user to confirm if it wants to exit from the app.
     *
     * @param view The widget that has been clicked.
     */
    public void closeApp(View view) {
        // Check that the activity is not finishing.
        if (!isFinishing()) {
            // Create a new AlertDialog Builder.
            new AlertDialog.Builder(view.getContext())
                    // Set the dialog title.
                    .setTitle(R.string.close_app_title)
                    // Set the dialog message.
                    .setMessage(R.string.close_app_message)
                    // Set the positive dialog button (the one that closes the app).
                    .setPositiveButton(R.string.exit, (dialogInterface, i) ->
                            dismissDialog(dialogInterface, true))
                    // Set the negative button (the one that just closes the dialog).
                    .setNegativeButton(R.string.cancel, (dialogInterface, i) ->
                            dismissDialog(dialogInterface, false))
                    // Display the dialog.
                    .show();
        }
    }

    /**
     * Opens the ClientSearchFragment class in the ProcedureActivity.
     *
     * @param view The button calling this method.
     */
    public void navigateToProcedureSelection(View view) {
        Utils.navigateTo(view.getContext(), ProcedureActivity.class, null);
    }

    /**
     * Initializes the necessary SDKs for the app.
     */
    private void initializeSDKs() {
        // This line will initialize Fabric's Crashlytics.
        //Fabric.with(this, new Crashlytics());
        // Then, proceed to initialize the Realm database.
        initializeRealmDatabase();
    }

    /**
     * Proceeds to initialize and configure the default Realm database instance.
     */
    private void initializeRealmDatabase() {
        // Initialize Realm.
        /*Realm.init(this);
        // Create a new configuration object for Realm.
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        // Set the default configuration.
        Realm.setDefaultConfiguration(config);*/
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayMessage(@NonNull Object payload) {
        if (payload instanceof Integer && !isFinishing()) {
            Integer message = (Integer) payload;
            if (R.string.dialog_wrong_branch == message) {
                new AlertDialog.Builder(this)
                        .setMessage(R.string.dialog_wrong_branch)
                        .setPositiveButton(R.string.accept, (dialog, which) -> dialog.dismiss())
                        .show();
            } else {
                super.displayMessage(payload);
            }
        } else {
            super.displayMessage(payload);
        }
    }

    private void requestPermission() {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {

            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                finish();
            }
        };

        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("Es necesario habilitar todos los permisos")
                .setPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                        //Manifest.permission.USE_FINGERPRINT
                )
                .check();
    }


}