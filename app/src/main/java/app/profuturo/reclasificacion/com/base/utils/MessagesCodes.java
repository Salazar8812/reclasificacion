package app.profuturo.reclasificacion.com.base.utils;


/**
 * Class holding all codes to get informative messages
 *
 * @author Luis Meléndez
 * @version 1.0
 * @since 20/08/2018 - 11:53 AM
 */
public class MessagesCodes {

    public static final String MESSAGE_AGENT_INFO_INCOMPLETE = "CODE001";
    public static final String MESSAGE_DIFFERENT_AGENT_SIGNED = "CODE002";
    public static final String MESSAGE_NO_SHARE_PRICE_CODE = "CODE003";
    public static final String ACCOUNT_WRONGLY_MARKED = "CODE004";

    // MessageCodes to get messages from web service
    public static final String SELECT_OPTION_CLIENT_RIGHTS = "MLE0001";
    public static final String ISR_LABEL = "MLE0002";
    public static final String CAPTURE_WORKED_WEEKS = "MLE0003";
    public static final String CONFIRM_ADD_DATA = "MLE0004";
    public static final String RETENTION_LABEL = "MLE0005";
    public static final String FILL_RESOLUTION_DATA = "MLE0006";
    public static final String RIGHTS_MATRIX_DETAIL = "MLE0007";
    public static final String AGENT_ALERT_MESSAGE = "MSYS001";
    public static final String CANCEL_PROCEDURE = "MSYS002";
    public static final String ONLY_CAPTURE_ONE_BENEFICIARY = "MSYS003";
    public static final String CLIENT_BANK_ACCOUNT_NO_VERIFIED = "MSYS004";
    public static final String AFFILIATION_STATE = "MSYS005";
    public static final String PROFUTURO_BIOMETRIA_PERMISSION = "MSYS006";
    public static final String RETRY_FINGER_SCAN = "MSYS007";
    public static final String AGENT_NOT_FOUND_IN_PROCESAR = "MSYS008";
    public static final String PROCEDURE_SERVICE_FAIL = "MSYS009";
    public static final String TIMEOUT_EXCEEDED = "MSYS010";
    public static final String CLIENT_AGE = "MSYS011";
    public static final String PROCEDURE_WITH_SEAL = "MSYS012";
    public static final String PROCESAR_CONECTION_ERROR = "MSYS013";
    public static final String CAPTURE_ALL_IMAGES = "MSYS014";
    public static final String PENDING_MONEY_TRANSFERENCE = "MSYS015";

    /* Codes with name pending of assign
    public static final String PENDING_OF_ASSIGN = "MSYS016";
    public static final String PENDING_OF_ASSIGN = "MSYS017";
    public static final String PENDING_OF_ASSIGN = "MSYS018";
    public static final String PENDING_OF_ASSIGN = "MSYS019";
    public static final String PENDING_OF_ASSIGN = "MSYS020";
    public static final String PENDING_OF_ASSIGN = "MSYS021";
    public static final String PENDING_OF_ASSIGN = "MSYS022";
    public static final String PENDING_OF_ASSIGN = "MSYS023";
    public static final String PENDING_OF_ASSIGN = "MSYS024";
    public static final String PENDING_OF_ASSIGN = "MSYS025";
    public static final String PENDING_OF_ASSIGN = "MSYS026";
    public static final String PENDING_OF_ASSIGN = "MSYS027";
    public static final String PENDING_OF_ASSIGN = "MSYS028";
    public static final String PENDING_OF_ASSIGN = "MSYS029";
    public static final String PENDING_OF_ASSIGN = "MSYS030";
    public static final String PENDING_OF_ASSIGN = "MSYS031";
    public static final String PENDING_OF_ASSIGN = "MSYS032";
    public static final String PENDING_OF_ASSIGN = "MSYS033";
    public static final String PENDING_OF_ASSIGN = "MSYS034";
    public static final String PENDING_OF_ASSIGN = "MSYS035";
    public static final String PENDING_OF_ASSIGN = "MSYS036";
    public static final String PENDING_OF_ASSIGN = "MSYS037";
    public static final String PENDING_OF_ASSIGN = "MSYS038";
    public static final String PENDING_OF_ASSIGN = "MSYS039";
    public static final String PENDING_OF_ASSIGN = "MSYS040";
    public static final String PENDING_OF_ASSIGN = "MSYS041";
    public static final String PENDING_OF_ASSIGN = "MSYS042";
    public static final String PENDING_OF_ASSIGN = "MSYS043";
    public static final String PENDING_OF_ASSIGN = "MSYS044";
    public static final String PENDING_OF_ASSIGN = "MSYS045";
    public static final String PENDING_OF_ASSIGN = "MSYS046";
    public static final String PENDING_OF_ASSIGN = "MSYS047";
    public static final String PENDING_OF_ASSIGN = "MSYS048";
    public static final String PENDING_OF_ASSIGN = "MSYS049";
    public static final String PENDING_OF_ASSIGN = "MSYS050";
    public static final String PENDING_OF_ASSIGN = "MSYS051";
    public static final String PENDING_OF_ASSIGN = "MSYS052";
    public static final String PENDING_OF_ASSIGN = "MSYS053";
    public static final String PENDING_OF_ASSIGN = "MSYS054";
    public static final String PENDING_OF_ASSIGN = "MSYS055";
    public static final String PENDING_OF_ASSIGN = "MSYS056";
    public static final String PENDING_OF_ASSIGN = "MSYS057";
    public static final String PENDING_OF_ASSIGN = "MSYS058";
    public static final String PENDING_OF_ASSIGN = "MSYS059";
    public static final String PENDING_OF_ASSIGN = "MSYS060";
    public static final String PENDING_OF_ASSIGN = "MSYS061";
    public static final String PENDING_OF_ASSIGN = "MSYS062";
    public static final String PENDING_OF_ASSIGN = "MSYS063";
    public static final String PENDING_OF_ASSIGN = "MSYS064";
    public static final String PENDING_OF_ASSIGN = "MSYS065";
    public static final String PENDING_OF_ASSIGN = "MSYS066";
    public static final String PENDING_OF_ASSIGN = "MSYS067";
    public static final String PENDING_OF_ASSIGN = "MSYS068";
    public static final String PENDING_OF_ASSIGN = "MSYS069";
    public static final String PENDING_OF_ASSIGN = "MSYS070";
    public static final String PENDING_OF_ASSIGN = "MSYS071";
    public static final String PENDING_OF_ASSIGN = "MSYS072";
    public static final String PENDING_OF_ASSIGN = "MSYS073";
    public static final String PENDING_OF_ASSIGN = "MSYS074";
    public static final String PENDING_OF_ASSIGN = "MIM001";
    public static final String PENDING_OF_ASSIGN = "MIM002";
    public static final String PENDING_OF_ASSIGN = "MIM003";
    public static final String PENDING_OF_ASSIGN = "MIM004";
    public static final String PENDING_OF_ASSIGN = "MIM005";
    public static final String PENDING_OF_ASSIGN = "MIM006";
    public static final String PENDING_OF_ASSIGN = "MIS001";
    public static final String PENDING_OF_ASSIGN = "MIS002";
    public static final String PENDING_OF_ASSIGN = "MIS003";
    public static final String PENDING_OF_ASSIGN = "MIS004";
    public static final String PENDING_OF_ASSIGN = "MIS005";
    * */

}
