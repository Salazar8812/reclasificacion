package app.profuturo.reclasificacion.com.base.View;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


import java.util.HashMap;

import app.profuturo.reclasificacion.com.widget.NavigationViewPager;

/**
 * Interface that defines methods for an activity to be
 * <b>configurable</b> by external sources (such as Fragments),
 * things like <b>replacing fragments</b> or <b>changing the activity title</b>.
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 29/08/2018 - 03:12 PM
 */
public interface Configurable {
    /**
     * HashMap that contains necessary extras for the Procedure.
     */
    HashMap<String, Object> mProcedureExtras = new HashMap<>();

    /**
     * Defines a new title for the activity toolbar.
     *
     * @param title The new title for the activity.
     */
    void setTitle(@NonNull String title);

    /**
     * Changes the current fragment being displayed with a new one.
     *
     * @param fragmentPosition Position for the fragment.
     * @see NavigationAdapter
     */
    void setFragment(@NonNull Integer fragmentPosition);

    /**
     * Defines the ProcedureViewPager view for the configurable implementation.
     *
     * @return The defined view for navigation.
     */
    @NonNull
    NavigationViewPager getViewPager();

    /**
     * Retrieve the value of a procedure extra.
     *
     * @param <T> Type of the extra.
     * @return The value of the extra.
     */
    @Nullable
    <T> T getProcedureExtra(@NonNull String key, @NonNull Class<T> result);

    /**
     * Method that has to add an element to the procedure extras HashMap.
     *
     * @param key   The key name for the extra.
     * @param value The value for the extra.
     * @param <T>   Generic type of the extra.
     */
    <T> void setProcedureExtra(@NonNull String key, @NonNull T value);
}

