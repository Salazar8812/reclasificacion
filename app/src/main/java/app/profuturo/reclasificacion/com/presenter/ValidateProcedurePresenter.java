package app.profuturo.reclasificacion.com.presenter;

import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.profuturo.reclasificacion.com.Background.BaseWSManager;
import app.profuturo.reclasificacion.com.Background.WSManager;
import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;
import app.profuturo.reclasificacion.com.Callbacks.WSCallback;
import app.profuturo.reclasificacion.com.Implementation.ReclasificationPresenter;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.Repository.local.ClientDAO;
import app.profuturo.reclasificacion.com.Utils.MessageUtils;
import app.profuturo.reclasificacion.com.Utils.PrefsReclasification;
import app.profuturo.reclasificacion.com.base.View.BaseView;
import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.model.ConfiguracionIndicador;
import app.profuturo.reclasificacion.com.model.IndicatorRequest;
import app.profuturo.reclasificacion.com.model.IndicatorsResponse;
import app.profuturo.reclasificacion.com.model.ListaIndicadore;
import app.profuturo.reclasificacion.com.model.SaleResponse;
import app.profuturo.reclasificacion.com.model.SaveProcedureRequest;
import app.profuturo.reclasificacion.com.model.Solicitante;
import app.profuturo.reclasificacion.com.model.ValidateProcedureResponse;
import app.profuturo.reclasificacion.com.model.ValidateProcudereRequest;

public class ValidateProcedurePresenter extends ReclasificationPresenter implements WSCallback {

    public interface ValidateProcedureCallback{
        void OnSuccessValidate();
        void OnSuccessConvivence();
        void OnSuccessIndicators(HashMap<String, String> mIndicators);
    }

    private String [] mBasicIndicator= {"707","706","61"};
    private HashMap<String,String> mStatusIndicator = new HashMap<>();
    private ValidateProcedureCallback mValidateProcedureCallback;
    private PrefsReclasification mPrefsReclasification;

    public ValidateProcedurePresenter(AppCompatActivity appCompatActivity, ValidateProcedureCallback validateProcedureCallback, BaseView view) {
        super(appCompatActivity,view);
        mValidateProcedureCallback = validateProcedureCallback;
        mPrefsReclasification = new PrefsReclasification(appCompatActivity);
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void validateProecedure(String idProceso, String subProceso, String numeroCuenta, String mExpBiometric, String mExpIdentification){
        ValidateProcudereRequest mValidateProcudereRequest = new ValidateProcudereRequest();
        mValidateProcudereRequest.idProceso = idProceso;
        mValidateProcudereRequest.idSubProceso = subProceso;
        mValidateProcudereRequest.numeroCuenta = numeroCuenta;
        mValidateProcudereRequest.expBiometrico = mExpBiometric;
        mValidateProcudereRequest.expIdentificacion = mExpIdentification;
        mWSManager.requestWs(ValidateProcedureResponse.class, WSManager.WS.VALIDATE_PROCEDURE, mValidateProcudereRequest);
    }

    public void getIndicator(String mNumberAccount){
        IndicatorRequest indicatorRequest = new IndicatorRequest(mNumberAccount);
        mWSManager.requestWs(IndicatorsResponse.class, WSManager.WS.INDICATORS, indicatorRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if(requestUrl.equals(WSManager.WS.VALIDATE_PROCEDURE)){
            OnSuccessValidate((ValidateProcedureResponse) baseResponse);
        }else if(requestUrl.equals(WSManager.WS.INDICATORS)){
            OnSuccessGetIndicators((IndicatorsResponse) baseResponse);
        }
    }

    public void OnSuccessGetIndicators(IndicatorsResponse mIndicatorsResponse){
        saveIndicators(mIndicatorsResponse);
        saveIndicatorsToRealm(mIndicatorsResponse);
        if(mIndicatorsResponse != null){
            if(mIndicatorsResponse.status == 200){
                for(int i = 0; i<mBasicIndicator.length; i++){
                    putStatusIndicador(mBasicIndicator[i],mIndicatorsResponse);
                }
            }
        }
        mValidateProcedureCallback.OnSuccessIndicators(mStatusIndicator);
    }

    public void saveIndicators(IndicatorsResponse trip){
        Gson gson = new Gson();
        String json = gson.toJson(trip);
        mPrefsReclasification.saveData("indicadores",json);
    }

    public void putStatusIndicador(String mIndicator, IndicatorsResponse mIndicatorResponse){
        for(ConfiguracionIndicador item: mIndicatorResponse.configuracionIndicador){
            if(item.configuracion.indicador.idIndicador.equals(mIndicator)){
                mStatusIndicator.put(String.valueOf(item.configuracion.indicador.idIndicador),item.valorIndicador);
            }
        }
    }

    public void OnSuccessValidate(ValidateProcedureResponse validateProcedureResponse){
        if(validateProcedureResponse != null){
            if(validateProcedureResponse.status != null){
                showMessage(validateProcedureResponse.continuarProceso);
            }else {
                sendMessage("No es posbible continuar con la captura, ocurrio un error al consultar informacion del cliente");
            }
        }else{
            sendMessage("No es posbible continuar con la captura, ocurrio un error al consultar informacion del cliente");
        }
    }

    public void showMessage(String mStatus){
        switch (mStatus){
            case "1":
                mValidateProcedureCallback.OnSuccessValidate();
                break;
            case "2":
                sendMessage(R.string.message5);
                break;
            case "3":
               sendMessage(R.string.message1);
                break;
            case "0":
                sendMessage(R.string.message0);
                break;
        }
    }

    public void savaInformtionClient(Client client){
        SaveProcedureRequest procedureEntity = new SaveProcedureRequest();
        deleteAll();
        mDataManager.tx(tx -> {
            procedureEntity.numCtaInvdual = client.getAccountNumber();
            procedureEntity.destinoNotificacion = client.getFacebookUser() != null ? client.getFacebookUser() : client.getEmail();
            procedureEntity.solicitante = new Solicitante();
            procedureEntity.solicitante.nombreCte = client.getAccountNumber() != null ? client.getAccountNumber() : "";
            procedureEntity.solicitante.apMaternoCte = client.getLastName();
            procedureEntity.solicitante.apPaternoCte = client.getSurName();
            procedureEntity.solicitante.calle = client.getStreet();
            procedureEntity.solicitante.codigoPostal = client.getZipCode();
            procedureEntity.solicitante.colonia = client.getNeighborhood();
            procedureEntity.solicitante.celular = client.getCellphoneNumber();
            procedureEntity.solicitante.curp = client.getCurp();
            procedureEntity.solicitante.municipioDelegacion = client.getDistrict();
            procedureEntity.solicitante.nss = client.getNss();
            procedureEntity.solicitante.numCtaInvdual = client.getAccountNumber();
            procedureEntity.solicitante.rfcCte = client.getRfc();
            procedureEntity.solicitante.correoElec = client.getEmail();
            procedureEntity.solicitante.estado = client.getState();
            procedureEntity.solicitante.numeroInterior = client.getInnerNumber();
            procedureEntity.solicitante.numeroExterior = client.getOuterNumber();
            procedureEntity.solicitante.usuCre = "";
            procedureEntity.solicitante.telefono = client.getCellphoneNumber();
            tx.save(procedureEntity);
        });
    }


    public void deleteAll() {
        mDataManager.tx(tx -> {
           tx.delete(SaveProcedureRequest.class);
        });
    }

    public void saveIndicatorsToRealm(IndicatorsResponse indicatorsResponse){
        SaveProcedureRequest procedureEntity = mDataManager.queryWhere(SaveProcedureRequest.class).findFirst();
        mDataManager.tx(tx -> {
            for(ConfiguracionIndicador mIndicator : indicatorsResponse.configuracionIndicador){
                ListaIndicadore indicator = new ListaIndicadore();
                indicator.descripcion = mIndicator.valorIndicador;
                indicator.id = mIndicator.configuracion.indicador.idIndicador;
                procedureEntity.listaIndicadores.add(indicator);
            }
            tx.save(procedureEntity);
        });
    }
}
