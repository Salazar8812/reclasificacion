package app.profuturo.reclasificacion.com;

import android.content.Context;

import com.resources.utils.Prefs;

import java.security.SecureRandom;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class ProcedureConfigure {
    private Context context;

    public ProcedureConfigure(Context context) {
        this.context = context;
    }

    public void setup() {
        initRealm();
    }

    private void initRealm() {
        byte[] key = new byte[64];
        new SecureRandom(key);
        Prefs.setDefaultContext(context);
        Realm.init(context);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
