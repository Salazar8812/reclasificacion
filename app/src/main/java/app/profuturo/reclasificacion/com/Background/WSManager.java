package app.profuturo.reclasificacion.com.Background;


import android.content.Context;

import java.util.Map;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseRequestInterface;
import app.profuturo.reclasificacion.com.Repository.remote.FilenetRoutes;
import app.profuturo.reclasificacion.com.Repository.remote.Routes;
import app.profuturo.reclasificacion.com.Repository.remote.ValidateProcedure;
import app.profuturo.reclasificacion.com.model.DocumentsRequest;
import app.profuturo.reclasificacion.com.model.IdcRequest;
import app.profuturo.reclasificacion.com.model.IndicatorActionValueRequest;
import app.profuturo.reclasificacion.com.model.IndicatorRequest;
import app.profuturo.reclasificacion.com.model.MatrixReclasificationRequest;
import app.profuturo.reclasificacion.com.model.RegisterReclasificationRequest;
import app.profuturo.reclasificacion.com.model.SaleRequest;
import app.profuturo.reclasificacion.com.model.SaveProcedureRequest;
import app.profuturo.reclasificacion.com.model.SendImageRequest;
import app.profuturo.reclasificacion.com.model.ValidateProcudereRequest;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by charlssalazar on 29/05/18.
 */

public class WSManager extends BaseWSManager {

    public static boolean DEBUG_ENABLED = false;
    private Context mContext;

    public static WSManager init() {
        return new WSManager();
    }


    public class WS {
        public static final String SALES = "getSaldos";
        public static final String VALIDATE_PROCEDURE = "validateProcedure";
        public static final String GET_IDC = "idc";
        public static final String INDICATORS = "getIndicadores";
        public static final String GET_DOCUMENTS = "getDocuments";
        public static final String CONVIVENCE = "validConvivence";
        public static final String GET_MATRIX = "matrix_reclasification";
        public static final String REGISTER_RECLASIFICATION = "register_reclasification";
        public static final String GET_ACTION_VALUE = "valor_accion";
        public static final String OPEN_TRANSACTION = "abrir_transaccion_filenet";
        public static final String SEND_IMAGE = "enviar_imagen";
        public static final String FINISH_PROCEDURE = "tramite_finalizado";
    }

    @Override
    protected Call<ResponseBody> getWebService(String webServiceValue, WSBaseRequestInterface wsBaseRequest) {
        Call<ResponseBody> call = null;
        Routes routesWebServicesDefinition = WebServices.routesAppDefinitions();
        ValidateProcedure validateProcedure = WebServices.validateProcedure();
        FilenetRoutes filenetRoutes = WebServices.routesFilenet();

        switch (webServiceValue) {
            case WS.SALES:
                SaleRequest saleRequest = (SaleRequest) wsBaseRequest;
                call = validateProcedure.getSales(saleRequest.numberAccount, saleRequest.family);
                break;
            case  WS.GET_IDC:
                /*IdcRequest idcRequest = (IdcRequest) wsBaseRequest;
                call = routesWebServicesDefinition.getClients(idcRequest.idc);*/
                break;
            case WS.VALIDATE_PROCEDURE:
                ValidateProcudereRequest mValidateProcudereRequest = (ValidateProcudereRequest) wsBaseRequest;
                call = validateProcedure.validateProcedure(mValidateProcudereRequest.idProceso,mValidateProcudereRequest.idSubProceso,mValidateProcudereRequest.numeroCuenta, mValidateProcudereRequest.expBiometrico,mValidateProcudereRequest.expIdentificacion);
                break;

            case WS.INDICATORS:
                IndicatorRequest mIndicatorRequest = (IndicatorRequest) wsBaseRequest;
                call = validateProcedure.getIndicators(mIndicatorRequest.numberAccount);
                break;

            case WS.CONVIVENCE:
                break;

            case WS.GET_MATRIX:
                MatrixReclasificationRequest matrixReclasificationRequest = (MatrixReclasificationRequest) wsBaseRequest;
                call = validateProcedure.getMatrixReclasification(matrixReclasificationRequest);
                break;

            case WS.GET_DOCUMENTS:
                DocumentsRequest documentsRequest = (DocumentsRequest) wsBaseRequest;
                call = routesWebServicesDefinition.getDocuments(documentsRequest.clasificacionRetiro,documentsRequest.tipoRetiro,documentsRequest.numeroBeneficiarios);
                break;

            case WS.REGISTER_RECLASIFICATION:
                RegisterReclasificationRequest mRegisterReclasificationRequest = (RegisterReclasificationRequest) wsBaseRequest;
                call = validateProcedure.registerReclasification(mRegisterReclasificationRequest.idProceso, mRegisterReclasificationRequest.idSubproceso,mRegisterReclasificationRequest.idCanal,mRegisterReclasificationRequest.usuCre,mRegisterReclasificationRequest.idEstatusProceso,mRegisterReclasificationRequest.idEtapa,mRegisterReclasificationRequest.idSubetapa);
                break;
            case WS.GET_ACTION_VALUE:
                IndicatorActionValueRequest mIndicatorActionValueRequest = (IndicatorActionValueRequest) wsBaseRequest;
                call = validateProcedure.getActionValue(mIndicatorActionValueRequest.fechaValorAccion,mIndicatorActionValueRequest.idSiefore);
                break;
            case WS.OPEN_TRANSACTION:

                break;

            case WS.SEND_IMAGE:
                SendImageRequest sendImageRequest = (SendImageRequest) wsBaseRequest;
                call = filenetRoutes.uploadFile();
                break;

            case WS.FINISH_PROCEDURE:
                SaveProcedureRequest saveProcedureRequest = (SaveProcedureRequest) wsBaseRequest;
                call = validateProcedure.finishProcedure(saveProcedureRequest);
                break;

        }

        return call;
    }

    @Override
    protected Call<ResponseBody> getQueryWebService(String webServiceValue, String requestValue) {
        return null;
    }

    @Override
    protected String getJsonDebug(String requestUrl) {
        String mJsonString= "";

        switch (requestUrl){
            case WS.SALES:
                mJsonString = "{\n" +
                        "\t\"saldoBean\": [{\n" +
                        "\t\t\"idSubcuenta\": 154,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 4,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 154,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACION ESTATAL\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 138,\n" +
                        "\t\t\t\t\"descripcion\": \"IMSS\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 141,\n" +
                        "\t\t\t\t\"descripcion\": \"BASICA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 145,\n" +
                        "\t\t\t\t\"descripcion\": \"RCV\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 77,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-B3\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 3415.01033,\n" +
                        "\t\t\"saldoAlDiaPesos\": 33193.0466550175,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 3415.01,\n" +
                        "\t\t\"saldoDisponiblePesos\": 33193.0466550175,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": 0.00,\n" +
                        "\t\t\"saldoPendientePesos\": 0,\n" +
                        "\t\t\"plazo\": 1000,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16181,\n" +
                        "\t\t\"valorAccion\": 9.71975,\n" +
                        "\t\t\"descripcionPlazo\": \"SIN PLAZO\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 171,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 21,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 171,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES COMPLEMENTARIAS DE RETIRO SUA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 144,\n" +
                        "\t\t\t\t\"descripcion\": \"LP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 150,\n" +
                        "\t\t\t\t\"descripcion\": \"ACR\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 76,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-B2\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 10.867578,\n" +
                        "\t\t\"saldoAlDiaPesos\": 100.146937388334,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 10.87,\n" +
                        "\t\t\"saldoDisponiblePesos\": 100.146937388334,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": -10.87,\n" +
                        "\t\t\"saldoPendientePesos\": -100,\n" +
                        "\t\t\"plazo\": 998,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16175,\n" +
                        "\t\t\"valorAccion\": 9.215203,\n" +
                        "\t\t\"descripcionPlazo\": \"12 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 171,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 21,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 171,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES COMPLEMENTARIAS DE RETIRO SUA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 144,\n" +
                        "\t\t\t\t\"descripcion\": \"LP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 150,\n" +
                        "\t\t\t\t\"descripcion\": \"ACR\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 83,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-LP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 374.455323,\n" +
                        "\t\t\"saldoAlDiaPesos\": 902.547418294962,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 374.46,\n" +
                        "\t\t\"saldoDisponiblePesos\": 902.547418294962,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": -374.46,\n" +
                        "\t\t\"saldoPendientePesos\": -900,\n" +
                        "\t\t\"plazo\": 998,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16180,\n" +
                        "\t\t\"valorAccion\": 2.410294,\n" +
                        "\t\t\"descripcionPlazo\": \"12 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 172,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 22,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 172,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES COMPLEMENTARIAS DE RETIRO VENTANILLA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 144,\n" +
                        "\t\t\t\t\"descripcion\": \"LP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 150,\n" +
                        "\t\t\t\t\"descripcion\": \"ACR\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 76,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-B2\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 543.378917,\n" +
                        "\t\t\"saldoAlDiaPesos\": 5007.347026075151,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 543.38,\n" +
                        "\t\t\"saldoDisponiblePesos\": 5007.347026075151,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": -543.38,\n" +
                        "\t\t\"saldoPendientePesos\": -5000,\n" +
                        "\t\t\"plazo\": 998,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16175,\n" +
                        "\t\t\"valorAccion\": 9.215203,\n" +
                        "\t\t\"descripcionPlazo\": \"12 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 172,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 22,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 172,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES COMPLEMENTARIAS DE RETIRO VENTANILLA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 144,\n" +
                        "\t\t\t\t\"descripcion\": \"LP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 150,\n" +
                        "\t\t\t\t\"descripcion\": \"ACR\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 83,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-LP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 0.091296,\n" +
                        "\t\t\"saldoAlDiaPesos\": 0.220050201024,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 0.09,\n" +
                        "\t\t\"saldoDisponiblePesos\": 0.220050201024,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": 0.00,\n" +
                        "\t\t\"saldoPendientePesos\": 0,\n" +
                        "\t\t\"plazo\": 999,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16180,\n" +
                        "\t\t\"valorAccion\": 2.410294,\n" +
                        "\t\t\"descripcionPlazo\": \"65 AÑOS\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 173,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 23,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 173,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES LARGO PLAZO SUA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 144,\n" +
                        "\t\t\t\t\"descripcion\": \"LP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 147,\n" +
                        "\t\t\t\t\"descripcion\": \"VOLUNTARIAS\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 83,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-LP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 395.258397,\n" +
                        "\t\t\"saldoAlDiaPesos\": 952.688942738718,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 395.26,\n" +
                        "\t\t\"saldoDisponiblePesos\": 952.688942738718,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": -395.26,\n" +
                        "\t\t\"saldoPendientePesos\": -950,\n" +
                        "\t\t\"plazo\": 998,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16180,\n" +
                        "\t\t\"valorAccion\": 2.410294,\n" +
                        "\t\t\"descripcionPlazo\": \"12 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 174,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 24,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 174,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES LARGO PLAZO VENTANILLA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 144,\n" +
                        "\t\t\t\t\"descripcion\": \"LP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 147,\n" +
                        "\t\t\t\t\"descripcion\": \"VOLUNTARIAS\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 83,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-LP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 881.456193,\n" +
                        "\t\t\"saldoAlDiaPesos\": 2124.568573250742,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 881.46,\n" +
                        "\t\t\"saldoDisponiblePesos\": 2124.568573250742,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": 185.97,\n" +
                        "\t\t\"saldoPendientePesos\": 449.35,\n" +
                        "\t\t\"plazo\": 998,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16180,\n" +
                        "\t\t\"valorAccion\": 2.410294,\n" +
                        "\t\t\"descripcionPlazo\": \"12 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 175,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 25,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 175,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES LARGO PLAZO VENTANILLA AVPIL\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 144,\n" +
                        "\t\t\t\t\"descripcion\": \"LP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 147,\n" +
                        "\t\t\t\t\"descripcion\": \"VOLUNTARIAS\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 83,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-LP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 3536.522499,\n" +
                        "\t\t\"saldoAlDiaPesos\": 8524.058960204706,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 3536.52,\n" +
                        "\t\t\"saldoDisponiblePesos\": 8524.058960204706,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": -3536.52,\n" +
                        "\t\t\"saldoPendientePesos\": -8500,\n" +
                        "\t\t\"plazo\": 998,\n" +
                        "\t\t\"deducible\": 1,\n" +
                        "\t\t\"idValorAccion\": 16180,\n" +
                        "\t\t\"valorAccion\": 2.410294,\n" +
                        "\t\t\"descripcionPlazo\": \"12 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 169,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 19,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 169,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES VOLUNTARIAS SUA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 143,\n" +
                        "\t\t\t\t\"descripcion\": \"CP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 147,\n" +
                        "\t\t\t\t\"descripcion\": \"VOLUNTARIAS\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 75,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-B1\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 12.512202,\n" +
                        "\t\t\"saldoAlDiaPesos\": 99.919955243802,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 12.51,\n" +
                        "\t\t\"saldoDisponiblePesos\": 99.919955243802,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": -12.51,\n" +
                        "\t\t\"saldoPendientePesos\": -100,\n" +
                        "\t\t\"plazo\": 998,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16170,\n" +
                        "\t\t\"valorAccion\": 7.985801,\n" +
                        "\t\t\"descripcionPlazo\": \"12 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 169,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 19,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 169,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES VOLUNTARIAS SUA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 143,\n" +
                        "\t\t\t\t\"descripcion\": \"CP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 147,\n" +
                        "\t\t\t\t\"descripcion\": \"VOLUNTARIAS\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 76,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-B2\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 10.867578,\n" +
                        "\t\t\"saldoAlDiaPesos\": 100.146937388334,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 10.87,\n" +
                        "\t\t\"saldoDisponiblePesos\": 100.146937388334,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": -10.87,\n" +
                        "\t\t\"saldoPendientePesos\": -100,\n" +
                        "\t\t\"plazo\": 998,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16175,\n" +
                        "\t\t\"valorAccion\": 9.215203,\n" +
                        "\t\t\"descripcionPlazo\": \"12 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 169,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 19,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 169,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES VOLUNTARIAS SUA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 143,\n" +
                        "\t\t\t\t\"descripcion\": \"CP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 147,\n" +
                        "\t\t\t\t\"descripcion\": \"VOLUNTARIAS\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 82,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-CP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 33.546217,\n" +
                        "\t\t\"saldoAlDiaPesos\": 99.912811502771,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 33.55,\n" +
                        "\t\t\"saldoDisponiblePesos\": 99.912811502771,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": -33.55,\n" +
                        "\t\t\"saldoPendientePesos\": -100,\n" +
                        "\t\t\"plazo\": 998,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16183,\n" +
                        "\t\t\"valorAccion\": 2.978363,\n" +
                        "\t\t\"descripcionPlazo\": \"12 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 169,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 19,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 169,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES VOLUNTARIAS SUA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 143,\n" +
                        "\t\t\t\t\"descripcion\": \"CP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 147,\n" +
                        "\t\t\t\t\"descripcion\": \"VOLUNTARIAS\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 82,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-CP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 2012.773057,\n" +
                        "\t\t\"saldoAlDiaPesos\": 5994.768800365691,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 2012.77,\n" +
                        "\t\t\"saldoDisponiblePesos\": 5994.768800365691,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": -2012.77,\n" +
                        "\t\t\"saldoPendientePesos\": -6000,\n" +
                        "\t\t\"plazo\": 998,\n" +
                        "\t\t\"deducible\": 1,\n" +
                        "\t\t\"idValorAccion\": 16183,\n" +
                        "\t\t\"valorAccion\": 2.978363,\n" +
                        "\t\t\"descripcionPlazo\": \"12 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 170,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 20,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 170,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES VOLUNTARIAS VENTANILLA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 143,\n" +
                        "\t\t\t\t\"descripcion\": \"CP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 147,\n" +
                        "\t\t\t\t\"descripcion\": \"VOLUNTARIAS\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 75,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-B1\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 312.805063,\n" +
                        "\t\t\"saldoAlDiaPesos\": 2497.998984910463,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 312.81,\n" +
                        "\t\t\"saldoDisponiblePesos\": 2497.998984910463,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": -312.81,\n" +
                        "\t\t\"saldoPendientePesos\": -2500,\n" +
                        "\t\t\"plazo\": 998,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16170,\n" +
                        "\t\t\"valorAccion\": 7.985801,\n" +
                        "\t\t\"descripcionPlazo\": \"12 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 170,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 20,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 170,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES VOLUNTARIAS VENTANILLA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 143,\n" +
                        "\t\t\t\t\"descripcion\": \"CP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 147,\n" +
                        "\t\t\t\t\"descripcion\": \"VOLUNTARIAS\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 76,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-B2\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 380.365241,\n" +
                        "\t\t\"saldoAlDiaPesos\": 3505.142909958923,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 380.37,\n" +
                        "\t\t\"saldoDisponiblePesos\": 3505.142909958923,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": -380.37,\n" +
                        "\t\t\"saldoPendientePesos\": -3500,\n" +
                        "\t\t\"plazo\": 998,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16175,\n" +
                        "\t\t\"valorAccion\": 9.215203,\n" +
                        "\t\t\"descripcionPlazo\": \"12 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 170,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 20,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 170,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES VOLUNTARIAS VENTANILLA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 143,\n" +
                        "\t\t\t\t\"descripcion\": \"CP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 147,\n" +
                        "\t\t\t\t\"descripcion\": \"VOLUNTARIAS\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 82,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-CP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 34.507184,\n" +
                        "\t\t\"saldoAlDiaPesos\": 102.774920059792,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 34.51,\n" +
                        "\t\t\"saldoDisponiblePesos\": 102.774920059792,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": 0.00,\n" +
                        "\t\t\"saldoPendientePesos\": 0,\n" +
                        "\t\t\"plazo\": 997,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16183,\n" +
                        "\t\t\"valorAccion\": 2.978363,\n" +
                        "\t\t\"descripcionPlazo\": \"2 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 170,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 20,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 170,\n" +
                        "\t\t\t\t\"descripcion\": \"APORTACIONES VOLUNTARIAS VENTANILLA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 140,\n" +
                        "\t\t\t\t\"descripcion\": \"OTRO\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 143,\n" +
                        "\t\t\t\t\"descripcion\": \"CP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 147,\n" +
                        "\t\t\t\t\"descripcion\": \"VOLUNTARIAS\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 82,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-CP\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 2348.235234,\n" +
                        "\t\t\"saldoAlDiaPesos\": 6993.896936241942,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 2348.24,\n" +
                        "\t\t\"saldoDisponiblePesos\": 6993.896936241942,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": -2348.24,\n" +
                        "\t\t\"saldoPendientePesos\": -7000,\n" +
                        "\t\t\"plazo\": 998,\n" +
                        "\t\t\"deducible\": 1,\n" +
                        "\t\t\"idValorAccion\": 16183,\n" +
                        "\t\t\"valorAccion\": 2.978363,\n" +
                        "\t\t\"descripcionPlazo\": \"12 MESES\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 152,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 2,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 152,\n" +
                        "\t\t\t\t\"descripcion\": \"CESANTIA Y VEJEZ\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 138,\n" +
                        "\t\t\t\t\"descripcion\": \"IMSS\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 141,\n" +
                        "\t\t\t\t\"descripcion\": \"BASICA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 145,\n" +
                        "\t\t\t\t\"descripcion\": \"RCV\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 77,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-B3\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 69793.654294,\n" +
                        "\t\t\"saldoAlDiaPesos\": 678376.8713241065,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 69793.65,\n" +
                        "\t\t\"saldoDisponiblePesos\": 678376.8713241065,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": 0.00,\n" +
                        "\t\t\"saldoPendientePesos\": 0,\n" +
                        "\t\t\"plazo\": 1000,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16181,\n" +
                        "\t\t\"valorAccion\": 9.71975,\n" +
                        "\t\t\"descripcionPlazo\": \"SIN PLAZO\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 153,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 3,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 153,\n" +
                        "\t\t\t\t\"descripcion\": \"CUOTA SOCIAL\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 138,\n" +
                        "\t\t\t\t\"descripcion\": \"IMSS\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 141,\n" +
                        "\t\t\t\t\"descripcion\": \"BASICA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 145,\n" +
                        "\t\t\t\t\"descripcion\": \"RCV\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 77,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-B3\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 3247.154611,\n" +
                        "\t\t\"saldoAlDiaPesos\": 31561.53103026725,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 3247.15,\n" +
                        "\t\t\"saldoDisponiblePesos\": 31561.53103026725,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": 0.00,\n" +
                        "\t\t\"saldoPendientePesos\": 0,\n" +
                        "\t\t\"plazo\": 1000,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16181,\n" +
                        "\t\t\"valorAccion\": 9.71975,\n" +
                        "\t\t\"descripcionPlazo\": \"SIN PLAZO\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 151,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 1,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 151,\n" +
                        "\t\t\t\t\"descripcion\": \"RETIRO 97\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 138,\n" +
                        "\t\t\t\t\"descripcion\": \"IMSS\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 141,\n" +
                        "\t\t\t\t\"descripcion\": \"BASICA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 145,\n" +
                        "\t\t\t\t\"descripcion\": \"RCV\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 77,\n" +
                        "\t\t\t\t\"descripcion\": \"PROF-B3\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 33413.501516,\n" +
                        "\t\t\"saldoAlDiaPesos\": 324770.881360141,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 33413.50,\n" +
                        "\t\t\"saldoDisponiblePesos\": 324770.881360141,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": 0.00,\n" +
                        "\t\t\"saldoPendientePesos\": 0,\n" +
                        "\t\t\"plazo\": 1000,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16181,\n" +
                        "\t\t\"valorAccion\": 9.71975,\n" +
                        "\t\t\"descripcionPlazo\": \"SIN PLAZO\"\n" +
                        "\t}, {\n" +
                        "\t\t\"idSubcuenta\": 165,\n" +
                        "\t\t\"numCuentaIndividual\": 1700003291,\n" +
                        "\t\t\"tablaNciMov\": null,\n" +
                        "\t\t\"tipoSubcuenta\": {\n" +
                        "\t\t\t\"idTipoSubcuenta\": 15,\n" +
                        "\t\t\t\"subcuenta\": {\n" +
                        "\t\t\t\t\"id\": 165,\n" +
                        "\t\t\t\t\"descripcion\": \"VIVIENDA 97\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"regimen\": {\n" +
                        "\t\t\t\t\"id\": 138,\n" +
                        "\t\t\t\t\"descripcion\": \"IMSS\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": 142,\n" +
                        "\t\t\t\t\"descripcion\": \"VIVIENDA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"familia\": {\n" +
                        "\t\t\t\t\"id\": 149,\n" +
                        "\t\t\t\t\"descripcion\": \"VIVIENDA\"\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"siefore\": {\n" +
                        "\t\t\t\"siefore\": {\n" +
                        "\t\t\t\t\"id\": 81,\n" +
                        "\t\t\t\t\"descripcion\": \"VIVIENDA\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"grupo\": {\n" +
                        "\t\t\t\t\"id\": null,\n" +
                        "\t\t\t\t\"descripcion\": null\n" +
                        "\t\t\t}\n" +
                        "\t\t},\n" +
                        "\t\t\"saldoAlDiaAcciones\": 112021.38,\n" +
                        "\t\t\"saldoAlDiaPesos\": 279258.098202,\n" +
                        "\t\t\"saldoDisponibleAcciones\": 112021.38,\n" +
                        "\t\t\"saldoDisponiblePesos\": 279258.098202,\n" +
                        "\t\t\"saldoComprometidoAcciones\": 0.00,\n" +
                        "\t\t\"saldoComprometidoPesos\": 0,\n" +
                        "\t\t\"saldoPendienteAcciones\": 0.00,\n" +
                        "\t\t\"saldoPendientePesos\": 0,\n" +
                        "\t\t\"plazo\": 1000,\n" +
                        "\t\t\"deducible\": 0,\n" +
                        "\t\t\"idValorAccion\": 16184,\n" +
                        "\t\t\"valorAccion\": 2.4929,\n" +
                        "\t\t\"descripcionPlazo\": \"SIN PLAZO\"\n" +
                        "\t}]\n" +
                        "}";
            break;

            case WS.INDICATORS :
                mJsonString = "{\n" +
                        "   \"status\": 200,\n" +
                        "   \"numeroCuentaIndividual\": \"1700003291\",\n" +
                        "\n" +
                        "   \"configuracionIndicador\": [{\n" +
                        "         \"configuracion\": {\n" +
                        "            \"fechaCreacion\": \"2016-09-02T20:20:39.724-05:00\",\n" +
                        "            \"usuarioCreacion\": \"SIS\",\n" +
                        "            \"categoria\": \"true\",\n" +
                        "            \"descripcion\": \"INDICADOR QUE PERMITE IDENTIFICAR SI LA CUENTA SE ENCUENTRA VIGENTE\",\n" +
                        "            \"estatus\": {\n" +
                        "               \"descripcion\": \"LIBERADO\",\n" +
                        "               \"idEstatus\": \"477\"\n" +
                        "            },\n" +
                        "            \"idConfiguracionIndicador\": \"2\",\n" +
                        "            \"indicador\": {\n" +
                        "               \"descripcion\": \"INDICADOR DE CUENTA VIGENTE\",\n" +
                        "               \"idIndicador\": \"58\"\n" +
                        "            },\n" +
                        "            \"regla\": \"La cuenta debe presentar el Indicador de cuenta vigente, desde la apertura de la CI, hasta que se vea involucrado un proceso terminal\",\n" +
                        "            \"tipoIndicador\": {\n" +
                        "               \"descripcion\": \"BOOLEANO\",\n" +
                        "               \"idTipoDatoIndicador\": \"63\"\n" +
                        "            },\n" +
                        "            \"vigencia\": \"true\"\n" +
                        "         },\n" +
                        "         \"detalle\": \"209\",\n" +
                        "         \"fechaRegistro\": \"2016-09-04T17:59:48-05:00\",\n" +
                        "         \"idIndicadorXCuentaIndividual\": \"4017837\",\n" +
                        "         \"valorIndicador\": \"1\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"configuracion\": {\n" +
                        "            \"fechaCreacion\": \"2016-09-02T20:20:39.760-05:00\",\n" +
                        "            \"usuarioCreacion\": \"SIS\",\n" +
                        "            \"categoria\": \"true\",\n" +
                        "            \"descripcion\": \"INDICADOR QUE MUESTRA LA FECHA DE APERTURA DE LA CUENTA INDIVIDUAL\",\n" +
                        "            \"estatus\": {\n" +
                        "               \"descripcion\": \"LIBERADO\",\n" +
                        "               \"idEstatus\": \"477\"\n" +
                        "            },\n" +
                        "            \"idConfiguracionIndicador\": \"5\",\n" +
                        "            \"indicador\": {\n" +
                        "               \"descripcion\": \"FECHA CERTIFICACION\",\n" +
                        "               \"idIndicador\": \"61\"\n" +
                        "            },\n" +
                        "            \"regla\": \"NULL\",\n" +
                        "            \"tipoIndicador\": {\n" +
                        "               \"descripcion\": \"FECHA\",\n" +
                        "               \"idTipoDatoIndicador\": \"64\"\n" +
                        "            },\n" +
                        "            \"vigencia\": \"true\"\n" +
                        "         },\n" +
                        "         \"detalle\": \"0\",\n" +
                        "         \"fechaRegistro\": \"2016-09-04T18:45:22-05:00\",\n" +
                        "         \"idIndicadorXCuentaIndividual\": \"12325152\",\n" +
                        "         \"valorIndicador\": \"08\\/08\\/2000\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"configuracion\": {\n" +
                        "            \"fechaCreacion\": \"2017-09-01T18:06:08.080-05:00\",\n" +
                        "            \"usuarioCreacion\": \"SIS\",\n" +
                        "            \"categoria\": \"true\",\n" +
                        "            \"descripcion\": \"INDICADOR QUE MUESTRA LA FECHA DE PRIMERA APORTACION DE AHORRO VOLUNTARIO CP 2 MESES\",\n" +
                        "            \"estatus\": {\n" +
                        "               \"descripcion\": \"LIBERADO\",\n" +
                        "               \"idEstatus\": \"477\"\n" +
                        "            },\n" +
                        "            \"idConfiguracionIndicador\": \"6\",\n" +
                        "            \"indicador\": {\n" +
                        "               \"descripcion\": \"FECHA PRIMERA APORTACION DE AHORRO VOLUNTARIO CP 2 MESES\",\n" +
                        "               \"idIndicador\": \"703\"\n" +
                        "            },\n" +
                        "            \"regla\": \"NULL\",\n" +
                        "            \"tipoIndicador\": {\n" +
                        "               \"descripcion\": \"FECHA\",\n" +
                        "               \"idTipoDatoIndicador\": \"64\"\n" +
                        "            },\n" +
                        "            \"vigencia\": \"true\"\n" +
                        "         },\n" +
                        "         \"detalle\": \"0\",\n" +
                        "         \"fechaRegistro\": \"2017-10-20T20:12:09-05:00\",\n" +
                        "         \"idIndicadorXCuentaIndividual\": \"28827413\",\n" +
                        "         \"valorIndicador\": \"20051207\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"configuracion\": {\n" +
                        "            \"fechaCreacion\": \"2017-09-01T18:06:08.094-05:00\",\n" +
                        "            \"usuarioCreacion\": \"SIS\",\n" +
                        "            \"categoria\": \"true\",\n" +
                        "            \"descripcion\": \"INDICADOR QUE MUESTRA LA FECHA DE ULTIMO RETIRO DE AHORRO VOLUNTARIO CP 2 MESES\",\n" +
                        "            \"estatus\": {\n" +
                        "               \"descripcion\": \"LIBERADO\",\n" +
                        "               \"idEstatus\": \"477\"\n" +
                        "            },\n" +
                        "            \"idConfiguracionIndicador\": \"7\",\n" +
                        "            \"indicador\": {\n" +
                        "               \"descripcion\": \"FECHA DE ULTIMO RETIRO CP 2 MESES\",\n" +
                        "               \"idIndicador\": \"704\"\n" +
                        "            },\n" +
                        "            \"regla\": \"NULL\",\n" +
                        "            \"tipoIndicador\": {\n" +
                        "               \"descripcion\": \"FECHA\",\n" +
                        "               \"idTipoDatoIndicador\": \"64\"\n" +
                        "            },\n" +
                        "            \"vigencia\": \"true\"\n" +
                        "         },\n" +
                        "         \"fechaRegistro\": \"2018-11-01T13:33:14-06:00\",\n" +
                        "         \"idIndicadorXCuentaIndividual\": \"40859624\",\n" +
                        "         \"valorIndicador\": \"20180215\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"configuracion\": {\n" +
                        "            \"fechaCreacion\": \"2017-09-01T18:06:08.096-05:00\",\n" +
                        "            \"usuarioCreacion\": \"SIS\",\n" +
                        "            \"categoria\": \"true\",\n" +
                        "            \"descripcion\": \"INDICADOR DE EXPEDIENTE DE IDENTIFICACION\",\n" +
                        "            \"estatus\": {\n" +
                        "               \"descripcion\": \"LIBERADO\",\n" +
                        "               \"idEstatus\": \"477\"\n" +
                        "            },\n" +
                        "            \"idConfiguracionIndicador\": \"9\",\n" +
                        "            \"indicador\": {\n" +
                        "               \"descripcion\": \"EXPEDIENTE DE IDENTIFICACION\",\n" +
                        "               \"idIndicador\": \"706\"\n" +
                        "            },\n" +
                        "            \"regla\": \"1= El indicador debe ser actualizado al confirmar el registro o Traspaso de la CI. para el caso de modificaci¿n de datos el resultado  corresponde al diagnostico de la OP 13 como aceptado. 2= El expediente tiene una vigencia de 10 a¿os, por lo que al cumplir esa fecha se debe actualizar de 1 a 2\",\n" +
                        "            \"tipoIndicador\": {\n" +
                        "               \"descripcion\": \"BOOLEANO\",\n" +
                        "               \"idTipoDatoIndicador\": \"63\"\n" +
                        "            },\n" +
                        "            \"vigencia\": \"true\"\n" +
                        "         },\n" +
                        "         \"fechaRegistro\": \"2018-10-25T09:56:58-05:00\",\n" +
                        "         \"idIndicadorXCuentaIndividual\": \"40859576\",\n" +
                        "         \"valorIndicador\": \"5\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"configuracion\": {\n" +
                        "            \"fechaCreacion\": \"2017-09-01T18:06:08.097-05:00\",\n" +
                        "            \"usuarioCreacion\": \"SIS\",\n" +
                        "            \"categoria\": \"true\",\n" +
                        "            \"descripcion\": \"INDICADOR DE EXPEDIENTE BIOMETRICO\",\n" +
                        "            \"estatus\": {\n" +
                        "               \"descripcion\": \"LIBERADO\",\n" +
                        "               \"idEstatus\": \"477\"\n" +
                        "            },\n" +
                        "            \"idConfiguracionIndicador\": \"10\",\n" +
                        "            \"indicador\": {\n" +
                        "               \"descripcion\": \"EXPEDIENTE BIOMETRICO\",\n" +
                        "               \"idIndicador\": \"707\"\n" +
                        "            },\n" +
                        "            \"regla\": \"1= El indicador debe ser actualizado al confirmar el registro o Traspaso de la CI. para el caso de modificaci¿n de datos el resultado  corresponde al diagnostico de la OP 13 como aceptado. 2= El expediente tiene una vigencia de 10 a¿os, por lo que al cumplir esa fecha se debe actualizar de 1 a 2\",\n" +
                        "            \"tipoIndicador\": {\n" +
                        "               \"descripcion\": \"BOOLEANO\",\n" +
                        "               \"idTipoDatoIndicador\": \"63\"\n" +
                        "            },\n" +
                        "            \"vigencia\": \"true\"\n" +
                        "         },\n" +
                        "         \"fechaRegistro\": \"2018-10-25T09:56:53-05:00\",\n" +
                        "         \"idIndicadorXCuentaIndividual\": \"40859575\",\n" +
                        "         \"valorIndicador\": \"1\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"configuracion\": {\n" +
                        "            \"fechaCreacion\": \"2017-09-01T18:06:08.098-05:00\",\n" +
                        "            \"usuarioCreacion\": \"SIS\",\n" +
                        "            \"categoria\": \"true\",\n" +
                        "            \"descripcion\": \"INDICADOR DE CUENTA ACTIVA\",\n" +
                        "            \"estatus\": {\n" +
                        "               \"descripcion\": \"LIBERADO\",\n" +
                        "               \"idEstatus\": \"477\"\n" +
                        "            },\n" +
                        "            \"idConfiguracionIndicador\": \"13\",\n" +
                        "            \"indicador\": {\n" +
                        "               \"descripcion\": \"CUENTA ACTIVA \",\n" +
                        "               \"idIndicador\": \"710\"\n" +
                        "            },\n" +
                        "            \"regla\": \"Deber¿ activar el indicador (SI), si la CI se ve afectada en el proceso de Traspaso A-A Receptora, Recaudaci¿n IMSS o ISSSTE. El indicador tendr¿ una vigencia de un a¿o para modificar el indicador (NO ACTIVA) se debe validar si la CI no presenta movimientos de Acreditaci¿n (Abono) consider¿ndose como tales aquellos movimientos por dep¿sitos de cuotas y aportaciones durante el periodo de un a¿o calendario contado a partir del ¿ltimo dep¿sito realizado. I.El indicador debe ser actualizado al realizar la acreditaci¿n de los sub procesos de abono (Activa) y al no presentar aportaciones en 6 bimestres como (Inactiva)\",\n" +
                        "            \"tipoIndicador\": {\n" +
                        "               \"descripcion\": \"BOOLEANO\",\n" +
                        "               \"idTipoDatoIndicador\": \"63\"\n" +
                        "            },\n" +
                        "            \"vigencia\": \"true\"\n" +
                        "         },\n" +
                        "         \"fechaRegistro\": \"2018-11-01T13:29:20-06:00\",\n" +
                        "         \"idIndicadorXCuentaIndividual\": \"40859623\",\n" +
                        "         \"valorIndicador\": \"1\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"configuracion\": {\n" +
                        "            \"fechaCreacion\": \"2017-09-01T18:06:08.098-05:00\",\n" +
                        "            \"usuarioCreacion\": \"SIS\",\n" +
                        "            \"categoria\": \"true\",\n" +
                        "            \"descripcion\": \"INDICADOR QUE MUESTRA LA FECHA PRIMERA APORTACION DE AHORRO VOLUNTARIO LP 12 MESES\",\n" +
                        "            \"estatus\": {\n" +
                        "               \"descripcion\": \"LIBERADO\",\n" +
                        "               \"idEstatus\": \"477\"\n" +
                        "            },\n" +
                        "            \"idConfiguracionIndicador\": \"16\",\n" +
                        "            \"indicador\": {\n" +
                        "               \"descripcion\": \"FECHA PRIMERA APORTACION DE AHORRO VOLUNTARIO LP 12 MESES\",\n" +
                        "               \"idIndicador\": \"809\"\n" +
                        "            },\n" +
                        "            \"regla\": \"NULL\",\n" +
                        "            \"tipoIndicador\": {\n" +
                        "               \"descripcion\": \"FECHA\",\n" +
                        "               \"idTipoDatoIndicador\": \"64\"\n" +
                        "            },\n" +
                        "            \"vigencia\": \"true\"\n" +
                        "         },\n" +
                        "         \"fechaRegistro\": \"2017-12-01T13:39:11-06:00\",\n" +
                        "         \"idIndicadorXCuentaIndividual\": \"37812600\",\n" +
                        "         \"valorIndicador\": \"01\\/12\\/2017\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"configuracion\": {\n" +
                        "            \"fechaCreacion\": \"2017-09-01T18:06:08.099-05:00\",\n" +
                        "            \"usuarioCreacion\": \"SIS\",\n" +
                        "            \"categoria\": \"true\",\n" +
                        "            \"descripcion\": \"INDICADOR QUE MUESTRA LA FECHA DE ULTIMO RETIRO LP 12 MESES\",\n" +
                        "            \"estatus\": {\n" +
                        "               \"descripcion\": \"LIBERADO\",\n" +
                        "               \"idEstatus\": \"477\"\n" +
                        "            },\n" +
                        "            \"idConfiguracionIndicador\": \"17\",\n" +
                        "            \"indicador\": {\n" +
                        "               \"descripcion\": \"FECHA DE ULTIMO RETIRO LP 12 MESES\",\n" +
                        "               \"idIndicador\": \"810\"\n" +
                        "            },\n" +
                        "            \"regla\": \"NULL\",\n" +
                        "            \"tipoIndicador\": {\n" +
                        "               \"descripcion\": \"FECHA\",\n" +
                        "               \"idTipoDatoIndicador\": \"64\"\n" +
                        "            },\n" +
                        "            \"vigencia\": \"true\"\n" +
                        "         },\n" +
                        "         \"fechaRegistro\": \"2018-11-01T13:35:06-06:00\",\n" +
                        "         \"idIndicadorXCuentaIndividual\": \"40859625\",\n" +
                        "         \"valorIndicador\": \"20180315\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"configuracion\": {\n" +
                        "            \"fechaCreacion\": \"2017-09-01T18:06:08.099-05:00\",\n" +
                        "            \"usuarioCreacion\": \"SIS\",\n" +
                        "            \"categoria\": \"true\",\n" +
                        "            \"descripcion\": \"INDICADOR QUE MUESTRA LA FECHA PRIMERA APORTACION DE AHORRO VOLUNTARIO AVPILP\",\n" +
                        "            \"estatus\": {\n" +
                        "               \"descripcion\": \"LIBERADO\",\n" +
                        "               \"idEstatus\": \"477\"\n" +
                        "            },\n" +
                        "            \"idConfiguracionIndicador\": \"18\",\n" +
                        "            \"indicador\": {\n" +
                        "               \"descripcion\": \"FECHA PRIMERA APORTACION DE AHORRO VOLUNTARIO AVPILP\",\n" +
                        "               \"idIndicador\": \"811\"\n" +
                        "            },\n" +
                        "            \"regla\": \"NULL\",\n" +
                        "            \"tipoIndicador\": {\n" +
                        "               \"descripcion\": \"FECHA\",\n" +
                        "               \"idTipoDatoIndicador\": \"64\"\n" +
                        "            },\n" +
                        "            \"vigencia\": \"true\"\n" +
                        "         },\n" +
                        "         \"fechaRegistro\": \"2018-11-01T13:36:05-06:00\",\n" +
                        "         \"idIndicadorXCuentaIndividual\": \"40859626\",\n" +
                        "         \"valorIndicador\": \"20180415\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"configuracion\": {\n" +
                        "            \"fechaCreacion\": \"2017-09-01T18:06:08.099-05:00\",\n" +
                        "            \"usuarioCreacion\": \"SIS\",\n" +
                        "            \"categoria\": \"true\",\n" +
                        "            \"descripcion\": \"INDICADOR QUE MUESTRA LA FECHA DE ULTIMO RETIRO AVPILP\",\n" +
                        "            \"estatus\": {\n" +
                        "               \"descripcion\": \"LIBERADO\",\n" +
                        "               \"idEstatus\": \"477\"\n" +
                        "            },\n" +
                        "            \"idConfiguracionIndicador\": \"19\",\n" +
                        "            \"indicador\": {\n" +
                        "               \"descripcion\": \"FECHA DE ULTIMO RETIRO AVPILP\",\n" +
                        "               \"idIndicador\": \"812\"\n" +
                        "            },\n" +
                        "            \"regla\": \"NULL\",\n" +
                        "            \"tipoIndicador\": {\n" +
                        "               \"descripcion\": \"FECHA\",\n" +
                        "               \"idTipoDatoIndicador\": \"64\"\n" +
                        "            },\n" +
                        "            \"vigencia\": \"true\"\n" +
                        "         },\n" +
                        "         \"fechaRegistro\": \"2018-11-01T13:38:04-06:00\",\n" +
                        "         \"idIndicadorXCuentaIndividual\": \"40859627\",\n" +
                        "         \"valorIndicador\": \"20180515\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"configuracion\": {\n" +
                        "            \"fechaCreacion\": \"2017-09-01T18:06:08.100-05:00\",\n" +
                        "            \"usuarioCreacion\": \"SIS\",\n" +
                        "            \"categoria\": \"true\",\n" +
                        "            \"descripcion\": \"INDICADOR QUE MUESTRA LA FECHA PRIMERA APORTACION DE AHORRO VOLUNTARIO ACR\",\n" +
                        "            \"estatus\": {\n" +
                        "               \"descripcion\": \"LIBERADO\",\n" +
                        "               \"idEstatus\": \"477\"\n" +
                        "            },\n" +
                        "            \"idConfiguracionIndicador\": \"20\",\n" +
                        "            \"indicador\": {\n" +
                        "               \"descripcion\": \"FECHA PRIMERA APORTACION DE AHORRO VOLUNTARIO ACR\",\n" +
                        "               \"idIndicador\": \"813\"\n" +
                        "            },\n" +
                        "            \"regla\": \"NULL\",\n" +
                        "            \"tipoIndicador\": {\n" +
                        "               \"descripcion\": \"FECHA\",\n" +
                        "               \"idTipoDatoIndicador\": \"64\"\n" +
                        "            },\n" +
                        "            \"vigencia\": \"true\"\n" +
                        "         },\n" +
                        "         \"fechaRegistro\": \"2018-11-01T13:38:57-06:00\",\n" +
                        "         \"idIndicadorXCuentaIndividual\": \"40859628\",\n" +
                        "         \"valorIndicador\": \"20180615\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "         \"configuracion\": {\n" +
                        "            \"fechaCreacion\": \"2017-09-01T18:06:08.100-05:00\",\n" +
                        "            \"usuarioCreacion\": \"SIS\",\n" +
                        "            \"categoria\": \"true\",\n" +
                        "            \"descripcion\": \"INDICADOR QUE MUESTRA LA FECHA DE ULTIMO RETIRO ACR\",\n" +
                        "            \"estatus\": {\n" +
                        "               \"descripcion\": \"LIBERADO\",\n" +
                        "               \"idEstatus\": \"477\"\n" +
                        "            },\n" +
                        "            \"idConfiguracionIndicador\": \"21\",\n" +
                        "            \"indicador\": {\n" +
                        "               \"descripcion\": \"FECHA DE ULTIMO RETIRO ACR\",\n" +
                        "               \"idIndicador\": \"814\"\n" +
                        "            },\n" +
                        "            \"regla\": \"NULL\",\n" +
                        "            \"tipoIndicador\": {\n" +
                        "               \"descripcion\": \"FECHA\",\n" +
                        "               \"idTipoDatoIndicador\": \"64\"\n" +
                        "            },\n" +
                        "            \"vigencia\": \"true\"\n" +
                        "         },\n" +
                        "         \"fechaRegistro\": \"2018-11-01T13:39:45-06:00\",\n" +
                        "         \"idIndicadorXCuentaIndividual\": \"40859629\",\n" +
                        "         \"valorIndicador\": \"20180715\"\n" +
                        "      }\n" +
                        "   ]\n" +
                        "}";

                break;

            case WS.VALIDATE_PROCEDURE:
                mJsonString = "{\n" +
                        "    \"status\": 200,\n" +
                        "    \"continuaProceso\": 1\n" +
                        "}";
                break;

            case WS.GET_ACTION_VALUE:
                mJsonString = "{\n" +
                        "\t\"valorAccion\" : [\n" +
                        "\t\t{\n" +
                        "\t\t\t\"fechaAccion\" : \"2018-11-10\",\n" +
                        "\t\t\t\"fechaActualizacion\" : \"2018-11-10\",\n" +
                        "\t\t\t\"fechaCreacion\" : \"2018-11-10\",\n" +
                        "\t\t\t\"idValorAccion\" : 47,\n" +
                        "\t\t\t\"regimen\" : {\n" +
                        "\t\t\t\t\"id\" : 30,\n" +
                        "\t\t\t\t\"descripcion\" : \"\"\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"siefore\" : {\n" +
                        "\t\t\t\t\"idSiefore\" : 30,\n" +
                        "\t\t\t\t\"descSiefore\" : \"\"\t\t\t\n" +
                        "\t\t\t},\n" +
                        "\t\t\t\"usuarioActualizacion\" : \"usuario\",\n" +
                        "\t\t\t\"usuarioCreacion\" : \"usuario\",\n" +
                        "\t\t\t\"valorAccion\" : 1.02\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "}";
                break;
        }

        return mJsonString;
    }

    @Override
    protected boolean getErrorDebugEnabled() {
        return false;
    }

    @Override
    protected boolean getDebugEnabled() {
        return DEBUG_ENABLED;
    }
}
