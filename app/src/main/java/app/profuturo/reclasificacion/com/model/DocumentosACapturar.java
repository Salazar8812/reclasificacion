
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocumentosACapturar {

    @SerializedName("claveDocumento")
    @Expose
    public String claveDocumento;
    @SerializedName("tipoDocumento")
    @Expose
    public String tipoDocumento;
    @SerializedName("tipoExpediente")
    @Expose
    public String tipoExpediente;

    @SerializedName("obligatorio")
    @Expose
    public Obligatorio obligatorio;
    @SerializedName("paginacion")
    @Expose
    public String paginacion;

    public boolean isTakePhoto;

}
