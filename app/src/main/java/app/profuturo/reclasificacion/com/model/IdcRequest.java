package app.profuturo.reclasificacion.com.model;

import java.util.Map;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseRequestInterface;

public class IdcRequest implements WSBaseRequestInterface {
   public Map<String, String> idc;

    public IdcRequest(Map<String, String> idc) {
        this.idc = idc;
    }
}
