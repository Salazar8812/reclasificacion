package app.profuturo.reclasificacion.com.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.Base64;

import app.profuturo.reclasificacion.com.model.ClientPicture;
import app.profuturo.reclasificacion.com.R;


/**
 * Custom ImageView class that displays the picture
 * of a client from a Base64 String object.
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 05/09/2018 - 07:23 PM
 */

public class Base64ImageView extends AppCompatImageView {
    /**
     * {@inheritDoc}
     */
    public Base64ImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setImageResource(R.drawable.ic_default_user);
    }

    /**
     * Receives the Base64 encoded picture and renders it into
     * this ImageView, if it fails, displays the default user picture.
     *
     * @param image The encoded image as a String.
     * @see Byte
     * @see String
     * @see Bitmap
     * @see BitmapFactory
     */
    public void setClientPicture(@NonNull ClientPicture.ClientPictureMetaData image) {
        // run this heavy task in a worker thread.
        Handler handler = new Handler(getContext().getMainLooper());
        new Thread(() -> {
            try {
                // Parse the encoded image as a array of bytes.
                byte[] imageAsBytes = Base64.decode(image.getBase64Content(), Base64.URL_SAFE);
                // Then use a BitmapFactory to decode the array of bytes into a Bitmap object.
                Bitmap decodedBytes =
                        BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
                // set the decoded Bitmap as the image for this ImageView.
                handler.post(() -> setImageBitmap(decodedBytes));
            } catch (Throwable t) {
                new Handler(getContext().getMainLooper()).post(() ->
                        // If something in the parsing process fails, assign the default user photo.
                        this.setImageResource(R.drawable.ic_default_user));
            }
        }).start(); // Start this thread.
    }
}
