package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.profuturo.reclasificacion.com.Utils.StringsKeys;

/**
        * Java class that defines the JSON structure of a Login request.
        *
        * @author Alfredo Bejarano
        * @version 1.0
        * @since 13/08/2018 - 12:49 PM
        */
public class Login {
    @SerializedName("curp")
    private String curp;
    @SerializedName("idSucAsignada")
    private int assignedBranchID;
    @SerializedName("nomSuc")
    private String branchName;
    @SerializedName("folioTramite")
    private String procedureFolio;
    @SerializedName("idLogin")
    private String idLogin;
    @SerializedName("numAsesor")
    private String agentNumber;
    @SerializedName("numAsesorConsar")
    private String agentConsarNumber;

    @SerializedName("apMaterno")
    private String apMaterno;
    @SerializedName("apPaterno")
    private String apPaterno;
    @SerializedName("nombre")
    private String nombre;
    @SerializedName("fehAcceso")
    private String fehAcceso;
    @SerializedName("idSubproceso")
    private int idSubproceso;
    @SerializedName("usuCre")
    private String usuCre;

    public Login() {
        // Empty constructor for Realm.
    }

    public Login(String curp, int assignedBranchID, String branchName, String idLogin, String agentNumber, String agentConsarNumber) {
        this.curp = curp;
        this.assignedBranchID = assignedBranchID;
        this.branchName = branchName;
        this.idLogin = idLogin;
        this.agentNumber = agentNumber;
        this.agentConsarNumber = agentConsarNumber;
    }

    public Login(String curp, int assignedBranchID, String branchName, String procedureFolio, String idLogin, String agentNumber, String agentConsarNumber) {
        this.curp = curp;
        this.assignedBranchID = assignedBranchID;
        this.branchName = branchName;
        this.procedureFolio = procedureFolio;
        this.idLogin = idLogin;
        this.agentNumber = agentNumber;
        this.agentConsarNumber = agentConsarNumber;
    }

    public Login(String curp, int assignedBranchID, String branchName, String procedureFolio, String idLogin, String agentNumber, String agentConsarNumber, String apMaterno, String apPaterno, String nombre, String fehAcceso, String idSubproceso, String usuCre) {
        this.curp = curp;
        this.assignedBranchID = assignedBranchID;
        this.branchName = branchName;
        this.procedureFolio = procedureFolio;
        this.idLogin = idLogin;
        this.agentNumber = agentNumber;
        this.agentConsarNumber = agentConsarNumber;
        this.apMaterno = apMaterno;
        this.apPaterno = apPaterno;
        this.nombre = nombre;
        this.fehAcceso = fehAcceso;
        this.idSubproceso = Integer.parseInt(StringsKeys.ID_SUBPROCESO);
        this.usuCre = usuCre;
    }

    public String getIdLogin() {
        return idLogin;
    }

    public void setIdLogin(String idLogin) {
        this.idLogin = idLogin;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public int getAssignedBranchID() {
        return assignedBranchID;
    }

    public void setAssignedBranchID(int assignedBranchID) {
        this.assignedBranchID = assignedBranchID;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getProcedureFolio() {
        return procedureFolio;
    }

    public void setProcedureFolio(String procedureFolio) {
        this.procedureFolio = procedureFolio;
    }

    public String getAgentNumber() {
        return agentNumber;
    }

    public void setAgentNumber(String agentNumber) {
        this.agentNumber = agentNumber;
    }

    public String getAgentConsarNumber() {
        return agentConsarNumber;
    }

    public void setAgentConsarNumber(String agentConsarNumber) {
        this.agentConsarNumber = agentConsarNumber;
    }

    public String getApMaterno() {
        return apMaterno;
    }

    public void setApMaterno(String apMaterno) {
        this.apMaterno = apMaterno;
    }

    public String getApPaterno() {
        return apPaterno;
    }

    public void setApPaterno(String apPaterno) {
        this.apPaterno = apPaterno;
    }

    public String getFehAcceso() {
        return fehAcceso;
    }

    public void setFehAcceso(String fehAcceso) {
        this.fehAcceso = fehAcceso;
    }

    public String getIdSubproceso() {
        return StringsKeys.ID_SUBPROCESO;
    }

    public void setIdSubproceso(int idSubproceso) {
        this.idSubproceso = idSubproceso;
    }

    public String getUsuCre() {
        return usuCre;
    }

    public void setUsuCre(String usuCre) {
        this.usuCre = usuCre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}

