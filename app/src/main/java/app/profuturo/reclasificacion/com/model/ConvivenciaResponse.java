package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.SerializedName;

public class ConvivenciaResponse {
    @SerializedName("convive")
    public String mConvive;
}
