package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.SerializedName;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseRequestInterface;

public class SendImageRequest implements WSBaseRequestInterface {
    @SerializedName("tramiteId")
    public String tramiteId;

    @SerializedName("tipoTramite")
    public String tipoTramite;

    @SerializedName("archivo")
    public String archivo;

    public SendImageRequest(String tramiteId, String tipoTramite, String archivo) {
        this.tramiteId = tramiteId;
        this.tipoTramite = tipoTramite;
        this.archivo = archivo;
    }
}
