package app.profuturo.reclasificacion.com.presenter;

import android.arch.lifecycle.MutableLiveData;
import android.os.Handler;
import android.support.annotation.NonNull;

import app.profuturo.reclasificacion.com.base.View.BaseView;

public class BasePresenter {
    /**
     * Reference to a BaseView implementation for this
     * presenter to use as its view.
     */
    private BaseView mView;

    /**
     * Status value that reports if this presenter is busy or not.
     * Wrapped in {@link MutableLiveData} allowing a {@link android.arch.lifecycle.LifecycleOwner}
     * to observe it.
     */
    private MutableLiveData<Status> mStatus = new MutableLiveData<>();

    /**
     * Constructor for this presenter class that assigns a View.
     * This view can be null, this is useful in Unit Test cases.
     *
     * @param view The view for this presenter.
     */
    public BasePresenter(@NonNull BaseView view) {
        // Assign the view to this presenter.
        mView = view;
    }

    /**
     * Returns the status {@link MutableLiveData} object for this presenter.
     *
     * @return MutableLiveData object of this presenter status.
     */
    public MutableLiveData<Status> getStatus() {
        return mStatus;
    }

    /**
     * Changes the status of the presenter.
     *
     * @param status The new status for the presenter.
     */
    protected void setStatus(Status status) {
        mStatus.postValue(status);
    }

    /**
     * This function will remove the reference to the given view,
     * preventing leakages of Context child classes as Views usually
     * extends from the Context class.
     */
    public void destroyView() {
        if (mView != null) {
            mView = null;
        }
    }

    /**
     * Defines an accessor to the mView property.
     *
     * @return Reference of BaseView for this presenter.
     */
    protected BaseView getView() {
        return mView;
    }

    /**
     * Sends a message object for the View to display it.
     * This is being displayed safely in the UI thread.
     *
     * @param message The message to be displayed.
     */
    protected void sendMessage(@NonNull final Object message) {
        if (mView != null) {
            new Handler(mView.asContext()
                    .getMainLooper())
                    .post(() -> mView.displayMessage(message));
        }
    }


    /**
     * This method will notify to the view if it is processing data
     * or if it has finished processing data.
     *
     * @param r True if the presenter is processing data.
     */
    protected void runOnWorkerThread(@NonNull final Runnable r) {
        // Notify that the presenter is busy, as this is running in another thread, use postValue().
        mStatus.postValue(Status.STATUS_BUSY);
        // Execute the runnable in the worker thread.
        new Thread(r).start();
    }

    /**
     * Enum class that defines if a presenter is busy or not.
     */
    public enum Status {
        /**
         * This value defines that the presenter is done doing work.
         */
        STATUS_DONE,
        /**
         * This value defines that the presenter is doing work.
         */
        STATUS_BUSY,
        /**
         * This value defines that an operation made by the presenter failed.
         */
        STATUS_FAILED
    }
}
