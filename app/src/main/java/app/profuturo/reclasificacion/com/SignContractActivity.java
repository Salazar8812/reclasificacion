package app.profuturo.reclasificacion.com;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.resources.mvp.BaseActivity;

import app.profuturo.reclasificacion.com.Callbacks.SignCapturedCallback;
import app.profuturo.reclasificacion.com.Utils.MessageUtils;

public class SignContractActivity extends AppCompatActivity implements SignaturePad.OnSignedListener, View.OnClickListener {
    private Button mSaveSignatureButton;
    private Button mClearSignatureButton;
    private SignaturePad mContentSignatureDraw;
    private SignCapturedCallback mSignCapturedCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signing_dialog);
        bindingViews();
        setFinishOnTouchOutside(false);
    }

    public void getCallback(SignCapturedCallback signCapturedCallback){
        this.mSignCapturedCallback = signCapturedCallback;
    }

    public void bindingViews(){
        mSaveSignatureButton = findViewById(R.id.mSaveSignatureButton);
        mClearSignatureButton = findViewById(R.id.mClearSignatureButton);
        mContentSignatureDraw = findViewById(R.id.mContentSignatureDraw);

        mSaveSignatureButton.setOnClickListener(this);
        mClearSignatureButton.setOnClickListener(this);
    }

    @Override
    public void onStartSigning() {

    }

    @Override
    public void onSigned() {

    }

    @Override
    public void onClear() {

    }

    @Override
    public void onClick(View v) {
        if(R.id.mSaveSignatureButton == v.getId()){
            if( mContentSignatureDraw.getSignatureBitmap() != null) {
                MessageUtils.toast(this, "Firma guardad exitosamente");
                setResult(666, getIntent());
                this.finish();
            }else{
                MessageUtils.LaunDialog(this,"Firma","Es necesario ingresar la firma",null);
            }
        }else if (R.id.mClearSignatureButton == v.getId()){
            this.finish();
        }
    }
}
