package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;

public class RegisterReclasifcationResponse implements WSBaseResponseInterface {

    @SerializedName("status")
    public String status;

    @SerializedName("CVE_SOLICITUD")
    public String cve_solicity;

    @SerializedName("FOLIO_FUS")
    public String folio_fus;
}
