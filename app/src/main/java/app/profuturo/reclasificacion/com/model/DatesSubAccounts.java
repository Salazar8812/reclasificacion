package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.SerializedName;

public class DatesSubAccounts {
    @SerializedName("fecha1erApor")
    public String firstInputDate;

    @SerializedName("fechaUltRetiroVol")
    public String lastDate;

    @SerializedName("idSubcuenta")
    public String idSubAccount;

    public DatesSubAccounts(String firstInputDate, String lastDate, String idSubAccount) {
        this.firstInputDate = firstInputDate;
        this.lastDate = lastDate;
        this.idSubAccount = idSubAccount;
    }

    public DatesSubAccounts() {
    }
}
