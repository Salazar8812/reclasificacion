package app.profuturo.reclasificacion.com.presenter;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import app.profuturo.reclasificacion.com.base.View.BaseView;
import app.profuturo.reclasificacion.com.base.utils.JsonMocker;
import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.Repository.local.ClientDAO;
import app.profuturo.reclasificacion.com.base.Utils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import app.profuturo.reclasificacion.com.model.SaveProcedureRequest;
import app.profuturo.reclasificacion.com.model.Solicitante;
import retrofit2.Call;

/**
 * Presenter class related to a client search operations.
 *
 * @author Luis Meléndez
 * @version 1.0
 * @since 15/08/2018 - 12:09 PM
 */
public class ClientPresenter extends NetworkPresenter {
    /**
     * Constant value that defines the length of an account number.
     */
    public static final int CURP_LENGTH = 18;
    /**
     * Constant that defines that a client is independent.
     */
    private static final Integer INDEPENDENT_CLIENT = 68;
    /**
     * Constant value that defines the key for the NSS number.
     */
    private static final String NSS = "nss";
    /**
     * Constant value that defines the key for the CURP number.
     */
    private static final String CURP = "curp";
    /**
     * Constant value that defines the key for an account number.
     */
    private static final String ACCOUNT_NUMBER = "numeroCuenta";
    /**
     * Constant value that defines the length of a NSS number.
     */
    private static final int NSS_LENGTH = 11;
    /**
     * Defines the results from the client search.
     */
    private MutableLiveData<Client> mClientResults = new MutableLiveData<>();

    /**
     * Reference to the stored client from the database.
     */
    private MutableLiveData<Client> mStoredClient = new MutableLiveData<>();

    /**
     * Constructor for this presenter class that assigns a View.
     * This view can be null, this is useful in Unit Test cases.
     *
     * @param view The view for this presenter.
     */
    public ClientPresenter(@NonNull BaseView view) {
        super(view);
    }

    /**
     * Reference to the list of client results retrieved from the query.
     *
     * @return The MutableLiveData object containing the list of client results.
     */
    public MutableLiveData<Client> getClientResults() {
        return mClientResults;
    }

    /**
     * Reference to the client retrieved from the local database.
     *
     * @return The MutableLiveData object containing the list of client results.
     */
    public MutableLiveData<Client> getStoredClient() {
        return mStoredClient;
    }

    /**
     * Validates that a given parameter matches the necessary formats.
     * If so it gets added to the map.
     *
     * @param queryParams Map containing the query params keys and values.
     * @param value       The value to be added to the given query params map.
     */
    private void validateQueryParam(Map<String, String> queryParams, String value) {
        // Create a reference that defines the name of the key for the given value.
        String parameterKey;
        // Validate the given parameter, if it matches any of the necessary formats.
        if (Utils.validateNumericCriteriaByLength(value, NSS_LENGTH)) {
            // Assign the key value for a Social Security Number.
            parameterKey = NSS;
        } else if (Utils.validateCriteriaByLength(value, CURP_LENGTH)) {
            // Assign the key value for a CURP value.
            parameterKey = CURP;
        } else {
            // Assign the key value for an account number.
            parameterKey = ACCOUNT_NUMBER;
        }
        // Check that the parameter key is not null.
        if (parameterKey != null) {
            // If not, add the value with its key to the query params map.
            queryParams.put(parameterKey, value);
        }
    }

    /**
     * Builds the query params for performing a client search.
     *
     * @param criteria The criteria for searching clients (NSS, Curp or account number).
     * @return The Map with the generated query params.
     */
    @NonNull
    private Map<String, String> generateQueryParams(String criteria) {
        // Build a new Map, it will define the query params.
        Map<String, String> queryParams = new HashMap<>();
        // Validate the given criteria and add it to the query params.
        validateQueryParam(queryParams, criteria);
        // Return the built query params.
        return queryParams;
    }

    /**
     * Method to make API call to get client details
     *
     * @param criteria The criteria for searching the client (NSS, NC or CURP).
     */
    public void searchClientsByCriteria(String criteria) {
        runOnWorkerThread(() -> {
            Map<String, String> queryParams = generateQueryParams(criteria);
            Call<Client> request = routes.getClients(queryParams);
            enqueueRequest(request, payload -> {
                // Check if the response is not null and contains results.
                if (payload != null && payload.isAccountNotNull()) {
                    // Check if the client is valid and enabled
                    if (payload.isAccountValid()) {
                        // Generate the client contact data
                        payload.digestContactData();
                        // Check if the client is independent, if so, notify to the agent.
                        if (INDEPENDENT_CLIENT == payload.getRegimentIndicatorId()) {
                            sendMessage(R.string.total_withdrawal_unavailable);
                        }
                        // Notify the change to the view.
                        mClientResults.postValue(payload);
                        // Notify that the presenter has finished doing work.
                        setStatus(Status.STATUS_DONE);
                    } else {
                        sendMessage(R.string.no_valid_clients);
                        setStatus(Status.STATUS_FAILED);
                    }
                } else {
                    sendMessage(R.string.no_matches_found);
                }
            }, errorPayload -> noClientsFound());
        });
    }

    /**
     * Reports that no matches were found for a search query.
     */
    private void noClientsFound() {
        // Notify that no clients were found.
        mClientResults.postValue(null);
        // Notify why it failed.
        sendMessage(R.string.no_valid_clients);
        // Notify that the presenter failed doing the main operation.
        setStatus(Status.STATUS_FAILED);
    }

    /**
     * Method to get the first criteria valid, return null if any is valid
     *
     * @param accountNumber The account number of the client
     * @param nss           the security number of the client
     * @param curp          the curp of the client
     * @return a valid criteria , null if any is valid
     */
    public String getFirstValidCriteria(String accountNumber, String nss, String curp) {
        // Initializes the criteria as null
        String criteria = null;
        // Check the account number has 10 digits exactly
        if (!TextUtils.isEmpty(accountNumber)) {
            // Assigns the criteria as the account number
            criteria = accountNumber;
            // Check the account number has 11 digits exactly
        } else if (Utils.validateNumericCriteriaByLength(nss, NSS_LENGTH)) {
            // Assigns the criteria as the account number
            criteria = nss;
        } else if (Utils.isCURPvalid(curp.toUpperCase(Locale.getDefault()))) {
            criteria = curp;
        }
        return criteria;
    }

    /**
     * Method to save the client into the database
     *
     * @param client The client to be stored
     */
    public void saveClient(Client client) {
        // Run this operation in a worker thread
        //client.setAccountNumber(validateClientAccount(client));
        runOnWorkerThread(() -> {
            setStatus(Status.STATUS_BUSY);
            // Persist client in the db
            ClientDAO.createClient(client);
            // Notify the presenter is done with its job
            setStatus(Status.STATUS_DONE);
        });
    }

    /**
     * Method to delete the client from database
     */
    public void deleteClient() {
        runOnWorkerThread(() -> {
            setStatus(Status.STATUS_BUSY);
            // Delete client from database
            ClientDAO.deleteClient();
            // Report the presenter is done with this action
            setStatus(Status.STATUS_DONE);
        });
    }

    /**
     * Retrieves the client stored in the local storage database
     * and reports the value to a MutableLiveData property.
     */
    public void fetchStoredClient() {
        // Execute this code in a worker thread.
        runOnWorkerThread(() -> {
            // Report that the presenter is doing work.
            setStatus(Status.STATUS_BUSY);
            // Read the client using the ClientDAO.
            Client c = ClientDAO.readClient();
            // Post the value of the results in the MutableLiveData property.
            mStoredClient.postValue(c);
            // Report the finish status of the presenter depending on the client result.
            if (c == null) {
                setStatus(Status.STATUS_FAILED);
            } else {
                setStatus(Status.STATUS_DONE);
            }
        });
    }
}