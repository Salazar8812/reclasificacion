package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;

public class PhoneContact extends ClientContact implements RealmModel {
    @Expose
    @SerializedName("sms")
    private Boolean sms;
    @Expose
    @SerializedName("lada")
    private Integer lada;
    @Expose
    @SerializedName("numero")
    private String number;
    @Expose
    @SerializedName("tipoTelefono")
    private Type type;

    public PhoneContact() {
        // Empty method scrub!
    }

    public PhoneContact(boolean valid, boolean prefereed, Boolean sms, Integer lada, String number, Type type) {
        super(valid, prefereed);
        this.sms = sms;
        this.lada = lada;
        this.number = number;
        this.type = type;
    }

    public Boolean getSms() {
        return sms;
    }

    public Integer getLada() {
        return lada;
    }

    public String getNumber() {
        return number;
    }

    public Type getType() {
        return type;
    }
}
