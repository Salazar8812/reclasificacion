
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class SaldosOriginales extends RealmObject {

    @SerializedName("saldo")
    @Expose
    public Saldo saldo;

}
