package app.profuturo.reclasificacion.com.model;

import app.profuturo.reclasificacion.com.base.Utils;
import io.realm.RealmObject;

public class Shares extends RealmObject {

    private Integer sharesCV;
    private Integer sharesCS;
    private Integer sharesR;
    private Integer sharesRetiro92;
    private Integer sharesVivienda92;
    private Integer sharesVivienda97;

    public Shares() {
    }

    public Integer getSharesCV() {
        return sharesCV;
    }

    public void setSharesCV(Integer sharesCV) {
        this.sharesCV = sharesCV;
    }

    public Integer getSharesCS() {
        return sharesCS;
    }

    public void setSharesCS(Integer sharesCS) {
        this.sharesCS = sharesCS;
    }

    public Integer getSharesR() {
        return sharesR;
    }

    public void setSharesR(Integer sharesR) {
        this.sharesR = sharesR;
    }

    public Integer getSharesRetiro92() {
        return sharesRetiro92;
    }

    public void setSharesRetiro92(Integer sharesRetiro92) {
        this.sharesRetiro92 = sharesRetiro92;
    }

    public Integer getSharesVivienda92() {
        return sharesVivienda92;
    }

    public void setSharesVivienda92(Integer sharesVivienda92) {
        this.sharesVivienda92 = sharesVivienda92;
    }

    public Integer getSharesVivienda97() {
        return sharesVivienda97;
    }

    public void setSharesVivienda97(Integer sharesVivienda97) {
        this.sharesVivienda97 = sharesVivienda97;
    }

    public void setShareByAccount(String account, Integer balanceInShares) {
        if (Utils.CS_ACCOUNT.equals(account)) {
            setSharesCS(balanceInShares);
        } else if (Utils.CV_ACCOUNT.equals(account)) {
            setSharesCV(balanceInShares);
        } else if (Utils.R_ACCOUNT.equals(account)) {
            setSharesR(balanceInShares);
        } else if (Utils.RETIRO92_ACCOUNT.equals(account)) {
            setSharesRetiro92(balanceInShares);
        } else if (Utils.VIVENDA92_ACCOUNT.equals(account)) {
            setSharesRetiro92(balanceInShares);
        } else if (Utils.VIVENDA97_ACCOUNT.equals(account)) {
            setSharesRetiro92(balanceInShares);
        }
    }

}
