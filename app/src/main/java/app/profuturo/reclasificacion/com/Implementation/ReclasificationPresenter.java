package app.profuturo.reclasificacion.com.Implementation;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import app.profuturo.reclasificacion.com.Background.BaseWSManager;
import app.profuturo.reclasificacion.com.Background.WSManager;
import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;
import app.profuturo.reclasificacion.com.Callbacks.WSCallback;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.Utils.MessageUtils;
import app.profuturo.reclasificacion.com.base.RealmPresenter;
import app.profuturo.reclasificacion.com.base.View.BaseView;

public  abstract class ReclasificationPresenter extends RealmPresenter implements WSCallback {
    protected BaseWSManager mWSManager;
    private BaseView mView;
    public AppCompatActivity mAppCompatActivity;

    @Override
    public void onResume() {
        super.onResume();
    }

    public ReclasificationPresenter(AppCompatActivity appCompatActivity, BaseView view) {
        super(appCompatActivity, view);
        mView = view;
        mAppCompatActivity = appCompatActivity;
        mWSManager = WSManager.init().settings(mContext, this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mWSManager = initWSManager();
    }

    public abstract BaseWSManager initWSManager();

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWSManager != null) mWSManager.onDestroy();
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.stopProgress();
        if (messageError.trim().equals("")) {
            MessageUtils.toast(mContext, R.string.dialog_intern_error);
        } else {
            MessageUtils.toast(mContext, messageError);
        }
    }

    @Override
    public void onErrorConnection() {
        MessageUtils.stopProgress();
        MessageUtils.toast(mContext, R.string.dialog_error_connection);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}
