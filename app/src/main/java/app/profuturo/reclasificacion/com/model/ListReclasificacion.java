
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class ListReclasificacion extends RealmObject{

    @SerializedName("acciones")
    @Expose
    public String acciones;
    @SerializedName("idSiefore")
    @Expose
    public String idSiefore;
    @SerializedName("idSubcta")
    @Expose
    public String idSubcta;
    @SerializedName("idTipoMov")
    @Expose
    public String idTipoMov;
    @SerializedName("idValorAccion")
    @Expose
    public String idValorAccion;
    @SerializedName("pesos")
    @Expose
    public String pesos;
    @SerializedName("porcentaje")
    @Expose
    public String porcentaje;
    @SerializedName("idFondoApp")
    @Expose
    public String idFondoApp;

    public ListReclasificacion() {
    }

    public ListReclasificacion(String acciones, String idSiefore, String idSubcta, String idTipoMov, String idValorAccion, String pesos, String porcentaje, String idFondoApp) {
        this.acciones = acciones;
        this.idSiefore = idSiefore;
        this.idSubcta = idSubcta;
        this.idTipoMov = idTipoMov;
        this.idValorAccion = idValorAccion;
        this.pesos = pesos;
        this.porcentaje = porcentaje;
        this.idFondoApp = idFondoApp;
    }
}
