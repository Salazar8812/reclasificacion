
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Estatus {

    @SerializedName("descripcion")
    @Expose
    public String descripcion;
    @SerializedName("idEstatus")
    @Expose
    public String idEstatus;

}
