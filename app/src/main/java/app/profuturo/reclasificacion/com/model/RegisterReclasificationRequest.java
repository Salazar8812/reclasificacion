package app.profuturo.reclasificacion.com.model;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseRequestInterface;

public class RegisterReclasificationRequest implements WSBaseRequestInterface {
    public String idProceso;
    public String idSubproceso;
    public String idCanal;
    public String usuCre;
    public String idEstatusProceso;
    public String idEtapa;
    public String idSubetapa;

    public RegisterReclasificationRequest(String idProceso, String idSubproceso, String idCanal, String usuCre, String idEstatusProceso, String idEtapa, String idSubetapa) {
        this.idProceso = idProceso;
        this.idSubproceso = idSubproceso;
        this.idCanal = idCanal;
        this.usuCre = usuCre;
        this.idEstatusProceso = idEstatusProceso;
        this.idEtapa = idEtapa;
        this.idSubetapa = idSubetapa;
    }
}
