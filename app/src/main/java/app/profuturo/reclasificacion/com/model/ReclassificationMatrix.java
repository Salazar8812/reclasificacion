package app.profuturo.reclasificacion.com.model;

import java.util.ArrayList;
import java.util.List;

public class ReclassificationMatrix {

    public String SieforeAccept;
    public List<SubAccountDetail> idSubcuentasAccept= new ArrayList<>();
    public String deducibleAccept;
    public String title;
    public String id;

    public ReclassificationMatrix() {
    }

    public List<ReclassificationMatrix> getSubAccounts(){
        List<ReclassificationMatrix> list = new ArrayList<>();

        ReclassificationMatrix mOne = new ReclassificationMatrix();
        mOne.SieforeAccept = "75";
        mOne.idSubcuentasAccept.add(new SubAccountDetail("169","0",""));
        mOne.idSubcuentasAccept.add(new SubAccountDetail("170","0",""));
        mOne.deducibleAccept = "0";
        mOne.title = "AV CP SB1";
        mOne.id = "2206";

        ReclassificationMatrix mTwo = new ReclassificationMatrix();
        mTwo.SieforeAccept = "76";
        mTwo.idSubcuentasAccept.add(new SubAccountDetail("169","0",""));
        mTwo.idSubcuentasAccept.add(new SubAccountDetail("170","0",""));
        mTwo.deducibleAccept = "0";
        mTwo.title = "AV";
        mTwo.id = "2210";

        ReclassificationMatrix mThree = new ReclassificationMatrix();
        mThree.SieforeAccept = "76";
        mThree.idSubcuentasAccept.add(new SubAccountDetail("171","0",""));
        mThree.idSubcuentasAccept.add(new SubAccountDetail("172","0",""));
        mThree.deducibleAccept = "0";
        mThree.title = "ACR";
        mThree.id = "2211";

        ReclassificationMatrix mFour = new ReclassificationMatrix();
        mFour.SieforeAccept = "82";
        mFour.idSubcuentasAccept.add(new SubAccountDetail("169","0",""));
        mFour.idSubcuentasAccept.add(new SubAccountDetail("170","0",""));
        mFour.deducibleAccept = "0";
        mFour.title = "Invier-T 2";
        mFour.id = "741";

        ReclassificationMatrix mFive = new ReclassificationMatrix();
        mFive.SieforeAccept = "82";
        mFive.idSubcuentasAccept.add(new SubAccountDetail("169","1",""));
        mFive.idSubcuentasAccept.add(new SubAccountDetail("170","1",""));
        mFive.deducibleAccept = "0";
        mFive.title = "AV CP MESES DED";
        mFive.id = "2208";

        ReclassificationMatrix mSix = new ReclassificationMatrix();
        mSix.SieforeAccept = "83";
        mSix.idSubcuentasAccept.add(new SubAccountDetail("173","0",""));
        mSix.idSubcuentasAccept.add(new SubAccountDetail("174","0",""));
        mSix.deducibleAccept = "0";
        mSix.title = "AV LP 6 MESES";
        mSix.id = "2209";

        ReclassificationMatrix mSeven = new ReclassificationMatrix();
        mSeven.SieforeAccept = "83";
        mSeven.idSubcuentasAccept.add(new SubAccountDetail("173","0","998"));
        mSeven.idSubcuentasAccept.add(new SubAccountDetail("174","0","998"));
        mSeven.deducibleAccept = "0";
        mSeven.title = "Invier-T 12 (ALP)";
        mSeven.id = "742";

        ReclassificationMatrix mEight = new ReclassificationMatrix();
        mEight.SieforeAccept = "83";
        mEight.idSubcuentasAccept.add(new SubAccountDetail("175","0",""));
        mEight.idSubcuentasAccept.add(new SubAccountDetail("175","0",""));
        mEight.deducibleAccept = "0";
        mEight.title = "Invier-T Deducibles (AVPILP)";
        mEight.id = "743";

        ReclassificationMatrix mNine = new ReclassificationMatrix();
        mNine.SieforeAccept = "83";
        mNine.idSubcuentasAccept.add(new SubAccountDetail("171","0",""));
        mNine.idSubcuentasAccept.add(new SubAccountDetail("172","0",""));
        mNine.deducibleAccept = "0";
        mNine.title = "Retira-T 65 Años (ACR)";
        mNine.id = "744";

        list.add(mOne);
        list.add(mTwo);
        list.add(mThree);
        list.add(mFour);
        list.add(mFive);
        list.add(mSix);
        list.add(mSeven);
        list.add(mEight);
        list.add(mNine);

        return list;
    }


    public static class SubAccountDetail{
        public String idSubAccount;
        public String idDeducible;
        public String plazo;

        public SubAccountDetail(String idSubAccount, String idDeducible, String plazo) {
            this.idSubAccount = idSubAccount;
            this.idDeducible = idDeducible;
            this.plazo = plazo;
        }
    }
}
