package app.profuturo.reclasificacion.com;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import app.profuturo.reclasificacion.com.Adapter.NavigationAdapter;
import app.profuturo.reclasificacion.com.Repository.local.AgentSessionDAO;
import app.profuturo.reclasificacion.com.base.BaseActivity;
import app.profuturo.reclasificacion.com.base.View.Configurable;
import app.profuturo.reclasificacion.com.databinding.ActivityProcedureBinding;
import app.profuturo.reclasificacion.com.model.AgentSession;
import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.presenter.ClientPresenter;
import app.profuturo.reclasificacion.com.widget.NavigationViewPager;

public class ProcedureActivity extends BaseActivity<ActivityProcedureBinding>
        implements  Configurable, ClientSearchFragment.ClientResultInteraction,
        ViewPager.OnPageChangeListener {

     /**
     * Listener for the ActionBar.
     */
    private ActionBarDrawerToggle mToggle;
    public static Activity mActivity;

    /**
     * Opens or closes the navigation drawer by pressing the menu icon.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.home_button) {
            mActivity.finish();
            return true;
        } else {
            return mToggle.onOptionsItemSelected(item);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getLayoutId() {
        return R.layout.activity_procedure;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setup() {
        mActivity = this;
        // Adapter that handles navigation for this activity.
        getViewPager().setAdapter(new NavigationAdapter(getSupportFragmentManager()));
        new Thread(() -> {
            // Assign bindings.
            ActivityProcedureBinding b = getDataBinding();
            try {
                AgentSession session = AgentSessionDAO.readAgentSession();
                b.setAgentSession(session);
                b.setVersionNumber(BuildConfig.VERSION_NAME);

                // Create a new drawer toggle.
                mToggle = new ActionBarDrawerToggle(this, getDataBinding().drawerLayout,
                        R.string.app_name, R.string.app_name);
                // Set the toggle listener.
                getDataBinding().drawerLayout.addDrawerListener(mToggle);
                // Attach Client presenter it can be call for saving a client
                attachPresenters(new ClientPresenter(this));
                // Attach ResumeProcedurePresenter it can be call for saving a client
                // Set the toolbar for this activity.
                setupActionBar();
                getViewPager().addOnPageChangeListener(this);
            } catch (IllegalStateException ise) {
                //finishAffinity();
                //System.exit(0);
            }
        }).start();
    }

    /**
     * Inflates the Home icon in the app menu.
     *
     * @param menu The menu from the activity.
     * @return true if the menu was inflated correctly.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    /**
     * Sets the menu and hamburger icon for this activity toolbar.
     */
    private void setupActionBar() {
        // Retrieve the toolbar from the layout.
        Toolbar tb = getDataBinding().toolbar;
        // Sets the layout action bar to this activity.
        setSupportActionBar(tb);
        // Retrieve the action bar.
        ActionBar ab = getSupportActionBar();
        // If the actionbar is not null.
        if (ab != null) {
            // Set the toolbar title
            ab.setTitle(R.string.app_name);
            // Enable the menu button.
            ab.setDisplayHomeAsUpEnabled(true);
            // Set the menu icon.
            ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTitle(@NonNull String title) {
        // Retrieve this activity actionbar.
        ActionBar actionBar = getSupportActionBar();
        // If the actionbar is no null, change its title.
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    @Override
    public void setFragment(@NonNull Integer fragmentPosition) {
        getViewPager().setCurrentItem(fragmentPosition, true);
    }

    /**
     * {@inheritDoc}
     */
    @NonNull
    @Override
    public NavigationViewPager getViewPager() {
        return findViewById(R.id.procedure_content);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nullable
    public <T> T getProcedureExtra(@NonNull String key, @NonNull Class<T> result) {
        return result.cast(mProcedureExtras.get(key));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> void setProcedureExtra(@NonNull String key, @NonNull T value) {
        mProcedureExtras.put(key, value);
    }

    /**
     * {@inheritDoc}
     *
     * @param client The client object that has been selected from the list.
     */
    public void onClientSelected(Client client, Boolean selected, Boolean clicked) {
        // Get a the client presenter to save the client
        ClientPresenter presenter = getPresenter(ClientPresenter.class);
        // Save the client selected
        presenter.saveClient(client);
        // Check if the view was clicked or selected.
        if (getViewPager().getAdapter() != null) {
            NavigationAdapter adapter = (NavigationAdapter) getViewPager().getAdapter();
            // Prevent double fragments.
            Integer currentPosition = getViewPager().getCurrentItem();
            if (currentPosition != adapter.getCount() - 1) {
                adapter.removePreviousFragment();
            }
            if (clicked) {
                adapter.setNextFragment(new ClientInformationFragment());
                getViewPager().setCurrentItem(getViewPager().getCurrentItem() + 1,
                        false);
                getViewPager().setAllowedDirection(NavigationViewPager.SwipeDirection.SWIPE_ALL);
            } else if (selected) {
                adapter.setNextFragment(new ClientInformationFragment());
                // Proceed with the navigation through history if the client is selected.
                getViewPager().setAllowedDirection(NavigationViewPager.SwipeDirection.SWIPE_ALL);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Integer currentItemPosition = getViewPager().getCurrentItem();
        if (currentItemPosition >= 1) {
            getViewPager().setCurrentItem(currentItemPosition - 1);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        // Empty method stub!
    }

    @Override
    public void onPageSelected(int position) {
        NavigationAdapter adapter = getAdapter();
        if (adapter != null) {
            if (position != adapter.getCount() - 1) {
                adapter.removePreviousFragment();
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        // Empty method stub!
    }

    @Nullable
    protected NavigationAdapter getAdapter() {
        NavigationViewPager navigationViewPager = getViewPager();
        return (NavigationAdapter) navigationViewPager.getAdapter();
    }

}
