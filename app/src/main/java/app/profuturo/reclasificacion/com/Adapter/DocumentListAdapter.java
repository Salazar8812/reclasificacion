package app.profuturo.reclasificacion.com.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.model.DocumentosACapturar;
import app.profuturo.reclasificacion.com.model.Respuestum;

public class DocumentListAdapter extends RecyclerView.Adapter<DocumentsHolder> implements DocumentsHolder.OnItemSelectionChanged{
    List<Respuestum> mListDocuments = new ArrayList<>();
    private List<DocumentsHolder> mViewHolders = new ArrayList<>();
    private DocumentListAdapter.DocumentsResultInteraction mDocumentsResultInteraction;

    public DocumentListAdapter(DocumentsResultInteraction mDocumentsResultInteraction) {
        this.mDocumentsResultInteraction = mDocumentsResultInteraction;
    }

    @NonNull
    @Override
    public DocumentsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return insertViewHolder(new DocumentsHolder(
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_document_capture,
                        viewGroup, false), this));
    }

    private DocumentsHolder insertViewHolder(@NonNull DocumentsHolder vh) {
        if (!mViewHolders.contains(vh)) {
            mViewHolders.add(vh);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull DocumentsHolder documentsHolder, int i) {
        documentsHolder.render(mListDocuments.get(i), false, mDocumentsResultInteraction);
    }

    public void update(List<Respuestum> mListDocuments){
        this.mListDocuments = mListDocuments;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mListDocuments.size();
    }

    @Override
    public void onItemSelectionChanged(DocumentsHolder vh, Boolean selection) {
        Respuestum documents = vh.getmDocuments();
        mDocumentsResultInteraction.onDocumentSelected(documents.documentosACapturar,true,false);
    }

    public interface DocumentsResultInteraction {
        /**
         * Method that depending of the implementation,
         * will operate with the clicked client object.
         *
         * @param documentosACapturar The client object that has been selected from the list.
         */
        void onDocumentSelected(DocumentosACapturar documentosACapturar, Boolean selected, Boolean clicked);
    }
}
