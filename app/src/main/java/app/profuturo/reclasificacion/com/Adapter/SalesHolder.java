package app.profuturo.reclasificacion.com.Adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.model.SaldoBean;
import app.profuturo.reclasificacion.com.model.SalesFormat;

public class SalesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    RadioButton mSelectSubAccountRadioButton;
    TextView mTypeSubAccountTextView;
    TextView mDateTextView;
    TextView mSaleTextView;
    private OnItemSelectionChanged mItemSelectionChanged;
    private SalesFormat mSaldoBean;
    private Context mContext;
    private SalesAdapter.SalesResultInteraction msalesResultInteraction;

    public SalesHolder(@NonNull View itemView,SalesHolder.OnItemSelectionChanged itemSelectionChangedListener) {
        super(itemView);
        mContext = itemView.getContext();
        mSelectSubAccountRadioButton = itemView.findViewById(R.id.mSelectSubAccountRadioButton);
        mTypeSubAccountTextView = itemView.findViewById(R.id.mTypeSubAccountTextView);
        mDateTextView = itemView.findViewById(R.id.mDateTextView);
        mSaleTextView = itemView.findViewById(R.id.mSaleTextView);
        this.mItemSelectionChanged = itemSelectionChangedListener;
    }

    public SalesFormat getSaldoBean() {
        return mSaldoBean;
    }

    public void render(@NonNull SalesFormat saldoBean, @NonNull Boolean selection, SalesAdapter.SalesResultInteraction mSalesResultInteraction) {
        mSaldoBean = saldoBean;
        mTypeSubAccountTextView.setText(saldoBean.mTitleSale);
        if(!saldoBean.listDates.isEmpty()) {
            mDateTextView.setText(formatDate(saldoBean.listDates.get(0).firstInputDate));
        }
        mSaleTextView.setText("$"+String.format("%.2f", Double.parseDouble(saldoBean.saleAmmount)));
        this.msalesResultInteraction = mSalesResultInteraction;
        setupSelectionCheckBox(this, selection);
    }

    public String formatDate(String mDate){
        try {
            mDate = new StringBuilder(mDate).insert(4, "/").toString();

            mDate = new StringBuilder(mDate).insert(7, "/").toString();
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        return mDate;
    }

    private void setupSelectionCheckBox(SalesHolder holder, Boolean selection) {
        // Remove a onCheckSelectionListener if it has been already added.
        mSelectSubAccountRadioButton.setOnCheckedChangeListener(null);
        // Change the itemView background depending on the selection.
        itemView.setBackgroundResource(selection ? R.drawable.background_item_selected :
                R.drawable.background_button_white);
        // Change the elevation depending on the selection if API >= 21.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            itemView.setElevation(selection ? 4 : 0);
        }
        // Set the new selection value for the check box.
        mSelectSubAccountRadioButton.setChecked(selection);
        // Assign the check changed listener again.
        mSelectSubAccountRadioButton.setOnCheckedChangeListener((compoundButton, bool) -> {
            if (bool) {
                reportClientSelection(false);
            }
            mItemSelectionChanged.onItemSelectionChanged(holder, bool);
        });
    }

    private void reportClientSelection(Boolean clicked) {
        if (msalesResultInteraction != null) {
            // If the interaction can be made, call the method for using this element client object.
            msalesResultInteraction.onSaleSelected(mSaldoBean, mSelectSubAccountRadioButton.isChecked(), clicked);
        }
    }

    @Override
    public void onClick(View v) {
    }

    public interface OnItemSelectionChanged {
        /**
         * This method is called when a ClientResultViewHolder changes its selection.
         *
         * @param vh        The ViewHolder that changed its selection.
         * @param selection The new selection value.
         */
        void onItemSelectionChanged(SalesHolder vh, Boolean selection);
    }
}
