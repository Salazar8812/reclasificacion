package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Class that wraps objects for body objects to
 * perform requests to Profuturo web services.
 *
 * @author Luis
 * @version 2.0
 * @since August 9th, 2018
 */

public class ProfuturoRequest<T> {

    @Expose
    @SerializedName("rqt")
    private T requestValue;

    public ProfuturoRequest(T requestValue) {
        this.requestValue = requestValue;
    }

    public T getRequestValue() {
        return requestValue;
    }

    public void setRequestValue(T requestValue) {
        this.requestValue = requestValue;
    }
}
