package app.profuturo.reclasificacion.com;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import app.profuturo.reclasificacion.com.base.BaseActivity;
import app.profuturo.reclasificacion.com.base.Utils;
import app.profuturo.reclasificacion.com.databinding.ActivityLoginBinding;

public class LoginActivity extends BaseActivity<ActivityLoginBinding> implements View.OnClickListener{
    private Button mButtonLogin;
    private EditText mUserEdittext;
    private EditText mPasswordEdittext;

    @Override
    public Integer getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.mLoginButton){
            if(!getDataBinding().mUserEdittext.getText().toString().equals("") && !getDataBinding().mPasswordEdittext.getText().toString().equals("")){
                Intent intent = new Intent(this, SplashActivity.class);
                intent.putExtra("user",getDataBinding().mUserEdittext.getText().toString());
                startActivity(intent);
            }else{
                Toast.makeText(this,"Es necesario llenar ambos campos",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void setup() {
        getDataBinding().mLoginButton.setOnClickListener(this);
    }
}
