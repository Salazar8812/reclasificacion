package app.profuturo.reclasificacion.com.presenter;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import app.profuturo.reclasificacion.com.base.View.BaseView;
import app.profuturo.reclasificacion.com.model.ClientAccountNumber;
import app.profuturo.reclasificacion.com.model.ClientPicture;
import retrofit2.Call;

/**
 * Presenter that make call to retrieve the client's picture
 *
 * @author Luis Meléndez
 * @version 1.0
 * @since 07/09/2018 - 10:43 AM
 */
public class ClientPicturePresenter extends NetworkPresenter {

    // This variable holds the data for the client's picture
    private MutableLiveData<ClientPicture.ClientPictureMetaData> pictureData = new MutableLiveData<>();

    /**
     * Constructor for this presenter class that assigns a View.
     * This view can be null, this is useful in Unit Test cases.
     *
     * @param view The view for this presenter.
     */
    public ClientPicturePresenter(@NonNull BaseView view) {
        super(view);
    }

    /**
     * Method to retrive the container of the PictureMetaData
     *
     * @return MutableLiveData with photo information to be observed
     */
    public MutableLiveData<ClientPicture.ClientPictureMetaData> getPictureData() {
        return pictureData;
    }

    /**
     * Method that makes service call to get client´s picture
     *
     * @param accountNumber The account number fot the client selected
     */
    public void getClientPicture(String accountNumber) {
        runOnWorkerThread(() -> {
            // Construct the body request with the client's account number
            ClientAccountNumber clientAccountNumber = new ClientAccountNumber(accountNumber);
            // Prepare the request
            Call<ClientPicture> request = routes.retrieveClientPicture(clientAccountNumber);
            // Enqueue the call to be processed by retrofit
            enqueueRequest(request, (payload -> {
                // Check the payload is not null
                if (payload != null) {
                    // Update the mutable live data with the client's photo data
                    pictureData.postValue(payload.getMetaData());
                    // Change status to indicate the presenter is done with this job
                    setStatus(Status.STATUS_DONE);
                } else {
                    // Post a null value to indicate failure
                    pictureData.postValue(null);
                    // Change status to indicate the presenter is done with this job
                    setStatus(Status.STATUS_FAILED);
                }
            }));
        });
    }


    public void getConvivence(String mAccountNumber){

    }
}
