package app.profuturo.reclasificacion.com.Utils;

public class StringsKeys {
    public static String ID_PROCESO = "7";
    public static String ID_SUBPROCESO = "130";
    public static String ID_ABONO = "181";
    public static String ID_PARCIAL = "2193";
    public static String ID_TOTAL = "2194";
    public static int ID_ANVERSO = 1193;
    public static int ID_REVERSO = 1194;

    public static String ID_SUBETAPA = "990";
    public static String ID_ETAPA = "19";
}
