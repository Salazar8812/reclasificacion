package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 *
 * Model class that defines the enclosed class
 * for a Profuturo request to retrieve the
 * assigned branch information for an Agent.
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 13/08/2018 - 10:42 AM
 */
public class AgentNumber {
    @Expose
    @SerializedName("numeroEmpleado")
    private String value;

    public AgentNumber(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
