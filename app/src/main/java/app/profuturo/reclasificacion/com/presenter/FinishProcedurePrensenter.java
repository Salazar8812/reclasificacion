package app.profuturo.reclasificacion.com.presenter;

import android.support.v7.app.AppCompatActivity;

import app.profuturo.reclasificacion.com.Background.BaseWSManager;
import app.profuturo.reclasificacion.com.Background.WSManager;
import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;
import app.profuturo.reclasificacion.com.Callbacks.WSCallback;
import app.profuturo.reclasificacion.com.Implementation.ReclasificationPresenter;
import app.profuturo.reclasificacion.com.Utils.MessageUtils;
import app.profuturo.reclasificacion.com.base.View.BaseView;
import app.profuturo.reclasificacion.com.model.SaveProcedureRequest;
import app.profuturo.reclasificacion.com.model.SaveProcedureResponse;

public class FinishProcedurePrensenter extends ReclasificationPresenter implements WSCallback {

    public interface FinishProcedureCallback{
        void OnSuccessFinish();

        void OnErrorSendProfcedure();
    }

    private FinishProcedureCallback mFinishProcedureCallback;

    public FinishProcedurePrensenter(AppCompatActivity appCompatActivity, BaseView view, FinishProcedureCallback mFinishProcedureCallback) {
        super(appCompatActivity, view);
        this.mFinishProcedureCallback = mFinishProcedureCallback;
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void finishProcedure(){
        SaveProcedureRequest saveProcedureRequest = retrieveReclasification();
        mWSManager.requestWs(SaveProcedureResponse.class, WSManager.WS.FINISH_PROCEDURE, saveProcedureRequest);
    }


    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if(requestUrl.equals(WSManager.WS.FINISH_PROCEDURE)){
            OnSuccessFinishProcedure((SaveProcedureResponse) baseResponse);
        }
    }

    public void OnSuccessFinishProcedure(SaveProcedureResponse saveProcedureResponse){
        if(saveProcedureResponse != null){
            if(saveProcedureResponse.save.equals("true")) {
                mFinishProcedureCallback.OnSuccessFinish();
            }else{
                mFinishProcedureCallback.OnErrorSendProfcedure();
            }
        }
    }

    public SaveProcedureRequest retrieveReclasification(){
        SaveProcedureRequest procedureEntiy = mDataManager.queryWhere(SaveProcedureRequest.class).findFirst();
        return  procedureEntiy;
    }
 }
