package app.profuturo.reclasificacion.com;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class FinishProcedureDialog extends AppCompatActivity implements View.OnClickListener {
    private Button mPrintDocumentationButton;
    private Button mHideDocumentationButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.finish_procedure_dialog);
        bindData();
    }

    public void bindData(){
        mPrintDocumentationButton = findViewById(R.id.mPrintDocumentationButton);
        mHideDocumentationButton = findViewById(R.id.mHideDocumentationButton);

        mPrintDocumentationButton.setOnClickListener(this);
        mHideDocumentationButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.mPrintDocumentationButton){
            this.finish();
        }else if(view.getId() == R.id.mHideDocumentationButton){
            this.finish();
        }
    }
}
