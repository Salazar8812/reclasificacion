package app.profuturo.reclasificacion.com.Repository.remote;

import java.util.Map;

import app.profuturo.reclasificacion.com.BuildConfig;
import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.model.IndicatorActionValueRequest;
import app.profuturo.reclasificacion.com.model.MatrixReclasificationRequest;
import app.profuturo.reclasificacion.com.model.SaveProcedureRequest;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.QueryName;

public interface ValidateProcedure {
    @GET("iib/apovol/reclasificacion/ra_validarreclasificacion/v1/validarTramite")
    Call<ResponseBody> validateProcedure(@Query("idProceso") String idProceso ,
                                         @Query("idSubproceso") String idSubProceso,
                                         @Query("numeroCuenta") String numeroCuenta,
                                         @Query("expBiometrico") String expBiometrico,
                                         @Query("expIdentificacion") String expIdentificacion );


    @GET("iib/apovol/reclasificacion/ra_indicadores/v1/consultaBasica")
    Call<ResponseBody> getIndicators(@Query("numCuenta") String numCuenta);


    @POST("iib/apovol/reclasificacion/ra_matrizreclasificacion/v1/MatrizReclasificacion")
    Call<ResponseBody> getMatrixReclasification(@Body MatrixReclasificationRequest matrixReclasificationRequest);

    @GET("iib/apovol/reclasificacion/ra_saldos/v1/consultarSaldo")
    Call<ResponseBody> getSales(@Query("numCuentaIndividual") String numberClient,@Query("familia") String family);

    @GET("iib/apovol/reclasificacion/ra_registrarreclasificacion/v1/registrarReclasificacion")
    Call<ResponseBody> registerReclasification(@Query("idProceso") String numberClient,
                                               @Query("idSubproceso") String idSubproceso,
                                               @Query("idCanal") String idCanal,
                                               @Query("usuCre") String usuCre,
                                               @Query("idEstatusProceso") String idEstatusProceso,
                                               @Query("idEtapa") String idEtapa,
                                               @Query("idSubetapa") String idSubetapa);

    @GET("iib/profuturo/reclasificacion/servicio/ra_valoraccion/v1/consultar")
    Call<ResponseBody> getActionValue(@Query("fechaValorAccion") String fechaValorAccion,
                                      @Query("idSiefore") String idSiefore);

    @POST("iib/profuturo/reclasificacion/servicio/ra_guardarreclasificacion/v1/GuardarReclasificacion")
    Call<ResponseBody>
    finishProcedure(@Body SaveProcedureRequest saveProcedureRequest);
}
