
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Siefore_ {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("idSiefore")
    @Expose
    private String idSiefore;

    @SerializedName("descSiefore")
    @Expose
    private String descSiefore;


    @SerializedName("descripcion")
    @Expose
    private String descripcion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getIdSiefore() {
        return idSiefore;
    }

    public void setIdSiefore(String idSiefore) {
        this.idSiefore = idSiefore;
    }

    public String getDescSiefore() {
        return descSiefore;
    }

    public void setDescSiefore(String descSiefore) {
        this.descSiefore = descSiefore;
    }
}
