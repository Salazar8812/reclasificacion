package app.profuturo.reclasificacion.com.base;

import android.app.ActivityManager;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import java.util.HashMap;

import app.profuturo.reclasificacion.com.Implementation.BasePresenterC;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.base.View.BaseView;
import app.profuturo.reclasificacion.com.base.View.SnackBar;
import app.profuturo.reclasificacion.com.presenter.BasePresenter;

public abstract class BaseActivity<VDB extends ViewDataBinding> extends
        AppCompatActivity implements BaseView {

    private BasePresenterC[] mPresentersC;
    private BasePresenterC mPresenterC;

    protected BasePresenterC getPresenterC() {
        return null;
    }

    protected BasePresenterC[] getPresentersC() {
        return null;
    }


    /**
     * Presenters for this activity.
     */
    private HashMap<String, BasePresenter> mPresenters = new HashMap<>();
    /**
     * Loading layout for this view.
     */
    private FrameLayout mLoadingView;

    /**
     * Observer for all the attached presenters, this observer will display or hide the loading view
     * if any of the attached presenters is busy or has done doing work, respectively.
     */
    private Observer<BasePresenter.Status> mPresenterStatusObserver = status -> {
        // Show the loading view if the status has changed to STATUS_BUSY.
        displayLoadingView(status == BasePresenter.Status.STATUS_BUSY);
    };

    /**
     * ViewDataBinding reference for this view.
     */
    private VDB mDataBinding;

    /**
     * Returns the data binding object for this view.
     *
     * @return This data binding object as the necessary class for this view.
     */
    protected VDB getDataBinding() {
        return mDataBinding;
    }

    /**
     * {@inheritDoc}
     */
    public abstract Integer getLayoutId();

    /**
     * Initializes the message SnackBar class and its DataBinding references.
     */
    private void startView() {
        // Initialize the loading view.
        mLoadingView = findViewById(R.id.loading);
        // Setup this view.
        setup();
    }

    /**
     * Destroys this view references from this view
     * presenters and clears the presenters Map.
     */
    private void detachPresenters() {
        // Check if the presenters HashMap is not null or empty.
        if (Utils.isNotNullOrEmpty(mPresenters)) {
            // Get every single key from the mPresenters HashMap.
            for (String key : mPresenters.keySet()) {
                BasePresenter p = mPresenters.get(key);
                // Remove all observers for the presenter status value.
                p.getStatus().removeObservers(this);
                // Proceed to destroy the view reference for said presenter.
                p.destroyView();
            }
            // Now clear the presenters HashMap.
            mPresenters.clear();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Lock the activity orientation to Landscape.
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        // Check if the inactivity service is not running.
        /*if (!isServiceRunning(InactivityService.class)) {
            // If it is not, create an intent to start it.
            Intent serviceIntent = new Intent(this, InactivityService.class);
            // Then, proceed to start it.
            startService(serviceIntent);
        }*/
        // Set the content view for this activity.
        setContentView(R.layout.layout_base);
        // Inflate the layout into the container.
        mDataBinding = DataBindingUtil.inflate(getLayoutInflater(), getLayoutId(), findViewById(R.id.content), true);
        // Set the binding reference LifeCycleOwner.
        mDataBinding.setLifecycleOwner(this);
        // Start this view.
        startView();


        this.mPresentersC = this.getPresentersC();
        this.mPresenterC = this.getPresenterC();
        if (this.mPresentersC != null) {
            BasePresenterC[] var2 = this.mPresentersC;
            int var3 = var2.length;

            for(int var4 = 0; var4 < var3; ++var4) {
                BasePresenterC basePresenter = var2[var4];
                if (basePresenter != null) {
                    basePresenter.onCreate();
                }
            }
        }

        if (this.mPresenterC != null) {
            this.mPresenterC.onCreate();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayMessage(@NonNull Object payload) {
        // Hide the activity loading view.
        displayLoadingView(false);
        // Get the activity root.
        View root = findViewById(android.R.id.content);
        // Show a SnackBar with the payload's message content.
        if (payload instanceof Integer) {
            SnackBar.show(root, (Integer) payload);
        } else if (payload instanceof String) {
            SnackBar.show(root, (String) payload);
        } else if (payload instanceof Throwable) {
            SnackBar.show(root, ((Throwable) payload).getLocalizedMessage());
        } else {
            SnackBar.show(root, String.valueOf(payload));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayLoadingView(@NonNull Boolean displayView) {
        if (mLoadingView != null) {
            mLoadingView.setVisibility(displayView ? View.VISIBLE : View.GONE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @NonNull
    @Override
    public Context asContext() {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        /* Create an Intent to broadcast the user interaction.
        Intent interactionIntent = new Intent(InactivityService.USER_INTERACTION_FILTER);
        // Broadcast this user interaction.
        LocalBroadcastManager.getInstance(this).sendBroadcast(interactionIntent);*/
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.mPresentersC != null) {
            BasePresenterC[] var1 = this.mPresentersC;
            int var2 = var1.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                BasePresenterC basePresenter = var1[var3];
                if (basePresenter != null) {
                    basePresenter.onDestroy();
                }
            }
        }

        if (this.mPresenterC != null) {
            this.mPresenterC.onDestroy();
        }
        detachPresenters();
    }

    /**
     * Check if a given Service class is already running-
     *
     * @param serviceClass The service to check if is running.
     * @return true if the service is running.
     */
    protected Boolean isServiceRunning(@NonNull Class<?> serviceClass) {
        // Retrieve the system's activity manager.
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        // Check if the manager is not null.
        if (manager != null) {
            // If it is not, get all the running services.
            for (ActivityManager.RunningServiceInfo serviceInfo : manager.getRunningServices(Integer.MAX_VALUE)) {
                // check if any of those service's class name matches the one we want.
                if (serviceClass.getName().equals(serviceInfo.service.getClassName())) {
                    // If so, return true.
                    return true;
                }
            }
            // If no service matches the class name, return false.
            return false;
        } else {
            // If it is null, it is impossible to know if the service is running.
            return false;
        }
    }

    /**
     * Adds an array of BasePresenter objects to the presenters for this view.
     *
     * @param presenters The presenters to be attached.
     */
    protected void attachPresenters(BasePresenter... presenters) {
        // Check if the presenters varargs is not null or empty.
        if (presenters != null && presenters.length > 0) {
            // If it isn't, observe all the attaching presenter status.
            for (BasePresenter p : presenters) {
                // Observe the current presenter status.
                p.getStatus().observe(this, mPresenterStatusObserver);
                // Add this presenter to the presenters list.
                mPresenters.put(p.getClass().getSimpleName(), p);
            }
        }
    }

    /**
     * Returns a presenter that has  been attached.
     *
     * @param clazz The presenter class to be retrieved.
     * @param <P>   Generic defining the type of presenter to be retrieved.
     * @return The expected presenter object.
     */
    protected <P extends BasePresenter> P getPresenter(@NonNull Class<P> clazz) {
        // Retrieve the simple name of the expected presenter class.
        String presenterClass = clazz.getSimpleName();
        // Check if the desired presenter has been attached.
        if (mPresenters.containsKey(presenterClass)) {
            // Return the casted presenter class, if found.
            return clazz.cast(mPresenters.get(presenterClass));
        } else {
            // If it hasn't been attached, return null.
            return null;
        }
    }

    /**
     * Observes changes from a {@link MutableLiveData} object.
     *
     * @param liveData The object to be observed.
     * @param observer How the changed values are going to behave.
     * @param <T>      Generic defining the type of data the liveData object has.
     */
    protected <T> void observe(MutableLiveData<T> liveData, Observer<T> observer) {
        liveData.observe(this, observer);
    }


    protected void onStart() {
        super.onStart();
        if (this.mPresentersC != null) {
            BasePresenterC[] var1 = this.mPresentersC;
            int var2 = var1.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                BasePresenterC basePresenter = var1[var3];
                if (basePresenter != null) {
                    basePresenter.onStart();
                }
            }
        }

        if (this.mPresenterC != null) {
            this.mPresenterC.onStart();
        }

    }

    protected void onResume() {
        super.onResume();
        if (this.mPresentersC != null) {
            BasePresenterC[] var1 = this.mPresentersC;
            int var2 = var1.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                BasePresenterC basePresenter = var1[var3];
                if (basePresenter != null) {
                    basePresenter.onResume();
                }
            }
        }

        if (this.mPresenterC != null) {
            this.mPresenterC.onResume();
        }

    }

    protected void onPause() {
        super.onPause();
        if (this.mPresentersC != null) {
            BasePresenterC[] var1 = this.mPresentersC;
            int var2 = var1.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                BasePresenterC basePresenter = var1[var3];
                if (basePresenter != null) {
                    basePresenter.onPause();
                }
            }
        }

        if (this.mPresenterC != null) {
            this.mPresenterC.onPause();
        }

    }

    protected void onStop() {
        super.onStop();
        if (this.mPresentersC != null) {
            BasePresenterC[] var1 = this.mPresentersC;
            int var2 = var1.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                BasePresenterC basePresenter = var1[var3];
                if (basePresenter != null) {
                    basePresenter.onStop();
                }
            }
        }

        if (this.mPresenterC != null) {
            this.mPresenterC.onStop();
        }

    }

}
