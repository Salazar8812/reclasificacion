package app.profuturo.reclasificacion.com;
import android.databinding.ViewDataBinding;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;

import app.profuturo.reclasificacion.com.Adapter.NavigationAdapter;
import app.profuturo.reclasificacion.com.base.BaseFragment;
import app.profuturo.reclasificacion.com.base.View.Configurable;
import app.profuturo.reclasificacion.com.widget.NavigationViewPager;

/**
 * Simple {@link BaseFragment} class that implements
 * swipe listeners for this view and proceeds to define itself
 * as the swipe listener for a fragment.
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 17/09/2018 - 12:37 PM
 */
public abstract class NavigationFragment<VDB extends ViewDataBinding>
        extends BaseFragment<VDB> {

    protected NavigationViewPager getViewPager() {
        // Check if the context implements Configurable.
        if (asContext() instanceof Configurable) {
            // If so, return the ViewPager from the configurable.
            return ((Configurable) asContext()).getViewPager();
        } else {
            // If not, return null.
            return null;
        }
    }

    @Nullable
    protected NavigationAdapter getAdapter() {
        NavigationViewPager navigationViewPager = getViewPager();
        if (navigationViewPager != null) {
            return (NavigationAdapter) navigationViewPager.getAdapter();
        } else {
            return null;
        }
    }

    /**
     * Returns the context object as configurable if it implements it.
     *
     * @return null if the context for this view doesn't implements configurable.
     */
    @Nullable
    protected Configurable getConfigurable() {
        if (asContext() instanceof Configurable) {
            return (Configurable) asContext();
        } else {
            return null;
        }
    }
}

