package app.profuturo.reclasificacion.com.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 *  Class representing a procedure
 *
 * @author Luis Meléndez
 * @version 1.0
 * @since 28/08/2018 - 09:40 AM
 */
public class Procedure {
    @Expose
    @SerializedName("acuse")
    private String acuse;
    @Expose
    @SerializedName("clienteNotificado")
    private Integer notifiedClient;
    @Expose
    @SerializedName("contReactivacio")
    private Integer contEmptyReagent;
    @Expose
    @SerializedName("curpSol")
    private String curpApplicant;
    @Expose
    @SerializedName("correoElecSol")
    private String applicantEmail;
    @Expose
    @SerializedName("applicantCURP")
    private String curpSol;
    @Expose
    @SerializedName("cveActAut")
    private String codeActAuth;
    @Expose
    @SerializedName("cveAseguradora")
    private String insuranceCarrierCode;
    @Expose
    @SerializedName("fecEstatus")
    private String statusDate;
    @SerializedName("fecIniPen")
    @Expose
    private String pensionStartDate;
    @Expose
    @SerializedName("fecNac")
    private String dateOfBirth;
    @Expose
    @SerializedName("fecResPen")
    private String pensionResDate;
    @Expose
    @SerializedName("fecRespBenef")
    private String beneficiaryResDate;
    @Expose
    @SerializedName("fecTramite")
    private String procedureDate;
    @Expose
    @SerializedName("fehAct")
    private String fehAct;
    @Expose
    @SerializedName("fehCre")
    private String creationDate;
    @Expose
    @SerializedName("folio")
    private String folio;
    @Expose
    @SerializedName("folioIdPlanPen")
    private String folioIdPlanPen;
    @Expose
    @SerializedName("folioInfonavit")
    private String infonavitFolio;
    @Expose
    @SerializedName("folioRel")
    private String folioRel;
    @Expose
    @SerializedName("folioTramite")
    private String procedureFolio;
    @Expose
    @SerializedName("idEstTramite")
    private String procedureStatusId;
    @Expose
    @SerializedName("indExistDM")
    private String indExistDM;
    @Expose
    @SerializedName("indPagViv")
    private String indPagViv;
    @Expose
    @SerializedName("indVerifPatronal")
    private String indVerifPatronal;
    @Expose
    @SerializedName("montoTotal")
    private String totalAmount;
    @Expose
    @SerializedName("nombreAsesor")
    private String agentName;
    @Expose
    @SerializedName("numAsesor")
    private String agentNumber;
    @Expose
    @SerializedName("numCtaInvdual")
    private String individualAccountNumber;
    @Expose
    @SerializedName("numParcialidad")
    private String partialityNumber;
    @Expose
    @SerializedName("numResolucion")
    private Integer resolutionNumber;
    @Expose
    @SerializedName("numSemCot")
    private String numSemCot;
    @Expose
    @SerializedName("pantalla")
    private String procedureScreen;
    @Expose
    @SerializedName("parcialidades")
    private String parcialities;
    @Expose
    @SerializedName("porcenSaldo")
    private String balancePercentage;
    @Expose
    @SerializedName("tipoSolicitante")
    private String applicantType;
    @Expose
    @SerializedName("tipoTramite")
    private String procedureType;
    @Expose
    @SerializedName("usuAct")
    private String agentWhoUpdate;
    @Expose
    @SerializedName("usuCre")
    private String agentWhoCreated;
    @Expose
    @SerializedName("idDetSubEtapa")
    private Integer idDetSubEtapa;
    @Expose
    @SerializedName("selVol")
    private String sealOfWill;
    @Expose
    @SerializedName("fechaLiquidacion")
    private String settlementDate;
    @Expose
    @SerializedName("idEstatusProceso")
    private Integer idStatusProcess;
    @Expose
    @SerializedName("idEstatus")
    private Integer idStatus;
    @Expose
    @SerializedName("idResultado")
    private Integer idResult;
    @Expose
    @SerializedName("idSubEtapa")
    private Integer idSubStage;
    @Expose
    @SerializedName("idProceso")
    private Integer idProcess;
    @Expose
    @SerializedName("idSubproceso")
    private Integer idSubProcess;
    @Expose
    @SerializedName("idCanal")
    private Integer idCanal;

    public Procedure() {
    }

    public Procedure(String folio, String procedureFolio, String individualAccountNumber, Integer idStatus, Integer idResult, Integer idSubStage, Integer idSubProcess) {
        this.folio = folio;
        this.procedureFolio = procedureFolio;
        this.individualAccountNumber = individualAccountNumber;
        this.idStatus = idStatus;
        this.idResult = idResult;
        this.idSubStage = idSubStage;
        this.idSubProcess = idSubProcess;
    }

    public Procedure(String acuse, Integer notifiedClient, Integer contEmptyReagent, String curpApplicant, String applicantEmail, String curpSol, String codeActAuth, String insuranceCarrierCode, String statusDate, String pensionStartDate, String dateOfBirth, String pensionResDate, String beneficiaryResDate, String procedureDate, String fehAct, String creationDate, String folio, String folioIdPlanPen, String infonavitFolio, String folioRel, String procedureFolio, String procedureStatusId, String indExistDM, String indPagViv, String indVerifPatronal, String totalAmount, String agentNumber, String individualAccountNumber, String partialityNumber, Integer resolutionNumber, String numSemCot, String procedureScreen, String parcialities, String balancePercentage, String applicantType, String procedureType, String agentWhoupdate, String agentWhoCreated, Integer idDetSubEtapa, String sealOfWill, String settlementDate, Integer idStatusProcess, Integer idStatus, Integer idResult, Integer idSubStage, Integer idProcess, Integer idSubProcess, Integer idCanal) {
        this.acuse = acuse;
        this.notifiedClient = notifiedClient;
        this.contEmptyReagent = contEmptyReagent;
        this.curpApplicant = curpApplicant;
        this.applicantEmail = applicantEmail;
        this.curpSol = curpSol;
        this.codeActAuth = codeActAuth;
        this.insuranceCarrierCode = insuranceCarrierCode;
        this.statusDate = statusDate;
        this.pensionStartDate = pensionStartDate;
        this.dateOfBirth = dateOfBirth;
        this.pensionResDate = pensionResDate;
        this.beneficiaryResDate = beneficiaryResDate;
        this.procedureDate = procedureDate;
        this.fehAct = fehAct;
        this.creationDate = creationDate;
        this.folio = folio;
        this.folioIdPlanPen = folioIdPlanPen;
        this.infonavitFolio = infonavitFolio;
        this.folioRel = folioRel;
        this.procedureFolio = procedureFolio;
        this.procedureStatusId = procedureStatusId;
        this.indExistDM = indExistDM;
        this.indPagViv = indPagViv;
        this.indVerifPatronal = indVerifPatronal;
        this.totalAmount = totalAmount;
        this.agentNumber = agentNumber;
        this.individualAccountNumber = individualAccountNumber;
        this.partialityNumber = partialityNumber;
        this.resolutionNumber = resolutionNumber;
        this.numSemCot = numSemCot;
        this.procedureScreen = procedureScreen;
        this.parcialities = parcialities;
        this.balancePercentage = balancePercentage;
        this.applicantType = applicantType;
        this.procedureType = procedureType;
        this.agentWhoUpdate = agentWhoupdate;
        this.agentWhoCreated = agentWhoCreated;
        this.idDetSubEtapa = idDetSubEtapa;
        this.sealOfWill = sealOfWill;
        this.settlementDate = settlementDate;
        this.idStatusProcess = idStatusProcess;
        this.idStatus = idStatus;
        this.idResult = idResult;
        this.idSubStage = idSubStage;
        this.idProcess = idProcess;
        this.idSubProcess = idSubProcess;
        this.idCanal = idCanal;
    }


    public String getSealOfWill() {
        return sealOfWill;
    }

    public void setSealOfWill(String sealOfWill) {
        this.sealOfWill = sealOfWill;
    }

    public String getApplicantEmail() {
        return applicantEmail;
    }

    public void setApplicantEmail(String applicantEmail) {
        this.applicantEmail = applicantEmail;
    }

    public String getCurpSol() {
        return curpSol;
    }

    public void setCurpSol(String curpSol) {
        this.curpSol = curpSol;
    }

    public String getCodeActAuth() {
        return codeActAuth;
    }

    public void setCodeActAuth(String codeActAuth) {
        this.codeActAuth = codeActAuth;
    }

    public String getInsuranceCarrierCode() {
        return insuranceCarrierCode;
    }

    public void setInsuranceCarrierCode(String insuranceCarrierCode) {
        this.insuranceCarrierCode = insuranceCarrierCode;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getPensionStartDate() {
        return pensionStartDate;
    }

    public void setPensionStartDate(String pensionStartDate) {
        this.pensionStartDate = pensionStartDate;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPensionResDate() {
        return pensionResDate;
    }

    public void setPensionResDate(String pensionResDate) {
        this.pensionResDate = pensionResDate;
    }

    public String getProcedureDate() {
        return procedureDate;
    }

    public void setProcedureDate(String procedureDate) {
        this.procedureDate = procedureDate;
    }

    public String getFehAct() {
        return fehAct;
    }

    public void setFehAct(String fehAct) {
        this.fehAct = fehAct;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFolioIdPlanPen() {
        return folioIdPlanPen;
    }

    public void setFolioIdPlanPen(String folioIdPlanPen) {
        this.folioIdPlanPen = folioIdPlanPen;
    }

    public String getInfonavitFolio() {
        return infonavitFolio;
    }

    public void setInfonavitFolio(String infonavitFolio) {
        this.infonavitFolio = infonavitFolio;
    }

    public String getFolioRel() {
        return folioRel;
    }

    public void setFolioRel(String folioRel) {
        this.folioRel = folioRel;
    }

    public String getProcedureFolio() {
        return procedureFolio;
    }

    public void setProcedureFolio(String procedureFolio) {
        this.procedureFolio = procedureFolio;
    }

    public String getProcedureStatusId() {
        return procedureStatusId;
    }

    public void setProcedureStatusId(String procedureStatusId) {
        this.procedureStatusId = procedureStatusId;
    }

    public String getIndExistDM() {
        return indExistDM;
    }

    public void setIndExistDM(String indExistDM) {
        this.indExistDM = indExistDM;
    }

    public String getIndPagViv() {
        return indPagViv;
    }

    public void setIndPagViv(String indPagViv) {
        this.indPagViv = indPagViv;
    }

    public String getIndVerifPatronal() {
        return indVerifPatronal;
    }

    public void setIndVerifPatronal(String indVerifPatronal) {
        this.indVerifPatronal = indVerifPatronal;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getAgentNumber() {
        return agentNumber;
    }

    public void setAgentNumber(String agentNumber) {
        this.agentNumber = agentNumber;
    }

    public String getIndividualAccountNumber() {
        return individualAccountNumber;
    }

    public void setIndividualAccountNumber(String individualAccountNumber) {
        this.individualAccountNumber = individualAccountNumber;
    }

    public String getPartialityNumber() {
        return partialityNumber;
    }

    public void setPartialityNumber(String partialityNumber) {
        this.partialityNumber = partialityNumber;
    }

    public String getNumSemCot() {
        return numSemCot;
    }

    public void setNumSemCot(String numSemCot) {
        this.numSemCot = numSemCot;
    }

    public String getProcedureScreen() {
        return procedureScreen;
    }

    public void setProcedureScreen(String procedureScreen) {
        this.procedureScreen = procedureScreen;
    }

    public String getParcialities() {
        return parcialities;
    }

    public void setParcialities(String parcialities) {
        this.parcialities = parcialities;
    }

    public String getBalancePercentage() {
        return balancePercentage;
    }

    public void setBalancePercentage(String balancePercentage) {
        this.balancePercentage = balancePercentage;
    }

    public String getApplicantType() {
        return applicantType;
    }

    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }

    public String getProcedureType() {
        return procedureType;
    }

    public void setProcedureType(String procedureType) {
        this.procedureType = procedureType;
    }

    public String getAgentWhoUpdate() {
        return agentWhoUpdate;
    }

    public void setAgentWhoUpdate(String agentWhoUpdate) {
        this.agentWhoUpdate = agentWhoUpdate;
    }

    public String getAgentWhoCreated() {
        return agentWhoCreated;
    }

    public void setAgentWhoCreated(String agentWhoCreated) {
        this.agentWhoCreated = agentWhoCreated;
    }

    public String getAcuse() {
        return acuse;
    }

    public void setAcuse(String acuse) {
        this.acuse = acuse;
    }

    public Integer getNotifiedClient() {
        return notifiedClient;
    }

    public void setNotifiedClient(Integer notifiedClient) {
        this.notifiedClient = notifiedClient;
    }

    public Integer getContEmptyReagent() {
        return contEmptyReagent;
    }

    public void setContEmptyReagent(Integer contEmptyReagent) {
        this.contEmptyReagent = contEmptyReagent;
    }

    public String getCurpApplicant() {
        return curpApplicant;
    }

    public void setCurpApplicant(String curpApplicant) {
        this.curpApplicant = curpApplicant;
    }

    public String getBeneficiaryResDate() {
        return beneficiaryResDate;
    }

    public void setBeneficiaryResDate(String beneficiaryResDate) {
        this.beneficiaryResDate = beneficiaryResDate;
    }

    public Integer getResolutionNumber() {
        return resolutionNumber;
    }

    public void setResolutionNumber(Integer resolutionNumber) {
        this.resolutionNumber = resolutionNumber;
    }

    public String getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public Integer getIdStatusProcess() {
        return idStatusProcess;
    }

    public void setIdStatusProcess(Integer idStatusProcess) {
        this.idStatusProcess = idStatusProcess;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Integer getIdResult() {
        return idResult;
    }

    public void setIdResult(Integer idResult) {
        this.idResult = idResult;
    }

    public Integer getIdSubStage() {
        return idSubStage;
    }

    public void setIdSubStage(Integer idSubStage) {
        this.idSubStage = idSubStage;
    }

    public Integer getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(Integer idProcess) {
        this.idProcess = idProcess;
    }

    public Integer getIdSubProcess() {
        return idSubProcess;
    }

    public void setIdSubProcess(Integer idSubProcess) {
        this.idSubProcess = idSubProcess;
    }

    public Integer getIdCanal() {
        return idCanal;
    }

    public void setIdCanal(Integer idCanal) {
        this.idCanal = idCanal;
    }

    public Integer getIdDetSubEtapa() {
        return idDetSubEtapa;
    }

    public void setIdDetSubEtapa(Integer idDetSubEtapa) {
        this.idDetSubEtapa = idDetSubEtapa;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }
}

