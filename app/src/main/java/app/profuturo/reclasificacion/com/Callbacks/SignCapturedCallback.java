package app.profuturo.reclasificacion.com.Callbacks;

import android.graphics.Bitmap;

public interface SignCapturedCallback {
    void OnGetSign(Bitmap bitmap);
}
