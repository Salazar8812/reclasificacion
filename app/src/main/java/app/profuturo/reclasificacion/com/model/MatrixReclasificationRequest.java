package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseRequestInterface;

public class MatrixReclasificationRequest implements WSBaseRequestInterface {

    @SerializedName("subcuentaCompuesta")
    String SubAccountGroup;

    @SerializedName("Indicadores")
    List<DatesSubAccounts> listIndicatorMat = new ArrayList<>();

    public MatrixReclasificationRequest(String subAccountGroup, List<DatesSubAccounts> listIndicatorMat) {
        SubAccountGroup = subAccountGroup;
        this.listIndicatorMat = listIndicatorMat;
    }
}
