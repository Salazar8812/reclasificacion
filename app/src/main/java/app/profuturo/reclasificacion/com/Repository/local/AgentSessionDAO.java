package app.profuturo.reclasificacion.com.Repository.local;

import android.support.annotation.NonNull;

import app.profuturo.reclasificacion.com.base.BaseDao;
import app.profuturo.reclasificacion.com.model.AgentSession;
import io.realm.Realm;
import io.realm.RealmResults;

public class AgentSessionDAO extends BaseDao {
    /**
     * Inserts an {@link AgentSession} object into Realm.
     * <p/>
     * Checks if no other session objects exist, if they do
     * Realm proceeds to delete all of them and then insert the new one.
     * <p/>
     *
     * @param agentSession The session to be stored.
     */
    public static void createAgentSession(final AgentSession agentSession) {
        performTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                // Retrieve all the records from the sessions table.
                RealmResults<AgentSession> sessions = realm.where(AgentSession.class).findAll();
                // Check if the list is not empty.
                if (!sessions.isEmpty()) {
                    // If the list is not empty, delete the records from realm.
                    sessions.deleteAllFromRealm();
                }
                // Then, proceed to insert the agent session object.
                realm.insertOrUpdate(agentSession);
            }
        });
    }

    /**
     * Retrieves the first AgentSession object from Realm.
     *
     * @return The AgentSession object from realm.
     */
    public static AgentSession readAgentSession() {
        // Create a new AgentSessionTransaction object.
        AgentSessionTransaction transaction = new AgentSessionTransaction();
        // Execute the transaction.
        performTransaction(transaction);
        // Then, proceed to retrieve the transaction.
        return transaction.retrievedSession;
    }

    /**
     * Delete all the records from the AgentSessions table.
     */
    public static void deleteAgentSession() {
        performTransaction(realm -> {
            // Retrieve the records from the sessions table.
            RealmResults<AgentSession> sessions = realm.where(AgentSession.class).findAll();
            // Delete all those records from realm.
            sessions.deleteAllFromRealm();
        });
    }

    /**
     * This AgentSessionTransaction class holds a reference of
     * a retrieved AgentSession object from the local database.
     */
    static class AgentSessionTransaction implements Realm.Transaction {
        /**
         * Reference to the AgentSession object copied from Realm.
         */
        private AgentSession retrievedSession;

        /**
         * Execute the transaction.
         *
         * @param realm Realm instance executing the transaction.
         */
        @Override
        public void execute(@NonNull Realm realm) {
            // Get the agent session from Realm.
            AgentSession session = realm.where(AgentSession.class).findFirst();
            // If the session is not null, copy the realm object to this transaction session.
            if (session != null) {
                // Copy the value from Realm.
                retrievedSession = realm.copyFromRealm(session);
            }
        }
    }
}
