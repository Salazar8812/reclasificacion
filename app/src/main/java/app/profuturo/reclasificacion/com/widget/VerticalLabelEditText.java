package app.profuturo.reclasificacion.com.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.constraint.ConstraintLayout;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import app.profuturo.reclasificacion.com.R;

public class VerticalLabelEditText extends ConstraintLayout {
    TextView mHintView;
    EditText mFieldView;

    /**
     * Creates this view, attaches its layout and bind its views.
     *
     * @param context Context for this view.
     * @param attrs   Attributes from the XML.
     */
    public VerticalLabelEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray attributes = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.VerticalLabelEditText,
                0,
                0
        );
        try {
            // Inflate the layout for this view.
            View root = inflate(context, R.layout.view_labeled_edittext, this);
            mHintView = root.findViewById(R.id.hint);
            mFieldView = root.findViewById(R.id.field);
            // Bind this view.
            mFieldView.setHint(attributes.getString(R.styleable.VerticalLabelEditText_android_hint));
            // Set the text for the hint view.
            mHintView.setText(mFieldView.getHint());
            // Set the EditText IME option.
            mFieldView.setImeOptions(attributes.getInt(
                    R.styleable.VerticalLabelEditText_android_imeOptions,
                    EditorInfo.IME_ACTION_NEXT)
            );
            // Set the EditText input type.
            mFieldView.setInputType(attributes.getInteger(
                    R.styleable.VerticalLabelEditText_android_inputType,
                    EditorInfo.TYPE_TEXT_VARIATION_NORMAL)
            );
            // Set the EditText input type.
            mFieldView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(
                    attributes.getInt(R.styleable.VerticalLabelEditText_android_maxLength,
                            99))});
        } finally {
            attributes.recycle();
        }
    }

    /**
     * Changes the hint value for this view.
     *
     * @param hint The new hint value.
     */
    public void setHint(@NonNull String hint) {
        mHintView.setText(hint);
        mFieldView.setHint(hint);
    }

    /**
     * Changes the hint value for this view.
     *
     * @param hint The new hint value.
     */
    public void setHint(@NonNull @StringRes Integer hint) {
        setHint(getResources().getString(hint));
    }

    /**
     * Retrieve the value of this view editable field.
     *
     * @return The value of the EditText widget.
     */
    public CharSequence getValue() {
        return mFieldView.getText();
    }

    /**
     * Method to set the text to the value of this custom view
     *
     * @param charSequence The text to set in the field
     */
    public void setText(CharSequence charSequence) {
        // Set the text to the field
        mFieldView.setText(charSequence);
    }

}

