package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;

public class IndicatorActionValueResponse implements WSBaseResponseInterface {
    @SerializedName("valorAccion")
    public List<ActionValue> listActionValue;
    @SerializedName("status")
    public String status;
}
