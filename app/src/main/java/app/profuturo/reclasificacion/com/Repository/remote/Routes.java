package app.profuturo.reclasificacion.com.Repository.remote;

import android.support.annotation.AnyRes;
import android.support.annotation.StringDef;
import android.support.v4.media.AudioAttributesCompat;

import java.util.List;
import java.util.Map;

import app.profuturo.reclasificacion.com.BuildConfig;
import app.profuturo.reclasificacion.com.model.AgentData;
import app.profuturo.reclasificacion.com.model.AgentNumber;
import app.profuturo.reclasificacion.com.model.AgentUser;
import app.profuturo.reclasificacion.com.model.AssignedBranch;
import app.profuturo.reclasificacion.com.model.BranchResponse;
import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.model.ClientAccountNumber;
import app.profuturo.reclasificacion.com.model.ClientPicture;
import app.profuturo.reclasificacion.com.model.InformativeMessage;
import app.profuturo.reclasificacion.com.model.Login;
import app.profuturo.reclasificacion.com.model.LoginResponse;
import app.profuturo.reclasificacion.com.model.ProfuturoRequest;
import app.profuturo.reclasificacion.com.model.SaleResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.QueryName;

/**
 * Interface that will contain the API definitions for Retrofit to use.
 *
 * @author Alfredo Bejarano
 * @since August 3rd, 2018 - 11:55 AM
 */
public interface Routes {
    /**
     * Simple API definition for testing a GET verb call.
     *
     * @return Call object.
     */
    @GET("/")
    Call<Void> testAPIDefinition();

    @GET("iib/retiros/utilerias/v1/obtenerMensajeInformativo")
    Call<InformativeMessage> getMessage(@Query("codigoMensaje") String code);

    /**
     * API definition that retrieves a list of branches by a given branch ID.
     *
     * @param branchId  branch id assigned to the agent
     * @param latitude  latitude of agent location
     * @param longitude longitude of agent location
     * @return list of branches wrapped inside a Call object.
     */
    @GET("iib/retiros/utilerias/v1/buscarSucursales")
    Call<BranchResponse> getBranches(@Query("idSucursal") String branchId,
                                     @Query("latitud") double latitude,
                                     @Query("longitud") double longitude);

    /**
     * API definition that retrieves a list of clients by a given criteria (NSS, CURP or Agent Account Number).
     *
     * @param clientId Query map that defines the query params for the request.
     * @return List of client objects wrapped in a {@link Call Call} object.
     * @see List
     * @see Client
     **/
    @GET("iib/nci/idc/v1/clientes/")
    Call<Client> getClients(@QueryMap Map<String, String> clientId);

    /**
     * API definition that reports the data of an
     * agent that has made login within the app.
     *
     * @param agentLogin Agent data for the login.
     * @return Details about the login transaction wrapped in a {@link Call} object.
     */
    @POST("iib/retiros/accesoRetiros/v1/guardarLogin")
    Call<LoginResponse> createLogin(@Body Login agentLogin);

    /**
     * API call to update login information with folio number and login id (required )
     *
     * @return Response wrapped inside a Call object
     */

    @POST("iib/retiros/accesoRetiros/v1/actualizaLogin")
    Call<LoginResponse> updateLogin(@Body Login login);

    /**
     * API definition that will retrieve the agent information.
     *
     * @param userAgent Class containing the agent number for this request.
     * @return Call instance containing the agent data
     */
    @POST("/mb/cusp/rest/consultaInformacionUsuario")
    Call<AgentData> getAgentInformation(@Body ProfuturoRequest<AgentUser> userAgent);

    /**
     * API definition that fetches missing details
     * about the assigned branch for a given agent.
     *
     * @param agentNumber Class containing the agent number for this request.
     * @return Branch details wrapped inside a Call object.
     */
    @POST("/mb/appMovil/rest/cuo/consultaDatosSucursal")
    Call<AssignedBranch> getAgentBranchInformation(@Body ProfuturoRequest<AgentNumber> agentNumber);

    @POST("/iib/identica/cliente/service/consultarImagencte/v1/obtenerImagenCte")
    Call<ClientPicture> retrieveClientPicture(@Body ClientAccountNumber clientAccountNumber);

    @GET("iib/retiros/reglasRetiros/v1/consultarDocumentoTramite")
    Call<ResponseBody> getDocuments(@Query("clasificacionRetiro") String clasificacionRetiro,
                                    @Query("tipoRetiro") String tipoRetiro,
                                    @Query("numeroBeneficiarios") String numeroBeneficiario);
}

