package app.profuturo.reclasificacion.com.SoapWS;

import android.util.Log;
import android.util.Xml;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.xmlpull.v1.XmlPullParser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import app.profuturo.reclasificacion.com.model.DatesSubAccounts;
import app.profuturo.reclasificacion.com.model.SubAccountsMatrixReclasification;

public class SoapsWebService {

    public interface SubAccountIndicatorCallback{
        void OnGetSubAccountIndicators(List<SubAccountsMatrixReclasification> list);
    }

    private List<SubAccountsMatrixReclasification> mListSubAccountIndicator = new ArrayList<>();
    private static final String ACTION = "datosCliente";
    private static final String URL =  "http://172.16.58.24:9080/DecisionService/ws/matrizReclasificacionRuleApp/matrizReclasificacionRules?WSDL";
    private String mSubAccountGroup;
    private List<DatesSubAccounts> listDates;
    private SubAccountIndicatorCallback mSubAccountIndicatorCallback;

    public SoapsWebService(String mSubAccountGroup, List<DatesSubAccounts> listDates, SubAccountIndicatorCallback mSubAccountIndicatorCallback) {
        this.mSubAccountGroup = mSubAccountGroup;
        this.listDates = listDates;
        this.mSubAccountIndicatorCallback = mSubAccountIndicatorCallback;
    }

    public String createEnvelope(){
        String cad = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mat=\"http://www.ibm.com/rules/decisionservice/MatrizReclasificacionRuleApp/MatrizReclasificacionRules\" xmlns:par=\"http://www.ibm.com/rules/decisionservice/MatrizReclasificacionRuleApp/MatrizReclasificacionRules/param\">" +
                "<soapenv:Header/>" +
                "<soapenv:Body>" +
                "<mat:MatrizReclasificacionRulesRequest>" +
                "<mat:DecisionID>?</mat:DecisionID>" +
                "<par:datosCliente>" +
                "<datosCliente>" +
                "<!--Zero or more repetitions:-->" +
                generateFields(listDates)+
                "<subcuentaCompuesta>742</subcuentaCompuesta>"+
                "</datosCliente>"+
                "</par:datosCliente>"+
                "</mat:MatrizReclasificacionRulesRequest>"+
                "</soapenv:Body>" +
                "</soapenv:Envelope>";
        return cad;
    }

    public String generateFields(List<DatesSubAccounts> list){
        String mXml = "";
        for (DatesSubAccounts item: list
             ) {
            mXml = mXml +"<fechasIndicadores>" +
                    "<!--Optional:-->" +
                    "<fecha1erApor>"+ item.firstInputDate +"</fecha1erApor>" +
                    "<!--Optional:-->" +
                    "<fechaUltRetiroVol>"+item.lastDate+"</fechaUltRetiroVol>" +
                    "<!--Optional:-->" +
                    "<idSubcuenta>"+item.idSubAccount+"</idSubcuenta>" +
                    "</fechasIndicadores>";
        }

        return mXml;
    }


    public String sendMessage(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                String body =createEnvelope();

                final DefaultHttpClient httpClient = new DefaultHttpClient();

                HttpParams params = httpClient.getParams();
                HttpConnectionParams.setConnectionTimeout(params, 10000);
                HttpConnectionParams.setSoTimeout(params, 15000);

                HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

                HttpPost httppost = new HttpPost(URL);

                httppost.setHeader("soapaction", ACTION);
                httppost.setHeader("Authorization","Basic bWF4aW1vLmpyYzptYXhpbW8yMDE1");
                httppost.setHeader("Content-Type", "application/soap+xml;charset=UTF-8;action=urn:processDocument");

                try {

                    HttpEntity entity = new StringEntity(body);
                    httppost.setEntity(entity);
                    String responseString = "";
                    ResponseHandler<String> rh = new ResponseHandler<String>() {
                        public String handleResponse(HttpResponse response)
                                throws ClientProtocolException, IOException {

                            HttpEntity entity = response.getEntity();


                            StringBuffer out = new StringBuffer();
                            byte[] b = EntityUtils.toByteArray(entity);

                            out.append(new String(b, 0, b.length));

                            SoapObject soapObject = string2SoapObject(b);

                            getProperties(soapObject);

                            if(!out.toString().equals("")){
                               Log.e("Success", "Datos Enviados");
                            }else{
                                Log.e("Failed", "No se enviaron los datos");
                            }
                            return getWSDL();
                        }
                    };

                    responseString = httpClient.execute(httppost, rh);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("me","Exc : "+ e.toString());

                }

                httpClient.getConnectionManager().shutdown();

            }
        }).start();

        return "";
    }

    public SoapObject string2SoapObject(byte[] bytes)
    {
        SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
        SoapObject soap=null;
        try {
            ByteArrayInputStream inputStream=new ByteArrayInputStream(bytes);
            XmlPullParser p= Xml.newPullParser();
            p.setInput(inputStream, "UTF8");
            envelope.parse(p);
            soap=(SoapObject)envelope.bodyIn;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return soap;
    }

    public void getProperties(SoapObject mObject){
        /*SoapObject getChildSubAccount = (SoapObject) mObject.getProperty(1);
        SoapObject getListChilds = (SoapObject) getChildSubAccount.getProperty(0);
        List<String> listApovolSubAccount = new ArrayList<>();

        for(int i = 0; i < getListChilds.getPropertyCount();i++){
            SubAccountsMatrixReclasification mSubAccountindicator = new SubAccountsMatrixReclasification();
            try{
                SoapObject getSiefore = (SoapObject) getListChilds.getProperty(i);
                try {
                   listApovolSubAccount = mapListSubAccountApovol(getSiefore);
                }catch (Exception e){

                }
                mSubAccountindicator = new SubAccountsMatrixReclasification(getSiefore.getProperty("aplica").toString(),
                        getSiefore.getProperty("tipoSubcuenta").toString(),
                        "$",
                        getSiefore.getProperty("siefore").toString(),
                        getSiefore.getProperty("descripcion").toString(),
                        listApovolSubAccount);
            }catch (RuntimeException e){
                e.printStackTrace();
            }
            mListSubAccountIndicator.add(mSubAccountindicator);
        }
        mSubAccountIndicatorCallback.OnGetSubAccountIndicators(mListSubAccountIndicator);*/
    }

    public List<String> mapListSubAccountApovol(SoapObject getSiefore){
        SoapObject getListSubAccountApovol = (SoapObject) getSiefore.getProperty("listasubcuentasApovol");
        List<String> list = new ArrayList<>();
        for(int i = 0; i < getListSubAccountApovol.getPropertyCount() ; i++){
            list.add(getListSubAccountApovol.getProperty(0).toString());
        }
        return list;
    }

    public String getWSDL(){
        return "<wsdl:definitions xmlns:dstns=\"http://www.ibm.com/rules/decisionservice/MatrizReclasificacionRuleApp/MatrizReclasificacionRules\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\"xmlns:wsdl=\"http://schemas.xmlsoap.org/wsdl/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" name=\"MatrizReclasificacionRulesDecisionService\"targetNamespace=\"http://www.ibm.com/rules/decisionservice/MatrizReclasificacionRuleApp/MatrizReclasificacionRules\">\n" +
                "<wsdl:types>\n" +
                "<xsd:schema xmlns:xom1=\"http://www.ibm.com/rules/decisionservice/MatrizReclasificacionRuleApp/MatrizReclasificacionRules\"targetNamespace=\"http://www.ibm.com/rules/decisionservice/MatrizReclasificacionRuleApp/MatrizReclasificacionRules/param\">\n" +
                "<xsd:import namespace=\"http://www.ibm.com/rules/decisionservice/MatrizReclasificacionRuleApp/MatrizReclasificacionRules\"/>\n" +
                "<xsd:element name=\"datosCliente\">\n" +
                "<xsd:complexType>\n" +
                "<xsd:sequence>\n" +
                "<xsd:element name=\"datosCliente\" type=\"xom1:datosCliente\"/>\n" +
                "</xsd:sequence>\n" +
                "</xsd:complexType>\n" +
                "</xsd:element>\n" +
                "<xsd:element name=\"subcuentasReclasificacion\">\n" +
                "<xsd:complexType>\n" +
                "<xsd:sequence>\n" +
                "<xsd:element name=\"subcuentasReclasificacion\" type=\"xom1:fondosReclasificacion\"/>\n" +
                "</xsd:sequence>\n" +
                "</xsd:complexType>\n" +
                "</xsd:element>\n" +
                "</xsd:schema>\n" +
                "<xs:schema xmlns:param=\"http://www.ibm.com/rules/decisionservice/MatrizReclasificacionRuleApp/MatrizReclasificacionRules/param\"xmlns:tns=\"http://www.ibm.com/rules/decisionservice/MatrizReclasificacionRuleApp/MatrizReclasificacionRules\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" targetNamespace=\"http://www.ibm.com/rules/decisionservice/MatrizReclasificacionRuleApp/MatrizReclasificacionRules\"version=\"1.0\">\n" +
                "<xsd:import namespace=\"http://www.ibm.com/rules/decisionservice/MatrizReclasificacionRuleApp/MatrizReclasificacionRules/param\"/>\n" +
                "<xs:complexType name=\"datosCliente\">\n" +
                "<xs:sequence>\n" +
                "<xs:element maxOccurs=\"unbounded\" minOccurs=\"0\" name=\"fechasIndicadores\" nillable=\"true\" type=\"tns:indicadoresFechasSubcuentas\"/>\n" +
                "<xs:element minOccurs=\"0\" name=\"subcuentaCompuesta\" type=\"xs:string\"/>\n" +
                "</xs:sequence>\n" +
                "</xs:complexType>\n" +
                "<xs:complexType name=\"indicadoresFechasSubcuentas\">\n" +
                "<xs:sequence>\n" +
                "<xs:element minOccurs=\"0\" name=\"fecha1erApor\" type=\"xs:dateTime\"/>\n" +
                "<xs:element minOccurs=\"0\" name=\"fechaUltRetiroVol\" type=\"xs:dateTime\"/>\n" +
                "<xs:element minOccurs=\"0\" name=\"idSubcuenta\" type=\"xs:string\"/>\n" +
                "</xs:sequence>\n" +
                "</xs:complexType>\n" +
                "<xs:complexType name=\"fondosReclasificacion\">\n" +
                "<xs:sequence>\n" +
                "<xs:element maxOccurs=\"unbounded\" minOccurs=\"0\" name=\"listaSubcuentas\" nillable=\"true\" type=\"tns:fondosSiefore\"/>\n" +
                "</xs:sequence>\n" +
                "</xs:complexType>\n" +
                "<xs:complexType name=\"fondosSiefore\">\n" +
                "<xs:sequence>\n" +
                "<xs:element name=\"aplica\" type=\"xs:boolean\"/>\n" +
                "<xs:element minOccurs=\"0\" name=\"descripcion\" type=\"xs:string\"/>\n" +
                "<xs:element maxOccurs=\"unbounded\" minOccurs=\"0\" name=\"listasubcuentasApovol\" nillable=\"true\" type=\"tns:subcuentas\"/>\n" +
                "<xs:element minOccurs=\"0\" name=\"mensaje\" type=\"xs:string\"/>\n" +
                "<xs:element minOccurs=\"0\" name=\"siefore\" type=\"xs:string\"/>\n" +
                "<xs:element minOccurs=\"0\" name=\"tipoSubcuenta\" type=\"xs:string\"/>\n" +
                "</xs:sequence>\n" +
                "</xs:complexType>\n" +
                "<xs:complexType name=\"subcuentas\">\n" +
                "<xs:sequence>\n" +
                "<xs:element minOccurs=\"0\" name=\"subcuentaApovol\" type=\"xs:string\"/>\n" +
                "</xs:sequence>\n" +
                "</xs:complexType>\n" +
                "<xsd:element name=\"MatrizReclasificacionRulesRequest\">\n" +
                "<xsd:complexType>\n" +
                "<xsd:sequence>\n" +
                "<xsd:element form=\"qualified\" maxOccurs=\"1\" minOccurs=\"0\" name=\"DecisionID\" type=\"xsd:string\"/>\n" +
                "<xsd:element ref=\"param:datosCliente\"/>\n" +
                "</xsd:sequence>\n" +
                "</xsd:complexType>\n" +
                "</xsd:element>\n" +
                "<xsd:element name=\"MatrizReclasificacionRulesResponse\">\n" +
                "<xsd:complexType>\n" +
                "<xsd:sequence>\n" +
                "<xsd:element form=\"qualified\" name=\"DecisionID\" type=\"xsd:string\"/>\n" +
                "<xsd:element ref=\"param:subcuentasReclasificacion\"/>\n" +
                "</xsd:sequence>\n" +
                "</xsd:complexType>\n" +
                "</xsd:element>\n" +
                "<xsd:element name=\"MatrizReclasificacionRulesException\">\n" +
                "<xsd:complexType>\n" +
                "<xsd:sequence>\n" +
                "<xsd:element maxOccurs=\"1\" minOccurs=\"1\" name=\"exception\" nillable=\"false\" type=\"xsd:string\"/>\n" +
                "</xsd:sequence>\n" +
                "</xsd:complexType>\n" +
                "</xsd:element>\n" +
                "</xs:schema>\n" +
                "</wsdl:types>\n" +
                "<wsdl:message name=\"MatrizReclasificacionRulesRequest\">\n" +
                "<wsdl:part element=\"dstns:MatrizReclasificacionRulesRequest\" name=\"MatrizReclasificacionRulesRequest\"> </wsdl:part>\n" +
                "</wsdl:message>\n" +
                "<wsdl:message name=\"MatrizReclasificacionRulesSoapFault\">\n" +
                "<wsdl:part element=\"dstns:MatrizReclasificacionRulesException\" name=\"fault\"> </wsdl:part>\n" +
                "</wsdl:message>\n" +
                "<wsdl:message name=\"MatrizReclasificacionRulesResponse\">\n" +
                "<wsdl:part element=\"dstns:MatrizReclasificacionRulesResponse\" name=\"MatrizReclasificacionRulesResponse\"> </wsdl:part>\n" +
                "</wsdl:message>\n" +
                "<wsdl:portType name=\"MatrizReclasificacionRulesDecisionService\">\n" +
                "<wsdl:operation name=\"MatrizReclasificacionRules\">\n" +
                "<wsdl:input message=\"dstns:MatrizReclasificacionRulesRequest\" name=\"MatrizReclasificacionRulesRequest\"> </wsdl:input>\n" +
                "<wsdl:output message=\"dstns:MatrizReclasificacionRulesResponse\" name=\"MatrizReclasificacionRulesResponse\"> </wsdl:output>\n" +
                "<wsdl:fault message=\"dstns:MatrizReclasificacionRulesSoapFault\" name=\"MatrizReclasificacionRulesSoapFault\"> </wsdl:fault>\n" +
                "</wsdl:operation>\n" +
                "</wsdl:portType>\n" +
                "<wsdl:binding name=\"MatrizReclasificacionRuleAppMatrizReclasificacionRulesBinding\" type=\"dstns:MatrizReclasificacionRulesDecisionService\">\n" +
                "<soap:binding style=\"document\" transport=\"http://schemas.xmlsoap.org/soap/http\"/>\n" +
                "<wsdl:operation name=\"MatrizReclasificacionRules\">\n" +
                "<soap:operation soapAction=\"MatrizReclasificacionRules\" style=\"document\"/>\n" +
                "<wsdl:input name=\"MatrizReclasificacionRulesRequest\">\n" +
                "<soap:body use=\"literal\"/>\n" +
                "</wsdl:input>\n" +
                "<wsdl:output name=\"MatrizReclasificacionRulesResponse\">\n" +
                "<soap:body use=\"literal\"/>\n" +
                "</wsdl:output>\n" +
                "<wsdl:fault name=\"MatrizReclasificacionRulesSoapFault\">\n" +
                "<soap:fault name=\"MatrizReclasificacionRulesSoapFault\" use=\"literal\"/>\n" +
                "</wsdl:fault>\n" +
                "</wsdl:operation>\n" +
                "</wsdl:binding>\n" +
                "<wsdl:service name=\"MatrizReclasificacionRulesDecisionService\">\n" +
                "<wsdl:port binding=\"dstns:MatrizReclasificacionRuleAppMatrizReclasificacionRulesBinding\" name=\"MatrizReclasificacionRuleAppMatrizReclasificacionRulesPort\">\n" +
                "<soap:address location=\"http://172.16.58.24:9080/DecisionService/ws/matrizReclasificacionRuleApp/matrizReclasificacionRules/v75\"/>\n" +
                "</wsdl:port>\n" +
                "</wsdl:service>\n" +
                "</wsdl:definitions>\n";
    }
}


