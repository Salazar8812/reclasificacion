package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.SerializedName;

public class SubAccountApovol {
    @SerializedName("subcuentaApovol")
    public String subCuentaApovol;
}
