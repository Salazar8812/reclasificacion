package app.profuturo.reclasificacion.com.base;

import io.realm.Realm;

public class BaseDao {
    /**
     * Executes a Realm transaction preventing Realm instances leakage.
     *
     * @param transaction The transaction to be performed.
     */
    protected static void performTransaction(final Realm.Transaction transaction) {
        // Hold a reference to the Realm default instance.
        Realm realm = null;
        try {
            //Get the default realm instance.
            realm = Realm.getDefaultInstance();
            // Execute the transaction.
            realm.executeTransaction(transaction);
        } finally {
            // Close the realm instance if it is not null.
            if (realm != null) {
                realm.close();
            }
        }
    }
}
