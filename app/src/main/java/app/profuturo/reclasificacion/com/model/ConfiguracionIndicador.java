
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfiguracionIndicador {

    @SerializedName("configuracion")
    @Expose
    public Configuracion configuracion;
    @SerializedName("fechaRegistro")
    @Expose
    public String fechaRegistro;
    @SerializedName("idIndicadorXCuentaIndividual")
    @Expose
    public String idIndicadorXCuentaIndividual;
    @SerializedName("valorIndicador")
    @Expose
    public String valorIndicador;

}
