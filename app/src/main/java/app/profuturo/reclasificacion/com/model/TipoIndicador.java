
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TipoIndicador {

    @SerializedName("descripcion")
    @Expose
    public String descripcion;
    @SerializedName("idTipoDatoIndicador")
    @Expose
    public String idTipoDatoIndicador;

}
