
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Configuracion {

    @SerializedName("fechaCreacion")
    @Expose
    public String fechaCreacion;
    @SerializedName("usuarioCreacion")
    @Expose
    public String usuarioCreacion;
    @SerializedName("categoria")
    @Expose
    public String categoria;
    @SerializedName("descripcion")
    @Expose
    public String descripcion;
    @SerializedName("estatus")
    @Expose
    public Estatus estatus;
    @SerializedName("idConfiguracionIndicador")
    @Expose
    public String idConfiguracionIndicador;
    @SerializedName("indicador")
    @Expose
    public Indicador indicador;
    @SerializedName("regla")
    @Expose
    public String regla;
    @SerializedName("tipoIndicador")
    @Expose
    public TipoIndicador tipoIndicador;
    @SerializedName("vigencia")
    @Expose
    public String vigencia;

}
