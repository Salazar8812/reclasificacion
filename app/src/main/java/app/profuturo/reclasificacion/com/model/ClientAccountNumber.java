package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Model class that defines the JSON structure
 * for a request involving a client account number.
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 06/09/2018 - 10:51 AM
 */
public class ClientAccountNumber {
    @Expose
    @SerializedName("numCuenta")
    private String value;

    public ClientAccountNumber(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
