package app.profuturo.reclasificacion.com.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import app.profuturo.reclasificacion.com.R;

public class ProcedureHeader  extends ConstraintLayout {
    TextView mTitleView;

    /**
     * {@inheritDoc}
     */
    public ProcedureHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        // Attach the layout for this view.
        View root = inflate(context, R.layout.view_procedure_header, this);
        // Bind the widgets for this view.
        mTitleView = root.findViewById(R.id.header_title);
        // Retrieve the custom attributes from the XML file.
        TypedArray attributes = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ProcedureHeader,
                0,
                0
        );
        try {
            // Assign the title property from the attributes.
            mTitleView.setText(attributes.getString(R.styleable.ProcedureHeader_title));
        } finally {
            // At the end, recycle the attributes.
            attributes.recycle();
        }
    }

    /**
     * Changes the text in the title view.
     *
     * @param title The new title value for this view.
     */
    public void setTitle(@NonNull String title) {
        mTitleView.setText(title);
    }

    /**
     * Changes the text in the title view.
     *
     * @param title The new title string ID for this view.
     */
    public void setTitle(@NonNull @StringRes Integer title) {
        setTitle(getResources().getString(title));
    }
}
