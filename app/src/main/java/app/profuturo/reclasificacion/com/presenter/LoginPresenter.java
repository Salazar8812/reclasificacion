package app.profuturo.reclasificacion.com.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import app.profuturo.reclasificacion.com.BuildConfig;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.Repository.local.AgentSessionDAO;
import app.profuturo.reclasificacion.com.Utils.StringsKeys;
import app.profuturo.reclasificacion.com.base.View.BaseView;
import app.profuturo.reclasificacion.com.base.utils.JsonMocker;
import app.profuturo.reclasificacion.com.base.utils.MessagesCodes;
import app.profuturo.reclasificacion.com.model.AgentSession;
import app.profuturo.reclasificacion.com.model.InformativeMessage;
import app.profuturo.reclasificacion.com.model.Login;
import app.profuturo.reclasificacion.com.model.LoginResponse;
import app.profuturo.reclasificacion.com.model.Procedure;
import retrofit2.Call;

public class LoginPresenter  extends NetworkPresenter {
    /**
     * Constructor for this presenter class that assigns a View.
     * This view can be null, this is useful in Unit Test cases.
     *
     * @param view The view for this presenter.
     */
    public LoginPresenter(@NonNull BaseView view) {
        super(view);
    }

    /**
     * Performs a request to report a new access for this agent.
     *
     * @param agentSession The session to be persisted.
     */
    public void createLogin(@NonNull final AgentSession agentSession) {
        runOnWorkerThread(() -> {
            // Build the request body with the necessary agent information.
            Login requestBody = buildLoginBodyRequest(agentSession, null);
            // Build the request object using the reportLogin API definition.
            final Call<LoginResponse> request = routes.createLogin(requestBody);
            // Enqueue the request using the Retrofit client.
            enqueueRequest(request, payload -> {
                /*
                 * Check if the LoginResponse is successful, it can be unsuccessful
                 * even when the response has been completed successfully.
                 */
                if (payload != null && payload.isSuccessful() && agentSession.isSessionValid()) {
                    // Save login id for the just created login and save in the agent session
                    agentSession.setIdLogin(payload.getResponse());
                    // If the login succeeded, store the agent session within Realm.
                    AgentSessionDAO.createAgentSession(agentSession);
                    /*
                     * Now, report to the status property that the process has been finished.
                     * The postValue() method is being used as this code is still being executed
                     * on a worker thread.
                     */
                    setStatus(Status.STATUS_DONE);
                } else {
                    // Request the message for the error.
                    requestInformativeMessage();
                }
            });
        });
    }

    /**
     * Performs a request to report a new access for this agent.
     */
    public void updateLogin(final AgentSession agentSession, final Procedure procedure) {
        runOnWorkerThread(() -> {
            // Build the request body with the necessary agent information. Adds folio and login
            Login requestBody = buildLoginBodyRequest(agentSession, procedure.getFolio());
            // Reports presenter is busy
            setStatus(Status.STATUS_BUSY);
            // Build the request object using the update API definition.
            final Call<LoginResponse> request = routes.updateLogin(requestBody);
            // Enqueue the request using the Retrofit client.
            enqueueRequest(request, payload -> {
                // TODO - notify to the view to which screen it needs to navigate.
                // Reports the presenter is done with this processing
                setStatus(Status.STATUS_DONE);
            });
        });
    }

    /**
     * Creates a {@link Login login} object containing the necessary parameters
     * to perform an agent login data update.
     *
     * @param agentSession   The object representing the current agent session.
     * @param procedureFolio Folio of the current procedure that the agent is performing.
     * @return Login object containing the data for performing the read request.
     * @see AgentSession
     * @see Login
     */
    private Login buildLoginBodyRequest(@Nullable AgentSession agentSession, @Nullable String procedureFolio) {
        Login login = new Login();
        if (agentSession != null) {
            login.setCurp(agentSession.getCurp());
            login.setProcedureFolio(procedureFolio != null ? procedureFolio : "0");
            login.setIdLogin(agentSession.getIdLogin());
            login.setAgentNumber(agentSession.getAgentNumber());
            login.setAssignedBranchID(Integer.parseInt(agentSession.getAssignedBranchId().substring(0, 4)));
            login.setApMaterno(agentSession.getSurName());
            login.setApPaterno(agentSession.getLastName());
            login.setNombre(agentSession.getName());
            login.setBranchName(agentSession.getAssignedBranchName());
            login.setUsuCre(agentSession.getName());
            login.setAgentConsarNumber(agentSession.getConsarCode());
            login.setIdSubproceso(Integer.parseInt(StringsKeys.ID_SUBPROCESO));
            login.setFehAcceso(getCurrentDate());
        }
        return login;
    }


    public String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ",Locale.ENGLISH);;
        String datetime = dateformat.format(c.getTime());
        return datetime;
    }
    /**
     * Performs a request to retrieve a message for a given code.
     * TODO - The dictionary of codes hasn't been sent by BUS.
     */
    private void requestInformativeMessage() {
        runOnWorkerThread(() -> {
            // Build the request object using the error code.
            Call<InformativeMessage> request = routes.getMessage(MessagesCodes.MESSAGE_AGENT_INFO_INCOMPLETE);
            // Enqueue the request.
            enqueueRequest(request, payload -> {
                // Notify that the presenter has finished doing work.
                setStatus(Status.STATUS_FAILED);
                // Send the retrieved message to the view.
                sendMessage(payload.getMessage());
            });
        });
    }

    @Override
    protected String getBaseURL() {
        return BuildConfig.BASE_URL;
    }
}
