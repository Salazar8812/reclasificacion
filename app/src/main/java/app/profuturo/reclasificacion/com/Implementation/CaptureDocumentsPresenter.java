package app.profuturo.reclasificacion.com.Implementation;

import android.support.v7.app.AppCompatActivity;

import app.profuturo.reclasificacion.com.Background.BaseWSManager;
import app.profuturo.reclasificacion.com.Background.WSManager;
import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;
import app.profuturo.reclasificacion.com.Callbacks.WSCallback;
import app.profuturo.reclasificacion.com.Utils.MessageUtils;
import app.profuturo.reclasificacion.com.base.View.BaseView;
import app.profuturo.reclasificacion.com.model.DocumentsRequest;
import app.profuturo.reclasificacion.com.model.DocumentsResponse;
import app.profuturo.reclasificacion.com.model.SendImageRequest;

public class CaptureDocumentsPresenter extends ReclasificationPresenter implements WSCallback {

    public interface GetDocumentsCallback{
        void OnSuccessGetListDocuments(DocumentsResponse documentsResponse);
        void OnSuccessUploadImage();
    }

    private GetDocumentsCallback mGetDocumentsCallback;

    public CaptureDocumentsPresenter(AppCompatActivity appCompatActivity, CaptureDocumentsPresenter.GetDocumentsCallback mGetDocumentsCallback, BaseView view) {
        super(appCompatActivity,view);
        this.mGetDocumentsCallback = mGetDocumentsCallback;
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void sendImage(String imgBase64, String idDocument, String consucutive){
        SendImageRequest documentsRequest = new SendImageRequest("", "", "");
        mWSManager.requestWs(DocumentsResponse.class, WSManager.WS.GET_DOCUMENTS, documentsRequest);
    }

    public void getListDocuments(String idTramite){
        DocumentsRequest documentsRequest = new DocumentsRequest("",idTramite,"");
        mWSManager.requestWs(DocumentsResponse.class, WSManager.WS.GET_DOCUMENTS, documentsRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if(WSManager.WS.GET_DOCUMENTS.equals(requestUrl)){
            OnSuccessGetDocuments((DocumentsResponse) baseResponse);
        }
        MessageUtils.stopProgress();
    }

    public void OnSuccessGetDocuments(DocumentsResponse documentsResponse){
        if(documentsResponse != null){
            mGetDocumentsCallback.OnSuccessGetListDocuments(documentsResponse);
        }
    }
}
