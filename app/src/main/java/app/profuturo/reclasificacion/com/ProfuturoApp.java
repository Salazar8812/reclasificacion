package app.profuturo.reclasificacion.com;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

public class ProfuturoApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ProcedureConfigure config = new ProcedureConfigure(this);
        config.setup();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}

