package app.profuturo.reclasificacion.com.Implementation;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import app.profuturo.reclasificacion.com.base.View.BaseView;

public abstract class BasePresenterC {

    protected AppCompatActivity mAppCompatActivity;
    protected final Context mContext;
    private BaseView mView;

    public BasePresenterC(AppCompatActivity appCompatActivity, BaseView view) {
        mAppCompatActivity = appCompatActivity;
        mContext = appCompatActivity;
        mView = view;
    }

    public void onCreate() {

    }

    public void onStart() {

    }

    public void onResume() {

    }

    public void onPause() {

    }

    public void onStop() {

    }

    public void onDestroy() {

    }

    protected void sendMessage(@NonNull final Object message) {
        if (mView != null) {
            new Handler(mView.asContext()
                    .getMainLooper())
                    .post(() -> mView.displayMessage(message));
        }
    }

}
