
package app.profuturo.reclasificacion.com.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;

public class SaleResponse implements WSBaseResponseInterface {

    @SerializedName("saldos")
    @Expose
    private List<SaldoBean> saldoBean = new ArrayList<>();

    public List<SaldoBean> getSaldoBean() {
        return saldoBean;
    }

    public void setSaldoBean(List<SaldoBean> saldoBean) {
        this.saldoBean = saldoBean;
    }

}
