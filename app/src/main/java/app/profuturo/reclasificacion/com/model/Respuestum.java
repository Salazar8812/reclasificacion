
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Respuestum {

    @SerializedName("documentosACapturar")
    @Expose
    public DocumentosACapturar documentosACapturar;

}
