package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.SerializedName;

public class ActionValue {
    @SerializedName("fechaAccion")
    public String fechaAccion;

    @SerializedName("fechaActualizacion")
    public String fechaActualizacion;

    @SerializedName("fechaCreacion")
    public String fechaCreacion;

    @SerializedName("idValorAccion")
    public String idValorAccion;

    @SerializedName("regimen")
    public Regimen regimen;

    @SerializedName("siefore")
    public SieforeActionValue siefore;

    @SerializedName("usuarioActualizacion")
    public String usuarioActualizacion;

    @SerializedName("usuarioCreacion")
    public String usuarioCreacion;

    @SerializedName("valorAccion")
    public String valorAccion;
}
