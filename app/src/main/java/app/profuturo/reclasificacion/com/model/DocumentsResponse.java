
package app.profuturo.reclasificacion.com.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;

public class DocumentsResponse implements WSBaseResponseInterface {

    @SerializedName("respuesta")
    @Expose
    public List<Respuestum> respuesta = new ArrayList<>();

}
