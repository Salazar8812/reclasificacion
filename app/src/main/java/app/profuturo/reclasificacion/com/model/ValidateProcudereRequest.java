package app.profuturo.reclasificacion.com.model;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseRequestInterface;

public class ValidateProcudereRequest implements WSBaseRequestInterface {
    public String idProceso;
    public String idSubProceso;
    public String numeroCuenta;
    public String expBiometrico;
    public String expIdentificacion;

    public ValidateProcudereRequest(String idProceso, String idSubProceso, String numeroCuenta, String expBiometrico, String expIdentificacion) {

        this.idProceso = idProceso;
        this.idSubProceso = idSubProceso;
        this.numeroCuenta = numeroCuenta;
        this.expBiometrico = expBiometrico;
        this.expIdentificacion = expIdentificacion;
    }

    public ValidateProcudereRequest() {
    }
}
