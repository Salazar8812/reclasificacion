package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.profuturo.reclasificacion.com.base.Utils;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class AgentSession extends RealmObject {

    @Expose
    @SerializedName("numeroEmpleado")
    @PrimaryKey
    private String agentNumber;
    @Expose
    @SerializedName("apellidoPaterno")
    private String surName;
    @Expose
    @SerializedName("apellidoMaterno")
    private String lastName;
    @Expose
    @SerializedName("nombre")
    private String name;
    @Expose
    @SerializedName("claveConsar")
    private String consarCode;
    @Expose
    @SerializedName("curp")
    private String curp;
    @Expose
    @SerializedName("fechaAltaConsar")
    private String creationDate;
    @Expose
    @SerializedName("centroCosto")
    private String assignedBranchId;
    private String assignedBranchName;
    private String idLogin;

    public AgentSession() {
        // Required by Realm
    }

    public AgentSession(String agentNumber, String surName, String lastName, String name, String consarCode, String curp, String creationDate, String assignedBranchId, String assignedBranchName, String idLogin) {
        this.agentNumber = agentNumber;
        this.surName = surName;
        this.lastName = lastName;
        this.name = name;
        this.consarCode = consarCode;
        this.curp = curp;
        this.creationDate = creationDate;
        this.assignedBranchId = assignedBranchId;
        this.assignedBranchName = assignedBranchName;
        this.idLogin = idLogin;
    }

    public String getAgentNumber() {
        return agentNumber;
    }

    public void setAgentNumber(String agentNumber) {
        this.agentNumber = agentNumber;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConsarCode() {
        return consarCode;
    }

    public void setConsarCode(String consarCode) {
        this.consarCode = consarCode;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getAssignedBranchId() {
        return assignedBranchId;
    }

    public void setAssignedBranchId(String assignedBranchId) {
        this.assignedBranchId = assignedBranchId;
    }

    public String getAssignedBranchName() {
        return assignedBranchName;
    }

    public void setAssignedBranchName(String assignedBranchName) {
        this.assignedBranchName = assignedBranchName;
    }

    public String getIdLogin() {
        return idLogin;
    }

    public void setIdLogin(String idLogin) {
        this.idLogin = idLogin;
    }

    /**
     * Asserts that the basic information for the session is not null or empty.
     *
     * @return true if the basic session data is valid.
     */
    public Boolean isSessionValid() {
        return Utils.areNotNullOrEmpty(agentNumber, surName, lastName, name, consarCode, curp,
                creationDate, assignedBranchId, assignedBranchName);
    }

    /**
     * Returns the full agent name for display.
     *
     * @return The agent name in this format: name surName.
     */
    public String getDisplayName() {
        return String.valueOf(this.name) + " " + String.valueOf(this.surName);
    }
}

