
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Solicitante extends RealmObject {

    @SerializedName("apMaternoCte")
    @Expose
    public String apMaternoCte;
    @SerializedName("apPaternoCte")
    @Expose
    public String apPaternoCte;
    @SerializedName("celular")
    @Expose
    public String celular;
    @SerializedName("correoElec")
    @Expose
    public String correoElec;
    @SerializedName("curp")
    @Expose
    public String curp;
    @SerializedName("nombreCte")
    @Expose
    public String nombreCte;
    @SerializedName("nss")
    @Expose
    public String nss;
    @SerializedName("numCtaInvdual")
    @Expose
    public String numCtaInvdual;
    @SerializedName("rfcCte")
    @Expose
    public String rfcCte;
    @SerializedName("usuCre")
    @Expose
    public String usuCre;
    @SerializedName("calle")
    @Expose
    public String calle;
    @SerializedName("codigoPostal")
    @Expose
    public String codigoPostal;
    @SerializedName("colonia")
    @Expose
    public String colonia;
    @SerializedName("estado")
    @Expose
    public String estado;
    @SerializedName("municipioDelegacion")
    @Expose
    public String municipioDelegacion;
    @SerializedName("numeroExterior")
    @Expose
    public String numeroExterior;
    @SerializedName("numeroInterior")
    @Expose
    public String numeroInterior;
    @SerializedName("telefono")
    @Expose
    public String telefono;

}
