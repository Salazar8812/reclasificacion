package app.profuturo.reclasificacion.com.Callbacks;


public interface WSCallback <R extends WSBaseResponseInterface> {

    void onRequestWS(String requestURL);

    void onSuccessLoadResponse(String requestURL, R baseResponse);

    void onErrorLoadResponse(String requestURL, String messageError);

    void onErrorConnection();

}
