package app.profuturo.reclasificacion.com.presenter;

import android.annotation.SuppressLint;
import android.arch.lifecycle.MutableLiveData;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.util.List;

import app.profuturo.reclasificacion.com.BuildConfig;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.base.Utils;
import app.profuturo.reclasificacion.com.base.View.BaseView;
import app.profuturo.reclasificacion.com.base.utils.JsonMocker;
import app.profuturo.reclasificacion.com.model.AgentSession;
import app.profuturo.reclasificacion.com.model.BranchResponse;
import retrofit2.Call;

/**
 * Presenter class that retrieves a list of ProFuturo branches
 * using an Agent number and his current location.
 *
 * @author Luis Meléndez
 * @since August 10th 2018 - 05:03 PM
 */

public class BranchesPresenter extends NetworkPresenter {
    /**
     * Constant that defines the maximum radius between neighbour branches
     */
    private static final double MAXIMUM_DISTANCE_BETWEEN_BRANCHES = 1300.0; // 50000.0 (50 km for prod)
    private Double mLatitude = 19.358823;
    private Double mLongitude = -99.194708;
    /**
     * Defines the last known location of the logged agent inside a {@link MutableLiveData}
     */
    private Location deviceLocation = new Location(this.getClass().getName());
    /**
     * MutableLiveData that reports changes to a list of retrieved branches.
     */
    private MutableLiveData<List<BranchResponse.Branch>> branches = new MutableLiveData<>();
    /**
     * MutableLiveData reference to the agent assigned branch.
     */
    private MutableLiveData<String> mAssignedBranch = new MutableLiveData<>();
    /**
     * MutableLiveData object that reports changes on the user location.
     */
    private MutableLiveData<Location> mUserLocation = new MutableLiveData<>();
    /**
     * Location provider to get last known location
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Constructor for this presenter class that assigns a View.
     * This view can be null, this is useful in Unit Test cases.
     *
     * @param view The view for this presenter.
     */
    public BranchesPresenter(@NonNull BaseView view) {
        super(view);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(view.asContext());
    }

    public MutableLiveData<String> getAssignedBranch() {
        return mAssignedBranch;
    }

    public MutableLiveData<Location> getUserLocation() {
        return mUserLocation;
    }

    @SuppressLint("MissingPermission")
    public void getLastKnownLocation() {
        if (mFusedLocationClient != null) {
            mFusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
                // Check if the received location is not null.
                if (location != null) {
                    // Get last latitude and longitude
                    mLatitude = location.getLatitude();
                    mLongitude = location.getLongitude();
                }
                // Set the null-safe Location l object the coordinates.
                deviceLocation.setLatitude(mLatitude);
                deviceLocation.setLongitude(mLongitude);
                // Report the found device location.
                mUserLocation.postValue(deviceLocation);
            });
        }

    }

    /**
     * Method that makes the call the branches web service.
     */
    public void callBranchesService(@NonNull AgentSession agent) {
        runOnWorkerThread(() -> {
            // Get branch Id from User session
            String branchId = BuildConfig.DEBUG ? "1498" : agent.getAssignedBranchId();
            // Build the request object for retrieving the agent information.
            Call<BranchResponse> request = routes.getBranches(branchId, mLatitude, mLongitude);
            // Enqueue the branches request
            enqueueRequest(request, payload -> processBranchesResult(payload, branchId));
        });
    }

    /**
     * Receives the results of a Branches service query and processes them.
     *
     * @param payload          The response from the server.
     * @param expectedBranchId The expected agent branch.
     */
    private void processBranchesResult(@Nullable BranchResponse payload,
                                       @NonNull String expectedBranchId) {
        // We don't know if the response is successful or not, so report the success as false.
        Boolean success = false;
        // Check if the payload object is not null and the branches list is not null.
        if (payload != null && Utils.isNotNullOrEmpty(payload.getBranchList())) {
            // Report the results to the view.
            branches.postValue(payload.getBranchList());
            // Check if the agent branch is in the results.
            success = isAgentBranchInResults(payload.getBranchList(), expectedBranchId);
        }
        // If there was no success, report that no branches were found.
        if (!success) {
            sendMessage(R.string.assigned_branch_not_found);
            // Report that something went wrong.
            setStatus(Status.STATUS_FAILED);
        } else {
            mAssignedBranch.postValue(expectedBranchId);
            // Report that the presenter has finished its operation successfully.
            setStatus(Status.STATUS_DONE);
        }
    }

    /**
     * Checks if in a list of branches an expected branch exists.
     *
     * @param branches   The list of branches to check upon.
     * @param expectedId The id of the expected branch.
     * @return true if the branch exists.
     */
    private boolean isAgentBranchInResults(@NonNull List<BranchResponse.Branch> branches,
                                           @NonNull String expectedId) {
        // Flag that indicates if the branch exists within the results.
        Boolean isWithin = false;
        // Iterate through the branches.
        for (BranchResponse.Branch b : branches) {
            // The branch exists if the id matches with the expected one and the agent is inside it.
            isWithin = expectedId.equals(b.getBranchOffice());
            // If the branch has been found, break the loop.
            if (isWithin) {
                break;
            }
        }
        return isWithin;
    }

    /**
     * Returns the status {@link MutableLiveData} object for this presenter.
     *
     * @return MutableLiveData object containing list of branches
     */
    public MutableLiveData<List<BranchResponse.Branch>> getBranches() {
        return branches;
    }

    /**
     * Checks if the current agent is within the distance of a branch.
     *
     * @param location The agent location object.
     * @return true if the agent is inside a branch.
     */
    public Boolean isAgentWithinBranchDistance(@NonNull Object location) {
        return location instanceof BranchResponse.Branch && deviceLocation.distanceTo(((BranchResponse.Branch) location)
                .toLocation()) <= MAXIMUM_DISTANCE_BETWEEN_BRANCHES;
    }
}