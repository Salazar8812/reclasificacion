package app.profuturo.reclasificacion.com.Adapter;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import app.profuturo.reclasificacion.com.CaptureDocumentsFragment;
import app.profuturo.reclasificacion.com.ClientInformationFragment;
import app.profuturo.reclasificacion.com.ClientResultFragment;
import app.profuturo.reclasificacion.com.ClientSearchFragment;
import app.profuturo.reclasificacion.com.NavigationFragment;
import app.profuturo.reclasificacion.com.base.BaseFragment;

/**
 * Fragment Pager Adapter that allows swipe navigation across all the Procedure views.
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 17/09/2018 - 12:11 PM
 */
public class NavigationAdapter extends FragmentStatePagerAdapter {
    private List<BaseFragment> mNavigationStack;

    public NavigationAdapter(FragmentManager fm) {
        super(fm);
        initializeFragmentList();
    }

    private void initializeFragmentList() {
        mNavigationStack = new ArrayList<>();
        mNavigationStack.add(new ClientSearchFragment());
        /*mNavigationStack.add(new ClientInformationFragment());
        mNavigationStack.add(new CaptureDocumentsFragment());*/
    }

    /**
     * Retrieves the fragment item at the given position.
     *
     * @param position The desired position for the fragment.
     * @return The fragment at said position.
     */
    @Override
    public Fragment getItem(int position) {
        // Prevents the position from under flowing or overflowing the list.
        Integer safePosition = position <= 0 ? position :
                position >= mNavigationStack.size() ? mNavigationStack.size() : position;
        // Return the item at the safe position.
        return mNavigationStack.get(safePosition);
    }

    /**
     * Checks if the current page has been removed or not, if so, proceeds to update the ViewPager.
     *
     * @param fragment The current fragment object in the list.
     * @return The index of the element or none if the page has changed.
     */
    @Override
    public int getItemPosition(@NonNull Object fragment) {
        BaseFragment f = (BaseFragment) fragment;
        if (mNavigationStack.contains(f)) {
            return mNavigationStack.indexOf(f);
        } else {
            return POSITION_NONE;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCount() {
        return mNavigationStack.size();
    }

    /**
     * Adds a navigation fragment to the last position of the stack.
     *
     * @param nf The fragment to be added.
     */
    public void setNextFragment(NavigationFragment nf) {
        mNavigationStack.add(nf);
        notifyDataSetChanged();
    }

    /**
     * Removes the last fragment from the stack.
     */
    public void removePreviousFragment() {
        Integer size = mNavigationStack.size();
        if (size > 1) {
            mNavigationStack.remove(size - 1);
            notifyDataSetChanged();
        }
    }
}

