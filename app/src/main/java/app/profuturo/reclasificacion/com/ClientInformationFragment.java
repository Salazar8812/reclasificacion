package app.profuturo.reclasificacion.com;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import org.kobjects.util.Strings;

import java.util.HashMap;

import app.profuturo.reclasificacion.com.Implementation.BasePresenterC;
import app.profuturo.reclasificacion.com.Repository.local.ClientDAO;
import app.profuturo.reclasificacion.com.Utils.StringsKeys;
import app.profuturo.reclasificacion.com.databinding.FragmentClientInformationBinding;
import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.presenter.ClientPicturePresenter;
import app.profuturo.reclasificacion.com.presenter.ValidateProcedurePresenter;
import app.profuturo.reclasificacion.com.widget.NavigationViewPager;

public class ClientInformationFragment extends NavigationFragment<FragmentClientInformationBinding> implements View.OnClickListener, ValidateProcedurePresenter.ValidateProcedureCallback {
    private Client mClient;
    private ValidateProcedurePresenter mValidateProcedurePresenter;
    private TextView identification_certification_description;
    private String mBiometricExpedient= "";
    private String mIdentifiacationExpedient= "";

    public static ClientInformationFragment newInstance(@NonNull Client client) {
        Bundle arguments = new Bundle();
        ClientInformationFragment resultsFragment = new ClientInformationFragment();
        resultsFragment.setArguments(arguments);
        return resultsFragment;
    }

    private String accountNumber;

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getLayoutId() {
        return R.layout.fragment_client_information;
    }

    /**
     * {@inheritDoc}
     */

    @Override
    public void setup() {
        identification_certification_description = getView().findViewById(R.id.identification_certification_description);
        // Create a picture presenter to retrieve the client's picture
        ClientPicturePresenter presenter = new ClientPicturePresenter(this);
        // Attach this presenter to this fragment so can be destroy automatically
        attachPresenters(presenter);

        getDataBinding().mProcedureContinueButton.setOnClickListener(this);
        // Observe for changes in Picture Meta Data
        getPresenter(ClientPicturePresenter.class)
                .getPictureData().observe(this, (pictureMetaData -> {
            // Check metadata is not null
            if (pictureMetaData != null) {
                // Assign the data to the B64ImageView
                getDataBinding().clientPhoto.setClientPicture(pictureMetaData);
            }
        }));
        // Get the currently stored client.
        new Thread(() -> {
            // Read the client from the local storage database.
            mClient = ClientDAO.readClient();
            // Checks the client is not null
            if (mClient != null) {
                // Assign the found client to the DataBinding if it is not null.
                getDataBinding().setClient(mClient);
                // Also get the client account number to retrieve photo
                accountNumber = mClient.getAccountNumber();
                // Make the service call to retrieve photo metadata
                //getPresenter(ClientPicturePresenter.class).getClientPicture(accountNumber);
                // Draw the client status indicators.
                FragmentActivity a = getActivity();
                if (a != null) {
                    a.runOnUiThread(() -> {
                        mValidateProcedurePresenter.savaInformtionClient(mClient);
                        setStatusToIndicators(mClient);
                        if (getAdapter() != null && getViewPager() != null) {
                            /*getAdapter().setNextFragment(new HistoryFragment());
                            getViewPager().setAllowedDirection(NavigationViewPager.SwipeDirection.SWIPE_ALL);*/
                        }
                    });
                }
            }
        }).start();

    }

    @Override
    public BasePresenterC getPresenterC() {
        mValidateProcedurePresenter = new ValidateProcedurePresenter((AppCompatActivity) getActivity(),this,this);
        return mValidateProcedurePresenter;
    }

    /**
     * Method to set the expedient status and the corresponding description
     *
     * @param client The client read from database
     */
    private void setStatusToIndicators(Client client) {
        if(client.getAffiliationStatus().equals("Inactiva")){
            getDataBinding().identificationTransactStatusClientAffiliate.setImageResource(R.drawable.ic_clear);
        }else{
            getDataBinding().identificationTransactStatusClientAffiliate.setImageResource(R.drawable.ic_done);
        }

        mValidateProcedurePresenter.getIndicator(mClient.getAccountNumber());
    }

    @Override
    public void onClick(View v) {
        if(R.id.mProcedureContinueButton == v.getId()){
             mValidateProcedurePresenter.validateProecedure(StringsKeys.ID_PROCESO,StringsKeys.ID_SUBPROCESO,mClient.getAccountNumber(),mBiometricExpedient,mIdentifiacationExpedient);
        }
    }

    @Override
    public void OnSuccessValidate() {
        if (getAdapter() != null) {
            getAdapter().setNextFragment(new ReclassificationFragment());
            getViewPager().setCurrentItem(getViewPager().getCurrentItem() + 1, true);
            getViewPager().setAllowedDirection(NavigationViewPager.SwipeDirection.SWIPE_ALL);
        }
    }

    @Override
    public void OnSuccessConvivence() {

    }

    @Override
    public void OnSuccessIndicators(HashMap<String, String> mIndicators) {
        try {
            if (mIndicators.get("707").equals("1") || mIndicators.get("707").equals("2")) {
                getDataBinding().identificationBiometric.setImageResource(R.drawable.ic_done);
                mBiometricExpedient = mIndicators.get("707");
            } else {
                getDataBinding().identificationBiometric.setImageResource(R.drawable.ic_clear);
            }
        }catch (NullPointerException e){
            e.printStackTrace();
            getDataBinding().identificationBiometric.setImageResource(R.drawable.ic_clear);
        }

        try {
            if (!mIndicators.get("61").equals("")) {
                getDataBinding().identificationCertificationStatusDate.setImageResource(R.drawable.ic_done);
                identification_certification_description.setText("-Fecha certificacion " + mIndicators.get("61"));
            } else {
                getDataBinding().identificationCertificationStatusDate.setImageResource(R.drawable.ic_clear);
            }
        }catch (NullPointerException e){
            e.printStackTrace();
            getDataBinding().identificationCertificationStatusDate.setImageResource(R.drawable.ic_clear);
        }

        try {
            if (mIndicators.get("706").equals("5")) {
                getDataBinding().identificationTransactStatusImg.setImageResource(R.drawable.ic_done);
                mIdentifiacationExpedient = mIndicators.get("706");
            } else {
                getDataBinding().identificationTransactStatusImg.setImageResource(R.drawable.ic_clear);
            }
        }catch (NullPointerException e){
            e.printStackTrace();
            getDataBinding().identificationTransactStatusImg.setImageResource(R.drawable.ic_clear);
        }
    }



}
