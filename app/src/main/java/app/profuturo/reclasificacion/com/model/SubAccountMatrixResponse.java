package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;

public class SubAccountMatrixResponse implements WSBaseResponseInterface {
    @SerializedName("subcuentas")
    public List<SubAccountsMatrixReclasification> listMatrix = new ArrayList<>();
}
