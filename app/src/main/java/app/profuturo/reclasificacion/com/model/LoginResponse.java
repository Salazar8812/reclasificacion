package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Java class that defines the JSON structure
 * of a login attempt response from the server.
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 13/08/2018 - 12:58 PM
 */
public class LoginResponse {
    @Expose
    @SerializedName("success")
    private Boolean success;

    @Expose
    @Deprecated // TODO - ask BUS to return the full login data.
    @SerializedName("response")
    private String response;

    @Expose
    @SerializedName("login")
    private Login login;

    public LoginResponse(Boolean success) {
        this.success = success;
    }

    public Boolean isSuccessful() {
        return success;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public String getResponse() {
        return response;
    }
}

