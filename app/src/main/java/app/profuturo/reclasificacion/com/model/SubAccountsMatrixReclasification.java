package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;

public class SubAccountsMatrixReclasification implements WSBaseResponseInterface {
    @SerializedName("aplica")
    public String aplica = "";

    @SerializedName("tipoSubcuenta")
    public String tipoSubcuenta = "";

    @SerializedName("mensaje")
    public String mensaje= "";
    public String simbol = "";

    @SerializedName("siefore")
    public String siefore = "";

    @SerializedName("descripcion")
    public String descripcion = "";

    @SerializedName("listasubcuentasApovol")
    public List<SubAccountApovol> listSubAccountApovol = new ArrayList<>();


    public SubAccountsMatrixReclasification() {
    }

    public SubAccountsMatrixReclasification(String aplica, String tipoSubcuenta, String simbol, String siefore, String descripcion, List<SubAccountApovol> list) {
        this.aplica = aplica;
        this.tipoSubcuenta = tipoSubcuenta;
        this.mensaje = mensaje;
        this.simbol = simbol;
        this.siefore = siefore;
        this.descripcion = descripcion;
        this.listSubAccountApovol = list;
    }

}
