package app.profuturo.reclasificacion.com;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class ChangeInfoDialog extends AppCompatActivity implements View.OnClickListener {
    private ImageView mCloseImageView;
    private Button mAcceptButton;
    private EditText mSMSEditText;
    private EditText mEmailEditText;
    private EditText mSocialEditText;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.change_info_notification_dialog);
        bindData();
    }

    public void bindData(){
        mCloseImageView = findViewById(R.id.mCloseImageView);
        mAcceptButton = findViewById(R.id.mAcceptButton);
        mSMSEditText = findViewById(R.id.mSMSEditText);
        mEmailEditText = findViewById(R.id.mEmailEditText);
        mSocialEditText = findViewById(R.id.mSocialEditText);

        mCloseImageView.setOnClickListener(this);
        mAcceptButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.mCloseImageView){
            this.finish();
        }else if(v.getId() == R.id.mAcceptButton){
            this.finish();
        }
    }
}
