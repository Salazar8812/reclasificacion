package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.SerializedName;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseRequestInterface;
import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;

public class IndicatorActionValueRequest implements WSBaseRequestInterface {
    @SerializedName("fechaValorAccion")
    public String fechaValorAccion;

    @SerializedName("idSiefore")
    public String idSiefore;



    public IndicatorActionValueRequest(String fechaValorAccion, String idSiefore, String idValorAccion) {
        this.fechaValorAccion = fechaValorAccion;
        this.idSiefore = idSiefore;
    }
}
