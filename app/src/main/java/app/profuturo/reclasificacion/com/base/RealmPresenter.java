package app.profuturo.reclasificacion.com.base;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import app.profuturo.reclasificacion.com.Implementation.BasePresenterC;
import app.profuturo.reclasificacion.com.base.View.BaseView;
import io.realm.Realm;

public class RealmPresenter extends BasePresenterC {

    public Realm mRealm;
    public DataManager mDataManager;

    public RealmPresenter(Context context, BaseView view) {
        super((AppCompatActivity) context,view);
    }

    @Override
    public void onResume() {
        super.onResume();
        mDataManager = DataManager.validateConnection(mDataManager);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mRealm = Realm.getDefaultInstance();
        mDataManager = DataManager.validateConnection(mDataManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRealm != null && !mRealm.isClosed()) mRealm.close();
        if (mDataManager != null) mDataManager.close();
    }
}

