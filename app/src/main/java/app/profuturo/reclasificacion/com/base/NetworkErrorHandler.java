package app.profuturo.reclasificacion.com.base;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import app.profuturo.reclasificacion.com.R;
import retrofit2.Response;

/**
 * Error handler for network operations.
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 03/10/2018 - 03:48 PM
 */
public class NetworkErrorHandler {
    /**
     * Returns a message object depending on its type.
     *
     * @param errorPayload The error object retrieved from the WebService.
     * @return error response from the payload object.
     */
    public static Object factory(Object errorPayload) {
        if (errorPayload instanceof Response) {
            return processResponse((Response) errorPayload);
        } else if (errorPayload instanceof Throwable) {
            if (isServerConnectionError((Throwable) errorPayload)) {
                return R.string.unable_to_connect;
            } else {
                return errorPayload;
            }
        } else {
            return errorPayload;
        }
    }

    private static Object processResponse(Response response) {
        try {
            String json = response.body().toString();
            JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);
            if (jsonObject.has("mensaje")) {
                return jsonObject.get("mensaje").getAsString();
            } else if (jsonObject.has("descripcion")) {
                return jsonObject.get("descripcion").getAsString();
            } else {
                return checkBUSAvailability(response);
            }
        } catch (Throwable t) {
            return "";
        }
    }

    /**
     * Checks if the response object is from the IBM BUS being down.
     *
     * @param response The response retrieved from the web service.
     * @return String resource Id indicating if the BUS is up or down.
     */
    private static Integer checkBUSAvailability(Response response) {
        String contentType = response.raw().body().contentType().toString();
        if (contentType.contains("xml")) {
            return R.string.bus_down;
        } else {
            return R.string.unexpected_error;
        }
    }

    /**
     * Checks if it is a TimeOutException or any of the related exception (IOException children).
     *
     * @param error The error object retrieved.
     * @return true if matches any of the mentioned exception classes.
     * @see TimeoutException
     * @see IOException
     */
    private static Boolean isServerConnectionError(Throwable error) {
        return error instanceof TimeoutException || error instanceof IOException;
    }
}
