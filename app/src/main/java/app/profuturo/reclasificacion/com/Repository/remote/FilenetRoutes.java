package app.profuturo.reclasificacion.com.Repository.remote;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface FilenetRoutes{

    @POST("imb/fileNet/insertaInformacionArchivoso")
    Call<ResponseBody> openTransactionDocuments();

    @POST("mb/fileNet/cargaArchivo")
    Call<ResponseBody> uploadFile();
}
