package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.annotations.PrimaryKey;

/**
 * Model class that defines the JSON structure
 * for a response of a client profile picture.
 * <p>
 * Note: <b>Handle this class with caution, as it contains a Base64
 * value it can be quite expensive with the memory, caching this
 * object is strictly recommended.</b>
 * </p>
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 05/09/2018 - 07:28 PM
 */
public class ClientPicture {
    @Expose
    @SerializedName("consultarFotosCliente")
    private ClientPictureMetaData mMetaData;

    public ClientPicture(ClientPictureMetaData mMetaData) {
        this.mMetaData = mMetaData;
    }

    public ClientPictureMetaData getMetaData() {
        return mMetaData;
    }

    /**
     * Inner class that contains references to the base64
     * value of the image and its id from the ProFuturo database.
     *
     * @author Alfredo Bejarano
     * @version 1.0
     * @since September 5th, 2018 - 07:55 PM
     */
    public static class ClientPictureMetaData {
        @Expose
        @PrimaryKey
        @SerializedName("idImagen")
        private String pictureId;
        @Expose
        @SerializedName("imagen")
        private String base64Content;

        public ClientPictureMetaData() {
            // Empty constructor required by Realm.
        }

        public ClientPictureMetaData(String pictureId, String base64Content) {
            this.pictureId = pictureId;
            this.base64Content = base64Content;
        }

        public String getPictureId() {
            return pictureId;
        }

        public void setPictureId(String pictureId) {
            this.pictureId = pictureId;
        }

        public String getBase64Content() {
            return base64Content;
        }

        public void setBase64Content(String base64Content) {
            this.base64Content = base64Content;
        }
    }
}

