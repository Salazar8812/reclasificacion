package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Model class that contains information about
 * the assigned branch for an Agent.
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 13/08/2018 - 10:50 AM
 */
public class AssignedBranch {
    @Expose
    @SerializedName("nombreSucursal")
    private String name;
    @Expose
    @SerializedName("numeroSucursal")
    private String number;

    public AssignedBranch(String name, String number) {
        this.name = name;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
