package app.profuturo.reclasificacion.com;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import java.util.ArrayList;
import java.util.List;
import app.profuturo.reclasificacion.com.Adapter.ClientSearchResultsAdapter;
import app.profuturo.reclasificacion.com.Implementation.ClientResultPresenter;
import app.profuturo.reclasificacion.com.databinding.FragmentClientResultBinding;
import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.presenter.ClientPresenter;
import app.profuturo.reclasificacion.com.widget.NavigationViewPager;

public class ClientResultFragment extends NavigationFragment<FragmentClientResultBinding> implements View.OnClickListener,ClientResultPresenter.ClientCallback
{

    private List<Client> mListClient = new ArrayList<>();
    private ClientSearchResultsAdapter mClientSearchResultsAdapter;

    public static ClientResultFragment newInstance(@NonNull String criteria) {
        Bundle arguments = new Bundle();
        arguments.putString(CRITERIA_EXTRA, criteria);
        ClientResultFragment resultsFragment = new ClientResultFragment();
        resultsFragment.setArguments(arguments);
        return resultsFragment;
    }

    /**
     * Constant that defines the extra containing the search criteria
     */
    public static final String CRITERIA_EXTRA = "search_criteria";

    @Override
    public Integer getLayoutId() {
        return R.layout.fragment_client_result;
    }
    @Override
    public void setup() {
        getDataBinding().newSearch.setOnClickListener(this);
        getDataBinding().cancel.setOnClickListener(this);
        mClientSearchResultsAdapter = new ClientSearchResultsAdapter(mListClient);
        getDataBinding().resultsList.setLayoutManager(new LinearLayoutManager(asContext()));
        getDataBinding().resultsList.setAdapter(mClientSearchResultsAdapter);
        mClientSearchResultsAdapter.notifyDataSetChanged();

        attachPresenters(new ClientPresenter(this));
        // Observe the client changes.
        observe(getPresenter(ClientPresenter.class).getClientResults(), results -> {
            getDataBinding().resultsList.setAdapter(null);
            ClientSearchResultsAdapter adapter;
            // If the results are not null or empty.
            if (results != null) {
                List<Client> clients = new ArrayList<>();
                clients.add(results);
                adapter = new ClientSearchResultsAdapter(clients);
            } else {
                adapter = new ClientSearchResultsAdapter(new ArrayList<>());
            }
            getDataBinding().resultsList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        });
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.cancel) {
            // Check this fragment is still attached to an activity
            if (getActivity() != null) {
                // Call to pop this fragment from back stack
                getActivity().finish();
            }
        } else {
            // Do a new search
            searchAgain();
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Bundle arguments = getArguments();
            if (arguments != null) {
                // Retrieve the search criteria from the arguments.
                String searchCriteria = getArguments().getString(CRITERIA_EXTRA, "");
                // Search the clients by the criteria.
                getPresenter(ClientPresenter.class).searchClientsByCriteria(searchCriteria);
                // prevent swiped if the ViewPager is not null.
                if (getViewPager() != null) {
                    getViewPager().setAllowedDirection(NavigationViewPager.SwipeDirection.SWIPE_NONE);
                }
            }
        }
    }

    // Method to present the ClientSearchFragment from the results of client
    private void searchAgain() {
        // Get the client presenter
        //getPresenter(ClientPresenter.class).deleteClient();
        getAdapter().setNextFragment(new ClientSearchFragment());
        getViewPager().setCurrentItem(getViewPager().getCurrentItem() + 1, true);
        getViewPager().setAllowedDirection(NavigationViewPager.SwipeDirection.SWIPE_ALL);


        //getViewPager().setCurrentItem(getViewPager().getCurrentItem() - 1, true);
    }

    //@Override
    public void getItemClient(Client client) {
        if (getAdapter() != null) {
            getAdapter().setNextFragment(ClientInformationFragment.newInstance(client));
            getViewPager().setCurrentItem(getViewPager().getCurrentItem() + 1, true);
            getViewPager().setAllowedDirection(NavigationViewPager.SwipeDirection.SWIPE_ALL);
        }
    }

    @Override
    public void OnSuccessGetClients(Client client) {
        getDataBinding().resultsList.setAdapter(null);
        ClientSearchResultsAdapter adapter;
        // If the results are not null or empty.
        if (client != null) {
            List<Client> clients = new ArrayList<>();
            clients.add(client);
            adapter = new ClientSearchResultsAdapter(clients);
        } else {
            adapter = new ClientSearchResultsAdapter(new ArrayList<>());
        }
        getDataBinding().resultsList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}

