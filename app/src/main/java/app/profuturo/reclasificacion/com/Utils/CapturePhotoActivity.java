package app.profuturo.reclasificacion.com.Utils;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.io.File;

import app.profuturo.reclasificacion.com.R;
import io.fotoapparat.Fotoapparat;
import io.fotoapparat.FotoapparatSwitcher;
import io.fotoapparat.parameter.LensPosition;
import io.fotoapparat.preview.Frame;
import io.fotoapparat.preview.FrameProcessor;
import io.fotoapparat.result.PendingResult;
import io.fotoapparat.result.PhotoResult;
import io.fotoapparat.view.CameraView;
import static io.fotoapparat.log.Loggers.fileLogger;
import static io.fotoapparat.log.Loggers.logcat;
import static io.fotoapparat.log.Loggers.loggers;
import static io.fotoapparat.parameter.selector.AspectRatioSelectors.standardRatio;
import static io.fotoapparat.parameter.selector.FlashSelectors.autoFlash;
import static io.fotoapparat.parameter.selector.FlashSelectors.autoRedEye;
import static io.fotoapparat.parameter.selector.FlashSelectors.off;
import static io.fotoapparat.parameter.selector.FlashSelectors.torch;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.autoFocus;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.continuousFocus;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.fixed;
import static io.fotoapparat.parameter.selector.Selectors.firstAvailable;
import static io.fotoapparat.parameter.selector.SizeSelectors.smallestSize;

public class CapturePhotoActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_CAPTURE = 0x12;
    private static int mImageCode;
    private CameraView mCameraView;
    private LinearLayout mCloseCapture;
    private FotoapparatSwitcher fotoapparatSwitcher;
    private String mImagePath;
    private boolean isTake = false;

    public static void launch(AppCompatActivity activity, String path) {
        Intent intent = new Intent(activity, CapturePhotoActivity.class);
        intent.putExtra("photo", path);
        activity.startActivityForResult(intent, CapturePhotoActivity.REQUEST_CODE_CAPTURE);
    }

    public static void launch(AppCompatActivity activity, String path, int requestCode) {
        Intent intent = new Intent(activity, CapturePhotoActivity.class);
        intent.putExtra("photo", path);
        if (requestCode == 33)
            mImageCode = 0;
        else
            mImageCode = 1;
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_camera_activity);

        mImagePath = getIntent().getStringExtra("photo");

        mCameraView = (CameraView) findViewById(R.id.act_capture_photo_camera);
        mCloseCapture = (LinearLayout) findViewById(R.id.mCloseCapture);

        ImageView captureView = findViewById(R.id.mCapturePhoto);

        Fotoapparat backPhotoApparat;
        if (mImageCode == 0) {
            backPhotoApparat = createFotoapparat(LensPosition.FRONT);
        } else {
            backPhotoApparat = createFotoapparat(LensPosition.BACK);
        }

        fotoapparatSwitcher = FotoapparatSwitcher.withDefault(backPhotoApparat);

        mCloseCapture.setOnClickListener(view -> {
            onBackPressed();
        });

        captureView.setOnClickListener(v -> {
            if (!isTake) {
                isTake = true;
                takePicture();
            }
        });

    }

    private Fotoapparat createFotoapparat(LensPosition position) {
        return Fotoapparat
                .with(this)
                .into(mCameraView)
                .photoSize(standardRatio(smallestSize()))
                .focusMode(firstAvailable(
                        continuousFocus(),
                        autoFocus(),
                        fixed()
                ))
                .flash(firstAvailable(
                        autoRedEye(),
                        autoFlash(),
                        torch(),
                        off()
                ))
                .frameProcessor(new SampleFrameProcessor())
                .logger(loggers(
                        logcat(),
                        fileLogger(this)
                ))
                .build();
    }

    private void takePicture() {
        try {
            PhotoResult photoResult = fotoapparatSwitcher.getCurrentFotoapparat().takePicture();
            File file = new File(mImagePath);
            PendingResult<Void> pendingResult = photoResult.saveToFile(file);
            pendingResult.whenDone(aVoid -> {
                setResult(RESULT_OK);
                finish();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        fotoapparatSwitcher.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        fotoapparatSwitcher.stop();
    }

    private class SampleFrameProcessor implements FrameProcessor {
        @Override
        public void processFrame(Frame frame) {
            // Perform frame processing, if needed
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
