package app.profuturo.reclasificacion.com.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.profuturo.reclasificacion.com.ClientResultViewHolder;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.model.Client;

/**
 * Java class that defines how a list of Client
 * objects has to be rendered within a RecyclerView.
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 06/09/2018 - 02:05 PM
 */



public class ClientSearchResultsAdapter
        extends RecyclerView.Adapter<ClientResultViewHolder> implements
        ClientResultViewHolder.OnItemSelectionChanged {
    /**
     * List of client elements.
     */
    private List<Client> mClientList;

    /**
     * List that references all the ViewHolders for this RecyclerView adapter.
     */
    private List<ClientResultViewHolder> mViewHolders = new ArrayList<>();

    public ClientSearchResultsAdapter(List<Client> clientList) {
        mClientList = clientList;
    }

    /**
     * {@inheritDoc}
     */
    @NonNull
    @Override
    public ClientResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return insertViewHolder(new ClientResultViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_client_result,
                        parent, false), this));
    }

    /**
     * Checks if a given {@link ClientResultViewHolder} item already exists in the
     * ViewHolder items list, if it doesn't exists, adds it to the list.
     *
     * @param vh The ViewHolder item to be added.
     */
    private ClientResultViewHolder insertViewHolder(@NonNull ClientResultViewHolder vh) {
        if (!mViewHolders.contains(vh)) {
            mViewHolders.add(vh);
        }
        return vh;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBindViewHolder(@NonNull ClientResultViewHolder holder, int position) {
        holder.render(mClientList.get(position), false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getItemCount() {
        return mClientList == null ? 0 : mClientList.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onItemSelectionChanged(@NonNull ClientResultViewHolder vh, Boolean selection) {
        // Retrieve the clicked ViewHolder client.
        Client client = vh.getClient();
        // Iterate through the list of ViewHolders.
        for (ClientResultViewHolder cVh : mViewHolders) {
            // Get the current ViewHolder client object.
            Client cClient = cVh.getClient();
            // Check if the ViewHolder is the same as the one reporting the change.
            if (client.equals(cClient)) {
                // If it is, set the new selection change to said ViewHolder.
                cVh.render(cClient, selection);
            } else {
                // If not, deselect the ViewHolder.
                cVh.render(cClient, false);
            }
        }
    }
}