package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Model class that defines the JSON structure of a response form an agent information request.
 *
 * @author Antonio Acevedo
 * @since August 10th, 2018
 */

public class AgentData {
    @Expose
    @SerializedName("empleado")
    private AgentSession employee;

    public AgentData(AgentSession employee) {
        this.employee = employee;
    }

    public AgentSession getEmployee() {
        return employee;
    }

    public void setEmployee(AgentSession employee) {
        this.employee = employee;
    }
}
