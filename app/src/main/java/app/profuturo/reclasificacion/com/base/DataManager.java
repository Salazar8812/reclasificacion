package app.profuturo.reclasificacion.com.base;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmResults;

public class DataManager extends  BaseDataManager{
    public Transactions mTransactions;

    private DataManager() {
        super();
        mTransactions = new Transactions(mRealm);
    }

    public static DataManager open() {
        return new DataManager();
    }

    public static DataManager validateConnection(DataManager manager) {
        if (manager == null || manager.isClosed()) manager = DataManager.open();
        return manager;
    }

    public void tx(DataManager.TX tx) {
        mRealm.executeTransaction(realm -> tx.execute(mTransactions));
    }

    public void deleteAll() {
        tx(tx -> mRealm.deleteAll());
    }

    /////////////////////////// CATALOGS

    public interface TX {
        void execute(DataManager.Transactions tx);
    }

    public static class Transactions {

        public Realm realm;

        Transactions(Realm realm) {
            this.realm = realm;
        }

        public <T extends RealmModel> T save(T t) {
            return realm.copyToRealmOrUpdate(t);
        }

        public <T extends RealmModel> T saveModel(T t) {
            return realm.copyToRealmOrUpdate(t);
        }

        public <T extends RealmModel> void save(List<T> t) {
            for (T item : t) {
                save(item);
            }
        }

        public <T extends RealmModel> void delete(Class<T> tClass, String id) {
            RealmResults results = realm.where(tClass).equalTo("id", id).findAll();
            if (results != null) results.deleteAllFromRealm();
        }

        public <T extends RealmModel> void delete(Class<T> tClass) {
            realm.delete(tClass);
        }

        public <T extends RealmModel> void removeListInModelByAttribute(Class<T> tClass, String attribute,
                                                                        String value) {
            RealmResults results = realm.where(tClass).equalTo(attribute, value).findAll();
            if (results != null) results.deleteAllFromRealm();
        }

    }

}
