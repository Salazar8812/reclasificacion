package app.profuturo.reclasificacion.com.base.View;
import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Interface that defines which methods a class needs to have to be defined
 * as a View for this MVP implementation. This interface should be implemented
 * by Activities, Fragments, Custom Views or Widgets.
 *
 * @author Alfredo Bejarano
 * @since August 3rd, 2018 - 11:27 AM
 */
public interface BaseView {
    /**
     * This method should be used for assigning listeners or callbacks to
     * widgets used by this View layout and attaching a presenter class.
     */
    void setup();

    /**
     * This method should check which class the message parameter
     * is and notify to the user accordingly.
     *
     * @param message The data to be displayed to the user.
     */
    void displayMessage(@NonNull Object message);

    /**
     * Sets the visibility of a given loading view or indicator defines in this View.
     * <ul>
     * <li><b>true</b> should be used for displaying the loading indicator.</li>
     * <li><b>false</b> should be used for hiding the loading indicator.</li>
     * </ul>
     *
     * @param displayView Boolean value for displaying or hiding the view.
     */
    void displayLoadingView(@NonNull Boolean displayView);

    /**
     * Method that will define this view as a Context instance.
     *
     * @return The context in one the view is currently existing.
     */
    @NonNull
    Context asContext();
}
