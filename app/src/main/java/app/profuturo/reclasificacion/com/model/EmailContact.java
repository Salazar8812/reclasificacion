package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;

public class EmailContact extends ClientContact implements RealmModel {
    @Expose
    @SerializedName("email")
    private String address;
    @Expose
    @SerializedName("tipoMail")
    private String type;

    public EmailContact() {
        // Empty method scrub!
    }

    public EmailContact(boolean valid, boolean prefereed, String address, String type) {
        super(valid, prefereed);
        this.address = address;
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public String getType() {
        return type;
    }
}
