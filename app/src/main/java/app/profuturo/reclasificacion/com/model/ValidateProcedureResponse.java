package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.SerializedName;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;

public class ValidateProcedureResponse implements WSBaseResponseInterface {

    @SerializedName("status")
    public String status;

    @SerializedName("continuaProceso")
    public String continuarProceso;

}
