package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.SerializedName;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;

public class SendImageResponse implements WSBaseResponseInterface {
    @SerializedName("codigo")
    public String codigo;

    @SerializedName("clave")
    public String clave;

    @SerializedName("descripcion")
    public String descripcion;

    @SerializedName("mensaje")
    public String mensaje;

}
