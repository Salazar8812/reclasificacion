
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TipoSubcuenta {

    @SerializedName("idTipoSubcuenta")
    @Expose
    private Integer idTipoSubcuenta;
    @SerializedName("subcuenta")
    @Expose
    private Subcuenta subcuenta;
    @SerializedName("regimen")
    @Expose
    private Regimen regimen;
    @SerializedName("grupo")
    @Expose
    private Grupo grupo;
    @SerializedName("familia")
    @Expose
    private Familia familia;

    public Integer getIdTipoSubcuenta() {
        return idTipoSubcuenta;
    }

    public void setIdTipoSubcuenta(Integer idTipoSubcuenta) {
        this.idTipoSubcuenta = idTipoSubcuenta;
    }

    public Subcuenta getSubcuenta() {
        return subcuenta;
    }

    public void setSubcuenta(Subcuenta subcuenta) {
        this.subcuenta = subcuenta;
    }

    public Regimen getRegimen() {
        return regimen;
    }

    public void setRegimen(Regimen regimen) {
        this.regimen = regimen;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Familia getFamilia() {
        return familia;
    }

    public void setFamilia(Familia familia) {
        this.familia = familia;
    }

}
