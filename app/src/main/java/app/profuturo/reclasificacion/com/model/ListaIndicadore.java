
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class ListaIndicadore extends RealmObject {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("descripcion")
    @Expose
    public String descripcion;

}
