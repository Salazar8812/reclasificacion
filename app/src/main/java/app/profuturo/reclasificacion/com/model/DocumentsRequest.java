package app.profuturo.reclasificacion.com.model;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseRequestInterface;

public class DocumentsRequest implements WSBaseRequestInterface
{
    public String clasificacionRetiro;
    public String tipoRetiro;
    public String numeroBeneficiarios;

    public DocumentsRequest(String clasificacionRetiro, String tipoRetiro, String numeroBeneficiarios) {
        this.clasificacionRetiro = clasificacionRetiro;
        this.tipoRetiro = tipoRetiro;
        this.numeroBeneficiarios = numeroBeneficiarios;
    }
}
