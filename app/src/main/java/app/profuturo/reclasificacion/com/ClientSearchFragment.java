package app.profuturo.reclasificacion.com;

import android.view.View;
import app.profuturo.reclasificacion.com.base.BaseFragment;
import app.profuturo.reclasificacion.com.databinding.FragmentClientSearchBinding;
import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.model.SaldoBean;
import app.profuturo.reclasificacion.com.presenter.ClientPresenter;
import app.profuturo.reclasificacion.com.widget.NavigationViewPager;

/**
 * Simple {@link BaseFragment} class that asks for a criteria to perform a clients search.
 */

public class ClientSearchFragment extends NavigationFragment<FragmentClientSearchBinding>{

    public ClientSearchFragment() {
        // Required empty public constructor
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer getLayoutId() {
        // The layout for this fragment
        return R.layout.fragment_client_search;
    }


   private View.OnClickListener searchClientListener = (view) -> {
        // Call the presenter to determine if any of the criteria is complete and get the first one
        String validCriteria = getPresenter(ClientPresenter.class).getFirstValidCriteria(
                getDataBinding().accountNumber.getValue().toString(),
                getDataBinding().nssField.getValue().toString(),
                getDataBinding().curpField.getValue().toString()
        );
        // Check the criteria is not null which means is valid
        if (validCriteria != null) {
            // Replace this fragment with the ClientResultsFragment
            getViewPager().setAllowedDirection(NavigationViewPager.SwipeDirection.SWIPE_NONE);
            if (getAdapter() != null) {
                getAdapter().setNextFragment(ClientResultFragment.newInstance(validCriteria));
                getViewPager().setCurrentItem(getViewPager().getCurrentItem() + 1, true);
            }
        } else {
            // Clear the input fields.
            clearFields();
            // Display message to the agent to introduce a valida criteria
            displayMessage(R.string.capture_at_least_a_valid_criteria);
        }
    };

    private void clearFields() {
        // Clear the NSS field
        getDataBinding().nssField.setText("");
        // Clear the curp field
        getDataBinding().curpField.setText("");
        // Clear the account number field
        getDataBinding().accountNumber.setText("");
    }

    /**
     * Interface that defines a method that has to handle
     * something to do with the clicked client from a list.
     */
    public interface ClientResultInteraction {
        /**
         * Method that depending of the implementation,
         * will operate with the clicked client object.
         *
         * @param client The client object that has been selected from the list.
         */
        void onClientSelected(Client client, Boolean selected, Boolean clicked);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void setup() {
        getViewPager().setAllowedDirection(NavigationViewPager.SwipeDirection.SWIPE_NONE);
        // Prevent the soft input keyboard from being displayed when launching this fragment.
        getDataBinding().title.requestFocus();
        // Attach the necessary presenters to this fragment.
        attachPresenters(new ClientPresenter(this));
        // Assign listener to button search of client
        getDataBinding().buttonSearch.setOnClickListener(searchClientListener);
    }
}

