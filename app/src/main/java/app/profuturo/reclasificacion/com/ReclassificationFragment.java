package app.profuturo.reclasificacion.com;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import app.profuturo.reclasificacion.com.Adapter.SalesAdapter;
import app.profuturo.reclasificacion.com.Background.WSManager;
import app.profuturo.reclasificacion.com.Implementation.BasePresenterC;
import app.profuturo.reclasificacion.com.Repository.local.ClientDAO;
import app.profuturo.reclasificacion.com.Utils.InputFilterMinMax;
import app.profuturo.reclasificacion.com.Utils.StringsKeys;
import app.profuturo.reclasificacion.com.databinding.FragmentCaptureReclasificacionBinding;
import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.model.DatesSubAccounts;
import app.profuturo.reclasificacion.com.model.IndicatorActionValueResponse;
import app.profuturo.reclasificacion.com.model.IndicatorsViews;
import app.profuturo.reclasificacion.com.model.ListReclasificacion;
import app.profuturo.reclasificacion.com.model.Saldo;
import app.profuturo.reclasificacion.com.model.SalesFormat;
import app.profuturo.reclasificacion.com.model.SubAccountsMatrixReclasification;
import app.profuturo.reclasificacion.com.presenter.SalesPresenter;
import app.profuturo.reclasificacion.com.widget.NavigationViewPager;

public class ReclassificationFragment extends NavigationFragment<FragmentCaptureReclasificacionBinding> implements SalesPresenter.CallbackListSales, View.OnClickListener, SalesAdapter.SalesResultInteraction {

    private LinearLayout mPorcentRB;
    private LinearLayout mAmmountRB;
    private ImageView mPorcentImageView;
    private ImageView mAmountlImageView;
    private EditText mAmmountParcialET;
    private TextView client_name_account_number;
    private TextView mTypeSubAccountOneTextView;
    private EditText mAmmountSubAccountOneTextView;
    private TextView mSimbolTextOneView;
    private TextView mTypeSubAccountTwoTextView;
    private EditText mAmmountSubAccountTwoTextView;
    private TextView mSimbolTwoTextView;
    private TextView mTypeSubAccountThreeTextView;
    private TextView mSimbolThreeTextView;
    private EditText mAmmountSubAccountThreeTextView;
    private TextView mSimbolFourTextView;
    private TextView mTypeSubAccountFourTextView;
    private EditText mAmmountSubAccountFourTextView;
    private EditText mAmmountFive;

    private ImageView mParcialAmmountImageView;
    private ImageView mTotalAmmountImageView;
    private LinearLayout mParcialLinearLayout;
    private LinearLayout mTotalLinearLayout;

    private SalesPresenter presenter;
    private SalesAdapter mSalesAdapter;
    private List<IndicatorsViews> mListIndicatorViews = new ArrayList<>();
    private List<SubAccountsMatrixReclasification> listSubAccountsMat = new ArrayList<>();
    private String mTotalAmmount;
    private String mAccountNumber;
    private Double mAmmountOne = 0.0;
    private Double mAmmountTwo = 0.0;
    private Double mAmmountThree = 0.0;
    private Double mAmmountFour = 0.0;
    private Double mTotalSumAmmount = 0.0;
    private Double mParcialAmmount = 0.0;
    private Button mNextButton;
    private String mAmmountCalculatePurchased="0.0";

    private boolean isPorcent = false;
    private boolean isAmmount = false;

    private static Double TOTAL_PORCENT = 100.0;
    public String mTypeReclasification = "";
    public SalesFormat mSalesFormat;
    public List<Saldo> listSaleOrigin = new ArrayList<>();
    public List<ListReclasificacion> listReaclasification= new ArrayList<>();
    public SubAccountsMatrixReclasification mSubAccountsMatrixReclasification;
    public boolean isPartial = false;

    private int mCont = 0;

    @Override
    protected Integer getLayoutId() {
        return R.layout.fragment_capture_reclasificacion;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setup() {
        cleanCache();
        getDataBinding().mNextButton.setOnClickListener(this);
        getDataBinding().cancel.setOnClickListener(this);

        bindViews();
        setListener();
        configureRecycler();
        retrieveClientFromInternalStorage();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        cleanFields();
        mAmmountParcialET.setText("");
    }

    public void setListener(){
        mPorcentRB.setOnClickListener(this);
        mAmmountRB.setOnClickListener(this);
        mTotalLinearLayout.setOnClickListener(this);
        mParcialLinearLayout.setOnClickListener(this);
        mTotalLinearLayout.setEnabled(false);
        mParcialLinearLayout.setEnabled(false);
        mAmmountRB.setEnabled(false);
        mPorcentRB.setEnabled(false);
    }

    public void bindViews(){
        mNextButton = getView().findViewById(R.id.mNextButton);
        mPorcentRB = getView().findViewById(R.id.mPorcentRB);
        mAmmountRB = getView().findViewById(R.id.mAmmountRB);
        mAmmountFive = getView().findViewById(R.id.mAmmountFive);
        mAmountlImageView = getView().findViewById(R.id.mAmountlImageView);
        mPorcentImageView = getView().findViewById(R.id.mPorcentImageView);
        mAmmountParcialET = getView().findViewById(R.id.mAmmountParcialET);

        mAmmountParcialET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String mAmmount = "0";
                mAmmount = mAmmount + charSequence;
                mAmmount = mAmmount.equals("") || mAmmount.isEmpty() ? "0.0" : mAmmount;
                mParcialAmmount = Double.parseDouble(mAmmount);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    if(isPorcent) {
                        Double mTotal = Double.parseDouble(mTotalAmmount);
                        if (mParcialAmmount > mTotal) {
                            presenter.sendMessageSales("El monto parcial, no debe ser mayor al total seleccionado");
                            mAmmountRB.setEnabled(false);
                            mPorcentRB.setEnabled(false);
                        } else {
                            mAmmountRB.setEnabled(true);
                            mPorcentRB.setEnabled(true);
                        }
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        });

        mTypeSubAccountOneTextView = getView().findViewById(R.id.mTypeSubAccountOneTextView);
        mAmmountSubAccountOneTextView = getView().findViewById(R.id.mAmmountSubAccountOneTextView);
        mSimbolTextOneView = getView().findViewById(R.id.mSimbolTextOneView);

        mAmmountSubAccountOneTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String mAmmount = "0";
                mAmmount = mAmmount + charSequence;
                mAmmount = mAmmount.equals("") || mAmmount.isEmpty() ? "0.0" : mAmmount;
                mAmmountOne = Double.parseDouble(mAmmount);

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mAmmountTwo = Double.parseDouble(!mAmmountSubAccountTwoTextView.getText().toString().equals("") ? mAmmountSubAccountTwoTextView.getText().toString() : "0.0" );
                mAmmountThree = Double.parseDouble(!mAmmountSubAccountThreeTextView.getText().toString().equals("") ? mAmmountSubAccountThreeTextView.getText().toString() : "0.0" );
                mAmmountFour = Double.parseDouble(!mAmmountSubAccountFourTextView.getText().toString().equals("") ? mAmmountSubAccountFourTextView.getText().toString() : "0.0" );


                mTotalSumAmmount = mAmmountTwo + mAmmountThree + mAmmountFour + mAmmountOne;

                mAmmountFive.setText(String.valueOf(mTotalSumAmmount));

                if(isPorcent){
                    compareSalePorcent(mTotalSumAmmount);
                }else{
                    compareSalesAmmount(mTotalSumAmmount);
                }
            }
        });

        mListIndicatorViews.add(new IndicatorsViews(mTypeSubAccountOneTextView,mAmmountSubAccountOneTextView,mSimbolTextOneView));

        mTypeSubAccountTwoTextView = getView().findViewById(R.id.mTypeSubAccountTwoTextView);
        mAmmountSubAccountTwoTextView = getView().findViewById(R.id.mAmmountSubAccountTwoTextView);
        mSimbolTwoTextView = getView().findViewById(R.id.mSimbolTwoTextView);

        mAmmountSubAccountTwoTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String mAmmount = "0";
                mAmmount = mAmmount + charSequence;
                mAmmount = mAmmount.equals("") || mAmmount.isEmpty() ? "0.0" : mAmmount ;
                mAmmountTwo = Double.parseDouble(mAmmount);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mAmmountOne = Double.parseDouble(!mAmmountSubAccountOneTextView.getText().toString().equals("") ? mAmmountSubAccountOneTextView.getText().toString() : "0.0" );
                mAmmountThree = Double.parseDouble(!mAmmountSubAccountThreeTextView.getText().toString().equals("") ? mAmmountSubAccountThreeTextView.getText().toString() : "0.0" );
                mAmmountFour = Double.parseDouble(!mAmmountSubAccountFourTextView.getText().toString().equals("") ? mAmmountSubAccountFourTextView.getText().toString() : "0.0" );

                mTotalSumAmmount = mAmmountTwo + mAmmountThree + mAmmountFour + mAmmountOne;

                mAmmountFive.setText(String.valueOf(mTotalSumAmmount));
                if(isPorcent){
                    compareSalePorcent(mTotalSumAmmount);
                }else{
                    compareSalesAmmount(mTotalSumAmmount);
                }
            }
        });

        mListIndicatorViews.add(new IndicatorsViews(mTypeSubAccountTwoTextView,mAmmountSubAccountTwoTextView,mSimbolTwoTextView));

        mTypeSubAccountThreeTextView = getView().findViewById(R.id.mTypeSubAccountThreeTextView);
        mSimbolThreeTextView = getView().findViewById(R.id.mSimbolThreeTextView);
        mAmmountSubAccountThreeTextView = getView().findViewById(R.id.mAmmountSubAccountThreeTextView);

        mAmmountSubAccountThreeTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String mAmmount = "0";
                mAmmount = mAmmount + charSequence;
                mAmmount = mAmmount.equals("") || mAmmount.isEmpty() ? "0.0" : mAmmount;
                mAmmountThree = Double.parseDouble(mAmmount);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mAmmountOne = Double.parseDouble(!mAmmountSubAccountOneTextView.getText().toString().equals("") ? mAmmountSubAccountOneTextView.getText().toString() : "0.0" );
                mAmmountTwo = Double.parseDouble(!mAmmountSubAccountTwoTextView.getText().toString().equals("") ? mAmmountSubAccountTwoTextView.getText().toString() : "0.0" );
                mAmmountFour = Double.parseDouble(!mAmmountSubAccountFourTextView.getText().toString().equals("") ? mAmmountSubAccountFourTextView.getText().toString() : "0.0" );

                mTotalSumAmmount = mAmmountTwo + mAmmountThree + mAmmountFour + mAmmountOne;

                mAmmountFive.setText(String.valueOf(mTotalSumAmmount));
                if(isPorcent){
                    compareSalePorcent(mTotalSumAmmount);
                }else{
                    compareSalesAmmount(mTotalSumAmmount);
                }
            }
        });

        mListIndicatorViews.add(new IndicatorsViews(mTypeSubAccountThreeTextView,mAmmountSubAccountThreeTextView,mSimbolThreeTextView));

        mSimbolFourTextView = getView().findViewById(R.id.mSimbolFourTextView);
        mTypeSubAccountFourTextView = getView().findViewById(R.id.mTypeSubAccountFourTextView);
        mAmmountSubAccountFourTextView = getView().findViewById(R.id.mAmmountSubAccountFourTextView);

        mAmmountSubAccountFourTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String mAmmount = "0";
                mAmmount = mAmmount + charSequence;
                mAmmount = mAmmount.equals("") || mAmmount.isEmpty() ? "0.0" : mAmmount;
                mAmmountFour = Double.parseDouble(mAmmount);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mAmmountOne = Double.parseDouble(!mAmmountSubAccountOneTextView.getText().toString().equals("") ? mAmmountSubAccountOneTextView.getText().toString() : "0.0" );
                mAmmountTwo = Double.parseDouble(!mAmmountSubAccountTwoTextView.getText().toString().equals("") ? mAmmountSubAccountTwoTextView.getText().toString() : "0.0" );
                mAmmountThree = Double.parseDouble(!mAmmountSubAccountThreeTextView.getText().toString().equals("") ? mAmmountSubAccountThreeTextView.getText().toString() : "0.0" );


                mTotalSumAmmount = mAmmountTwo + mAmmountThree + mAmmountFour + mAmmountOne;

                mAmmountFive.setText(String.valueOf(mTotalSumAmmount));
                if(isPorcent){
                    compareSalePorcent(mTotalSumAmmount);
                }else{
                    compareSalesAmmount(mTotalSumAmmount);
                }
            }
        });

        mListIndicatorViews.add(new IndicatorsViews(mTypeSubAccountFourTextView,mAmmountSubAccountFourTextView,mSimbolFourTextView));

        mParcialAmmountImageView = getView().findViewById(R.id.mParcialAmmountImageView);
        mTotalAmmountImageView = getView().findViewById(R.id.mTotalAmmountImageView);

        mParcialLinearLayout = getView().findViewById(R.id.mParcialLinearLayout);
        mTotalLinearLayout = getView().findViewById(R.id.mTotalLinearLayout);

        client_name_account_number = getView().findViewById(R.id.client_name_account_number);
    }

    private void configureRecycler(){
        List<SalesFormat> list= new ArrayList<>();
        mSalesAdapter = new SalesAdapter(this);
        getDataBinding().documentsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        getDataBinding().documentsList.setAdapter(mSalesAdapter);
        mSalesAdapter.update(list);
    }

    public void retrieveClientFromInternalStorage(){
        // Read the client from the local storage database.
        Client client = ClientDAO.readClient();
        // Checks the client is not null
        if (client != null) {
            client_name_account_number.setText(client.getName()+" "+client.getLastName()+" "+client.getSurName()+" No. de cuenta "+client.getAccountNumber());
            mAccountNumber = client.getAccountNumber();
            callWS();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mParcialLinearLayout:
                    mAmmountParcialET.setText("");
                    mAmmountParcialET.setEnabled(true);
                    mTypeReclasification = StringsKeys.ID_PARCIAL;
                    mAmmountRB.setEnabled(true);
                    mPorcentRB.setEnabled(true);
                    isPartial = true;
                    mParcialAmmountImageView.setImageResource(R.drawable.ic_checked);
                    mTotalAmmountImageView.setImageResource(R.drawable.ic_uncheck_radio);
                    enableRadioButtons();
                    cleanFields();
                    mAmmountParcialET.setText("");

                break;

            case R.id.mTotalLinearLayout:
                    isPartial = false;
                    mTypeReclasification = StringsKeys.ID_TOTAL;
                    mAmmountRB.setEnabled(true);
                    mPorcentRB.setEnabled(true);
                    mAmmountParcialET.setEnabled(false);
                    try {
                        mAmmountParcialET.setText(String.format("%.2f", Double.parseDouble(mTotalAmmount)));
                    }catch (NullPointerException e){
                        e.printStackTrace();
                    }
                    mTotalAmmountImageView.setImageResource(R.drawable.ic_checked);
                    mParcialAmmountImageView.setImageResource(R.drawable.ic_uncheck_radio);
                    enableRadioButtons();
                    cleanFields();
                break;

            case R.id.mAmmountRB:
                setSimbol("$");
                isAmmount= true;
                isPorcent = false;
                setMaxValueAmmount();
                enableFields();
                cleanFields();
                mAmountlImageView.setImageResource(R.drawable.ic_checked);
                mPorcentImageView.setImageResource(R.drawable.ic_uncheck_radio);
                break;

            case R.id.mPorcentRB:
                setSimbol("%");
                isAmmount= false;
                isPorcent = true;
                setMaxValuePorcent();
                enableFields();
                cleanFields();
                mPorcentImageView.setImageResource(R.drawable.ic_checked);
                mAmountlImageView.setImageResource(R.drawable.ic_uncheck_radio);
                break;

            case R.id.cancel:
                startActivity(new Intent(getActivity(),CancelProcedureDialog.class));
                break;

            case R.id.mNextButton:
                calculateCharge(mTypeReclasification);
                verifiFieldEnableToCalculate();
                break;
            default:break;
        }
    }

    public void enableField(TextView mTitle, TextView mSimbol, EditText mAmmount){
        mTitle.setEnabled(true);
        mSimbol.setEnabled(true);
        if(isPorcent == true || isAmmount == true) {
            mAmmount.setEnabled(true);
        }
    }

    public void disableField(TextView mTitle, TextView mSimbol, EditText mAmmount){
        mTitle.setEnabled(false);
        mSimbol.setEnabled(false);
        mAmmount.setEnabled(false);
    }

    public void setSimbol(String simbol){
        getDataBinding().mSimbolFive.setText(simbol);
        mSimbolTextOneView.setText(simbol);
        mSimbolTwoTextView.setText(simbol);
        mSimbolThreeTextView.setText(simbol);
        mSimbolFourTextView.setText(simbol);
    }

    public void enableRadioButtons(){
        mAmmountRB.setEnabled(true);
        mPorcentRB.setEnabled(true);
    }

    public void callWS(){
        presenter.getSales(mAccountNumber,"147");
    }

    @Override
    public BasePresenterC getPresenterC() {
        presenter = new SalesPresenter((AppCompatActivity) getActivity(),this,this);
        return presenter;
    }

    @Override
    public void getListSales(List<SalesFormat> saleResponse) {
        mSalesAdapter.update(saleResponse);
    }

    @Override
    public void onSaleSelected(SalesFormat mSaldoBean, Boolean selected, Boolean clicked) {
        Log.e("SaleSelected", mSaldoBean.idSale);
        mTotalAmmount = mSaldoBean.saleAmmount;
        mSalesFormat = mSaldoBean;
        setButton();
        disableFields();
        callMatrixWS(mSaldoBean.idSale,mSaldoBean.listDates);
    }

    public void callMatrixWS(String mSubAccountGroup, List<DatesSubAccounts> list){
        presenter.consultingMatrix(mSubAccountGroup, list);
    }

    public void setButton(){
        mParcialLinearLayout.setEnabled(true);
        mTotalLinearLayout.setEnabled(true);
        mAmmountRB.setEnabled(false);
        mPorcentRB.setEnabled(false);
        mAmountlImageView.setImageResource(R.drawable.ic_uncheck_radio);
        mPorcentImageView.setImageResource(R.drawable.ic_uncheck_radio);
        mParcialAmmountImageView.setImageResource(R.drawable.ic_uncheck_radio);
        mTotalAmmountImageView.setImageResource(R.drawable.ic_uncheck_radio);
        mAmmountParcialET.setText("");
        cleanFields();
        disableFields();
        setFlags();
    }

    public void setFlags(){
        isPorcent = false;
        isAmmount = false;
    }

    public void cleanFields(){
        mAmmountSubAccountOneTextView.setText("");
        mAmmountSubAccountTwoTextView.setText("");
        mAmmountSubAccountThreeTextView.setText("");
        mAmmountSubAccountFourTextView.setText("");
    }

    public void disableFields(){
        mAmmountSubAccountOneTextView.setEnabled(false);
        mAmmountSubAccountTwoTextView.setEnabled(false);
        mAmmountSubAccountThreeTextView.setEnabled(false);
        mAmmountSubAccountFourTextView.setEnabled(false);
    }

    @Override
    public void OnGetSubAccountIndicators(List<SubAccountsMatrixReclasification> list) {
        listSubAccountsMat = list;
        enableFields();
    }

    @Override
    public void OnGetActionValue(IndicatorActionValueResponse mIndicatorActionValueResponse) {
        try {
            if (isPorcent) {
                calculatePaymentPorcent(mIndicatorActionValueResponse.listActionValue.get(0).valorAccion, mAmmountCalculatePurchased);
            } else {
                calculatePaymentAmmount(mIndicatorActionValueResponse.listActionValue.get(0).valorAccion, mAmmountCalculatePurchased);
            }
        }catch (IndexOutOfBoundsException e){
            e.printStackTrace();
        }

        if(mCont >=3){
             presenter.registerReclasification("7","130","462","509","21","3",mTypeReclasification,mTotalAmmount);
                presenter.saveListReclasification(listReaclasification);
                presenter.saveListSaleOrigin(listSaleOrigin);
                if (getAdapter() != null) {
                    getAdapter().setNextFragment(new CaptureDocumentsFragment());
                    getViewPager().setCurrentItem(getViewPager().getCurrentItem() + 1, true);
                    getViewPager().setAllowedDirection(NavigationViewPager.SwipeDirection.SWIPE_ALL);
                }
        }
    }

    public void enableFields(){
        for(int i = 0; i < listSubAccountsMat.size() ; i++){
            if(listSubAccountsMat.get(i).aplica.equals("true")){
                int finalI = i;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        enableField(mListIndicatorViews.get(finalI).mTitle, mListIndicatorViews.get(finalI).mSimbol, mListIndicatorViews.get(finalI).mBoxEdith);
                    }
                });
            }else{
                int finalI1 = i;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        disableField(mListIndicatorViews.get(finalI1).mTitle, mListIndicatorViews.get(finalI1).mSimbol, mListIndicatorViews.get(finalI1).mBoxEdith);
                    }
                });
            }

            int finalI2 = i;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mListIndicatorViews.get(finalI2).mTitle.setText(listSubAccountsMat.get(finalI2).descripcion);
                }
            });
        }
    }

    @SuppressLint("ResourceAsColor")
    public void compareSalesAmmount(Double saleReclasification){
        Double mTotal;
        Double mAmmountCaptured = Double.parseDouble(String.format("%.1f", saleReclasification));
        try {
            if(isPartial) {
                mTotal  = Double.parseDouble(String.format("%.1f", Double.parseDouble(mAmmountParcialET.getText().toString())));
            }else{
                mTotal = Double.parseDouble(String.format("%.1f", Double.parseDouble(mTotalAmmount)));
            }
            if (mAmmountCaptured > mTotal) {
                mNextButton.setEnabled(false);
                int color = Color.parseColor("#FFC1C2C2");
                mNextButton.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
                mNextButton.setBackgroundColor(Color.parseColor("#FFC1C2C2"));
                presenter.sendMessageSales("La reclasificacion debe ser igual al monto total o parcial");
            } else if (mTotal.equals(mAmmountCaptured)) {
                mNextButton.setEnabled(true);
                int color = Color.parseColor("#194178");
                mNextButton.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
                mNextButton.setBackgroundColor(Color.parseColor("#194178"));
            }else if (mAmmountCaptured < mTotal){
                mNextButton.setEnabled(false);
                int color = Color.parseColor("#FFC1C2C2");
                mNextButton.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
                mNextButton.setBackgroundColor(Color.parseColor("#FFC1C2C2"));
            }
        }catch (NullPointerException | NumberFormatException e){
            e.printStackTrace();
        }
    }

    @SuppressLint("ResourceAsColor")
    public void compareSalePorcent(Double saleReclasification){
        try {
            if (saleReclasification > TOTAL_PORCENT) {
                mNextButton.setEnabled(false);
                int color = Color.parseColor("#FFC1C2C2");
               // mNextButton.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
                mNextButton.setBackgroundColor(Color.parseColor("#FFC1C2C2"));
                presenter.sendMessageSales("La reclasificacion debe ser igual al monto total o parcial");
            } else if (TOTAL_PORCENT.equals(saleReclasification)) {
                mNextButton.setEnabled(true);
                int color = Color.parseColor("#194178");
                //mNextButton.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
                mNextButton.setBackgroundColor(Color.parseColor("#194178"));
            }else if(saleReclasification < TOTAL_PORCENT){
                mNextButton.setEnabled(false);
                int color = Color.parseColor("#FFC1C2C2");
                // mNextButton.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
                mNextButton.setBackgroundColor(Color.parseColor("#FFC1C2C2"));
            }
        }catch (NullPointerException | NumberFormatException e){
            e.printStackTrace();
        }
    }

    public void verifiFieldEnableToCalculate(){
        if (mAmmountSubAccountOneTextView.isEnabled()) {
            if (!mAmmountSubAccountOneTextView.getText().toString().equals("") || !mAmmountSubAccountOneTextView.getText().toString().isEmpty()) {
                mAmmountCalculatePurchased = mAmmountSubAccountOneTextView.getText().toString();
                presenter.getActionValue(listSubAccountsMat.get(0).siefore, getDateAction(), "");
                mSubAccountsMatrixReclasification = listSubAccountsMat.get(0);
                mCont = mCont + 1;
            } else {
                mCont = mCont + 1;
            }
        }else{
            mCont = mCont + 1;
        }

        if (mTypeSubAccountTwoTextView.isEnabled()) {
            if (mAmmountSubAccountTwoTextView.getText().toString().equals("") || mAmmountSubAccountTwoTextView.getText().toString().isEmpty()) {
                mAmmountCalculatePurchased = mAmmountSubAccountTwoTextView.getText().toString();
                presenter.getActionValue(listSubAccountsMat.get(1).siefore, getDateAction(), "");
                mSubAccountsMatrixReclasification = listSubAccountsMat.get(1);
                mCont = mCont + 1;
            } else {
                mCont = mCont + 1;
            }
        }
        if (mAmmountSubAccountThreeTextView.isEnabled()) {
            if (mAmmountSubAccountThreeTextView.getText().toString().equals("") || mAmmountSubAccountThreeTextView.getText().toString().isEmpty()) {
                mAmmountCalculatePurchased = mAmmountSubAccountThreeTextView.getText().toString();
                presenter.getActionValue(listSubAccountsMat.get(2).siefore, getDateAction(), "");
                mSubAccountsMatrixReclasification = listSubAccountsMat.get(2);
                mCont = mCont + 1;
            } else {
                mCont = mCont + 1;
            }
        }else{
            mCont = mCont + 1;
        }

        if (mAmmountSubAccountFourTextView.isEnabled()) {
            if (mAmmountSubAccountFourTextView.getText().toString().isEmpty() || mAmmountSubAccountFourTextView.getText().toString().equals("")) {
                mAmmountCalculatePurchased = mAmmountSubAccountFourTextView.getText().toString();
                presenter.getActionValue(listSubAccountsMat.get(3).siefore, getDateAction(), "");
                mSubAccountsMatrixReclasification = listSubAccountsMat.get(3);
                mCont = mCont + 1;
            } else {
                mCont = mCont + 1;
            }
        }else{
            mCont = mCont + 1;
        }
    }

    public void calculateCharge(String typeReclasification){
            if(mSalesFormat.mListSaldoSubAccount.size() <= 1) {
                if (Double.parseDouble(mSalesFormat.mListSaldoSubAccount.get(0).getSaldoAlDiaPesos()) > Double.parseDouble(mTotalAmmount)) {
                    Double saldoTotalSubcuenta = Double.parseDouble(mSalesFormat.mListSaldoSubAccount.get(0).getSaldoAlDiaPesos());
                    Double montoParcial = Double.parseDouble(mTotalAmmount);
                    Double SaldoDescontado = saldoTotalSubcuenta - montoParcial;
                    createSaldoOrigin(mSalesFormat.mListSaldoSubAccount.get(0).getSaldoAlDiaAcciones(),String.valueOf(montoParcial),mSalesFormat.mListSaldoSubAccount.get(0).getSiefore().getSiefore().getIdSiefore(),mSalesFormat.mListSaldoSubAccount.get(0).getIdSubcuenta());
                }
            }else{
                if (Double.parseDouble(mSalesFormat.mListSaldoSubAccount.get(0).getSaldoAlDiaPesos()) < Double.parseDouble(mTotalAmmount)) {
                    Double saldoTotalPrimerSubcuenta = Double.parseDouble(mSalesFormat.mListSaldoSubAccount.get(0).getSaldoAlDiaPesos());
                    Double montoParcial = Double.parseDouble(mTotalAmmount);
                    Double SaldoRestanteParcial = montoParcial - saldoTotalPrimerSubcuenta;

                    createSaldoOrigin(mSalesFormat.mListSaldoSubAccount.get(0).getSaldoAlDiaAcciones(),String.valueOf(saldoTotalPrimerSubcuenta),mSalesFormat.mListSaldoSubAccount.get(0).getSiefore().getSiefore().getIdSiefore(),mSalesFormat.mListSaldoSubAccount.get(0).getIdSubcuenta());

                    Double saldoTotalSegundaSubcuenta = Double.parseDouble(mSalesFormat.mListSaldoSubAccount.get(1).getSaldoAlDiaPesos());
                    Double SaldoDecontado = saldoTotalSegundaSubcuenta - SaldoRestanteParcial;
                    createSaldoOrigin(mSalesFormat.mListSaldoSubAccount.get(1).getSaldoAlDiaAcciones(),String.valueOf(SaldoRestanteParcial),mSalesFormat.mListSaldoSubAccount.get(1).getSiefore().getSiefore().getIdSiefore(),mSalesFormat.mListSaldoSubAccount.get(1).getIdSubcuenta());

                }else{
                    Double saldoTotalSubcuenta = Double.parseDouble(mSalesFormat.mListSaldoSubAccount.get(0).getSaldoAlDiaPesos());
                    Double montoParcial = Double.parseDouble(mTotalAmmount);
                    Double SaldoDescontado = saldoTotalSubcuenta - montoParcial;
                    createSaldoOrigin(mSalesFormat.mListSaldoSubAccount.get(0).getSaldoAlDiaAcciones(),String.valueOf(montoParcial),mSalesFormat.mListSaldoSubAccount.get(0).getSiefore().getSiefore().getIdSiefore(),mSalesFormat.mListSaldoSubAccount.get(0).getIdSubcuenta());
                }
            }
    }

    public void createSaldoOrigin(String actionValue, String ammount,String siefore,String idSubcuenta){
        Double actionSold = calculateActionSold(actionValue,ammount);
        Saldo mSaldo = new Saldo();
        mSaldo.acciones = String.valueOf(actionSold);
        mSaldo.idSiefore = siefore;
        mSaldo.idSubcta = idSubcuenta;
        mSaldo.idTipoMov = "180";
        mSaldo.idValorAccion = actionValue;
        mSaldo.pesos = ammount;
        mSaldo.porcentaje = "";
        listSaleOrigin.add(mSaldo);
    }

    public void calculatePaymentAmmount(String actionValue, String ammount){
        Double actionPurchase = calcultaeActionPurchased(actionValue, ammount);

        ListReclasificacion item = new ListReclasificacion(String.valueOf(actionPurchase),
                mSubAccountsMatrixReclasification.siefore,
                mSubAccountsMatrixReclasification.listSubAccountApovol.get(0).subCuentaApovol,
                "181",
                actionValue,
                ammount,
                "",
                mSubAccountsMatrixReclasification.tipoSubcuenta);

        listReaclasification.add(item);
    }

    public void calculatePaymentPorcent(String actionValue, String porcent){
        Double mAmmount = Double.parseDouble(mTotalAmmount);
        Double mPorcent = Double.parseDouble(porcent);

        Double mAmmountToPurchase =  mAmmount / mPorcent;


        Double actionPurchase = calcultaeActionPurchased(actionValue,String.valueOf(mAmmountToPurchase));

        ListReclasificacion item = new ListReclasificacion(String.valueOf(actionPurchase),
                mSubAccountsMatrixReclasification.siefore,
                mSubAccountsMatrixReclasification.listSubAccountApovol.get(0).subCuentaApovol,
                "181",
                actionValue,
                String.valueOf(mAmmountToPurchase),
                "",
                mSubAccountsMatrixReclasification.tipoSubcuenta);

        listReaclasification.add(item);
    }

    public Double calculateActionSold(String actionValue, String ammount){
        Double actionV = Double.parseDouble(actionValue.equals("") ? "0.0" : actionValue);
        Double ammountTotal = Double.parseDouble(ammount.equals("")? "0.0" : ammount);
        Double actionValueSold = ammountTotal / actionV ;

        return actionValueSold;
    }

    public Double calcultaeActionPurchased(String actionValue, String ammount){
        Double actionV = Double.parseDouble(actionValue.equals("") ? "0.0" : actionValue);
        Double ammountTotal = Double.parseDouble(ammount.equals("") ? "0.0" : ammount);
        Double actionValuePurchased = actionV * ammountTotal;

        return actionValuePurchased;
    }

    private String getDateAction() {
        Calendar cal = Calendar.getInstance();

        String mAnio = String.valueOf(cal.get(Calendar.YEAR));
        String mMont = String.valueOf(cal.get(Calendar.MONTH)+1);
        String mDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));

        return mAnio+"-"+mMont+"-"+mDay;
    }

    public void setMaxValuePorcent(){
        mAmmountSubAccountOneTextView.setFilters(new InputFilter[]{ new InputFilterMinMax("1", "100")});
        mAmmountSubAccountTwoTextView.setFilters(new InputFilter[]{ new InputFilterMinMax("1", "100")});
        mAmmountSubAccountThreeTextView.setFilters(new InputFilter[]{ new InputFilterMinMax("1", "100")});
        mAmmountSubAccountFourTextView.setFilters(new InputFilter[]{ new InputFilterMinMax("1", "100")});
    }

    public void setMaxValueAmmount(){
        double mMaxAmmount = Double.parseDouble(mTotalAmmount);
        mAmmountSubAccountOneTextView.setFilters(new InputFilter[] {});
        mAmmountSubAccountTwoTextView.setFilters(new InputFilter[] {});
        mAmmountSubAccountThreeTextView.setFilters(new InputFilter[] {});
        mAmmountSubAccountFourTextView.setFilters(new InputFilter[] {});
    }

    public void cleanCache(){
        mCont = 0;
        mTotalAmmount = "0.0";
    }

}
