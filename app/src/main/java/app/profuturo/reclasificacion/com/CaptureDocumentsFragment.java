package app.profuturo.reclasificacion.com;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.GetChars;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import app.profuturo.reclasificacion.com.Adapter.DocumentListAdapter;
import app.profuturo.reclasificacion.com.Background.WSManager;
import app.profuturo.reclasificacion.com.Implementation.BasePresenterC;
import app.profuturo.reclasificacion.com.Implementation.CaptureDocumentsPresenter;
import app.profuturo.reclasificacion.com.Repository.local.ClientDAO;
import app.profuturo.reclasificacion.com.Utils.CapturePhotoActivity;
import app.profuturo.reclasificacion.com.Utils.StringsKeys;
import app.profuturo.reclasificacion.com.base.BaseFragment;
import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.model.DocumentosACapturar;
import app.profuturo.reclasificacion.com.model.DocumentsResponse;
import app.profuturo.reclasificacion.com.model.Respuestum;
import app.profuturo.reclasificacion.com.widget.NavigationViewPager;

public class CaptureDocumentsFragment extends NavigationFragment implements View.OnClickListener, CaptureDocumentsPresenter.GetDocumentsCallback,DocumentListAdapter.DocumentsResultInteraction {
    private Button mNextButton;
    private Button cancelButton;
    private TextView client_name_account_number;
    private RecyclerView mDocumentListRecyclerView;
    private DocumentListAdapter mDocumentListAdapter;
    public static List<Respuestum> mListDocuemnts = new ArrayList<>();
    private CaptureDocumentsPresenter mCaptureDocumentsPresenter;
    private static final int CAMERA_PHOTO = 111;
    private Uri imageToUploadUri;
    private ImageView mPreviewPhotoImageView;
    private String pathTemp= "";
    private String idPhotoSelected = "";
    private WebView webView;
    private String idContract = "";
    List<Respuestum> mListDocumentsStatusPhoto = new ArrayList<>();

    @Override
    protected Integer getLayoutId() {
        return R.layout.fragment_capture_documents;
    }

    @Override
    public void setup() {
        bindViews();
        retrieveClientFromInternalStorage();
        configureRecycler();
        cosumingWS();
    }

    public void loadPDF(){
        webView.getSettings().setJavaScriptEnabled(true);
        String pdf = "https://www.google.com/";
        webView.loadUrl(pdf);
    }

    public void bindViews(){
        webView = getView().findViewById(R.id.mPdfPreviewWebView);
        client_name_account_number = getView().findViewById(R.id.client_name_account_number);
        mDocumentListRecyclerView = getView().findViewById(R.id.mDocumentListRecyclerView);
        mPreviewPhotoImageView = getView().findViewById(R.id.mPreviewPhotoImageView);

        mNextButton = getView().findViewById(R.id.mNextButton);
        cancelButton = getView().findViewById(R.id.cancel);

        mNextButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        mPreviewPhotoImageView.setOnClickListener(this);
    }

    public void cosumingWS(){
        mCaptureDocumentsPresenter.getListDocuments("130");
    }

    @Override
    public BasePresenterC getPresenterC() {
        mCaptureDocumentsPresenter = new CaptureDocumentsPresenter((AppCompatActivity) getActivity(), this,this);
        return mCaptureDocumentsPresenter;
    }

    public void configureRecycler(){
        mDocumentListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mDocumentListAdapter = new DocumentListAdapter(this);
        mDocumentListAdapter.update(mListDocuemnts);
        mDocumentListRecyclerView.setAdapter(mDocumentListAdapter);
    }

    @Override
    public void onClick(View v) {
        if(R.id.mNextButton == v.getId()){
            getAdapter().setNextFragment(new SincronizeDocumentsFragment());
            getViewPager().setAllowedDirection(NavigationViewPager.SwipeDirection.SWIPE_ALL);
            getViewPager().setCurrentItem(getViewPager().getCurrentItem() + 1, true);
        }else if(R.id.cancel == v.getId()){
            startActivity(new Intent(getActivity(), CancelProcedureDialog.class));
        }else if(R.id.mPreviewPhotoImageView == v.getId()){
            if(idContract.equals("1341")){

                startActivityForResult(new Intent(getActivity(), SignContractActivity.class),666);
            }
        }
    }

    public void retrieveClientFromInternalStorage(){
        new Thread(() -> {
            // Read the client from the local storage database.
            Client client = ClientDAO.readClient();
            // Checks the client is not null
            if (client != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        client_name_account_number.setText(client.getName()+" "+client.getLastName()+" "+client.getSurName()+" No. de cuenta "+client.getAccountNumber());
                    }
                });
            }
        }).start();
    }

    @Override
    public void OnSuccessGetListDocuments(DocumentsResponse documentsResponse) {
        mListDocuemnts = documentsResponse.respuesta;
        mDocumentListAdapter.update(mListDocuemnts);
    }

    @Override
    public void OnSuccessUploadImage() {

    }

    @Override
    public void onDocumentSelected(DocumentosACapturar documentosACapturar, Boolean selected, Boolean clicked) {
        idContract = documentosACapturar.claveDocumento;
        if(documentosACapturar.claveDocumento.equals("1341")){
            idPhotoSelected = documentosACapturar.claveDocumento;
            mPreviewPhotoImageView.setImageResource(R.drawable.example_contract);
        }else{
            Log.e("Clave OnClick", documentosACapturar.claveDocumento);
            idPhotoSelected = documentosACapturar.claveDocumento;
            takePhoto(idPhotoSelected);
        }
    }

    public void takePhoto(String namePhoto){
        Intent chooserIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment.getExternalStorageDirectory(), namePhoto+".jpg");
        chooserIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        imageToUploadUri = Uri.fromFile(f);
        startActivityForResult(chooserIntent,CAMERA_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_PHOTO && resultCode == Activity.RESULT_OK) {
            if(imageToUploadUri != null){
                Uri selectedImage = imageToUploadUri;
                Bitmap reducedSizeBitmap = getBitmap(imageToUploadUri.getPath());

                if(reducedSizeBitmap != null){
                    changeStatusTakePhoto();
                    mPreviewPhotoImageView.setImageBitmap(reducedSizeBitmap);
                }else{
                    Toast.makeText(getActivity(),"Error while capturing Image",Toast.LENGTH_LONG).show();
                }
            }else{
                Toast.makeText(getActivity(),"Error while capturing Image",Toast.LENGTH_LONG).show();
            }
        }else if(requestCode == 666){
            changeStatusTakePhoto();
        }
    }

    public void uploadImage(Bitmap bitmap){

    }

    private Bitmap getBitmap(String path) {

        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = getActivity().getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = getActivity().getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();
                Log.d("", "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            Log.e("", e.getMessage(), e);
            return null;
        }
    }

    public void changeStatusTakePhoto(){
        mListDocumentsStatusPhoto.clear();
        mDocumentListAdapter.update(mListDocumentsStatusPhoto);
        for(Respuestum item :  mListDocuemnts){
            if(idPhotoSelected.equals(item.documentosACapturar.claveDocumento)){
                item.documentosACapturar.isTakePhoto = true;
                mListDocumentsStatusPhoto.add(item);
            }else{
                mListDocumentsStatusPhoto.add(item);
            }
        }
        mDocumentListAdapter.update(mListDocumentsStatusPhoto);
        enableSwip(mListDocumentsStatusPhoto);
    }

    public static String toBase64(String message) {
        byte[] data;
        try {
            data = message.getBytes("UTF-8");
            String base64Sms = Base64.encodeToString(data, Base64.DEFAULT);
            return base64Sms;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String fromBase64(String message) {
        byte[] data = Base64.decode(message, Base64.DEFAULT);
        try {
            return new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void enableSwip(List<Respuestum> mListDocumentsStatusPhoto){
        try {
            boolean enableSwip = false;
            if (mListDocumentsStatusPhoto.size() >= 3) {
                for (Respuestum item : mListDocumentsStatusPhoto) {
                    if (item.documentosACapturar.isTakePhoto) {
                        enableSwip = true;
                    } else {
                        enableSwip = false;
                        break;
                    }
                }
            }

        if(enableSwip){
            getAdapter().setNextFragment(new SincronizeDocumentsFragment());
            getViewPager().setAllowedDirection(NavigationViewPager.SwipeDirection.SWIPE_ALL);
        }
        }catch (RuntimeException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        enableSwip(mListDocumentsStatusPhoto);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        enableSwip(mListDocumentsStatusPhoto);

    }
}
