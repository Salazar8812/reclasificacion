package app.profuturo.reclasificacion.com;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import app.profuturo.reclasificacion.com.model.Client;

/**
 * Class that defines how a found client will behave when displayed in a list.
 *
 * @author Alfredo Bejarano
 * @version 1.0
 * @since 06/09/2018 - 01:46 PM
 */
public class ClientResultViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {
    private Client mClient;
    private TextView mRFCView;
    private TextView mNSSView;
    private TextView mCurpView;
    private TextView mNameView;
    private TextView mAccountNumberView;
    private RadioButton mSelectionCheckBox;
    private OnItemSelectionChanged mItemSelectionChanged;

    public ClientResultViewHolder(View itemView,
                                  OnItemSelectionChanged itemSelectionChangedListener) {
        super(itemView);
        itemView.setOnClickListener(this);
        mRFCView = itemView.findViewById(R.id.rfc);
        mNSSView = itemView.findViewById(R.id.nss);
        mCurpView = itemView.findViewById(R.id.curp);
        mNameView = itemView.findViewById(R.id.name);
        mItemSelectionChanged = itemSelectionChangedListener;
        mAccountNumberView = itemView.findViewById(R.id.account_number);
        mSelectionCheckBox = itemView.findViewById(R.id.selection_radio_button);
    }

    /**
     * Draws the data of a Client object in this item view widgets.
     *
     * @param client The object to be assigned to this recycler view.
     * @see Client
     */
    public void render(@NonNull Client client, @NonNull Boolean selection) {
        mClient = client;
        mRFCView.setText(client.getRfc());
        mNSSView.setText(client.getNss());
        mCurpView.setText(client.getCurp());
        mNameView.setText(client.getDisplayName());
        mAccountNumberView.setText(client.getAccountNumber());
        setupSelectionCheckBox(this, selection);
    }

    /**
     * Initializes the selection CheckBox widget and adds a onCheckedChangeListener to it.
     *
     * @param holder    This view holder.
     * @param selection The new selection state for this ViewHolder.
     */
    private void setupSelectionCheckBox(ClientResultViewHolder holder, Boolean selection) {
        // Remove a onCheckSelectionListener if it has been already added.
        mSelectionCheckBox.setOnCheckedChangeListener(null);
        // Change the itemView background depending on the selection.
        itemView.setBackgroundResource(selection ? R.drawable.background_item_selected :
                R.drawable.background_button_white);
        // Change the elevation depending on the selection if API >= 21.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            itemView.setElevation(selection ? 4 : 0);
        }
        // Set the new selection value for the check box.
        mSelectionCheckBox.setChecked(selection);
        // Assign the check changed listener again.
        mSelectionCheckBox.setOnCheckedChangeListener((compoundButton, bool) -> {
            if (bool) {
                reportClientSelection(false);
            }
            mItemSelectionChanged.onItemSelectionChanged(holder, bool);
        });
    }

    /**
     * Performs an action depending of which view has been clicked.
     * <ul>
     * <li>ViewHolder: Calls the onClientSelected method</li>
     * <li>CheckBox: Reports a selection change and inverts the other ViewHolder selections.</li>
     * </ul>
     *
     * @param view The view being clicked.
     */
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.selection_radio_button) {
            mSelectionCheckBox.setChecked(!mSelectionCheckBox.isChecked());
        }
        reportClientSelection(view.getId() == itemView.getId());
    }

    /**
     * Notifies that a client has been selected.
     */
    private void reportClientSelection(Boolean clicked) {
        if (itemView.getContext() instanceof ClientSearchFragment.ClientResultInteraction) {
            // If the interaction can be made, call the method for using this element client object.
            ((ClientSearchFragment.ClientResultInteraction) itemView.getContext())
                    .onClientSelected(mClient, mSelectionCheckBox.isChecked(), clicked);
        }
    }

    /**
     * Allows access to this ViewHolder client object.
     *
     * @return The client for this ViewHolder.
     */
    public Client getClient() {
        return mClient;
    }

    /**
     * Interface that defines methods for an Object
     * to react when a ClientResult object changes selection.
     */
    public interface OnItemSelectionChanged {
        /**
         * This method is called when a ClientResultViewHolder changes its selection.
         *
         * @param vh        The ViewHolder that changed its selection.
         * @param selection The new selection value.
         */
        void onItemSelectionChanged(ClientResultViewHolder vh, Boolean selection);
    }
}