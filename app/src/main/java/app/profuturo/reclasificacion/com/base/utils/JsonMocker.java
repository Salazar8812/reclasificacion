package app.profuturo.reclasificacion.com.base.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.RawRes;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;

/**
 * Utilities class that uses GSON to parse a
 * defined JSON file as a POJO object.
 *
 * @version 1.0
 * @since August 6th, 2018 - 07:04 PM
 */
public class JsonMocker {
    private JsonMocker() {
        // Hidden constructor for this utility class.
    }

    /**
     * This method will open a file from the RAW folder using a context
     * instance and then will proceed to try to parse it as a JSON to
     * return a POJO of said JSON using GSON annotations.
     *
     * @param file    The resource ID of the file.
     * @param context Context instance that is referencing this method.
     * @return POJO of the expected object specified by the Generic parameter.
     */
    public static <T> T mock(@RawRes int file, @NonNull Context context, @NonNull Type type) {
        try {
            // Create a new StringWriter for building the JSON file.
            Writer writer = new StringWriter();
            // Obtain the file from the RAW folder as an InputStream.
            InputStream resource = context.getResources().openRawResource(file);
            // Now read the file using UTF-8 encoding.
            BufferedReader reader = new BufferedReader(new InputStreamReader(resource, "UTF-8"));
            // Read the current line of the reader.
            String line = reader.readLine();
            // Execute this with every line if it is not null.
            while (line != null) {
                // Write the line using the writer.
                writer.write(line);
                // Now, read the next line.
                line = reader.readLine();
            }
            // Now, parse the wrote line.
            return mock(writer.toString(), type);
        } catch (IOException e) {
            // If the parsing fails at any point, return a null object.
            return null;
        }
    }

    /**
     * This method will use a String value as a JSON then will
     * proceed to try to parse it to return a POJO of said JSON
     * using GSON annotations.
     *
     * @param json The JSON to be mocked, as a String.
     * @return POJO of the expected object specified by the Generic parameter.
     */
    public static <T> T mock(@NonNull String json, @NonNull Type type) {
        // Create a new GSON parses instance.
        Gson parser = new Gson();
        // Finally, parse the JSON file to our given object class using JSON.
        return parser.fromJson(json, type);
    }
}

