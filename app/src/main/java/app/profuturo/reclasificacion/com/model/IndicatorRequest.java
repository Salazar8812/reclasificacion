package app.profuturo.reclasificacion.com.model;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseRequestInterface;

public class IndicatorRequest implements WSBaseRequestInterface {
    public String numberAccount;

    public IndicatorRequest(String numberAccount) {
        this.numberAccount = numberAccount;
    }
}
