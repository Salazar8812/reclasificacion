package app.profuturo.reclasificacion.com.model;

import java.util.ArrayList;
import java.util.List;

public class SalesFormat {
    public String mTitleSale;
    public String saleAmmount;
    public String idSale;
    public List<SaldoBean> mListSaldoSubAccount= new ArrayList<>();
    public List<DatesSubAccounts> listDates = new ArrayList<>();

    public SalesFormat(String mTitleSale, String saleAmmount, String idSale, List<SaldoBean> mListSaldoSubAccount, List<DatesSubAccounts> listDates) {
        this.mTitleSale = mTitleSale;
        this.saleAmmount = saleAmmount;
        this.idSale = idSale;
        this.mListSaldoSubAccount = mListSaldoSubAccount;
        this.listDates = listDates;
    }

    public SalesFormat() {

    }

}
