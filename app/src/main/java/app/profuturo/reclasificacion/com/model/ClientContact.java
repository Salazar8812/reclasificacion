package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClientContact {
    public static final int PHONE_CONTACT_LANDLINE = 1;
    public static final int PHONE_CONTACT_CELLPHONE = 3;
    @Expose
    @SerializedName("valido")
    private boolean contactValid;
    @Expose
    @SerializedName("preferente")
    private boolean preferred;

    public ClientContact() {
        // Empty method scrub!
    }

    public ClientContact(boolean contactValid, boolean preferred) {
        this.contactValid = contactValid;
        this.preferred = preferred;
    }

    public boolean isContactValid() {
        return contactValid;
    }

    public boolean isPreferred() {
        return preferred;
    }

    /**
     * Defines the metadata type of the contact object.
     */
    public static class Type {
        @Expose
        @SerializedName("clave")
        private Integer key;
        @Expose
        @SerializedName("descripcion")
        private String description;

        public Type(Integer key, String description) {
            this.key = key;
            this.description = description;
        }

        public Integer getKey() {
            return key;
        }

        public String getDescription() {
            return description;
        }
    }
}
