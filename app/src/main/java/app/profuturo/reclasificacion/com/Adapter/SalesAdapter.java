package app.profuturo.reclasificacion.com.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.model.SaldoBean;
import app.profuturo.reclasificacion.com.model.SalesFormat;

public class SalesAdapter extends RecyclerView.Adapter<SalesHolder> implements SalesHolder.OnItemSelectionChanged {

    private List<SalesFormat> listSale = new ArrayList<>();
    private List<SalesHolder> mViewHolders = new ArrayList<>();
    private SalesResultInteraction mSalesResultInteraction;

    public void update(List<SalesFormat> listSale){
        this.listSale = listSale;
        notifyDataSetChanged();
    }

    public SalesAdapter(SalesResultInteraction mSalesResultInteraction){
        this.mSalesResultInteraction = mSalesResultInteraction;
    }

    @NonNull
    @Override
    public SalesHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return insertViewHolder(new SalesHolder(
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_sale,
                        viewGroup, false), this));
    }

    private SalesHolder insertViewHolder(@NonNull SalesHolder vh) {
        if (!mViewHolders.contains(vh)) {
            mViewHolders.add(vh);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull SalesHolder salesHolder, int i) {
        salesHolder.render(listSale.get(i), false, mSalesResultInteraction);
    }

    @Override
    public int getItemCount() {
        return listSale.size();
    }

    @Override
    public void onItemSelectionChanged(@NonNull SalesHolder vh, Boolean selection) {
        // Retrieve the clicked ViewHolder client.
        SalesFormat client = vh.getSaldoBean();
        // Iterate through the list of ViewHolders.
        for (SalesHolder cVh : mViewHolders) {
            // Get the current ViewHolder client object.
            SalesFormat mSaldoBean = cVh.getSaldoBean();
            // Check if the ViewHolder is the same as the one reporting the change.
            if (client.equals(mSaldoBean)) {
                // If it is, set the new selection change to said ViewHolder.
                cVh.render(mSaldoBean, selection,mSalesResultInteraction);
            } else {
                // If not, deselect the ViewHolder.
                cVh.render(mSaldoBean, false,mSalesResultInteraction);
            }
        }
    }

    public interface SalesResultInteraction {
        /**
         * Method that depending of the implementation,
         * will operate with the clicked client object.
         *
         * @param client The client object that has been selected from the list.
         */
        void onSaleSelected(SalesFormat client, Boolean selected, Boolean clicked);
    }

}
