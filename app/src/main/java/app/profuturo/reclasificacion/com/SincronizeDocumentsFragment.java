package app.profuturo.reclasificacion.com;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import app.profuturo.reclasificacion.com.Adapter.DocumentListAdapter;
import app.profuturo.reclasificacion.com.Implementation.BasePresenterC;
import app.profuturo.reclasificacion.com.Repository.local.ClientDAO;
import app.profuturo.reclasificacion.com.databinding.FragmentSincronizeDocumentsBinding;
import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.model.DocumentosACapturar;
import app.profuturo.reclasificacion.com.model.Respuestum;
import app.profuturo.reclasificacion.com.presenter.FinishProcedurePrensenter;

public class SincronizeDocumentsFragment extends NavigationFragment<FragmentSincronizeDocumentsBinding> implements View.OnClickListener,DocumentListAdapter.DocumentsResultInteraction, FinishProcedurePrensenter.FinishProcedureCallback {
    private Button mChangeNotification;
    private LinearLayout mSmsRB;
    private LinearLayout mEmailRB;
    private LinearLayout mSocialRB;
    private LinearLayout mNoneRB;

    private RadioButton mSmsRadio;
    private RadioButton mEmailRadio;
    private RadioButton mSocialRadio;
    private RadioButton mNoneRadio;

    private TextView client_name_account_number;

    private Button mfinishProcedureButton;
    private Button cancel;
    private RecyclerView mListDocumentsSincronizaRecyclerView;
    private DocumentListAdapter documentListAdapter;
    private FinishProcedurePrensenter mFinishProcedurePrensenter;


    @Override
    protected Integer getLayoutId() {
        return R.layout.fragment_sincronize_documents;
    }

    @Override
    public void setup() {
       bindViews();
       configureRecyclerView();
       retrieveClientFromInternalStorage();
    }

    public void bindViews(){
        mChangeNotification = getView().findViewById(R.id.mChangeNotification);
        mSmsRB = getView().findViewById(R.id.mSmsRB);
        mEmailRB = getView().findViewById(R.id.mEmailRB);
        mSocialRB = getView().findViewById(R.id.mSocialRB);
        mNoneRB = getView().findViewById(R.id.mNoneRB);
        client_name_account_number = getView().findViewById(R.id.client_name_account_number);
        cancel = getView().findViewById(R.id.cancel);

        mSmsRadio = getView().findViewById(R.id.mSMSRadio);
        mEmailRadio = getView().findViewById(R.id.mEmailRadio);
        mSocialRadio = getView().findViewById(R.id.mSocialRadio);
        mNoneRadio = getView().findViewById(R.id.mNoneRadio);
        mfinishProcedureButton = getView().findViewById(R.id.mfinishProcedureButton);
        mListDocumentsSincronizaRecyclerView = getView().findViewById(R.id.mListDocumentsSincronizaRecyclerView);

        mSmsRB.setOnClickListener(this);
        mEmailRB.setOnClickListener(this);
        mSocialRB.setOnClickListener(this);
        mNoneRB.setOnClickListener(this);
        mfinishProcedureButton.setOnClickListener(this);
        mChangeNotification.setOnClickListener(this);
        mfinishProcedureButton.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }


    @Override
    public BasePresenterC getPresenterC() {
        mFinishProcedurePrensenter = new FinishProcedurePrensenter((AppCompatActivity) getActivity(),this,this);
        return mFinishProcedurePrensenter;
    }


    public void configureRecyclerView(){
        mListDocumentsSincronizaRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        documentListAdapter = new DocumentListAdapter(this);
        mListDocumentsSincronizaRecyclerView.setAdapter(documentListAdapter);
        documentListAdapter.update(CaptureDocumentsFragment.mListDocuemnts);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mChangeNotification:
                startActivity(new Intent(getActivity(), ChangeInfoDialog.class));
                break;
            case R.id.mSmsRB:
                enableRB(mSmsRadio);
                disableRB(mEmailRadio);
                disableRB(mSocialRadio);
                disableRB(mNoneRadio);
                break;
            case R.id.mEmailRB:
                disableRB(mSmsRadio);
                enableRB(mEmailRadio);
                disableRB(mSocialRadio);
                disableRB(mNoneRadio);
                break;
            case R.id.mSocialRB:
                disableRB(mSmsRadio);
                disableRB(mEmailRadio);
                enableRB(mSocialRadio);
                disableRB(mNoneRadio);
                break;
            case R.id.mNoneRB:
                disableRB(mSmsRadio);
                disableRB(mEmailRadio);
                disableRB(mSocialRadio);
                enableRB(mNoneRadio);
                break;

            case R.id.mfinishProcedureButton:
                mFinishProcedurePrensenter.finishProcedure();
                break;
            case R.id.cancel:
                startActivity(new Intent(getActivity(),CancelProcedureDialog.class));
                default:break;
        }
    }

    public void enableRB(RadioButton rB){
        rB.setChecked(true);
    }

    public void disableRB(RadioButton rB){
        rB.setChecked(false);
    }

    @Override
    public void onDocumentSelected(DocumentosACapturar documentosACapturar, Boolean selected, Boolean clicked) {

    }

    @Override
    public void OnSuccessFinish() {
        sendPDFEmail();
        startActivity(new Intent(getActivity(), FinishProcedureDialog.class));
    }

    @Override
    public void OnErrorSendProfcedure() {
        sendPDFEmail();
    }

    public void sendPDFEmail(){
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        File file = new File(path, "example_pdf.pdf");
        shareIntent.setType("application/pdf");
        shareIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { "abc@gmail.com" });
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Profuturo PDF ");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "test");
        shareIntent.putExtra(Intent.EXTRA_STREAM,Uri.fromFile(file));
        startActivity(shareIntent);
    }
    public void retrieveClientFromInternalStorage(){
        new Thread(() -> {
            // Read the client from the local storage database.
            Client client = ClientDAO.readClient();
            // Checks the client is not null
            if (client != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        client_name_account_number.setText(client.getName()+" "+client.getLastName()+" "+client.getSurName()+" No. de cuenta "+client.getAccountNumber());
                    }
                });
            }
        }).start();
    }
}
