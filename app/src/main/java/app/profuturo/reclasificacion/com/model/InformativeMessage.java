package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InformativeMessage {
    @Expose
    @SerializedName("mensajeInformativo")
    private String message;

    public InformativeMessage() {
    }

    public InformativeMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}