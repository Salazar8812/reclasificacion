package app.profuturo.reclasificacion.com.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import app.profuturo.reclasificacion.com.presenter.ClientPresenter;

public class Utils {
    public static final String CS_ACCOUNT = "cs";
    public static final String CV_ACCOUNT = "cv";
    public static final String R_ACCOUNT = "r";
    public static final String RETIRO92_ACCOUNT = "retiro92";
    public static final String VIVENDA92_ACCOUNT = "vivenda92";
    public static final String VIVENDA97_ACCOUNT = "vivenda97";
    // Constant that indicates the procedure is at initial capture
    public static final int INITIAL_CAPTURE_SCREEN = 1;
    // Constant that indicates the procedure is at beneficiaries
    public static final int BENEFICIARIES_SCREEN = 2;
    // Constant that indicates the procedure is at payment method
    public static final int PAYMENT_FORM_SCREEN = 3;
    // Constant that indicates the procedure is at payment form
    public static final int DOCUMENTS_CAPTURE_SCREEN = 4;
    // Constant that indicates the procedure is at documents capture
    public static final int BIOMETRICS_SCREEN = 5;
    // Constant that indicates the procedure is at save procedure
    public static final int SAVE_PROCEDURE_SCREEN = 6;
    // Constant for the time format to be show to the client
    private static final String TIME_FORMAT = "HH:mm";
    //Constant for the date time format use to parse timestamp
    private static final String DATE_TIME_FORMAT_MILLIS = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    //Constant for the date time format use to parse timestamp
    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    // Constants for the TimeStamps formats.
    private static final List<String> DATE_TIME_FORMATS = Arrays.asList(DATE_TIME_FORMAT_MILLIS, DATE_TIME_FORMAT);
    // Constant for the date format to be show to the client
    private static final String DATE_FORMAT = "dd/MM/yyyy";

    private Utils() {
        // Private constructor to hide this utilities class.
    }


    /**
     * Checks if a CharSequence implementation is not null
     * and has characters in its sequence.
     *
     * @param charSequence The value to check.
     * @return true if it is not null or empty.
     */
    @NonNull
    public static Boolean isNotNullOrEmpty(@Nullable CharSequence charSequence) {
        return charSequence != null && charSequence.length() > 0;
    }

    /**
     * Checks if a Map implementation is not null or empty.
     *
     * @param map The value to check.
     * @return true if it is not null or empty.
     */
    @NonNull
    public static Boolean isNotNullOrEmpty(@Nullable Map map) {
        return map != null && !map.isEmpty();
    }

    /**
     * Checks if a collection is not null and if it contains at
     * least one element within it.
     *
     * @param collection The collection to be check.
     * @return true if the collection is not null and it contains elements.
     */
    @NonNull
    public static Boolean isNotNullOrEmpty(@Nullable Collection<?> collection) {
        return collection != null && !collection.isEmpty();
    }

    /**
     * Checks if an array is not null and if it contains at
     * least one element within it.
     *
     * @param array The array to be check.
     * @return true if the collection is not null and it contains elements.
     */
    @NonNull
    @SafeVarargs
    public static <T> Boolean isNotNullOrEmpty(@Nullable T... array) {
        return array != null && array.length > 0;
    }

    /**
     * Checks if any element of a given String array is null or empty.
     *
     * @param values The values to be verified.
     * @return true if no value is null or empty.
     */
    @NonNull
    public static Boolean isNotNullOrEmpty(@Nullable String... values) {
        // If the array is null or it is empty, return false.
        if (values == null || values.length == 0) {
            return false;
        } else {
            // If not, check every single item of the array.
            for (String value : values) {
                // If the current element is null or empty, return false.
                if (TextUtils.isEmpty(value)) {
                    return false;
                }
            }
            // If the whole array was iterated, return true.
            return true;
        }
    }

    /**
     * Method to check if a permission has been granted
     *
     * @param context    Context which is asking permission
     * @param permission permission to be granted
     * @return boolean indication if permission is granted
     */
    public static boolean isPermissionGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Gets the first element of a given list.
     *
     * @param elements The list of elements to retrieve the first element.
     * @param <T>      Generic defining the class of the element to be retrieved.
     * @return The first element of the list or null if the list is invalid (null or empty).
     */
    public static <T> T firstOf(List<T> elements) {
        // Reference to the element to be returned.
        T element = null;
        // Check if the list is not null or empty.
        if (isNotNullOrEmpty(elements)) {
            // If not, get the first element.
            element = elements.get(0);
        }
        // Return the found element.
        return element;
    }

    /**
     * Gets the last element of a given list.
     *
     * @param elements The list of elements to retrieve the first element.
     * @param <T>      Generic defining the class of the element to be retrieved.
     * @return The last element of the list or null if the list is invalid (null or empty).
     */
    public static <T> T lastOf(List<T> elements) {
        // Reference to the element to be returned.
        T element = null;
        // Check if the list is not null or empty.
        if (isNotNullOrEmpty(elements)) {
            // If not, get the last element.
            element = elements.get(elements.size() - 1);
        }
        // Return the found element.
        return element;
    }

    /**
     * Parses a boolean value into an integer.
     *
     * @param booleanValue The value to be parsed.
     * @return 0 if the boolean is null or false, 1 if the boolean is true.
     */
    public static Integer booleanAsInt(Boolean booleanValue) {
        return booleanValue == null ? 0 : booleanValue ? 1 : 0;
    }


    /**
     * Checks that a given criteria matches the necessary length.
     *
     * @param criteria The criteria to be checked.
     * @param length   The desired length for the given criteria.
     * @return true if the criteria matches the desired length.
     */
    @NonNull
    public static Boolean validateCriteriaByLength(String criteria, Integer length) {
        // Flag that checks if the given criteria is not empty.
        Boolean criteriaNotEmpty = !TextUtils.isEmpty(criteria);
        // Flag that checks if the given criteria matches the desired length.
        // Return if the criteria matches the desired length.
        return criteriaNotEmpty && (criteria.length() == length);
    }

    /**
     * Checks that a given criteria is only formed by digits and matches the necessary length.
     *
     * @param criteria The criteria to be checked.
     * @param length   The desired length for the given criteria.
     * @return true if the criteria matches the desired length.
     */
    @NonNull
    public static Boolean validateNumericCriteriaByLength(String criteria, Integer length) {
        return validateCriteriaByLength(criteria, length) && TextUtils.isDigitsOnly(criteria);
    }

    /**
     * Method to inflate a view from a given layout
     *
     * @param parent         ViewGroup in which the view is gonna be inflated
     * @param layoutId       Layout resource id
     * @param attachToParent flag to attach the just inflated view to the parent
     * @return The view inflated
     */
    @NonNull
    public static View inflateFromContext(@NonNull ViewGroup parent, @NonNull Integer layoutId, @NonNull Boolean attachToParent) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, attachToParent);
    }

    /**
     * Allows single-line navigation to other activities.
     *
     * @param context  The context starting the navigation
     * @param activity The destination activity class for this navigation.
     * @param extras   The parameters for the Intent.
     * @param <T>      Generic defining the destination activity.
     */
    public static <T extends AppCompatActivity> void navigateTo
    (@NonNull Context context, @NonNull Class<T> activity, @Nullable Bundle extras) {
        // Create a new intent using the given context and activity class.
        Intent navigationIntent = new Intent(context, activity);
        // Check if the arguments are not null.
        if (extras != null) {
            // If the aren't, put them in the intent.
            navigationIntent.putExtras(extras);
        }
        // Start the given activity with the built intent.
        context.startActivity(navigationIntent);
    }

    /**
     * Adds a vararg of elements of a given type to a list of
     * the same type if the element to be added is not null.
     *
     * @param list     The list of objects.
     * @param elements The element(s) to be added to the given list.
     * @param <T>      The type of element that conform the list.
     */
    @SafeVarargs
    @SuppressWarnings("ConstantConditions")
    public static <T> void addToListIfNotNull(@NonNull List<T> list, @Nullable T... elements) {
        // Check if the elements is not null or empty.
        if (isNotNullOrEmpty(elements)) {
            // If the elements are not null or empty, iterate through them.
            for (T element : elements) {
                // If the element is not null, add it to the list.
                if (element != null) {
                    list.add(element);
                }
            }
        }
    }

    /**
     * Checks if an array of String elements are not null or empty.
     *
     * @param elements The elements to be checked. (vararg)
     * @return true if no string element is null or empty.
     */
    public static Boolean areNotNullOrEmpty(String... elements) {
        // Check that at least an element was sent.
        if (elements.length > 0) {
            // Iterate through the list of elements
            for (String element : elements) {
                // Check if the current element is null or empty.
                if (element == null || TextUtils.isEmpty(element)) {
                    // If it is, return false.
                    return false;
                }
            }
            // If no element is null or empty, return true.
            return true;
        } else {
            // If not, return false.
            return false;
        }
    }

    /**
     * Method to verify a CURP is valid
     *
     * @param curp The CURP to be checked
     * @return Flag indicating the CURP satisfies the official structure
     */
    public static Boolean isCURPvalid(String curp) {
        String cleanCURP = String.valueOf(curp).toUpperCase(Locale.getDefault());
        // Regular expression that must satisfy a valid CURP
        String CURP_REGEX = "^([A-Z][AEIOUX][A-Z]{2}\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\" +
                "d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|" +
                "S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\\d])(\\d)$";
        // Check the if CURP has the correct length
        return cleanCURP.length() == ClientPresenter.CURP_LENGTH && cleanCURP.matches(CURP_REGEX);
    }

    /**
     * Method to return a formatted date from a timestamp
     *
     * @param timestamp The time timestamp to be used as the base
     * @return The date formatted
     */
    public static String getDateFromTimestamp(@Nullable String timestamp) {
        if (timestamp != null) {
            // Elimate colon from the time zone
            String dateTime = timestamp.substring(0, timestamp.lastIndexOf(':')) + "00";
            SimpleDateFormat timeFormatter = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
            SimpleDateFormat formatter;
            for (String dateTimePattern : DATE_TIME_FORMATS) {
                formatter = new SimpleDateFormat(dateTimePattern, Locale.getDefault());
                try {
                    // Return the time formatted
                    return timeFormatter.format(formatter.parse(dateTime));
                } catch (ParseException e) {
                    // Do noting
                }
            }
        }
        return timestamp;
    }

    /**
     * Method to return a formatted time from a timestamp
     *
     * @param timestamp The time timestamp to be used as the base
     * @return The time formatted
     */
    public static String getTimeFromTimestamp(@Nullable String timestamp) {
        if (timestamp != null) {
            // Elimate colon from the time zone
            String dateTime = timestamp.substring(0, timestamp.lastIndexOf(':')) + "00";
            SimpleDateFormat timeFormatter = new SimpleDateFormat(TIME_FORMAT, Locale.getDefault());
            SimpleDateFormat formatter;
            for (String dateTimePattern : DATE_TIME_FORMATS) {
                formatter = new SimpleDateFormat(dateTimePattern, Locale.getDefault());
                try {
                    // Return the time formatted
                    return timeFormatter.format(formatter.parse(dateTime));
                } catch (ParseException e) {
                    // Do noting
                }
            }
        }
        return timestamp;
    }

    public static long getTimeMillisFromTimestamp(String timestamp) {
        if (timestamp != null) {
            SimpleDateFormat formatter;
            for (String dateTimePattern : DATE_TIME_FORMATS) {
                formatter = new SimpleDateFormat(dateTimePattern, Locale.getDefault());
                try {
                    // Return the time formatted
                    return formatter.parse(timestamp).getTime();
                } catch (ParseException e) {
                    // Do noting
                }
            }
        }
        return 0;
    }
}
