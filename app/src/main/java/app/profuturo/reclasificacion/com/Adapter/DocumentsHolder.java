package app.profuturo.reclasificacion.com.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.model.Respuestum;

class DocumentsHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    Context mContext;
    TextView mDescriptionDocument;
    ImageView capture_method;
    private Respuestum mDocuments;
    ImageView document_added_icon;
    private DocumentsHolder.OnItemSelectionChanged mItemSelectionChanged;

    public DocumentsHolder(@NonNull View itemView,DocumentsHolder.OnItemSelectionChanged itemSelectionChangedListener) {
        super(itemView);
        mContext = itemView.getContext();
        mDescriptionDocument = itemView.findViewById(R.id.mDescriptionDocument);
        capture_method = itemView.findViewById(R.id.capture_method);
        document_added_icon = itemView.findViewById(R.id.document_added_icon);
        this.mItemSelectionChanged = itemSelectionChangedListener;
        itemView.setOnClickListener(this);

    }

    public void render(@NonNull Respuestum documentosACapturar, @NonNull Boolean selection, DocumentListAdapter.DocumentsResultInteraction mSalesResultInteraction) {
        mDescriptionDocument.setText(documentosACapturar.documentosACapturar.tipoDocumento);
        if(documentosACapturar.documentosACapturar.isTakePhoto){
            document_added_icon.setImageResource(R.drawable.ic_accept_sincronize);
        }else{
            document_added_icon.setImageResource(R.drawable.ic_error);
        }
        mDocuments = documentosACapturar;

    }

    public Respuestum getmDocuments() {
        return mDocuments;
    }


    @Override
    public void onClick(View view) {
        mItemSelectionChanged.onItemSelectionChanged(this,true);
    }

    public interface OnItemSelectionChanged {
        /**
         * This method is called when a ClientResultViewHolder changes its selection.
         *
         * @param vh        The ViewHolder that changed its selection.
         * @param selection The new selection value.
         */
        void onItemSelectionChanged(DocumentsHolder vh, Boolean selection);
    }
}
