
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;

public class IndicatorsResponse implements WSBaseResponseInterface {

    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("numeroCuentaIndividual")
    @Expose
    public String numeroCuentaIndividual;

    @SerializedName("configuracionIndicador")
    @Expose
    public List<ConfiguracionIndicador> configuracionIndicador = new ArrayList<>();

}
