
package app.profuturo.reclasificacion.com.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Siefore {

    @SerializedName("siefore")
    @Expose
    private Siefore_ siefore;

    private Grupo_ grupo;

    public Siefore_ getSiefore() {
        return siefore;
    }

    public void setSiefore(Siefore_ siefore) {
        this.siefore = siefore;
    }

    public Grupo_ getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo_ grupo) {
        this.grupo = grupo;
    }

}
