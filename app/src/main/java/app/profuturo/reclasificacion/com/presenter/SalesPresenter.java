package app.profuturo.reclasificacion.com.presenter;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import app.profuturo.reclasificacion.com.Background.BaseWSManager;
import app.profuturo.reclasificacion.com.Background.WSManager;
import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;
import app.profuturo.reclasificacion.com.Callbacks.WSCallback;
import app.profuturo.reclasificacion.com.Implementation.ReclasificationPresenter;
import app.profuturo.reclasificacion.com.Repository.local.AgentSessionDAO;
import app.profuturo.reclasificacion.com.Repository.local.ClientDAO;
import app.profuturo.reclasificacion.com.Utils.MessageUtils;
import app.profuturo.reclasificacion.com.Utils.PrefsReclasification;
import app.profuturo.reclasificacion.com.base.View.BaseView;
import app.profuturo.reclasificacion.com.model.AgentSession;
import app.profuturo.reclasificacion.com.model.Client;
import app.profuturo.reclasificacion.com.model.ConfiguracionIndicador;
import app.profuturo.reclasificacion.com.model.DatesSubAccounts;
import app.profuturo.reclasificacion.com.model.IndicatorActionValueRequest;
import app.profuturo.reclasificacion.com.model.IndicatorActionValueResponse;
import app.profuturo.reclasificacion.com.model.IndicatorRequest;
import app.profuturo.reclasificacion.com.model.IndicatorsResponse;
import app.profuturo.reclasificacion.com.model.ListReclasificacion;
import app.profuturo.reclasificacion.com.model.MatrixReclasificationRequest;
import app.profuturo.reclasificacion.com.model.ReclassificationMatrix;
import app.profuturo.reclasificacion.com.model.RegisterReclasifcationResponse;
import app.profuturo.reclasificacion.com.model.RegisterReclasificationRequest;
import app.profuturo.reclasificacion.com.model.Saldo;
import app.profuturo.reclasificacion.com.model.SaldoBean;
import app.profuturo.reclasificacion.com.model.SaleRequest;
import app.profuturo.reclasificacion.com.model.SaleResponse;
import app.profuturo.reclasificacion.com.model.SalesFormat;
import app.profuturo.reclasificacion.com.model.SaveProcedureRequest;
import app.profuturo.reclasificacion.com.model.SubAccountMatrixResponse;
import app.profuturo.reclasificacion.com.model.SubAccountsMatrixReclasification;
import io.realm.RealmList;

public class SalesPresenter extends ReclasificationPresenter implements WSCallback {

    private List<SalesFormat> mListSalesToShow = new ArrayList<>();
    private CallbackListSales mCallbackListSales;
    private ReclassificationMatrix mReclassificationMatrix;
    private Boolean mOtherDate = false;
    List<SaldoBean> mListTemp = new ArrayList<>();
    private PrefsReclasification mPrefsReclasification;
    private IndicatorsResponse mIndicatorsResponse;
    private String mAmmount = "";
    private String mTypeReclasification = "";
    private PrefsReclasification prefsReclasification;

    public interface CallbackListSales {
        void getListSales(List<SalesFormat> saleResponse);
        void OnGetSubAccountIndicators(List<SubAccountsMatrixReclasification> list);
        void OnGetActionValue(IndicatorActionValueResponse mIndicatorActionValueResponse);
    }

    public SalesPresenter(AppCompatActivity appCompatActivity, CallbackListSales mCallbackListSales, BaseView view) {
        super(appCompatActivity,view);
        this.mCallbackListSales = mCallbackListSales;
        mPrefsReclasification = new PrefsReclasification(appCompatActivity);
        prefsReclasification = new PrefsReclasification(appCompatActivity);
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void getSales(String numberAccount, String family){
        SaleRequest mSaleRequest = new SaleRequest();
        mSaleRequest.numberAccount = numberAccount;
        mSaleRequest.family = family;
        mWSManager.requestWs(SaleResponse.class, WSManager.WS.SALES, mSaleRequest);
    }

    public IndicatorsResponse getIndicadores(){
        Gson gson = new Gson();
        String json = mPrefsReclasification.getData("indicadores");
        IndicatorsResponse obj = gson.fromJson(json, IndicatorsResponse.class);
        return obj;
    }

    public void consultingMatrix(String mSubAccountGroup, List<DatesSubAccounts> list){
        MatrixReclasificationRequest matrixReclasificationRequest = new MatrixReclasificationRequest(mSubAccountGroup,list);
        mWSManager.requestWs(SubAccountMatrixResponse.class, WSManager.WS.GET_MATRIX, matrixReclasificationRequest);
    }

    public void registerReclasification(String idProceso, String idSubproceso,String idCanal,String idEstatusProceso, String idEtapa,String idSubetapa, String mTypeReclasification, String mAmmount){
        this.mTypeReclasification = mTypeReclasification;
        this.mAmmount = mAmmount;
        RegisterReclasificationRequest registerReclasificationRequest = new RegisterReclasificationRequest(idProceso,idSubproceso,idCanal,retrieveAgent(),idEstatusProceso,idEtapa,idSubetapa);
        mWSManager.requestWs(RegisterReclasifcationResponse.class, WSManager.WS.REGISTER_RECLASIFICATION, registerReclasificationRequest);
    }

    private String dateCaptured() {
        Date date = new Date();
        SimpleDateFormat dateFormatWithZone = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",Locale.getDefault());
        String currentDate = dateFormatWithZone.format(date);

        return currentDate;
    }

    public void getActionValue(String id_siefore,String fechaAccionValor, String idValorAccion){
        IndicatorActionValueRequest indicatorActionValueRequest = new IndicatorActionValueRequest(fechaAccionValor,id_siefore,idValorAccion);
        mWSManager.requestWs(IndicatorActionValueResponse.class, WSManager.WS.GET_ACTION_VALUE, indicatorActionValueRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        switch (requestUrl){
            case WSManager.WS.SALES:
                OnSalesSuccessResponse((SaleResponse) baseResponse);
                break;
            case WSManager.WS.GET_MATRIX:
                OnSuccessGetMatrix((SubAccountMatrixResponse) baseResponse);
                break;
            case WSManager.WS.REGISTER_RECLASIFICATION:
                OnSuccessRegister((RegisterReclasifcationResponse) baseResponse);
                break;
            case WSManager.WS.GET_ACTION_VALUE:
                OnSuccessGetActionValue((IndicatorActionValueResponse) baseResponse);
                break;
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        super.onErrorLoadResponse(requestUrl, messageError);
    }

    public void OnSalesSuccessResponse(SaleResponse mSaleResponse){
        if(mSaleResponse != null){
            if(mSaleResponse.getSaldoBean().size() > 0 ){
                groupBySiefore(mSaleResponse);
            }
        }
    }

    public void OnSuccessGetMatrix(SubAccountMatrixResponse subAccountsMatrixReclasification){
        if(subAccountsMatrixReclasification != null){
            if(subAccountsMatrixReclasification.listMatrix.get(0) != null){
                mCallbackListSales.OnGetSubAccountIndicators(subAccountsMatrixReclasification.listMatrix);
            }
        }
    }

    public void OnSuccessRegister(RegisterReclasifcationResponse registerReclasifcationResponse){
        if(registerReclasifcationResponse != null){
            if(registerReclasifcationResponse.status.equals("200")){
                Log.e("Registro Exitoso",registerReclasifcationResponse.folio_fus);
                saveReclasificationData(registerReclasifcationResponse.folio_fus,registerReclasifcationResponse.cve_solicity);
            }
        }
    }

    public void OnSuccessGetActionValue(IndicatorActionValueResponse mIndicatorActionValueResponse) {
        if(mIndicatorActionValueResponse != null){
            if(mIndicatorActionValueResponse.status.equals("200")){
                mCallbackListSales.OnGetActionValue(mIndicatorActionValueResponse);
            }
        }
    }

    public void groupBySiefore(SaleResponse mSaleResponse){
        mReclassificationMatrix = new ReclassificationMatrix();
        for(int i = 0 ; i < mReclassificationMatrix.getSubAccounts().size() ;i++){
            agroupCalculateSales(mSaleResponse.getSaldoBean(), mReclassificationMatrix.getSubAccounts().get(i));
        }
        mCallbackListSales.getListSales(mListSalesToShow);
    }

    private void agroupCalculateSales(List<SaldoBean> mListSale, ReclassificationMatrix reclassificationMatrix){
        Double mAmmountTotal = 0.0;
        mListTemp.clear();

        for (SaldoBean saldo: mListSale){
            if(reclassificationMatrix.SieforeAccept.equals(String.valueOf(saldo.getSiefore().getSiefore().getId()))){
                if (reclassificationMatrix.idSubcuentasAccept.get(0).idSubAccount.equals(saldo.getIdSubcuenta()) || reclassificationMatrix.idSubcuentasAccept.get(1).idSubAccount.equals(saldo.getIdSubcuenta())) {
                    if (reclassificationMatrix.idSubcuentasAccept.get(0).idDeducible.equals(saldo.getDeducible()) || reclassificationMatrix.idSubcuentasAccept.get(1).idDeducible.equals(saldo.getDeducible())) {
                        mAmmountTotal = mAmmountTotal + Double.parseDouble(saldo.getSaldoAlDiaPesos());
                        mListTemp.add(saldo);
                    }else{
                        if(reclassificationMatrix.idSubcuentasAccept.get(0).plazo.equals(saldo) || reclassificationMatrix.idSubcuentasAccept.get(1).plazo.equals(saldo.getPlazo())){
                            mAmmountTotal = mAmmountTotal + Double.parseDouble(saldo.getSaldoAlDiaPesos());
                            mListTemp.add(saldo);
                        }else{
                            mAmmountTotal = mAmmountTotal + Double.parseDouble(saldo.getSaldoAlDiaPesos());
                            mListTemp.add(saldo);
                        }
                    }
                }
            }
        }

        if(mListTemp.size() > 0) {
            generateItem(mListTemp, mAmmountTotal,reclassificationMatrix);
        }
    }

    public void generateItem(List<SaldoBean> mList, Double mTotalAmmount,ReclassificationMatrix reclassificationMatrix){
        List<DatesSubAccounts> listDates = new ArrayList<>();

        for(SaldoBean itemSaldo : mList){
            listDates.add(generatDates(itemSaldo.getIdSubcuenta()));
        }

        SalesFormat salesFormat = new SalesFormat();
        for(SaldoBean itemSaldo : mList){
            salesFormat.mListSaldoSubAccount.add(itemSaldo);
        }
        salesFormat.listDates = listDates;
        salesFormat.idSale = reclassificationMatrix.id;
        salesFormat.mTitleSale = reclassificationMatrix.title;
        salesFormat.saleAmmount = String.valueOf(mTotalAmmount);

        if(salesFormat.mListSaldoSubAccount.size() > 0) {
            mListSalesToShow.add(salesFormat);
        }
    }

    @SuppressLint("LongLogTag")
    public DatesSubAccounts generatDates(String idSubcuenta){
        DatesSubAccounts datesSubAccounts = new DatesSubAccounts();
        datesSubAccounts.idSubAccount = idSubcuenta;
        mIndicatorsResponse = getIndicadores();
       for(ConfiguracionIndicador item : mIndicatorsResponse.configuracionIndicador){
           switch (item.configuracion.indicador.idIndicador){
               case "703":
                   if(idSubcuenta.equals("169") || idSubcuenta.equals("170")){
                       datesSubAccounts.firstInputDate = item.valorIndicador;
                   }
                   break;
               case "704":
                   if(idSubcuenta.equals("169") || idSubcuenta.equals("170")){
                       datesSubAccounts.lastDate = item.valorIndicador;
                   }
                   break;
               case "809":
                   if(idSubcuenta.equals("173") || idSubcuenta.equals("174")){
                       datesSubAccounts.firstInputDate = item.valorIndicador;
                   }
                   break;
               case "810":
                   if(idSubcuenta.equals("173") || idSubcuenta.equals("174")){
                       datesSubAccounts.lastDate = item.valorIndicador;
                   }
                   break;
               case "811":
                   if(idSubcuenta.equals("175")){
                       datesSubAccounts.firstInputDate = item.valorIndicador;
                   }
                   break;
               case "812":
                   if(idSubcuenta.equals("175")){
                       datesSubAccounts.lastDate = item.valorIndicador;
                   }
                   break;
               case "813":
                   if(idSubcuenta.equals("171") || idSubcuenta.equals("172")){
                       datesSubAccounts.firstInputDate = item.valorIndicador;
                   }
                   break;
               case "814":
                   if(idSubcuenta.equals("171") || idSubcuenta.equals("172")){
                       datesSubAccounts.lastDate = item.valorIndicador;
                   }
                   break;
           }
       }

       return datesSubAccounts;
    }

    public void sendMessageSales(Object msj){
        sendMessage(msj);
    }

    public void saveReclasificationData(String folioFus, String cveSolicitud){
        SaveProcedureRequest procedureEntiy = mDataManager.queryWhere(SaveProcedureRequest.class).findFirst();
        mDataManager.tx(tx -> {
            procedureEntiy.folioFus = folioFus;
            procedureEntiy.cveSolicitud = cveSolicitud;
            procedureEntiy.fechaCaptura = dateCaptured();
            procedureEntiy.idEstatusSolicitud = "509";
            procedureEntiy.idOrigenCaptura = "402";
            procedureEntiy.montoReclasificacion = mAmmount;
            procedureEntiy.idTipoReclasificacion = mTypeReclasification;
            procedureEntiy.idEstatus = "32";
            procedureEntiy.idEtapa = "19";
            procedureEntiy.idSubproceso = "130";
            procedureEntiy.idSubetapa = "990";
            procedureEntiy.idResultado = "0";

            tx.save(procedureEntiy);
        });
    }

    public void saveListReclasification(List<ListReclasificacion> listReclasification){
        SaveProcedureRequest procedureEntiy = mDataManager.queryWhere(SaveProcedureRequest.class).findFirst();
        mDataManager.tx(tx -> {
            for(ListReclasificacion item : listReclasification){
                procedureEntiy.listReclasificacion.add(item);
            }
            tx.save(procedureEntiy);
        });
    }

    public void saveListSaleOrigin(List<Saldo> listSale){
        SaveProcedureRequest procedureEntiy = mDataManager.queryWhere(SaveProcedureRequest.class).findFirst();
        mDataManager.tx(tx -> {
            for(Saldo item : listSale){
                procedureEntiy.saldosOriginales.add(item);
            }
            tx.save(procedureEntiy);
        });
    }

    public String retrieveAgent(){
        return prefsReclasification.getData("idAgent");
    }

}

