package app.profuturo.reclasificacion.com.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.base.Utils;

public class HorizontalLabeledTextView extends LinearLayout {
    TextView mLabel;
    TextView mContent;

    public HorizontalLabeledTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOrientation(HORIZONTAL);
        View root =
                Utils.inflateFromContext(this, R.layout.view_horizontal_labeled_textview, true);
        mLabel = root.findViewById(R.id.label);
        mContent = root.findViewById(R.id.content);
        TypedArray attributes = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.HorizontalLabeledTextView, 0, 0);
        try {
            String label = attributes.getString(R.styleable.HorizontalLabeledTextView_clientDataLabel);
            String content = attributes.getString(R.styleable.HorizontalLabeledTextView_clientDataContent);
            mLabel.setText(label);
            mContent.setText(content);
        } finally {
            attributes.recycle();
        }
    }

    public String getClientDataLabel() {
        return String.valueOf(mLabel.getText());
    }

    public void setClientDataLabel(String label) {
        mLabel.setText(label);
        invalidate();
    }

    public String getClientDataContent() {
        return String.valueOf(mContent.getText());
    }

    public void setClientDataContent(String content) {
        mContent.setText(content);
        invalidate();
    }
}
