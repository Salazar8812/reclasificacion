package app.profuturo.reclasificacion.com.presenter;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import app.profuturo.reclasificacion.com.BuildConfig;
import app.profuturo.reclasificacion.com.base.View.BaseView;
import app.profuturo.reclasificacion.com.model.ClientAccountNumber;
import app.profuturo.reclasificacion.com.model.ClientPicture;
import app.profuturo.reclasificacion.com.model.InformativeMessage;
import app.profuturo.reclasificacion.com.R;
import app.profuturo.reclasificacion.com.Repository.remote.Routes;
import app.profuturo.reclasificacion.com.base.ProfuturoErrorMessage;
import app.profuturo.reclasificacion.com.base.Utils;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.internal.platform.Platform;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Base class for a Presenter that will perform Network operations.
 */
public abstract class NetworkPresenter extends BasePresenter {
    /**
     * Reference for accessing the Retrofit API definitions.
     */
    protected Routes routes;

    /**
     * Default failure listener for enqueuing a request.
     */
    private OnRequestFailureListener mRequestFailureListener = errorPayload -> {
        if (errorPayload instanceof Response) {
            try {
                String contentType = ((Response) errorPayload).raw().body().contentType().toString();
                if (contentType.contains("xml")) {
                    sendMessage(R.string.bus_down);
                } else {
                    sendMessage(R.string.unable_to_connect);
                }
            } catch (Throwable t) {
                sendMessage(R.string.unable_to_connect);
            }
        } else if (isServerConnectionException(errorPayload)) {
            sendMessage(R.string.unable_to_connect);
        } else
            try {
                Gson parser = new Gson();
                ProfuturoErrorMessage error =
                        parser.fromJson(String.valueOf(errorPayload), ProfuturoErrorMessage.class);
                sendMessage(error.getMessage());
            } catch (Throwable t) {
                if (t instanceof IllegalStateException || t instanceof JsonSyntaxException) {
                    sendMessage(R.string.wrong_response_from_server);
                } else {
                    sendMessage(t);
                }
            }
        setStatus(Status.STATUS_FAILED);
    };

    /**
     * Constructor for this presenter class that assigns a View.
     * This view can be null, this is useful in Unit Test cases.
     *
     * @param view The view for this presenter.
     */
    public NetworkPresenter(@NonNull BaseView view) {
        super(view);
        initClient();
    }

    /**
     * Initializes the API definitions and assigns it to the routes property.
     */
    private void initClient() {
        // Create a new Implementation of the Routes interface using a Retrofit instance.
        routes = buildRetrofit().create(Routes.class);
    }

    /**
     * Builds the retrofit instance for performing HTTP operations.
     *
     * @return The retrofit client, completely built.
     */
    private Retrofit buildRetrofit() {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(180, TimeUnit.SECONDS);
        builder.connectTimeout(180, TimeUnit.SECONDS);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(interceptor);

        builder.addInterceptor(chain -> {
            Request.Builder builder1 = chain.request().newBuilder();
            builder1.headers(getJsonHeader());
            return chain.proceed(builder1.build());
        });

        return new Retrofit.Builder()
                // Assign the BaseURL for this Retrofit instance requests.
                .baseUrl(getBaseURL())
                // Add a GsonConverter for mapping DTOs from JSON or XML.
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                // Assign a OkHTTPClient to this Retrofit instance.
                .client(getUnsafeOkHttpClient(builder))
                // Build the instance.
                .build();
    }


    private static okhttp3.Headers getJsonHeader() {
        okhttp3.Headers.Builder builder = new okhttp3.Headers.Builder();
        builder.add("Content-Type", "application/json");
        builder.add("Accept", "application/json");
        builder.add("Authorization", "Basic cndzcHJheGlzcDpQcjR4MXMjdTVS");
        return builder.build();
    }

    /**
     * Builds an OkHTTP interceptor that will log the body of HTTP requests
     * to LogCat only in the debug flavor.
     *
     * @return HttpInterceptor for a Retrofit client.
     */
    private OkHttpClient buildHttpInterceptor() {
        // Create a logging interceptor, this will print logs from the HTTP request into LogCat.
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        //  Set the logging level to the Body of the request.
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        // Create a new OkHttpClient instance.
        OkHttpClient.Builder httpClient = new OkHttpClient
                .Builder()
                // Set the read timeout to 10 seconds.
                .readTimeout(10, TimeUnit.SECONDS)
                // Set the connection timeout to 10 seconds.
                .connectTimeout(10, TimeUnit.SECONDS);
        /*
          If the app is running under debug, add the interceptor.
          This prevent HTTP calls being logged in production.
         */
        if (BuildConfig.DEBUG) {
            httpClient.addInterceptor(interceptor);
        }
        // Return the OkHTTPClient that has been built.
        return httpClient.build();
    }

    /**
     * @return Defines the base URL for this Presenter network operations.
     */
    protected String getBaseURL() {
        return BuildConfig.BASE_URL;
    }

    /**
     * Enqueues a given request with a given success listener and failure listener..
     *
     * @return true if the request has been enqueued correctly.
     */
    protected <M> Boolean enqueueRequest(@NonNull Call<M> request,
                                         @NonNull final OnRequestSuccessListener<M> onRequestSuccessListener,
                                         @NonNull final OnRequestFailureListener onRequestFailureListener) {
        // Checks if the request has not been executed.
        if (!request.isExecuted()) {
            // Enqueue the request creating a new Callback instance.
            request.enqueue(
                    new Callback<M>() {
                        @Override
                        public void onResponse(@NonNull Call<M> call, @NonNull Response<M> response) {
                            if (response != null && response.isSuccessful()) {
                                onRequestSuccessListener.onSuccess(response.body());
                                setStatus(Status.STATUS_DONE);
                            } else {
                                onRequestFailureListener.onFailure(response);
                                setStatus(Status.STATUS_FAILED);
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<M> call, @NonNull Throwable t) {
                            onRequestFailureListener.onFailure(t);
                            setStatus(Status.STATUS_FAILED);
                        }
                    });
            // Return that the request was successfully enqueued.
            return true;
        } else {
            // Return that the request failed while enqueuing.
            return false;
        }
    }

    /**
     * Enqueues a given request.
     *
     * @return true if the request has been enqueued correctly.
     */
    protected <M> Boolean enqueueRequest(@NonNull Call<M> request,
                                         @NonNull final OnRequestSuccessListener<M> onRequestSuccessListener) {
        return enqueueRequest(request, onRequestSuccessListener, mRequestFailureListener);
    }

    /**
     * Checks if it is a TimeOutException or any of the related exception (IOException children).
     *
     * @param payload The error object retrieved.
     * @return true if matches any of the mentioned exception classes.
     * @see TimeoutException
     * @see IOException
     */
    private Boolean isServerConnectionException(Object payload) {
        return payload instanceof TimeoutException || payload instanceof IOException;
    }

    /**
     * This method retrieves informative message in case agent is different from the one
     * signed in documents section
     */
    protected void requestInformativeMessage(String messageCode) {
        runOnWorkerThread(() -> {
            // Build the request object using the error code.
            Call<InformativeMessage> request = routes.getMessage(messageCode);
            // Enqueue the request.
            enqueueRequest(request, payload -> {
                if (payload != null && Utils.isNotNullOrEmpty(payload.getMessage())) {
                    // Notify that the presenter has finished doing work.
                    setStatus(Status.STATUS_DONE);
                    // Send the retrieved message to the view.
                    sendMessage(payload.getMessage());
                } else {
                    sendMessage(R.string.error_querying_service_message);
                }
            });
        });
    }

    /**
     * Inner interface that listens when a request has been made successfully.
     */
    protected interface OnRequestSuccessListener<M> {
        /**
         * This method will execute when the response is successful.
         *
         * @param payload The data retrieved from the server.
         */
        void onSuccess(M payload);
    }

    /**
     * Inner interface that listens when a request has failed.
     */
    protected interface OnRequestFailureListener {
        /**
         * This method will execute when the response fails.
         *
         * @param errorPayload The error object retrieved from the request.
         */
        void onFailure(Object errorPayload);
    }


    @SuppressLint("TrustAllX509TrustManager")
    private static OkHttpClient getUnsafeOkHttpClient(OkHttpClient.Builder builder) {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();


            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier((hostname, session) -> true);

            builder.addInterceptor(new LoggingInterceptor.Builder()
                    .loggable(BuildConfig.DEBUG)
                    .setLevel(Level.BASIC)
                    .log(Platform.INFO)
                    .request("Request")
                    .response("Response")
                    .addHeader("version", BuildConfig.VERSION_NAME)
                    .build());

            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
