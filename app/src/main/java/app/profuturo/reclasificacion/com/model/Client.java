package app.profuturo.reclasificacion.com.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import app.profuturo.reclasificacion.com.Callbacks.WSBaseResponseInterface;
import app.profuturo.reclasificacion.com.base.Utils;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

import static app.profuturo.reclasificacion.com.model.ClientContact.PHONE_CONTACT_CELLPHONE;
import static app.profuturo.reclasificacion.com.model.ClientContact.PHONE_CONTACT_LANDLINE;


/**
 * Model class that defines the JSON and Realm structure of a client.
 * <br>
 * Data like the client name, address, contact information and
 * NCI records are stored and referenced through this model class.
 *
 * @author Alfredo Bejarano
 * @version 2.0
 * @since 12/09/2018 - 12:39 PM
 */
public class Client extends RealmObject implements WSBaseResponseInterface {
    public static final int VALID_BIOMETRIC_RECORD = 1;
    public static final int VALID_IDENTIFICATION_RECORD = 5;
    public static final String ACCOUNT_VALID = "Vigente";
    public static final String AFFILIATION_VALID = "Inactiva";//"Activa";
    public static Map<Integer, String> idStatusDescriptionMap = new HashMap<>();
    public static Map<Integer, String> biometricStatusDescriptionMap = new HashMap<>();

    static {
        biometricStatusDescriptionMap.put(1, "Existe enrolamiento biométrico");
        biometricStatusDescriptionMap.put(2, "Existe enrolamiento biométrico en tránsito");
        idStatusDescriptionMap.put(1, "Expediente de identificación incompleto");
        idStatusDescriptionMap.put(2, "Expediente de identificación temporal");
        idStatusDescriptionMap.put(3, "Expediente de identificación en tránsito");
        idStatusDescriptionMap.put(5, "Expediente de identificación permanente");
    }

    @Expose
    @Ignore
    @SerializedName("telefonos")
    List<PhoneContact> phoneContactList;
    @Expose
    @Ignore
    @SerializedName("correos")
    List<EmailContact> emailContactList;
    @Expose
    @PrimaryKey
    @SerializedName("numeroCuenta")
    private String accountNumber;
    @Expose
    @SerializedName("nombre")
    private String name;
    @Expose
    @SerializedName("apellidoPaterno")
    private String surName;
    @Expose
    @SerializedName("apellidoMaterno")
    private String lastName;
    @SerializedName("idPersona")
    @Expose
    private String idPersona;
    @Expose
    @SerializedName("nss")
    private String nss;
    @Expose
    @SerializedName("curp")
    private String curp;
    @Expose
    @SerializedName("rfc")
    private String rfc;
    @Expose
    @SerializedName("regimenAfiliacion")
    private String affiliationRegime;
    @Expose
    @SerializedName("cuentaVigente")
    private String accountValid;
    @Expose
    @SerializedName("origenAfiliacion")
    private String affiliationOrigin;
    @Expose
    @SerializedName("fechaNacimiento")
    private String birthday;
    @Expose
    @SerializedName("sexo")
    private String gender;
    @Expose
    @SerializedName("calle")
    private String street;
    @Expose
    @SerializedName("colonia")
    private String neighborhood;
    @Expose
    @SerializedName("codigoPostal")
    private String zipCode;

    @Expose
    @SerializedName("entidadFederativa")
    private String state;

    @Expose
    @Ignore
    @SerializedName("estatusCuenta")
    private Object accountStatus;

    @Expose
    @SerializedName("descripcionIndicadorExpediente")
    private String expedientIndicatorDescription;

    @SerializedName("estatusIndicadorExpediente")
    @Expose
    private String estatusIndicadorExpediente;

    @Expose
    @SerializedName("valorIndicadorExpediente")
    private int expedientIndicatorValue;

    @Expose
    @SerializedName("descripcionIndicadorBiometrico")
    private String biometricIndicatorDescription;

    @SerializedName("estatusIndicadorBiometrico")
    @Expose
    private String estatusIndicadorBiometrico;

    @Expose
    @SerializedName("valorIndicadorBiometrico")
    private int biometricIndicatorValue;

    @Expose
    @SerializedName("valorIndicadorTipoCliente")
    private int regimentIndicatorId = 66;

    @SerializedName("estatusTipoCliente")
    @Expose
    private String estatusTipoCliente;

    /*@SerializedName("valorIndicadorTipoCliente")
    @Expose
    private String valorIndicadorTipoCliente;*/

    @SerializedName("fechaCertificacion")
    @Expose
    private String fechaCertificacion;

    /*@SerializedName("indicadorAdicional")
    @Expose
    private List<IndicadorAdicional> indicadorAdicional;*/

    @SerializedName("estatus")
    @Expose
    private String estatus;

    @SerializedName("estadoAfiliacion")
    @Expose
    private String affiliationStatus;

    @Expose
    @SerializedName("numeroExterior")
    private String outerNumber;

    @Expose
    @SerializedName("numeroInterior")
    private String innerNumber;

    @Expose
    @SerializedName("municipio")
    private String district;

    @Expose
    @SerializedName("usuarioFacebook")
    private String facebookUser = "";
    @Expose
    @SerializedName("usuarioTwitter")
    private String twitterUser = "";
    // Indicator properties

    @Expose
    @SerializedName("estatusExpediente")
    private String expedientStatus;


    @Expose
    @SerializedName("estatusBiometrico")
    private String biotmetricStatus;


    /*@Expose
    @Ignore
    @SerializedName("estadoAfiliacion")
    private Object affiliationStatus;*/
    /**
     * Property that holds the type of regime that a client has.
     * <ul>
     * <li>66 IMSS</li>
     * <li>67 ISSSTE</li>
     * <li>68 Independent</li>
     * <li>69 Mixed regime</li>
     * <li>1608 Underage</li>
     * </ul>
     */

    //  Realm object to keep amount for each sub account for this client
    private SavingsAccount savingsAccount;
    // Digested contact data.
    private String email;
    private String secondaryEmail;
    private String landlineNumber;
    private String cellphoneNumber;

    public Client() {
        // Empty Realm required constructor.
    }

    /**
     * Process the list of client contacts and assign them to the client properties.
     */
    public void digestContactData() {
        // Check if the list is not null or empty.
        if (Utils.isNotNullOrEmpty(phoneContactList)) {
            // if not, iterate through each element of the client.
            for (PhoneContact cc : phoneContactList) {
                // Get the contact key.
                Integer ccKey = cc.getType().getKey();
                // If the phone is a cellphone, assign it as such.
                if (ccKey == PHONE_CONTACT_CELLPHONE) {
                    cellphoneNumber = cc.getNumber();
                } else if (ccKey == PHONE_CONTACT_LANDLINE) {
                    landlineNumber = cc.getNumber();
                }
            }
        }
        // Assign the email depending on their position on the list.
        if (Utils.isNotNullOrEmpty(emailContactList)) {
            // Get the first element of the list.
            email = Utils.firstOf(emailContactList).getAddress();
            // Check if the list has more than one element.
            if (emailContactList.size() >= 2) {
                // If it has, set as secondary email the address of the last one of the list.
                email = Utils.lastOf(emailContactList).getAddress();
            }
        }
    }

    public String getAccountValid() {
        return accountValid;
    }

    public void setAccountValid(String accountValid) {
        this.accountValid = accountValid;
    }

    public String getAccountStatus() {
        return String.valueOf(accountStatus);
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getExpedientStatus() {
        return expedientStatus;
    }

    public void setExpedientStatus(String expedientStatus) {
        this.expedientStatus = expedientStatus;
    }

    public Integer getExpedientIndicatorValue() {
        return expedientIndicatorValue;
    }

    public void setExpedientIndicatorValue(Integer expedientIndicatorValue) {
        this.expedientIndicatorValue = expedientIndicatorValue;
    }

    public Integer getBiometricIndicatorValue() {
        return biometricIndicatorValue;
    }

    public void setBiometricIndicatorValue(Integer biometricIndicatorValue) {
        this.biometricIndicatorValue = biometricIndicatorValue;
    }

    public String getExpedientIndicatorDescription() {
        return expedientIndicatorDescription;
    }

    public void setExpedientIndicatorDescription(String expedientIndicatorDescription) {
        this.expedientIndicatorDescription = expedientIndicatorDescription;
    }

    public String getBiotmetricStatus() {
        return biotmetricStatus;
    }

    public void setBiotmetricStatus(String biotmetricStatus) {
        this.biotmetricStatus = biotmetricStatus;
    }

    public String getBiometricIndicatorDescription() {
        return biometricIndicatorDescription;
    }

    public void setBiometricIndicatorDescription(String biometricIndicatorDescription) {
        this.biometricIndicatorDescription = biometricIndicatorDescription;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getAffiliationRegime() {
        return affiliationRegime;
    }

    public void setAffiliationRegime(String affiliationRegime) {
        this.affiliationRegime = affiliationRegime;
    }

    public Boolean isAccountValid() {
        return ACCOUNT_VALID.equals(accountValid) &&
                AFFILIATION_VALID.equals(affiliationStatus);
    }

    public boolean isAccountNotNull() {
        return accountNumber != null && curp != null && nss != null;
    }

    public String getAffiliationOrigin() {
        return affiliationOrigin;
    }

    public void setAffiliationOrigin(String affiliationOrigin) {
        this.affiliationOrigin = affiliationOrigin;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getOuterNumber() {
        return outerNumber;
    }

    public void setOuterNumber(String outerNumber) {
        this.outerNumber = outerNumber;
    }

    public String getInnerNumber() {
        return innerNumber;
    }

    public void setInnerNumber(String innerNumber) {
        this.innerNumber = innerNumber;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCellphoneNumber() {
        return cellphoneNumber;
    }

    public void setCellphoneNumber(String cellphoneNumber) {
        this.cellphoneNumber = cellphoneNumber;
    }

    public String getLandlineNumber() {
        return landlineNumber;
    }

    public void setLandlineNumber(String landlineNumber) {
        this.landlineNumber = landlineNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    public void setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    public String getFacebookUser() {
        return facebookUser;
    }

    public void setFacebookUser(String facebookUser) {
        this.facebookUser = facebookUser;
    }

    public String getTwitterUser() {
        return twitterUser;
    }

    public void setTwitterUser(String twitterUser) {
        this.twitterUser = twitterUser;
    }

    public SavingsAccount getSavingsAccount() {
        return savingsAccount;
    }

    public void setSavingsAccount(SavingsAccount savingsAccount) {
        this.savingsAccount = savingsAccount;
    }

    /**
     * Appends this client name and last name to generate a displayable name.
     *
     * @return The client name and last name appended.
     */
    public String getDisplayName() {
        return String.format("%1$s %2$s", String.valueOf(name), String.valueOf(surName));
    }

    /**
     * Method to verify if identification expedient has valid status 5
     *
     * @return if the criteria is satisfied
     */
    public Boolean isExpedientIndentificationValid() {
        return VALID_IDENTIFICATION_RECORD == expedientIndicatorValue;
    }

    /**
     * Method to verify if biometric expedient has valid status 1
     *
     * @return if the criteria is satisfied
     */
    public Boolean isBiometricExpedientValid() {
        return VALID_BIOMETRIC_RECORD == biometricIndicatorValue;
    }

    public String getAffiliationStatus() {
        return affiliationStatus;
    }

    public void setAffiliationStatus(String affiliationStatus) {
        this.affiliationStatus = affiliationStatus;
    }

    /**
     * Parses the Client birthday, if it fails, returns the date as it is.
     *
     * @return The client birthday.
     */
    public String getDisplayDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hhmmssZ",
                Locale.getDefault());
        SimpleDateFormat displayFormat = new SimpleDateFormat("dd 'de' MMMM, yyyy",
                Locale.getDefault());
        try {
            String cleanDate = birthday.replace(":", "");
            Date formatDate = sdf.parse(cleanDate);
            return displayFormat.format(formatDate);
        } catch (Exception e) {
            return birthday;
        }
    }

    public int getRegimentIndicatorId() {
        return regimentIndicatorId;
    }

    public void setRegimentIndicatorId(int regimentIndicatorId) {
        this.regimentIndicatorId = regimentIndicatorId;
    }

    public String getEstatusIndicadorBiometrico() {
        return estatusIndicadorBiometrico;
    }

    public void setEstatusIndicadorBiometrico(String estatusIndicadorBiometrico) {
        this.estatusIndicadorBiometrico = estatusIndicadorBiometrico;
    }
}