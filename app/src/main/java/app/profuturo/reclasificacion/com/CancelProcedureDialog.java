package app.profuturo.reclasificacion.com;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import app.profuturo.reclasificacion.com.model.Procedure;

public class CancelProcedureDialog extends AppCompatActivity implements View.OnClickListener {
    private Button mCancelProcedureButton;
    private Button mAcceptProcedureButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.cancel_procedure_dialog);
        bindData();
    }

    private void bindData(){
        mCancelProcedureButton = findViewById(R.id.mCancelProcedureButton);
        mAcceptProcedureButton = findViewById(R.id.mAcceptProcedureButton);
        mCancelProcedureButton.setOnClickListener(this);
        mAcceptProcedureButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.mCancelProcedureButton){
            this.finish();
        }else if(view.getId() == R.id.mAcceptProcedureButton){
            this.finish();
            ProcedureActivity.mActivity.finish();
        }
    }

}
