package app.profuturo.reclasificacion.com.widget;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Simple {@link ViewPager} class that allows enabling
 * and disabling swiping gestures at Runtime
 *
 * @author Alfredo Bejarano
 * @version 2.0
 * @since 17/09/2018 - 11:58 AM
 */
public class NavigationViewPager extends ViewPager {
    private Float mInitialXValue = 0f;
    private SwipeDirection mDirection = SwipeDirection.SWIPE_NONE;

    /**
     * Constructor for this custom ViewPager implementation.
     *
     * @param context The context in which this view exists.
     * @param attrs   Attributes for this view from the XML file.
     */
    public NavigationViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOffscreenPageLimit(1);
    }

    /**
     * Detects when a touch event is detected and checks if the swipe direction is enabled.
     *
     * @param ev The intercepted MotionEvent.
     * @return true or false if the swipe event is valid.
     * @see MotionEvent
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return isSwipeAllowed(ev) && super.onInterceptTouchEvent(ev);
    }

    /**
     * Detect if a motion event is enabled, if it is, perform the swipe action.
     *
     * @param ev The motion event that has been detected.
     * @return true if the swipe event has been enabled.
     */
    @Override
    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouchEvent(MotionEvent ev) {
        return isSwipeAllowed(ev) && super.onTouchEvent(ev);
    }

    /**
     * Detects if a swipe in a given direction is allowed.
     *
     * @param event The motion event to detect.
     * @return true or false depending on the direction set.
     */
    private Boolean isSwipeAllowed(@NonNull MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                return onSwipeBegin(event.getX());
            case MotionEvent.ACTION_MOVE:
                return onSwipe(event.getX());
            case MotionEvent.ACTION_UP:
                return true;
            default:
                return false;
        }
    }

    /**
     * Sets the initial X value
     *
     * @param eventX The X value where the touch event was started.
     */
    @NonNull
    private Boolean onSwipeBegin(@NonNull Float eventX) {
        mInitialXValue = eventX;
        return true;
    }

    /**
     * Checks the results of a swipe gesture.
     *
     * @param eventX The X value of the event.
     * @return If the event can be done or not.
     */
    @NonNull
    private Boolean onSwipe(@NonNull Float eventX) {
        // Check if the swipe listener is not null or if the swipe direction is not swipe none.
        if (mDirection != SwipeDirection.SWIPE_NONE) {
            // Get the event X value and subtract it from the initial X value.
            Float xDifference = mInitialXValue - eventX;
            // If the difference is bigger than 0 and the swipe direction is LEFT (backwards).
            // If the difference is smaller than 0 and the swipe direction is RIGHT (backwards).
            return xDifference > 0 && mDirection != SwipeDirection.SWIPE_RIGHT ||
                    xDifference < -0 && mDirection != SwipeDirection.SWIPE_LEFT;
        } else {
            // If the listener is null or the gesture is NONE, return false.
            return false;
        }
    }

    /**
     * Sets the allowed navigation direction for the view pager.
     *
     * @param direction The allowed direction for the pager.
     */
    public void setAllowedDirection(@NonNull SwipeDirection direction) {
        mDirection = direction;
        Log.d("Direction:", direction.toString());
    }

    /**
     * Enum class that contain definitions for allowed swipe directions for this ViewPager.
     */
    public enum SwipeDirection {
        /**
         * Defines that all directions are allowed.
         */
        SWIPE_ALL,
        /**
         * Defines that just swipes to the right side of the screen are allowed.
         */
        SWIPE_RIGHT,
        /**
         * Defines that just swipes to the left side of the screen are allowed.
         */
        SWIPE_LEFT,
        /**
         * Defines that no swipe interaction is allowed.
         */
        SWIPE_NONE
    }
}
